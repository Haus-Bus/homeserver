<?php
include("../include/all.php");

echo "<html>";
echo '<head><link rel="StyleSheet" href="/homeserver/css/main.css" type="text/css" />';
echo '<body><div class="contentWrap"  id="content" style="margin-right:16px;">';

echo "<br><table width=95% align=center><tr><td>";
echo "<b>Amazon Alexa Plugin</b><hr>";

if ($submitted == 1)
{
	 generateAlexaConfig();
	 callObjectMethodByName(800981249, "exec", array("command"=>"-3"));
	 echo "<b>Plugin aktualisiert</b> <br><br>";
}

echo "Bei jedem Aktor kann man unterhalb des Aktornamens einen gesonderten Namen für Alexa angeben, mit dem dieser dann per Sprachbefehl gesteuert werden kann. 
Folgende Aktoren können gesteuert werden: Dimmer, Schalter, Rollos, Taster. Möchte man eine Lichtszene schalten oder mehrere Rollos gleichzeitig fahren, verwendet man einfach einen Taster der die entsprechenden Aktoren ansteuert lässt Alexa diesen Taster anschalten. Zu diesem Zweck können auch virtuelle Taster des Raspberries verwendet werden.<br>
<br>
Wurde ein Name ergänzt oder geändert, muss das Plugin einmal aktualisiert werden.<br> Dafür auf folgenden Button klicken und in der Alexa App nach neuen Geräten suchen:<br><br>
<form action='index.php' method='POST'><input type=hidden name=submitted value=1><input type=submit value='Refresh'></form><br><br>";


echo "<br><br><b>Sprachbefehle</b><hr><br>";
echo "Aktuell werden folgende 4 Alexa Sprachbefehle unterstützt:<br><br>
<li> \"Alexa, schaltet >AKTORNAME< an\" <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Mit diesem Befehl kann man eine Lampe oder ein Relais anschalten.<br>
&nbsp;&nbsp;&nbsp;&nbsp;Da es allerdings keinen dedizierten Befehl für Rollos gibt, wird so auch ein Rollo aufgefahren<br>
<br>
<br><br>
<li> \"Alexa, schaltet >AKTORNAME< aus\" <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Mit diesem Befehl kann man eine Lampe oder ein Relais ausschalten.<br>
&nbsp;&nbsp;&nbsp;&nbsp;Da es allerdings keinen dedizierten Befehl für Rollos gibt, wird so auch ein Rollo zugefahren<br>
<br>
<br><br>
<li> \"Alexa, dimme >AKTORNAME< auf >X< Prozent\" <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Mit diesem Befehl kann man eine Lampe auf einem beliebigen Helligkeitswert dimmen.<br>
&nbsp;&nbsp;&nbsp;&nbsp;Da es allerdings keinen dedizierten Befehl für Rollos gibt, wird so auch ein Rollo auf eine bestimmte Position gefahren<br>
&nbsp;&nbsp;&nbsp;&nbsp;Stoppen kann man ein Rollo indem man auf 1% dimmt.

<br>
<br><br>
<li> \"Alexa, stelle die Farbe von >AKTORNAME< auf >X< \" <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Dieser Befehl wird aktuell noch nicht verwendet<br>
<br>
";

function generateAlexaConfig()
{
	$result="";
	$id=1;
	
	$erg = QUERY("select objectId, featureClassesId,name, alexaName from featureInstances where alexaName!=''") or die(MYSQL_ERROR());
	while($obj=MYSQLi_FETCH_OBJECT($erg))
	{
		  echo "Registriere: ".$obj->objectId." -> ".$obj->name." -> ".$obj->alexaName."<br>";
		  if ($obj->featureClassesId=="9")
		  {
		  	if ($result!="") $result.=",";
		    $result.=getDimmerEntry($id, $obj->objectId, $obj->alexaName);
		  }
		  else if ($obj->featureClassesId=="8")
		  {
		  	if ($result!="") $result.=",";
		    $result.=getSwitchEntry($id, $obj->objectId, $obj->alexaName);
		  }
		  else if ($obj->featureClassesId=="14")
		  {
		  	if ($result!="") $result.=",";
		    $result.=getRolloEntry($id, $obj->objectId, $obj->alexaName);
		  }
		  else if ($obj->featureClassesId=="1")
		  {
		  	if ($result!="") $result.=",";
		    $result.=getTasterEntry($id, $obj->objectId, $obj->alexaName);
		  }
		  else echo "<b>Fehler: Aktorklasse nicht unterstützt</b><br>";
		  
		  $id++;
	}
	
	if (file_exists("../../data/device.db"))	unlink("../../data/device.db");
	
	$result="[".$result."]\n";
	file_put_contents("../../data/device.db",$result);
}

function getDimmerEntry($id, $objectId, $alexaName)
{
	 return getEntry($id, $alexaName, $objectId, "setBrightness", "brightness", "100", "duration", "0", "setBrightness", "brightness", "0", "duration", "0", "setBrightness", "brightness", '${intensity.percent}', "duration", "0");
}

function getSwitchEntry($id, $objectId, $alexaName)
{
	 return getEntry($id, $alexaName, $objectId, "on", "duration", "0", "", "", "off", "", "", "", "", "", "", '', "", "");
}

function getRolloEntry($id, $objectId, $alexaName)
{
	 return getEntry($id, $alexaName, $objectId, "start", "direction", "TO_OPEN", "", "", "start", "direction", "TO_CLOSE", "", "", "moveToPosition", "position", '${intensity.percent}', "", "");
}

function getTasterEntry($id, $objectId, $alexaName)
{
	 return getEntry($id, $alexaName, $objectId, "evCovered", "", "", "", "", "evHoldStart", "", "", "", "", "", "", '', "", "");
}

function getEntry($id, $name, $objectId, $onCommand, $onParam1Name, $onParam1Value, $onParam2Name, $onParam2Value, $offCommand, $offParam1Name, $offParam1Value, $offParam2Name, $offParam2Value, $dimmCommand, $dimmParam1Name, $dimmParam1Value, $dimmParam2Name, $dimmParam2Value)
{ 
	$id=$objectId;
	$name = utf8_decode(query_real_escape_string($name));
	$entry = '{"id":"'.$id.'","uniqueid":"00:17:88:5E:D3:'.dechex($id).'-'.dechex($id).'","name":"'.$name.'","mapType":"hueDevice","offUrl":"[{\"item\":\"http://raspberry/homeserver/alexa/alexa.php?id\u003d'.$objectId.'\u0026command\u003d'.$offCommand.'\u0026param1Name\u003d'.$offParam1Name.'\u0026param1Value\u003d'.$offParam1Value.'\u0026param2Name\u003d'.$offParam2Name.'\u0026param2Value\u003d'.$offParam2Value.'\",\"type\":\"httpDevice\"}]","dimUrl":"[{\"item\":\"http://raspberry/homeserver/alexa/alexa.php?id\u003d'.$objectId.'\u0026command\u003d'.$dimmCommand.'\u0026param1Name\u003d'.$dimmParam1Name.'\u0026param1Value\u003d'.$dimmParam1Value.'\u0026param2Name\u003d'.$dimmParam2Name.'\u0026param2Value\u003d'.$dimmParam2Value.'\",\"type\":\"httpDevice\"}]","onUrl":"[{\"item\":\"http://raspberry/homeserver/alexa/alexa.php?id\u003d'.$objectId.'\u0026command\u003d'.$onCommand.'\u0026param1Name\u003d'.$onParam1Name.'\u0026param1Value\u003d'.$onParam1Value.'\u0026param2Name\u003d'.$onParam2Name.'\u0026param2Value\u003d'.$onParam2Value.'\",\"type\":\"httpDevice\"}]","inactive":false,"noState":false,"offState":false,"description":"Test","comments":"comment","onFirstDim":false,"onWhenDimPresent":false}';
	return $entry;
}
?>  