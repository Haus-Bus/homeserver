<?php
error_reporting(E_ERROR);

$_SERVER["DOCUMENT_ROOT"]="../../";
include("../include/all.php");

if (!file_exists("../../ha-bridge-5.2.2.jar"))
{
	 echo "Lade benötigte Dateien von haus-bus.de.... \n";
	 file_put_contents("../../ha-bridge-5.2.2.jar", file_get_contents ( "http://www.haus-bus.de/ha-bridge-5.2.2.jar", False, getStreamContext () ) );
}
else echo "Alexa Bibliothek bereits vorhanden \n"; 

if (!file_exists("../../data"))
{
	 echo "Erstelle Konfigurationsverzeichnis.... \n";
	 mkdir("../../data");
	 exec("chmod 777 ../../data");
}
else echo "Konfigurationsverzeichnis bereits vorhanden \n"; 

if (!file_exists("/etc/init.d/alexaService"))
{
	echo "aktiviere alexaService init script ... \n";
	exec("cp alexaService /etc/init.d/");
  exec("chmod 777 /etc/init.d/alexaService");
}
else echo "alexaService bereits vorhanden \n"; 

$output = shell_exec('crontab -l');
if(strpos($output,"alexaService")===FALSE)
{
	echo "aktiviere alexaService cronJob ... \n";
  file_put_contents('/tmp/crontab.txt', $output."* * * * * ps ax | grep 'alexaService' | grep -v 'grep' || /etc/init.d/alexaService start".PHP_EOL);
  exec('crontab /tmp/crontab.txt');
}
else echo "alexaService cronJob bereits vorhanden \n";
?>