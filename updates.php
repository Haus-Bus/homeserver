<?php
include($_SERVER["DOCUMENT_ROOT"]."/homeserver/include/all.php");


if ($action=="readOnlineVersion")
{
  $_SESSION["fwSelectedGroup"] = $updateGroup;
  QUERY("delete from basicconfig where paramKey='updateGroup' limit 1");
  QUERY("INSERT into basicconfig (paramKey,paramValue) values('updateGroup','$updateGroup')");
	
  $groupDir="";
  if ($_SESSION["fwSelectedGroup"]=="Stabile Version" || $_SESSION["fwSelectedGroup"]=="") $groupDir="latest";
  else if ($_SESSION["fwSelectedGroup"]=="In Entwicklung") $groupDir="develop";
  else $groupDir="HBC_".$_SESSION["fwSelectedGroup"];

  $cxContext = getStreamContext();

  $in = @file_get_contents("http://www.haus-bus.de/version2018.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlineversion Homeserver konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionWebapplication"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateWebapplication"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }

  $in = @file_get_contents("http://www.haus-bus.de/firmware/$groupDir/HBC.chk", False, $cxContext);
  if ($in=="") $in = @file_get_contents("http://www.haus-bus.de/HBC.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion HBC konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionHausbusControls"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateHausbusControls"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }

  $in = @file_get_contents("http://www.haus-bus.de/firmware/$groupDir/HBX192C3.chk", False, $cxContext);
  if ($in=="")  $in = @file_get_contents("http://www.haus-bus.de/HBX192C3.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion HBX konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionHBX"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateHBX"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }

  $in = @file_get_contents("http://www.haus-bus.de/firmware/$groupDir/AR8.chk", False, $cxContext);
  if ($in=="") $in = @file_get_contents("http://www.haus-bus.de/AR8.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion AR8 konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionMainController"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateMainController"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }

  $in = @file_get_contents("http://www.haus-bus.de/firmware/$groupDir/MS6.chk", False, $cxContext);
  if ($in=="") $in = @file_get_contents("http://www.haus-bus.de/MS6.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion MS6 konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionMultiTaster"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateMultiTaster"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }

  $in = @file_get_contents("http://www.haus-bus.de/firmware/$groupDir/SD6.chk", False, $cxContext);
  if ($in=="") $in = @file_get_contents("http://www.haus-bus.de/SD6.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion SD6 konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionMultiTasterSD6"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateMultiTasterSD6"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }
  
  $in = @file_get_contents("http://www.haus-bus.de/firmware/$groupDir/SD485.chk", False, $cxContext);
  if ($in=="") $in = @file_get_contents("http://www.haus-bus.de/SD485.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion SD485 konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionMultiTasterSD485"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateMultiTasterSD485"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }  

  $in = @file_get_contents("http://www.haus-bus.de/firmware/$groupDir/HBC_BOOTER.chk", False, $cxContext);
  if ($in=="") $in = @file_get_contents("http://www.haus-bus.de/HBC_BOOTER.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion HBC_BOOTER konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionHausbusControlsBooter"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateHausbusControlsBooter"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }

  $in = @file_get_contents("http://www.haus-bus.de/firmware/$groupDir/HBX192C3_BOOTER.chk", False, $cxContext);
  if ($in=="")  $in = @file_get_contents("http://www.haus-bus.de/HBX192C3_BOOTER.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion HBX192C3_BOOTER konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionHBXBooter"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateHBXBooter"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }
  
  $in = @file_get_contents("http://www.haus-bus.de/firmware/$groupDir/AR8_BOOTER.chk", False, $cxContext);
  if ($in=="") $in = @file_get_contents("http://www.haus-bus.de/AR8_BOOTER.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion AR8_BOOTER konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionMainControllerBooter"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateMainControllerBooter"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }

  $in = @file_get_contents("http://www.haus-bus.de/firmware/$groupDir/MS6_BOOTER.chk", False, $cxContext);
  if ($in=="") $in = @file_get_contents("http://www.haus-bus.de/MS6_BOOTER.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion MS6_BOOTER konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionMultiTasterBooter"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateMultiTasterBooter"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }
  
  $in = @file_get_contents("http://www.haus-bus.de/firmware/$groupDir/SD6_BOOTER.chk", False, $cxContext);
  if ($in=="") $in = @file_get_contents("http://www.haus-bus.de/SD6_BOOTER.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion SD6_BOOTER konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionMultiTasterBooterSD6"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateMultiTasterBooterSD6"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }
  
  $in = @file_get_contents("http://www.haus-bus.de/firmware/$groupDir/SD485_BOOTER.chk", False, $cxContext);
  if ($in=="") $in = @file_get_contents("http://www.haus-bus.de/SD485_BOOTER.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion SD485_BOOTER konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionMultiTasterBooterSD485"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateMultiTasterBooterSD485"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }  
/*
  $in = @file_get_contents("http://www.haus-bus.de/ESP.chk", False, $cxContext);
  $pos = strpos($in,"-");
  if ($pos===FALSE) $message="Onlinerversion ESP konnten nicht gelesen werden - Keine Internetverbindung?";
  else
  {
    $_SESSION["onlineVersionESP"]=trim(substr($in,0,$pos));
    $_SESSION["onlineVersionDateESP"]=trim(substr($in,$pos+1,strlen($in)-$pos-1));
  }  
 */
  $in = @file_get_contents("http://www.haus-bus.de/firmware/index.php", False, $cxContext);
  $pos = strpos($in,",");
  if ($pos===FALSE) $message="Onlinerversion Gruppen konnten nicht gelesen werden - Keine Internetverbindung?";
  else $_SESSION["fwVersionGroups"]=trim($in);
  
  
  if ($forward!="")
  {
  	 header("Location: $forward"."?forward=1");
  	 exit;
  }

}

setupTreeAndContent("updates_design.html", $message);

$versionWebApplication=trim(file_get_contents("version2018.chk"));
$html = str_replace("%VERSION_WEB_APPLICATION%","V".$versionWebApplication, $html);

$versionHausbusControls=trim(@file_get_contents("../firmware/HBC.chk"));
$html = str_replace("%VERSION_HAUSBUS_CONTROLS%","V".$versionHausbusControls, $html);

$versionHbx=trim(@file_get_contents("../firmware/HBX192C3.chk"));
$html = str_replace("%VERSION_HBX%","V".$versionHbx, $html);

$versionMainController=trim(@file_get_contents("../firmware/AR8.chk"));
$html = str_replace("%VERSION_MAIN_CONTROLLER%","V".$versionMainController, $html);

$versionMultiTaster=trim(@file_get_contents("../firmware/MS6.chk"));
$html = str_replace("%VERSION_MULTI_TASTER%","V".$versionMultiTaster, $html);

$versionHausbusControls=trim(@file_get_contents("../firmware/HBC_BOOTER.chk"));
$html = str_replace("%VERSION_HAUSBUS_CONTROLS_BOOTER%","V".$versionHausbusControls, $html);

$versionHbx=trim(@file_get_contents("../firmware/HBX192C3_BOOTER.chk"));
$html = str_replace("%VERSION_HBX_BOOTER%","V".$versionHbx, $html);

$versionMainController=trim(@file_get_contents("../firmware/AR8_BOOTER.chk"));
$html = str_replace("%VERSION_MAIN_CONTROLLER_BOOTER%","V".$versionMainController, $html);

$versionMultiTaster=trim(@file_get_contents("../firmware/MS6_BOOTER.chk"));
$html = str_replace("%VERSION_MULTI_TASTER_BOOTER%","V".$versionMultiTaster, $html);

$versionMultiTaster=trim(@file_get_contents("../firmware/SD6.chk"));
$html = str_replace("%VERSION_MULTI_TASTER_SD6%","V".$versionMultiTaster, $html);

$versionMultiTaster=trim(@file_get_contents("../firmware/SD6_BOOTER.chk"));
$html = str_replace("%VERSION_MULTI_TASTER_SD6_BOOTER%","V".$versionMultiTaster, $html);

$versionMultiTaster=trim(@file_get_contents("../firmware/SD485.chk"));
$html = str_replace("%VERSION_MULTI_TASTER_SD485%","V".$versionMultiTaster, $html);

$versionMultiTaster=trim(@file_get_contents("../firmware/SD485_BOOTER.chk"));
$html = str_replace("%VERSION_MULTI_TASTER_SD485_BOOTER%","V".$versionMultiTaster, $html);

$versionESP=trim(@file_get_contents("../firmware/ESP.chk"));
$html = str_replace("%VERSION_ESP%","V".$versionESP, $html);

chooseTag("%OPT_READ%", $html);

$html = str_replace("%VERSION_WEB_APPLICATION_ONLINE%","V".$_SESSION["onlineVersionWebapplication"]." - ".$_SESSION["onlineVersionDateWebapplication"], $html);
chooseTag("%OPT_UPDATE%", $html);

$html = str_replace("%VERSION_HAUSBUS_CONTROLS_ONLINE%","V".$_SESSION["onlineVersionHausbusControls"]." - ".$_SESSION["onlineVersionDateHausbusControls"], $html);
chooseTag("%OPT_UPDATE_HAUSBUS_CONTROLS%", $html);

$html = str_replace("%VERSION_HBX_ONLINE%","V".$_SESSION["onlineVersionHBX"]." - ".$_SESSION["onlineVersionDateHBX"], $html);
chooseTag("%OPT_UPDATE_HBX%", $html);

$html = str_replace("%VERSION_MAIN_CONTROLLER_ONLINE%","V".$_SESSION["onlineVersionMainController"]." - ".$_SESSION["onlineVersionDateMainController"], $html);
chooseTag("%OPT_UPDATE_MAINCONTROLLER%", $html);

$html = str_replace("%VERSION_MULTI_TASTER_ONLINE%","V".$_SESSION["onlineVersionMultiTaster"]." - ".$_SESSION["onlineVersionDateMultiTaster"], $html);
chooseTag("%OPT_UPDATE_MULTI_TASTER%", $html);

$html = str_replace("%VERSION_MULTI_TASTER_SD6_ONLINE%","V".$_SESSION["onlineVersionMultiTasterSD6"]." - ".$_SESSION["onlineVersionDateMultiTasterSD6"], $html);
chooseTag("%OPT_UPDATE_MULTI_TASTER_SD6%", $html);

$html = str_replace("%VERSION_MULTI_TASTER_SD485_ONLINE%","V".$_SESSION["onlineVersionMultiTasterSD485"]." - ".$_SESSION["onlineVersionDateMultiTasterSD485"], $html);
chooseTag("%OPT_UPDATE_MULTI_TASTER_SD485%", $html);

$html = str_replace("%VERSION_HAUSBUS_CONTROLS_BOOTER_ONLINE%","V".$_SESSION["onlineVersionHausbusControlsBooter"]." - ".$_SESSION["onlineVersionDateHausbusControlsBooter"], $html);
chooseTag("%OPT_UPDATE_HAUSBUS_CONTROLS_BOOTER%", $html);

$html = str_replace("%VERSION_HBX_BOOTER_ONLINE%","V".$_SESSION["onlineVersionHBXBooter"]." - ".$_SESSION["onlineVersionDateHBXBooter"], $html);
chooseTag("%OPT_UPDATE_HBX_BOOTER%", $html);

$html = str_replace("%VERSION_MAIN_CONTROLLER_BOOTER_ONLINE%","V".$_SESSION["onlineVersionMainControllerBooter"]." - ".$_SESSION["onlineVersionDateMainControllerBooter"], $html);
chooseTag("%OPT_UPDATE_MAINCONTROLLER_BOOTER%", $html);

$html = str_replace("%VERSION_MULTI_TASTER_BOOTER_ONLINE%","V".$_SESSION["onlineVersionMultiTasterBooter"]." - ".$_SESSION["onlineVersionDateMultiTasterBooter"], $html);
chooseTag("%OPT_UPDATE_MULTI_TASTER_BOOTER%", $html);

$html = str_replace("%VERSION_MULTI_TASTER_SD6_BOOTER_ONLINE%","V".$_SESSION["onlineVersionMultiTasterBooterSD6"]." - ".$_SESSION["onlineVersionDateMultiTasterBooterSD6"], $html);
chooseTag("%OPT_UPDATE_MULTI_TASTER_SD6_BOOTER%", $html);

$html = str_replace("%VERSION_MULTI_TASTER_SD485_BOOTER_ONLINE%","V".$_SESSION["onlineVersionMultiTasterBooterSD485"]." - ".$_SESSION["onlineVersionDateMultiTasterBooterSD485"], $html);
chooseTag("%OPT_UPDATE_MULTI_TASTER_SD485_BOOTER%", $html);

$html = str_replace("%VERSION_ESP_ONLINE%","V".$_SESSION["onlineVersionESP"]." - ".$_SESSION["onlineVersionDateESP"], $html);
chooseTag("%OPT_UPDATE_ESP%", $html);

$groupOptions = "Stabile Version";
$parts = explode(",",$_SESSION["fwVersionGroups"]);
foreach((array)$parts as $version)
{
	$groupOptions.=",".$version;
}
$groupOptions.=",In Entwicklung";

if ($_SESSION["fwSelectedGroup"]=="") $_SESSION["fwSelectedGroup"]="Stabile Version";
if (strpos($groupOptions,$_SESSION["fwSelectedGroup"])===FALSE) $groupOptions.=",".$_SESSION["fwSelectedGroup"];

$html = str_replace("%UPDATE_GROUP_OPTIONS%", getSelect($_SESSION["fwSelectedGroup"], $groupOptions), $html);


$versionsTag = getTag("%VERSIONS%",$html);
$versions="";
$lastId="";
$erg = QUERY("select name,firmwareId,majorRelease,minorRelease,booterMajor,booterMinor,online,size,id from controller where not (bootloader=1 and online!=1) order by firmwareId");
while($obj=mysqli_fetch_OBJECT($erg))
{
	 $actTag = $versionsTag;
	 
	 if ($obj->firmwareId!=$lastId && $lastId!="")
	 {
	 	  $versions.="<tr><td colspan=5><hr></td></tr>";
	 }
	 $lastId=$obj->firmwareId;
	 
	 $onlineVersion="";
	 $onlineVersionBooter="";
	 
	 if ($obj->firmwareId==1)
	 {
		 $typ="Maincontroller";
		 $onlineVersion = $_SESSION["onlineVersionMainController"];
		 $onlineVersionBooter = $_SESSION["onlineVersionMainControllerBooter"];
	 }
	 else if ($obj->firmwareId==2)
	 {
		 $typ="Multitaster MS6";
		 $onlineVersion = $_SESSION["onlineVersionMultiTaster"];
		 $onlineVersionBooter = $_SESSION["onlineVersionMultiTasterBooter"];
	 }
	 else if ($obj->firmwareId==3)
	 {
		 $typ="Multitaster SD6";
		 $onlineVersion = $_SESSION["onlineVersionMultiTasterSD6"];
		 $onlineVersionBooter = $_SESSION["onlineVersionMultiTasterSD6Booter"];
	 }
	 else if ($obj->firmwareId==4)
	 {
		 $typ="Multitaster SD485";
		 $onlineVersion = $_SESSION["onlineVersionMultiTasterSD485"];
		 $onlineVersionBooter = $_SESSION["onlineVersionMultiTasterSD485Booter"];
	 }
	// else if ($obj->firmwareId==5) $typ="Sonoff WLAN Relais";
   else if ($obj->firmwareId==6) $typ="ESP TCP Brücke";
   else if ($obj->firmwareId==7) $typ="ESP WLAN Modul";
   else if ($obj->firmwareId==8)
   {
	   $typ="HausBus Controls";
	   $onlineVersion = $_SESSION["onlineVersionHausbusControls"];
	   $onlineVersionBooter = $_SESSION["onlineVersionHausbusControlsBooter"];
   }
   else if ($obj->firmwareId==9)
   {
	   $typ="HausBus Controls X192C3";
	   $onlineVersion = $_SESSION["onlineVersionHBX"];
	   $onlineVersionBooter = $_SESSION["onlineVersionHBXBooter"];
   }
	 else $typ="Unbekannt ".$obj->firmwareId;
	 
	 if ($obj->size==999) $onlineVersion="";
		 
	 $actTag = str_replace("%TYP%",$typ,$actTag);
	 if ($obj->online!=1) $obj->name="[offline] ".$obj->name;
	 $actTag = str_replace("%NAME%",$obj->name,$actTag);
	 $actTag = str_replace("%VERSION%",$obj->majorRelease.".".$obj->minorRelease,$actTag);
	 
	 $update="";
	 if ($onlineVersion!="" && $onlineVersion!=$obj->majorRelease.".".$obj->minorRelease) $update=" <a href='updatesProceed.php?action=updateFirmware&firmwareId=$obj->firmwareId&autoload=$obj->id'><u>$onlineVersion</a>";
	 $actTag = str_replace("%UPDATE%", $update,$actTag);

	 $actTag = str_replace("%BOOTER_VERSION%",$obj->booterMajor.".".$obj->booterMinor,$actTag);

	 $update="";
	 if ($onlineVersionBooter!="" && $onlineVersionBooter!=$obj->booterMajor.".".$obj->booterMinor) $update=" <a href='updatesProceed.php?action=updateBooter&firmwareId=$obj->firmwareId&autoload=$obj->id'><u>$onlineVersion</a>";
	 $actTag = str_replace("%UPDATE_BOOTER%",$update,$actTag);
	 
	 $versions.=$actTag;
}
$html = str_replace("%VERSIONS%",$versions,$html);



show();


?>
