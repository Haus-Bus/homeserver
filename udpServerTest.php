<?php
error_reporting((E_ERROR | E_WARNING | E_PARSE) &~E_NOTICE);

$UDP_PORT = 15557;

$ifConfig = shell_exec('/sbin/ifconfig eth0');
$pos = strpos($ifConfig,"inet");
$pos = strpos($ifConfig," ",$pos);
$pos2 = strpos($ifConfig," ",$pos+1);
$address = substr($ifConfig,$pos+1,$pos2-$pos-1);
$addressParts = explode(".", $address);

$lb="\n";
$line="--------------------------------------------------------".$lb;

set_time_limit(0);
ob_implicit_flush();

logMe("Opening UDP Socket on port $UDP_PORT - my address ".$address.$lb); 
if (($udpSocket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP)) === false) die("UDP socket_create() fehlgeschlagen: Grund: " . socket_strerror(socket_last_error()));
if (!socket_set_option($udpSocket, SOL_SOCKET, SO_REUSEADDR, 1)) die("UDP Could not set option SO_REUSEADDR to socket: ". socket_strerror(socket_last_error()) . PHP_EOL);
if (!socket_set_option($udpSocket, 1, 6, TRUE)) die("UDP Could not set broadcast option to socket: ". socket_strerror(socket_last_error()) . PHP_EOL);
if (!socket_set_nonblock($udpSocket)) die("UDP Could not set socket to non blocking mode: ". socket_strerror(socket_last_error()) . PHP_EOL);
if (socket_bind($udpSocket, 0, $UDP_PORT) === false) traceError("UDP socket_bind() fehlgeschlagen: Grund: " . socket_strerror(socket_last_error($sock)));
logMe( "UDP server ready ".$lb);

ob_implicit_flush(true);

// Mainloop
$totalReceived=0;
$lastChange=time();
$startTime = time();
sendUdpDataToClient("192.168.178.183", "456");
while(true)
{
	if (checkAnySocketHasChanged())
	{
	   $lastChange=time();
	   checkForUdpData();
    }
}

function checkAnySocketHasChanged()
{
	global $udpSocket;
  
	$null = NULL;
	$allSocketsArray[]=$udpSocket;
	
	$num_changed_sockets = socket_select($allSocketsArray, $null, $null, 60);
    if ($num_changed_sockets === false) logMe( "TCP socket_select hat Fehler gemeldet: ". socket_strerror(socket_last_error())."\n");
    else if ($num_changed_sockets > 0) return TRUE;
    return FALSE;
}

function checkForUdpData()
{
  global $udpSocket;
  global $lb;
  global $totalReceived;
  global $UDP_PORT;
	
  $null = NULL;
  $socketArray = array($udpSocket);
  $num_changed_sockets = socket_select($socketArray, $null, $null, 0);
  if ($num_changed_sockets === false) logMe( "UDP socket_select hat Fehler gemeldet: ". socket_strerror(socket_last_error())."\n");
  else if ($num_changed_sockets > 0)
  {
    //$data = @socket_read($udpSocket, 1024);

    $from = '';
  	$data="";
    socket_recvfrom($udpSocket, $data, 1024, 0, $from, $UDP_PORT);
    if ($data!=false && $data!='')
	{
    	$dataLength = strlen($data);
	    if ($dataLength==3)	$totalReceived++;

         logMe("Received ".$dataLength." bytes from $from - Total ".$totalReceived." - ");
         for ($i = 0; $i < strlen($data); $i++)
         {
	       echo ord($data[$i])." ";
         }
	     echo $lb;
		 
		 sendUdpDataToClient($from, "456");

         flush();
         ob_flush();
	}
  }
}

function sendUdpDataToClient($ip, $data)
{
	global $udpSocket;
	global $UDP_PORT;
    global $lb;
	
	$write = socket_sendto($udpSocket, $data, strlen($data), 0, $ip, $UDP_PORT);
	if ($write===false || $write===0) logMe( "Send an $ip fehlgeschlagen: ".socket_strerror(socket_last_error())."\n");
}

function closeClient($client)
{
	  $client->active=false;
	  $client->deviceId=-1;
	  $client->ip="";
	  socket_close($client->socket);
}

function logMe($message, $withTimeStamp=true)
{
	 if ($withTimeStamp) echo date("d.m.y H:i:s").": ";
	 echo $message;
}


?>
