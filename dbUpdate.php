<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/homeserver/include/all.php';

unlink($_SERVER["DOCUMENT_ROOT"]."/homeserver/cron.cnf");
$classesId=12;
$functionId=128;
$functionsId = getOrCreateFunction($classesId, "ModuleId", $functionId, "RESULT", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "firmwareId", "ENUM", "","Standard");
getOrCreateFunctionEnum($functionsId, $paramsId, "SONOFF", 5);
getOrCreateFunctionEnum($functionsId, $paramsId, "ESP Bridge", 6);
QUERY("UPDATE featurefunctionenums set name='S0 Reader' where paramId='$paramsId' and name='ESP Bridge' limit 1");

// Doppelte S0 Reader Einträge löschen
$first=true;
$erg = QUERY("select id from featurefunctionenums where paramId='$paramsId' and name='S0 Reader' order by id");
while($row=MYSQLi_FETCH_ROW($erg))
{
	if ($first) $first=false;
	else QUERY("delete from featurefunctionenums where id='$row[0]' limit 1");
}


createConfigParam("current1d",0);
createConfigParam("current7d",0);
createConfigParam("current30d",0);

createConfigParam("webRollo1",15);
createConfigParam("webRollo2",30);
createConfigParam("webRollo3",60);
createConfigParam("webRollo4",85);

createConfigParam("webDimmer1",15);
createConfigParam("webDimmer2",30);
createConfigParam("webDimmer3",60);
createConfigParam("webDimmer4",85);

createConfigParam("webRoomTemp",1);
createConfigParam("webRoomHumidity",1);


//// TcpClient ////
$classId=91;
$classesId=32;
getOrCreateClass($classId, "TcpClient", $classesId, "Standard");

$functionId=1;
$functionsId = getOrCreateFunction($classesId, "announceServer", $functionId, "ACTION", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "IP0", "BYTE",'',"Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "IP1", "BYTE",'',"Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "IP2", "BYTE",'',"Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "IP3", "BYTE",'',"Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "port", "WORD",'',"Standard");


$functionId=2;
$functionsId = getOrCreateFunction($classesId, "getCurrentIp", $functionId, "FUNCTION", "Standard");

$functionId=128;
$functionsId = getOrCreateFunction($classesId, "CurrentIp", $functionId, "RESULT", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "IP0", "BYTE",'',"Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "IP1", "BYTE",'',"Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "IP2", "BYTE",'',"Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "IP3", "BYTE",'',"Standard");

$functionId=200;
$functionsId = getOrCreateFunction($classesId, "evWhoIsServer", $functionId, "EVENT", "Standard");


//// CurrentReader ////
$classId=90;
$classesId=31;
getOrCreateClass($classId, "CurrentReader", $classesId, "Standard");

$functionId=3;
$functionsId = getOrCreateFunction($classesId, "setConfiguration", $functionId, "FUNCTION", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "config", "BITMASK",'',"Standard");
getOrCreateFunctionBitMask($functionsId, $paramsId, "enableSignalEvent", 0);
getOrCreateFunctionBitMask($functionsId, $paramsId, "enableCurrentEvent", 1);
getOrCreateFunctionBitMask($functionsId, $paramsId, "enableInterruptEvent", 2);
getOrCreateFunctionBitMask($functionsId, $paramsId, "enableDebugEvent", 3);
getOrCreateFunctionBitMask($functionsId, $paramsId, "", 4);
getOrCreateFunctionBitMask($functionsId, $paramsId, "", 5);
getOrCreateFunctionBitMask($functionsId, $paramsId, "", 6);
getOrCreateFunctionBitMask($functionsId, $paramsId, "", 7);

$paramsId = getOrCreateFunctionParam($functionsId, "impPerKwh", "WORD", "Anzahl Signale pro kWh","Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "startCurrent", "DWORD", "Startwert Stromverbrauch in Wattstunden","Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "currentReportInterval", "WORD", "Interval in Sekunden nach dem immer der aktuelle Gesamtstromverbrauch gemeldet wird","Standard");


$functionId=4;
$functionsId = getOrCreateFunction($classesId, "getConfiguration", $functionId, "FUNCTION", "Standard");

$functionId=129;
$functionsId = getOrCreateFunction($classesId, "Configuration", $functionId, "RESULT", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "config", "BITMASK", "","Standard");
getOrCreateFunctionBitMask($functionsId, $paramsId, "enableSignalEvent", 0);  
getOrCreateFunctionBitMask($functionsId, $paramsId, "enableCurrentEvent", 1);  
getOrCreateFunctionBitMask($functionsId, $paramsId, "enableInterruptEvent", 2);  
getOrCreateFunctionBitMask($functionsId, $paramsId, "enableDebugEvent", 3);  
getOrCreateFunctionBitMask($functionsId, $paramsId, "", 4);  
getOrCreateFunctionBitMask($functionsId, $paramsId, "", 5);  
getOrCreateFunctionBitMask($functionsId, $paramsId, "", 6);  
getOrCreateFunctionBitMask($functionsId, $paramsId, "", 7);  
$paramsId = getOrCreateFunctionParam($functionsId, "impPerKwh", "WORD", "Anzahl Signale pro kWh","Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "startCurrent", "DWORD", "Startwert Stromverbrauch in Wattstunden","Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "currentReportInterval", "WORD", "Interval in Sekunden nach dem immer der aktuelle Gesamtstromverbrauch gemeldet wird","Standard");



$functionId=200;
$functionsId = getOrCreateFunction($classesId, "evSignal", $functionId, "EVENT", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "time", "DWORD", "Systemzeit des ESP zu Debugzwecken","Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "signalCount", "DWORD", "Anzahl gezählter S0 Signale seit dem letzten Zurücksetzen","Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "power", "WORD", "Aktuelle Leistung in Watt","Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "signalDuration", "DWORD", "Dauer des gemessenen S0 Signals in ms","Standard");


$functionId=1;
$functionsId = getOrCreateFunction($classesId, "getCurrent", $functionId, "FUNCTION", "Standard");

$functionId=201;
$functionsId = getOrCreateFunction($classesId, "evCurrent", $functionId, "EVENT", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "current", "DWORD", "Verbrauchter Strom in Wattstunden","Standard");
  
$functionId=130;
$functionsId = getOrCreateFunction($classesId, "Power", $functionId, "RESULT", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "power", "WORD", "Aktuelle Leistung in Watt","Standard");
  
$functionId=128;
$functionsId = getOrCreateFunction($classesId, "Current", $functionId, "RESULT", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "current", "DWORD", "verbrauchter Strom in Wattstunden","Standard");

$functionId=6;
$functionsId = getOrCreateFunction($classesId, "getSignalCount", $functionId, "FUNCTION", "Standard");

$functionId=131;
$functionsId = getOrCreateFunction($classesId, "SignalCount", $functionId, "RESULT", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "signalCount", "DWORD", "Anzahl gezählter S0 Signale seit dem letzten Zurücksetzen","Standard");
  
$functionId=7;
$functionsId = getOrCreateFunction($classesId, "clearSignalCount", $functionId, "FUNCTION", "Standard");

$functionId=2;
$functionsId = getOrCreateFunction($classesId, "setSignalCount", $functionId, "FUNCTION", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "signalCount", "DWORD", "","Standard");

$functionId=5;
$functionsId = getOrCreateFunction($classesId, "getPower", $functionId, "FUNCTION", "Standard");

$functionId=9;
$functionsId = getOrCreateFunction($classesId, "incSignalCount", $functionId, "FUNCTION", "Standard");
	 
$functionId=10;
$functionsId = getOrCreateFunction($classesId, "decSignalCount", $functionId, "FUNCTION", "Standard");


$functionId=210;
$functionsId = getOrCreateFunction($classesId, "evDebug", $functionId, "EVENT", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "data", "DWORD", "","Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "type", "BITMASK", "","Standard");
getOrCreateFunctionBitMask($functionsId, $paramsId, "invalidSignalLength", 0);  
getOrCreateFunctionBitMask($functionsId, $paramsId, "firmwareUpdateFinished", 1);  
getOrCreateFunctionBitMask($functionsId, $paramsId, "mainLoopTooLong", 2);  
getOrCreateFunctionBitMask($functionsId, $paramsId, "gotMultipleEvents", 3);  

deleteFunction($classesId, "getCurrentIp", 8);
deleteFunction($classesId, "CurrentIp", 132);

$functionId=211;
$functionsId = getOrCreateFunction($classesId, "evInterrupt", $functionId, "EVENT", "Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "value", "BYTE", "","Standard");
$paramsId = getOrCreateFunctionParam($functionsId, "stamp", "DWORD", "","Standard");

QUERY("ALTER TABLE `controller` ADD `receivedModuleId` TINYINT NOT NULL AFTER `lastChange`, ADD `receivedConfiguration` TINYINT NOT NULL AFTER `receivedModuleId`, ADD `receivedObjects` TINYINT NOT NULL AFTER `receivedConfiguration`",TRUE);
QUERY("CREATE TABLE IF NOT EXISTS `smoketest` (`id` int(11) NOT NULL AUTO_INCREMENT,`controllerId` int(11) NOT NULL,`objectId` int(11) NOT NULL,`command` varchar(50) NOT NULL,`logid` int(11) NOT NULL,UNIQUE KEY `id` (`id`)) ENGINE=MyISAM DEFAULT CHARSET=latin1",TRUE);
QUERY("CREATE TABLE IF NOT EXISTS `smoketesthelper` (`commandLogId` int(11) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1",TRUE);


if (!hasIndex("plugins","id"))
{
	QUERY("ALTER TABLE `plugins` DROP `id`");
	QUERY("ALTER TABLE `plugins` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);");
	addPrimaryKey("plugins","id");
}

checkPlugin("Amazon Alexa", "/homeserver/alexa/index.php");
checkPlugin("Heizungssteuerung", "/homeserver/heatingControl.php");
checkPlugin("Loxone Miniserver", "/homeserver/loxone/index.php");
checkPlugin("Raspberry Webstream Player", "/homeserver/plugins/radio/index.php");
checkPlugin("Lüftungssteuerung", "/homeserver/ventilationControl.php");
checkPlugin("Anzeige UserPlugin Daten", "/homeserver/editUserPluginData.php");
checkPlugin("Gruppen Watchdog", "/homeserver/plugins/watchdog/index.php");
checkPlugin("Systemvariablen", "/homeserver/systemVariables.php");


QUERY("ALTER TABLE `featureinstances` ADD `extra` TINYINT NOT NULL AFTER `lastChange`",TRUE);

QUERY("CREATE TABLE IF NOT EXISTS `heating` (`id` int(11) NOT NULL AUTO_INCREMENT, `sensor` int(11) NOT NULL, `relay` int(11) NOT NULL, `diagram` int(11) NOT NULL, PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=latin1", TRUE);

QUERY("UPDATE featurefunctionparams set type='SBYTE' where type='BYTE' and (id='158' or id='160' or id='155' or id='157' or id='161')");

QUERY("ALTER TABLE `basicrulegroupsignals` ADD `andCondition` TINYINT NOT NULL AFTER `eventType`",TRUE);
QUERY("ALTER TABLE `basicrulegroupsignals` ADD `generated` TINYINT NOT NULL AFTER `andCondition`",TRUE);
QUERY("ALTER TABLE `heating` ADD `pump` INT NOT NULL AFTER `diagram`",TRUE);
QUERY("ALTER TABLE `basicrulegroupsignals` ADD `controllerInstanceId` INT NOT NULL AFTER `eventType`, ADD `groupIndex` SMALLINT NOT NULL AFTER `controllerInstanceId`",TRUE);

QUERY("ALTER TABLE `featureinstances` ADD `alexaName` VARCHAR(150) NOT NULL AFTER `name`",TRUE);


QUERY("ALTER TABLE `controller` ADD `renamed` TINYINT NOT NULL AFTER `receivedObjects`",TRUE);
QUERY("ALTER TABLE `groups` ADD `forcedController` INT NOT NULL AFTER `active`",TRUE);

QUERY("update featureclasses set guiControl='humidityControl.php' where classId='34' limit 1",TRUE);
QUERY("update featureclasses set guiControl='brightnessControl.php 	' where classId='39' limit 1",TRUE);
QUERY("ALTER TABLE `controller` ADD `fcke` VARCHAR(5) NOT NULL AFTER `renamed`",TRUE);
QUERY("ALTER TABLE `controller` ADD `wifiName` VARCHAR(20) NOT NULL AFTER `fcke`",TRUE);
QUERY("ALTER TABLE `controller` ADD `wifiIp` VARCHAR(15) NOT NULL AFTER `wifiName`",TRUE);
QUERY("ALTER TABLE `udpdatalog` CHANGE `data` `data` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL",TRUE);

QUERY("ALTER TABLE `heating` ADD `startHour` SMALLINT NOT NULL AFTER `pump`, ADD `endHour` SMALLINT NOT NULL AFTER `startHour`",TRUE);
QUERY("ALTER TABLE `featureinstances` ADD `guiType` VARCHAR(20) NOT NULL AFTER `extra`",TRUE);
addUniqueKey("basicconfig","paramKey");


QUERY("CREATE TABLE IF NOT EXISTS `ventilation` (`id` int(11) NOT NULL AUTO_INCREMENT, `sensor` int(11) NOT NULL, `diagram` int(11) NOT NULL, `ventilator` int(11) NOT NULL, `startHour` SMALLINT NOT NULL, `endHour` SMALLINT NOT NULL, `basicRuleId` int(11) NOT NULL, PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=latin1", TRUE);
QUERY("CREATE TABLE IF NOT EXISTS `ventilationfenster` (`ventilationId` int(11) NOT NULL, `fensterInstanceId` int(11) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1", TRUE);

QUERY("ALTER TABLE `heating` ADD `basicRuleId` int(11) NOT NULL AFTER `endHour`",TRUE);
QUERY("ALTER TABLE `ventilation` ADD `type` VARCHAR(30) NOT NULL AFTER `basicRuleId`",TRUE);
QUERY("ALTER TABLE `rooms` ADD `stage` TINYINT NOT NULL AFTER `name`",TRUE);

QUERY("CREATE TABLE IF NOT EXISTS `brightnessexceptions` (`id` int(11) NOT NULL AUTO_INCREMENT, `objectId` int(11) NOT NULL,`brightness` int(11) NOT NULL, PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=latin1", TRUE);

QUERY("ALTER TABLE `groups` ADD `groupController` INT NOT NULL AFTER `forcedController`, ADD `groupIndex` TINYINT NOT NULL AFTER `groupController`",TRUE);


QUERY("CREATE TABLE `influx` (`id` int(11) NOT NULL,`title` varchar(255) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1",TRUE);
QUERY("CREATE TABLE `influxsignalevents` (`id` int(11) NOT NULL,`influxSignalsId` int(11) NOT NULL,`featureInstanceId` int(11) NOT NULL,`functionId` int(11) NOT NULL,`fkt` varchar(50) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1",TRUE);
QUERY("CREATE TABLE `influxsignals` (`id` int(11) NOT NULL,`title` varchar(150) NOT NULL,`influxId` int(11) NOT NULL,`featureInstanceId` int(11) NOT NULL,`functionId` int(11) NOT NULL,`fkt` varchar(150) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=latin1",TRUE);
addPrimaryKey("influx","id");
addPrimaryKey("influxsignalevents","id");
addPrimaryKey("influxsignals","id");
QUERY("ALTER TABLE `influx` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1",TRUE);
QUERY("ALTER TABLE `influxsignalevents` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1",TRUE);
QUERY("ALTER TABLE `influxsignals` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1",TRUE);
QUERY("ALTER TABLE `basicconfig` CHANGE `paramValue` `paramValue` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL",TRUE);

addCronEntry("* * * * * /var/www/homeserver/graphDataCron.sh");

QUERY("ALTER TABLE `heating` ADD `reduction` FLOAT NOT NULL AFTER `basicRuleId`;",TRUE);

checkAlert(1, "Achtung, falls du das Heizungsplugin verwendest, musst du nach dem Update einmal die Regeln neu übermitteln, da sich etwas inkompatibel geändert hat. Am besten zur Sicherheit auch einmal die eingestellten Themperaturen prüfen!");

QUERY("CREATE TABLE `systemvariablehelper` (`id` int(11) NOT NULL,`controllerId` int(11) NOT NULL,`variableType` tinyint(4) NOT NULL,`nr` tinyint(4) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;",TRUE);
QUERY("ALTER TABLE `systemVariableHelper` ADD PRIMARY KEY (`id`), ADD KEY `controllerId` (`controllerId`);",TRUE);
QUERY("ALTER TABLE `systemVariableHelper` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;",TRUE);

$erg = QUERY("select userkey from userdata where userKey='SYSTEM_VARIABLE_BIT0' limit 1");
if ($row=mysqli_fetch_row($erg)) {}
else QUERY("INSERT into userdata (userkey, uservalue) values('SYSTEM_VARIABLE_BIT0','Nachtabsenkung inaktiv')");

$erg = QUERY("select userkey from userdata where userKey='SYSTEM_VARIABLE_BIT1' limit 1");
if ($row=mysqli_fetch_row($erg)) {}
else QUERY("INSERT into userdata (userkey, uservalue) values('SYSTEM_VARIABLE_BIT1','Nachtabsenkung daueraktiv')");

QUERY("update ruleactionparams set paramValue='0' where featureFunctionParamsId='540'");
echo "OK";

function checkAlert($id, $message)
{
	if (!file_exists("../alert_$id.tmp"))
	{
		file_put_contents("../alert_$id.tmp","dummy");
		
		$foundSensors=false;
		$erg = QUERY("select heating.sensor from heating where diagram!=-1");
        while($obj=MYSQLi_FETCH_OBJECT($erg))
		{
		   $foundSensors=true;
	 	   $data = getFeatureInstanceData($obj->sensor);
		   $objectId = $data->objectId;
		   $callArray = array();
           $erg2 = QUERY("select functionData from lastReceived where senderObj='".$objectId."' and function='Configuration' order by id desc limit 1");
           if ($row2=MYSQLi_FETCH_ROW($erg2))
           {
     	       $functionData = unserialize($row2[0]);
      	     $paramData = $functionData->paramData;
   	         foreach ($paramData as $object)
   	         {
			    $callArray[$object->name]=$object->dataValue;
     	     }
     	   }
		 
		   $callArray["upperThreshold"]=$callArray["lowerThreshold"];
		   $callArray["upperThresholdFraction"]=$callArray["lowerThresholdFraction"];
	 
 	       callObjectMethodByName($objectId, "setConfiguration", $callArray);
		   usleep(200000);
		   callObjectMethodByName($objectId, "getConfiguration");
		}
		
		if ($foundSensors) echo "<script>alert('$message');</script>";
	}
}

function addCronEntry($entry)
{
  file_put_contents($_SERVER["DOCUMENT_ROOT"]."/homeserver/cron.cnf",$entry."\n",FILE_APPEND);
}

function getOrCreateClass($classId, $className, $classesId, $view)
{
	$erg = QUERY("select id from featureclasses where name='$className' limit 1");
  if ($row=mysqli_fetch_ROW($erg)) return $row[0];
  $sql = "INSERT into featureclasses (id,classId,name,view) values('$classesId','$classId','$className','$view')";
  QUERY($sql);
  return query_insert_id();
}

function getOrCreateFunction($classesId, $functionName, $functionId, $functionType, $view)
{
	$erg = QUERY("select id from featurefunctions where featureClassesId='$classesId' and name='$functionName' limit 1");
  if ($row=mysqli_fetch_ROW($erg)) return $row[0];

  $sql = "INSERT into featurefunctions (featureClassesId,type,name,functionId, view) values('$classesId','$functionType','$functionName','$functionId', 'Standard')";
  QUERY($sql);
  return query_insert_id();
}

function deleteFunction($classesId, $functionName, $functionId)
{
	QUERY("delete from featurefunctions where featureClassesId='$classesId' and name='$functionName' and functionId='$functionId' limit 1");
}


function getOrCreateFunctionParam($functionsId, $paramName, $paramType,$comment,$view)
{
	$erg = QUERY("select id from featurefunctionparams where featureFunctionId='$functionsId' and name='$paramName' limit 1");
	if ($row=mysqli_fetch_ROW($erg)) return $row[0];
	
	$comment = query_real_escape_string($comment);
	
  $sql = "INSERT into featurefunctionparams (featureFunctionId,name,type,comment,view) values('$functionsId','$paramName','$paramType','$comment','$view')";
  QUERY($sql);
  return query_insert_id();
}

function getOrCreateFunctionEnum($functionsId, $functionParamsId, $functionParamName, $functionParamValue)
{
	$erg = QUERY("select id from featurefunctionenums where paramId='$functionParamsId' and name='$functionParamName' limit 1");
  if ($row=mysqli_fetch_ROW($erg)) return $row[0];

  $sql = "INSERT into featurefunctionenums (featureFunctionId,paramId,name,value) values('$functionsId','$functionParamsId','$functionParamName','$functionParamValue')";
  QUERY($sql);
  return query_insert_id();
}

function getOrCreateFunctionBitMask($functionsId, $functionParamsId, $maskName, $maskBit)
{
  $erg = QUERY("select id,name from featurefunctionbitmasks where featureFunctionId='$functionsId' and paramId='$functionParamsId' and bit='$maskBit' limit 1");
	if ($row=mysqli_fetch_ROW($erg))
	{
		if ($row[1]!=$maskName)
		{
			$maskName = query_real_escape_string($maskName);
			$sql = "UPDATE featurefunctionbitmasks set name='$maskName' where id='$row[0]' limit 1";
			QUERY($sql);
		}
		return $row[0];
	}
	
	$maskName = query_real_escape_string($maskName);
  
  $sql = "INSERT into featurefunctionbitmasks (featureFunctionId,paramId,bit,name) values('$functionsId','$functionParamId','$maskBit','$maskName')";
  QUERY($sql);
  return query_insert_id();
}

function createConfigParam($paramKey, $paramValue)
{
  $erg = QUERY("select paramValue from basicConfig where paramKey='$paramKey' limit 1");
  if ($obj=mysqli_fetch_OBJECT($erg)) return;

  $sql = "insert into basicConfig (paramKey,paramValue) values('$paramKey','$paramValue')";
  QUERY($sql);
}

function addIndex($table,$column)
{
	$erg = QUERY("SHOW INDEX FROM $table WHERE Column_name = '$column'");
  if ($row=mysqli_fetch_ROW($erg)) {}
  else
  {
  	//echo "Ergänze Index $column in $table <br>";
  	QUERY("ALTER TABLE $table ADD INDEX ( $column)");
  }
}

function hasIndex($table,$column)
{
	$erg = QUERY("SHOW INDEX FROM $table WHERE Column_name = '$column'");
  if ($row=mysqli_fetch_ROW($erg)) return TRUE;
  return FALSE;
}

function addPrimaryKey($table,$column)
{
	if (!hasIndex($table,$column))
  {
  	//echo "Ergänze Index $column in $table <br>";
  	QUERY("ALTER TABLE $table ADD PRIMARY KEY ( $column)");
  }
}

function addUniqueKey($table,$column)
{
	if (!hasIndex($table,$column))
  {
  	//echo "Ergänze Index $column in $table <br>";
  	QUERY("ALTER TABLE $table ADD UNIQUE ( $column)");
  }
}

function repairDoubleIndexes()
{
	$erg = QUERY("SHOW TABLES");
	while($row=mysqli_fetch_ROW($erg))
	{
		$table=$row[0];

      $check=array();
	  $erg2 = QUERY("SHOW INDEX FROM $table");
	  while($obj=mysqli_fetch_OBJECT($erg2))
	  {
	  	  if ($check[$obj->Column_name]==1)
	  	  {
	  	  	//echo "Lösche Index: ".$table.": ".$obj->Key_name." - ".$obj->Column_name."<br>";
	  	  	QUERY("ALTER TABLE $table DROP INDEX ".$obj->Key_name);
	  	  }
	  	  $check[$obj->Column_name]=1;
	  }
	}
}

function checkPlugin($title, $link)
{
  $title = query_real_escape_string($title);
  
  $erg = QUERY("select id from plugins where url='$link' limit 1");
  if ($row=MYSQLi_FETCH_ROW($erg)){}
  else QUERY("INSERT into plugins (title, url) values('$title','$link')");
}

?>