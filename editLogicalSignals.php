<?php
include ($_SERVER["DOCUMENT_ROOT"] . "/homeserver/include/all.php");

if ($action == "removeState")
{
  $activationState = ($nr - 1) * 10;
  $deactivationState = ($nr - 1) * 10 + 1;
  $erg = QUERY("select id from rules where groupId='$groupId' and activationStateId='$activationState' or activationStateId='$deactivationState' limit 2");
  while ( $row = mysqli_fetch_ROW($erg) )
  {
    deleteRule($row[0]);
  }
  
  $nr = 0;
  $erg = QUERY("select id,activationStateId from rules where groupId='$groupId' order by activationStateId");
  while ( $row = mysqli_fetch_ROW($erg) )
  {
    $activationStateId = $row[1];
    
    if ($activationStateId % 2 == 0)
    {
      $state = $nr * 10;
      QUERY("UPDATE rules set activationStateId='$state' where id='$row[0]' limit 1");
    }
    else
    {
      $state = $nr * 10 + 1;
      QUERY("UPDATE rules set activationStateId='$state' where id='$row[0]' limit 1");
      $nr++;
    }
  }
}

setupTreeAndContent("editLogicalSignals_design.html", $message);

$html = str_replace("%GROUP_ID%", $groupId, $html);

$allFeatureInstances = readFeatureInstances();
$allFeatureClasses = readFeatureClasses();
$allFeatureFunctions = readFeatureFunctions();

$evTimeFunctionId = getClassesIdFunctionsIdByName($CONTROLLER_CLASSES_ID, "evTime");
$evDayFunctionId = getClassesIdFunctionsIdByName($CONTROLLER_CLASSES_ID, "evDay");
$evNightFunctionId = getClassesIdFunctionsIdByName($CONTROLLER_CLASSES_ID, "evNight");

$statesTag = getTag("%STATES%", $html);
$activeTag = getTag("%ACTIVE%", $statesTag);

$ii=0;
$theStates = array();
$erg = QUERY("select rules.id as ruleId, ruleSignals.id as signalId, activationStateId,featureInstanceId,featureFunctionId from rules left join ruleSignals on (ruleSignals.ruleId=rules.id) where groupId='$groupId'");
while ( $obj = mysqli_fetch_OBJECT($erg) )
{
  $nr = (int)($obj->activationStateId / 10);
  
  //$ii++;
  //if ($ii==2) QUERY("update rules set activationStateId='1' where id='$obj->ruleId' limit 1");
  //if ($ii==2) QUERY("INSERT into rules (groupId, activationStateId,startDay,startHour,startMinute,endDay,endHour,endMinute) values('$groupId','1','7','31','255','7','31','255')");
  //echo $nr."-".$obj->activationStateId."<br>";

  $roomName = getRoomForFeatureInstance($obj->featureInstanceId)->name;
  $actFeatureInstance = $allFeatureInstances[$obj->featureInstanceId];
  $actClass = $allFeatureClasses[$actFeatureInstance->featureClassesId];
  $actTag = $activeTag;
  
  if ($obj->featureInstanceId < 0) //gruppenSignale
  {
        $signalGroupId = $obj->featureInstanceId * - 1;
        
        $erg34 = QUERY("select eventType,groups.name as groupName, groupType, andCondition from  basicRuleGroupSignals left join groups on (groups.id=basicRuleGroupSignals.groupId) where basicRuleGroupSignals.id='$signalGroupId' limit 1");
        $obj34 = mysqli_fetch_OBJECT($erg34);

          if ($obj34->groupType == "SIGNALS-AND") $groupType = "UG";
          else if ($obj34->groupType == "SIGNALS-OR") $groupType = "OG";
          else $groupType = "??";

          if ($obj34->eventType == "ACTIVE") $action = "Erreichen";
          else if ($obj34->eventType == "DEACTIVE") $action = "Verlassen";
          else $action = "Unbekannt";
        
          
          $actTag = str_replace("%SIGNAL_ROOM%", "", $actTag);
          $actTag = str_replace("%SIGNAL_CLASS%", "", $actTag);
          $actTag = str_replace("%SIGNAL_NAME%", "</a>Logische Grupppe " . $obj34->groupName, $actTag);
          $actTag = str_replace("%FEATURE_FUNCTION_ID%", "", $actTag);
          $actTag = str_replace("%SIGNAL_FUNCTION%", $action . " des aktiven Zustandes", $actTag);
  }  
  else if ($actClass == "" && $obj->featureFunctionId == $evTimeFunctionId) // evTime vom controller
  {
    $erg2 = QUERY("select paramValue from ruleSignalParams where ruleSignalId='$obj->signalId' limit 1");
    if ($row2 = mysqli_fetch_ROW($erg2))
    $weekTime = parseWeekTimeToObject($row2[0]);
	
    $actTag = str_replace("%SIGNAL_ROOM%", "", $actTag);	
    $actTag = str_replace("%SIGNAL_NAME%", "Zeitgesteuert", $actTag);
    $actTag = str_replace("%SIGNAL_CLASS%", "", $actTag);
    $actTag = str_replace("%SIGNAL_FUNCTION%", $weekTime->tag . " " . $weekTime->stunde . ":" . $weekTime->minute, $actTag);
    $actTag = str_replace("%RULE_ID%", $obj->ruleId, $actTag);
    $actTag = str_replace("%SIGNAL_ID%", $obj->signalId, $actTag);
    $actTag = str_replace("%FEATURE_FUNCTION_ID%", "", $actTag);
  }
  else if ($actClass == "" && $obj->featureFunctionId == $evDayFunctionId)
  {
    $actTag = str_replace("%SIGNAL_ROOM%", "", $actTag);	
    $actTag = str_replace("%SIGNAL_NAME%", "Zeitgesteuert", $actTag);
    $actTag = str_replace("%SIGNAL_CLASS%", "", $actTag);
    $actTag = str_replace("%SIGNAL_FUNCTION%", "Bei Sonnenaufgang", $actTag);
    $actTag = str_replace("%RULE_ID%", $obj->ruleId, $actTag);
    $actTag = str_replace("%SIGNAL_ID%", $obj->signalId, $actTag);
    $actTag = str_replace("%FEATURE_FUNCTION_ID%", "", $actTag);
  }
  else if ($actClass == "" && $obj->featureFunctionId == $evNightFunctionId)
  {
    $actTag = str_replace("%SIGNAL_ROOM%", "", $actTag);	
    $actTag = str_replace("%SIGNAL_NAME%", "Zeitgesteuert", $actTag);
    $actTag = str_replace("%SIGNAL_CLASS%", "", $actTag);
    $actTag = str_replace("%SIGNAL_FUNCTION%", "Bei Sonnenuntergang", $actTag);
    $actTag = str_replace("%RULE_ID%", $obj->ruleId, $actTag);
    $actTag = str_replace("%SIGNAL_ID%", $obj->signalId, $actTag);
    $actTag = str_replace("%FEATURE_FUNCTION_ID%", "", $actTag);
  }
  else
  {
    $actTag = str_replace("%SIGNAL_ROOM%", $roomName, $actTag);
    $actTag = str_replace("%SIGNAL_NAME%", $actFeatureInstance->name, $actTag);
    $actTag = str_replace("%SIGNAL_CLASS%", $actClass->name, $actTag);
    $actTag = str_replace("%SIGNAL_FUNCTION%", i18n($allFeatureFunctions[$obj->featureFunctionId]->name), $actTag);
    $actTag = str_replace("%RULE_ID%", $obj->ruleId, $actTag);
    $actTag = str_replace("%SIGNAL_ID%", $obj->signalId, $actTag);
    $actTag = str_replace("%FEATURE_FUNCTION_ID%", $obj->featureFunctionId, $actTag);
  }
  
  if ($obj->activationStateId % 2 == 0)
  {
    if ($obj->featureInstanceId != null) $theStates[$nr]["actives"] .= $actTag;
    $theStates[$nr]["activeRuleId"] = $obj->ruleId;
    $theStates[$nr]["activationStateIdActive"] = $obj->activationStateId;
  }
  else
  {
    if ($obj->featureInstanceId != null) $theStates[$nr]["deactives"] .= $actTag;
    $theStates[$nr]["deactiveRuleId"] = $obj->ruleId;
    $theStates[$nr]["activationStateIdInactive"] = $obj->activationStateId;
  }
}

$last = - 1;
$foundEmpty = 0;
if ($theStates != "")
{
  ksort($theStates);
  foreach ( $theStates as $nr => $content )
  {
    $last = $nr;
    $actTag = $statesTag;
    
    if ($content["actives"] == "" && $content["deactives"] == "")
    {
      $foundEmpty = 1;
      removeTag("%OPT_DELETE%", $actTag);
    }
    else
      chooseTag("%OPT_DELETE%", $actTag);
    
    $actTag = str_replace("%NR%", $nr + 1, $actTag);
    $actTag = str_replace("%ACTIVE_RULE_ID%", $content["activeRuleId"], $actTag);
    $actTag = str_replace("%DEACTIVE_RULE_ID%", $content["deactiveRuleId"], $actTag);
	if ($content["activeRuleId"]=="")
	{
		$activationState = $last * 10;
	    QUERY("INSERT into rules (groupId, activationStateId,startDay,startHour,startMinute,endDay,endHour,endMinute) values('$groupId','$activationState','7','31','255','7','31','255')");
		$foundErrors=1;
	}
    $actTag = str_replace("%ACTIVE%", $content["actives"], $actTag);
	if ($content["deactiveRuleId"]=="")
	{
		$activationState = $last * 10+1;
	    QUERY("INSERT into rules (groupId, activationStateId,startDay,startHour,startMinute,endDay,endHour,endMinute) values('$groupId','$activationState','7','31','255','7','31','255')");
		$foundErrors=1;
	}
    $actTag = str_replace("%DEACTIVE%", $content["deactives"], $actTag);
    $actTag = str_replace("%LOGICAL_STATE_ID_ACTIVE%", $content["activationStateIdActive"], $actTag);
    $actTag = str_replace("%LOGICAL_STATE_ID_INACTIVE%", $content["activationStateIdInactive"], $actTag);
    
    $states .= $actTag;
  }
}

if ($foundErrors==1) die("Es wurde ein Fehler in der Signalkonfiguration gefunden, der automatisch korrigiert wurde. Bitte diese Seite neu laden");

if ($foundEmpty == 0)
{
  $last++;
  $activationState = $last * 10;
  QUERY("INSERT into rules (groupId, activationStateId,startDay,startHour,startMinute,endDay,endHour,endMinute) values('$groupId','$activationState','7','31','255','7','31','255')");
  $acticationRuleId = query_insert_id();
  $deactivationState = ($last * 10) + 1;
  QUERY("INSERT into rules (groupId, activationStateId,startDay,startHour,startMinute,endDay,endHour,endMinute) values('$groupId','$deactivationState','7','31','255','7','31','255')");
  $deacticationRuleId = query_insert_id();
  
  $actTag = $statesTag;
  removeTag("%OPT_DELETE%", $actTag);
  $actTag = str_replace("%NR%", $last + 1, $actTag);
  $actTag = str_replace("%ACTIVE_RULE_ID%", $acticationRuleId, $actTag);
  $actTag = str_replace("%DEACTIVE_RULE_ID%", $deacticationRuleId, $actTag);
  $actTag = str_replace("%ACTIVE%", "", $actTag);
  $actTag = str_replace("%DEACTIVE%", "", $actTag);
  $states .= $actTag;
}

$html = str_replace("%STATES%", $states, $html);

show();

function parseWeekTimeToObject($value)
{
  $result = "";
  $weekTime = getWeekTime("dummy", $value);
  //selected value='1'>Di.
  

  $pos = strpos($weekTime, "selected");
  if ($pos === FALSE) return $result;
  $pos2 = strpos($weekTime, ">", $pos);
  if ($pos2 === FALSE) return $result;
  $pos3 = strpos($weekTime, "<", $pos2 + 1);
  if ($pos3 === FALSE) return $result;
  $result->tag = substr($weekTime, $pos2 + 1, $pos3 - $pos2 - 1);
  
  $pos = strpos($weekTime, "selected", $pos3);
  if ($pos === FALSE) return $result;
  $pos2 = strpos($weekTime, ">", $pos);
  if ($pos2 === FALSE) return $result;
  $pos3 = strpos($weekTime, "<", $pos2 + 1);
  if ($pos3 === FALSE) return $result;
  $result->stunde = substr($weekTime, $pos2 + 1, $pos3 - $pos2 - 1);
  
  $pos = strpos($weekTime, "selected", $pos3);
  if ($pos === FALSE) return $result;
  $pos2 = strpos($weekTime, ">", $pos);
  if ($pos2 === FALSE) return $result;
  $pos3 = strpos($weekTime, "<", $pos2 + 1);
  if ($pos3 === FALSE) return $result;
  $result->minute = substr($weekTime, $pos2 + 1, $pos3 - $pos2 - 1);
  
  return $result;
}
?>