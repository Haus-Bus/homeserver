<?php
include("include/all.php");

if ($submitted==1)
{
   QUERY("delete from userData where userKey like 'SYSTEM_VARIABLE%'");
   
   for ($i=0;$i<32;$i++)
   {
	   $var = "BIT".$i;
       $varValue=$$var;
	   if ($varValue!="") QUERY("insert into userData (userKey, userValue) values('SYSTEM_VARIABLE_$var', '$varValue')");
   }
   for ($i=0;$i<16;$i++)
   {
	   $var = "BYTE".$i;
       $varValue=$$var;
	   if ($varValue!="") QUERY("insert into userData (userKey, userValue) values('SYSTEM_VARIABLE_$var', '$varValue')");
   }
   for ($i=0;$i<16;$i++)
   {
	   $var = "WORD".$i;
       $varValue=$$var;
	   if ($varValue!="") QUERY("insert into userData (userKey, userValue) values('SYSTEM_VARIABLE_$var', '$varValue')");
   }
}
else if ($action=="read")
{
   $controllerThatHoldsSystemProperties = getFirstNormalOnlineController()->objectId;	
   if (strpos($var,"BIT")!==FALSE)
   {
	   $type="BIT";
	   $index = substr($var,3);
   }
   else if (strpos($var,"BYTE")!==FALSE)
   {
	   $type="BYTE";
	   $index = substr($var,4);
   }
   else if (strpos($var,"WORD")!==FALSE)
   {
	   $type="WORD";
	   $index = substr($var,4);
   }
   
   $result = executeCommand($controllerThatHoldsSystemProperties, "getSystemVariable", array("type" => $type, "index" => $index), "SystemVariable");
   $$var = $result["value"];
}

$html = file_get_contents("templates/systemVariables_design.html");
$elementsTag = getTag("%ELEMENTS%",$html);

$systemVariables=array();
$erg = QUERY("select userkey, uservalue from userData  where userKey like 'SYSTEM_VARIABLE%'");
while($obj=mysqli_fetch_object($erg))
{
	$systemVariables[str_replace("SYSTEM_VARIABLE_","",$obj->userkey)]=$obj->uservalue;
}

$elements="";
for ($i=0;$i<32;$i++)
{
  $var = "BIT".$i;
  $actTag = $elementsTag;
  $actTag = str_replace("%INDEX%",$i,$actTag);
  $actTag = str_replace("%NAME%",$systemVariables[$var],$actTag);
  $actTag = str_replace("%VAR_NAME%",$var,$actTag);
  $actTag = str_replace("%VALUE%",$$var,$actTag);
  $elements.=$actTag;
}
$html = str_replace("%ELEMENTS%",$elements,$html);

$byteElements="";
for ($i=0;$i<15;$i++)
{
  $var = "BYTE".$i;
  $actTag = $elementsTag;
  $actTag = str_replace("%INDEX%",$i,$actTag);
  $actTag = str_replace("%NAME%",$systemVariables[$var],$actTag);
  $actTag = str_replace("%VAR_NAME%",$var,$actTag);
  $actTag = str_replace("%VALUE%",$$var,$actTag);
  $byteElements.=$actTag;
}
$html = str_replace("%BYTE_ELEMENTS%",$byteElements,$html);

$wordElements="";
for ($i=0;$i<15;$i++)
{
  $var = "WORD".$i;
  $actTag = $elementsTag;
  $actTag = str_replace("%INDEX%",$i,$actTag);
  $actTag = str_replace("%NAME%",$systemVariables[$var],$actTag);
  $actTag = str_replace("%VAR_NAME%",$var,$actTag);
  $actTag = str_replace("%VALUE%",$$var,$actTag);
  $wordElements.=$actTag;
}
$html = str_replace("%WORD_ELEMENTS%",$wordElements,$html);

die($html);
?>