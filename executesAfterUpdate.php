<?php

if (file_exists($_SERVER["DOCUMENT_ROOT"]."/homeserver/my.cnf"))
{
  $erg = shell_exec("diff /etc/mysql/my.cnf ".$_SERVER["DOCUMENT_ROOT"]."/homeserver/my.cnf");
  if (strlen($erg)>2)
  {
   	exec("cp ".$_SERVER["DOCUMENT_ROOT"]."/homeserver/my.cnf /etc/mysql/");
    exec("/etc/init.d/mysql restart");
  }
  exec("rm ".$_SERVER["DOCUMENT_ROOT"]."/homeserver/my.cnf");
}

if (file_exists("/var/www/homeserver/cron.cnf"))
{
  $output = shell_exec('crontab -l');
  unlink('/tmp/crontab.txt');

  $entries = file("/var/www/homeserver/cron.cnf");
  $add="";
  foreach ($entries as $entry)
  {
    if(strpos($output,$entry)===FALSE) $add.=$entry;
  }
		
  if ($add!="")
  {
	file_put_contents('/tmp/crontab.txt', $output.$add,FILE_APPEND);
    exec('crontab /tmp/crontab.txt');
	unlink("/var/www/homeserver/cron.cnf");
  }
}
	
exec("chmod 777 /var/www/homeserver/graphDataCron.sh");

if (file_exists("/var/www/homeserver/jpgraph")) exec("rm -R /var/www/homeserver/jpgraph");
checkUpdatePhpMyAdminRights();

	
function checkUpdatePhpMyAdminRights()
{
  $file = "/etc/phpmyadmin/apache.conf";
  if (file_exists($file))
  {
	$text = file_get_contents($file);
	if (strpos($text,"allow from 127.0.0.1")===FALSE)
	{
	  $pos = strpos($text, "<Directory /usr/share/phpmyadmin>");
	  if ($pos!==FALSE) $pos = strpos($text, "</Directory>",$pos);
	  if ($pos!==FALSE)
	  {
	  	 $text = substr($text,0,$pos)."order deny,allow\ndeny from all\nallow from 127.0.0.1\nallow from 192.\n".substr($text,$pos);
	  	 file_put_contents($file, $text);
	  	 return;
	  }
	}
  }
}

?>