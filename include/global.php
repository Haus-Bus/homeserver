<?php
ini_set ( 'max_execution_time', 300 );
ini_set ( "memory_limit", '512M' );
//error_reporting ( E_ERROR | E_PARSE );

$res = @session_start ();
if (! $res) session_start ();

if (! isset ( $_SESSION ["actLanguage"] )) switchLanguage ( "Deutsch" );

if (strtolower($PHP_AUTH_USER)=="homeserver") $webaccess=true;
else $webaccess=false;


  
function readControllers()
{
  $erg = QUERY ( "select SQL_CACHE * from controller" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $controllers [$obj->id] = $obj;
  }
  
  return $controllers;
}

function readFeatureClasses()
{
  $erg = QUERY ( "select SQL_CACHE * from featureClasses order by name" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $featureClasses [$obj->id] = $obj;
  }
  
  return $featureClasses;
}

function getNameForFeatureClass($featureClassesId)
{
  $erg = QUERY ( "select name from featureClasses where id='$featureClassesId' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) return $row [0];
  return "";
}

function getClassesIdByName($featureClassName)
{
	if (isset($_SESSION["featureClassNameCache"][$featureClassName])) return $_SESSION["featureClassNameCache"][$featureClassName];
	
  $erg = QUERY ( "select id from featureClasses where name='$featureClassName' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
  {
  	$_SESSION["featureClassNameCache"][$featureClassName]=$row[0];
    return $row [0];
  }
  return "";
}

function getClassesIdByFeatureInstanceId($featureInstanceId)
{
  $erg = QUERY ( "select featureClassesId from featureInstances where id='$featureInstanceId' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
    return $row [0];
  return "";
}

function getClassIdByName($featureClassName)
{
  $erg = QUERY ( "select classId from featureClasses where name='$featureClassName' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
    return $row [0];
  return "";
}

function readFeatureClassesThatSupportType($type)
{
  $erg = QUERY ( "select SQL_CACHE distinct(featureClassesId) from featureFunctions where type='$type'" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    $result [$row [0]] = 1;
  }
  return $result;
}

function getRoomForFeatureInstance($featureInstanceId)
{
  $erg = QUERY ( "select roomId from roomFeatures where featureInstanceId='$featureInstanceId' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
  {
    $erg = QUERY ( "select * from rooms where id='$row[0]' limit 1" );
    if ($obj = MYSQLi_FETCH_OBJECT ( $erg ))
    {
      return $obj;
    }
  }
  
  $erg = QUERY ( "select controller.name from controller join featureInstances on (featureInstances.controllerId = controller.id) where featureInstances.id='$featureInstanceId' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
  {
    $result->name = "Controller " . $row [0];
    $result->id = "0";
    return $result;
  }
  
  $result->name = "Keinem Raum zugeordnet";
  $result->id = "0";
  return $result;
}

function getFeaturesForRoom($roomId)
{
  $pos = 0;
  $erg = QUERY ( "select SQL_CACHE featureInstanceId from roomFeatures where roomId='$roomId'" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    $erg2 = QUERY ( "select SQL_CACHE * from featureInstances where id='$row[0]' limit 1" );
    if ($obj = MYSQLi_FETCH_OBJECT ( $erg2 ))
    {
      $result [$pos ++] = $obj;
    }
  }
  return $result;
}

function readRooms()
{
  global $roomsCache;
  
  if (! isset ( $roomsCache ))
  {
    $erg = QUERY ( "select SQL_CACHE * from rooms order by name" );
    while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
    {
      $roomsCache [$obj->id] = $obj;
    }
  }
  
  return $roomsCache;
}

function readRoomFeatures()
{
  $erg = QUERY ( "select SQL_CACHE * from roomFeatures" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $roomFeatures [$obj->id] = $obj;
  }
  
  return $roomFeatures;
}

function readMyRuleSignals($ruleId)
{
  $result = array();
  $erg = QUERY ( "select * from ruleSignals where ruleId='$ruleId'" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $result [$obj->featureInstanceId . "-" . $obj->featureFunctionId] = 1;
  }
  return $result;
}

function readMyBasicRuleSignals($ruleId)
{
  $erg = QUERY ( "select * from basicRuleSignals where ruleId='$ruleId'" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $result [$obj->featureInstanceId] = 1;
  }
  return $result;
}

function readMyRuleActions($ruleId)
{
  $erg = QUERY ( "select * from ruleActions where ruleId='$ruleId'" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $result [$obj->featureInstanceId] = 1;
  }
  return $result;
}

function readRuleActions()
{
  $erg = QUERY ( "select SQL_CACHE * from ruleActions order by id" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $ruleActions [$obj->id] = $obj;
  }
  
  return $ruleActions;
}

function readRuleSignals()
{
  $erg = QUERY ( "select SQL_CACHE * from ruleSignals order by id" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $ruleSignals [$obj->id] = $obj;
  }
  
  return $ruleSignals;
}

function readBasicRuleSignals()
{
  $erg = QUERY ( "select SQL_CACHE * from basicRuleSignals order by id" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $ruleSignals [$obj->id] = $obj;
  }
  
  return $ruleSignals;
}

function readGroupFeatures($groupId)
{
  $erg = QUERY ( "select * from groupFeatures where groupId='$groupId'" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $result [$obj->featureInstanceId] = $obj;
  }
  return $result;
}

function readGroups()
{
  $erg = QUERY ( "select SQL_CACHE * from groups order by name" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $groups [$obj->id] = $obj;
  }
  
  return $groups;
}

// Sucht feste Einzelgruppe zu einem Feature
function readMySingleGroup($featureInstanceId)
{
  $erg = QUERY ( "select groups.id, groups.single from groups join groupFeatures on (groupFeatures.groupId=groups.id) where groups.single='1' and featureInstanceId='$featureInstanceId' limit 1" );
  if ($obj = MYSQLi_FETCH_OBJECT ( $erg ))
    return $obj;
  echo $featureInstanceId . " hat keinen Gruppeneintrag <br>";
}

function readFeatureInstances($fields="*")
{
	if ($fields!="*") $fields="id,".$fields;
  $erg = QUERY ( "select SQL_CACHE $fields from featureInstances" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $featureInstances [$obj->id] = $obj;
  }
  return $featureInstances;
}

function readGroupStates()
{
  $erg = QUERY ( "select SQL_CACHE * from groupStates order by id" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $groupStates [$obj->id] = $obj;
  }
  return $groupStates;
}

function generateGroupState($groupId, $name, $basics, $value=-1)
{
	if ($value==-1)
	{
	  $erg = QUERY("select max(value) from groupStates where groupId='$groupId'");
	  $row=mysqli_fetch_row($erg);
	  $nextValue = $row[0]+1;
	}
	else $nextValue=$value;
	
	QUERY("insert into groupStates (groupId, name, value, basics, generated) values('$groupId','$name','$nextValue','$basics','1')");
	$id = query_insert_id();
	return $id;
}

function getOrGenerateGroupDefaultState($groupId, $value, $name)
{
	$erg = QUERY("select id from groupStates where groupId='$groupId' and value='$value' limit 1");
	if ($row=mysqli_fetch_row($erg)) return $row[0];
	
	QUERY("INSERT into groupStates(groupId, value, name) value('$groupId', '$value', '$name')");
	$id = QUERY_insert_id();
	return $id;
}

function readFeatureFunctions()
{
  $erg = QUERY ( "select SQL_CACHE * from featureFunctions order by type,functionId" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $featureFunctions [$obj->id] = $obj;
  }
  return $featureFunctions;
}

function readFeatureFunctionParams()
{
  $erg = QUERY ( "select SQL_CACHE * from featureFunctionParams order by id" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $featureFunctionParams [$obj->id] = $obj;
  }
  return $featureFunctionParams;
}

function readFeatureFunctionEnums()
{
  $erg = QUERY ( "select SQL_CACHE * from featureFunctionEnums order by id" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $featureFunctionEnums [$obj->id] = $obj;
  }
  return $featureFunctionEnums;
}

function readFeatureFunctionBitmasks()
{
  $erg = QUERY ( "select SQL_CACHE * from featureFunctionBitmasks order by id" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $featureFunctionBitmasks [$obj->id] = $obj;
  }
  return $featureFunctionBitmasks;
}

function getFunctionIdByName($objectId, $featureFunctionName)
{
  $classId = getFeatureClassesId ( $objectId );
  $erg = QUERY ( "select functionId from featureFunctions where name='$featureFunctionName' and featureClassesId='$classId' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
    return $row [0];
  return "";
}

function getFunctionIdByNameForClassName($className, $featureFunctionName)
{
	$classesId = getClassesIdByName($className);
  $erg = QUERY ( "select functionId from featureFunctions where name='$featureFunctionName' and featureClassesId='$classesId' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) return $row [0];
  return "";
}

function getFunctionsIdByNameForClassName($className, $featureFunctionName)
{
	$classesId = getClassesIdByName($className);
  $erg = QUERY ( "select id from featureFunctions where name='$featureFunctionName' and featureClassesId='$classesId' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
    return $row [0];
  return "";
}

function readFeatureFunctionBitmaskNames($featureFunctionId, $paramId)
{
  $i = 0;
  $erg = QUERY ( "select name from featureFunctionBitmasks where featureFunctionId='$featureFunctionId' and paramId='$paramId' order by id limit 8" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $featureFunctionBitmaskNames [$i ++] = $obj->name;
  }
  return $featureFunctionBitmaskNames;
}

function getSelect($value, $possibleValues, $possibleNames = "")
{
  $result = "";
  if ($possibleNames == "") $possibleNames = $possibleValues;
  
  $parts = explode ( ",", $possibleValues );
  $partsNames = explode ( ",", $possibleNames );
  
  $i = 0;
  foreach ( $parts as $act )
  {
    $result .= getSelectItem ( $act, $partsNames [$i ++], $value );
  }
  
  return $result;
}

function getSelectItem($act, $name, $value)
{
	//echo $act." / ".$name." / ".$value."<br>";
  if ($act == $value) $selected = "selected";
  else $selected = "";
  return "<option $selected value='$act'>$name";
}

/// RECURSIVES L�EN
function checkDatabaseIntegrity()
{
	require_once($_SERVER["DOCUMENT_ROOT"]."/homeserver/include/dataBaseIntegrity.php");
	/*echo "checkDatabaseIntegrity <br>";
	print_r(debug_backtrace());
	echo "<hr>";
	*/
	checkReferenceIntegrity(0);
	
	exec("rm ".$_SERVER["DOCUMENT_ROOT"]."/homeserver/suspend.*");
}

function deleteController($controllerId, $autoClean=1)
{
  QUERY ("DELETE from controller where id='$controllerId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteFeatureClass($featureClassesId, $autoClean=1)
{
  QUERY ( "DELETE from featureClasses where id='$featureClassesId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteGroup($groupId, $autoClean=1)
{
  QUERY ( "DELETE from groups where id='$groupId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}
 
function deleteFeatureFunctionEnum($featureFunctionEnumId, $autoClean=1)
{
  QUERY ( "DELETE from featureFunctionEnums where id='$featureFunctionEnumId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteFeatureFunctionParam($featureFunctionParamsId, $autoClean=1)
{
  QUERY ( "DELETE from featureFunctionParams where id='$featureFunctionParamsId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteFeatureFunction($featureFunctionId, $autoClean=1)
{
  QUERY ( "DELETE from featureFunctions where id='$featureFunctionId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteFeatureInstance($featureInstanceId, $autoClean=1)
{
  QUERY ( "DELETE from featureInstances where id='$featureInstanceId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteGroupFeature($groupFeatureId, $autoClean=1)
{
  QUERY ( "DELETE from groupFeatures where id='$groupFeatureId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteGroupState($groupStateId, $autoClean=1)
{
  QUERY ( "DELETE from groupStates where id='$groupStateId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteRoomFeature($roomFeatureId, $autoClean=1)
{
  QUERY ( "DELETE from roomFeatures where id='$roomFeatureId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteRoom($roomId, $autoClean=1)
{
  QUERY ( "DELETE from rooms where id='$roomId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteBasicRuleSignalParam($ruleSignalParamId, $autoClean=1)
{
  QUERY ( "DELETE from basicRuleSignalParams where id='$ruleSignalParamId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteRuleAction($ruleActionId, $autoClean=1)
{
  QUERY ( "DELETE from ruleActions where id='$ruleActionId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteRuleActionParam($ruleActionParamId, $autoClean=1)
{
  QUERY ( "DELETE from ruleActionParams where id='$ruleActionParamId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteRule($ruleId, $autoClean=1)
{
  QUERY ( "DELETE from rules where id='$ruleId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteBaseRule($ruleId, $autoClean=1)
{
  QUERY ( "DELETE from basicRules where id='$ruleId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteRuleSignal($ruleSignalId, $autoClean=1)
{
  QUERY ( "DELETE from ruleSignals where id='$ruleSignalId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteRuleSignalParam($signalParamId, $autoClean=1)
{
  QUERY ( "DELETE from ruleSignalParams where id='$signalParamId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function deleteBaseRuleSignal($ruleSignalId, $autoClean=1)
{
  QUERY ( "DELETE from basicRuleSignals where id='$ruleSignalId' limit 1" );
  if ($autoClean==1) checkDatabaseIntegrity();
}

function switchLanguage($language)
{
  $_SESSION ["actLanguage"] = $language;
  
  require_once $_SERVER ["DOCUMENT_ROOT"] . '/homeserver/include/dbconnect.php';
  
  $erg = QUERY ( "select theKey,translation from languages where language = '$language'" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $_SESSION ["actLanguageSet"] [strtolower ( $obj->theKey )] = $obj->translation;
  }
}

function i18n($key)
{
  // if ($_SESSION["actLanguageSet"][strtolower($key)]!="") return $_SESSION["actLanguageSet"][strtolower($key)];
  return $key;
}

function parseWeekTime($value)
{
  $obj = new stdClass ();
  $obj->minute = $value & 0xff;
  $value = $value >> 8;
  $obj->hour = $value & 0x1F;
  $obj->day = $value >> 5;
  return $obj;
}
 
function toWeekTime($day, $hour, $minute)
{
  $value = $day << 5;
  $value += $hour;
  $value = $value << 8;
  $value += $minute;
  return $value;
}

function changesSince($lastStatusId)
{
  $actId = updateLastLogId ();
  if ($actId > $lastStatusId)
    return true;
  return false;
}

function waitForIdle()
{
  $rememberedId = updateLastLogId ();
  
  while ( 1 )
  {
    sleepMS ( 500 );
    $next = updateLastLogId ();
    if ($next == $rememberedId) return;
    $rememberedId = $next;
  }
}

function flushIt()
{
  for($i = 0; $i < 40; $i ++)
  {
    echo "          
    		                                                                              ";
    ob_end_flush ();
    ob_flush ();
    flush ();
  }
}

function updateLastLogId()
{
  global $lastLogId;
  
  $erg = QUERY ( "select max(id) from udpCommandLog" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) $lastLogId = $row [0];
  if ($lastLogId < 1) $lastLogId = 0;
  return $lastLogId;
}

function sleepMS($ms)
{
  usleep ( $ms * 1000 );
}

function trace($text, $output=0)
{
  global $PHP_SELF;
  
  $text = query_real_escape_string ( $text );
  $time = time ();
  $script = $PHP_SELF;
  QUERY ( "INSERT into trace (time,message,script) values('$time', '$text','$script')" );
  
  if ($output==1) echo $text."\n";
}

function getBitMask($name, $value, $names)
{
  global $bitScriptDone;
  
  $result = '<table cellspacing="0" cellpadding="0" border="0" style="display:inline-block">
 <tr>
 <td><a href="#" onclick="toggle(\'' . $name . '\',7);return false;"><img src="img/bitOff.gif" border="0" id="' . $name . 'img7" title="' . $names [7] . '"></a></td>
 <td><a href="#" onclick="toggle(\'' . $name . '\',6);return false;"><img src="img/bitOff.gif" border="0" id="' . $name . 'img6" title="' . $names [6] . '"></a></td>
 <td><a href="#" onclick="toggle(\'' . $name . '\',5);return false;"><img src="img/bitOff.gif" border="0" id="' . $name . 'img5" title="' . $names [5] . '"></a></td>
 <td><a href="#" onclick="toggle(\'' . $name . '\',4);return false;"><img src="img/bitOff.gif" border="0" id="' . $name . 'img4" title="' . $names [4] . '"></a></td>
 <td><a href="#" onclick="toggle(\'' . $name . '\',3);return false;"><img src="img/bitOff.gif" border="0" id="' . $name . 'img3" title="' . $names [3] . '"></a></td>
 <td><a href="#" onclick="toggle(\'' . $name . '\',2);return false;"><img src="img/bitOff.gif" border="0" id="' . $name . 'img2" title="' . $names [2] . '"></a></td>
 <td><a href="#" onclick="toggle(\'' . $name . '\',1);return false;"><img src="img/bitOff.gif" border="0" id="' . $name . 'img1" title="' . $names [1] . '"></a></td>
 <td><a href="#" onclick="toggle(\'' . $name . '\',0);return false;"><img src="img/bitOff.gif" border="0" id="' . $name . 'img0" title="' . $names [0] . '"></a></td>
 <td> &nbsp;&nbsp;<input type=text size=2 name="' . $name . '" id="' . $name . '" value="' . $value . '"  maxlength=3 onkeyup="updateBits(\'' . $name . '\')"></td>
</tr>
</table>

<script>
  function toggle(name, id)
  {
     if ((document.getElementById(name).value&Math.pow(2,id))==Math.pow(2,id))
    	 document.getElementById(name).value=Number(document.getElementById(name).value)-Math.pow(2,id);
     else
    	 document.getElementById(name).value=Number(document.getElementById(name).value)+Math.pow(2,id);
     updateBits(name);
  }

  function updateBits(name)
  {
	 for(i=0;i<8;i++)
	 {
		 if ((document.getElementById(name).value&Math.pow(2,i))==Math.pow(2,i))
			 document.getElementById(name+"img"+i).src="img/bitOn.gif";
		 else
			 document.getElementById(name+"img"+i).src="img/bitOff.gif";
	 }
  }

updateBits("' . $name . '");
</script>';
  
  return $result;
}

function getWeekTime($name, $value)
{
  $times = parseWeekTime ( $value );
  
  $type = "Wochentag: <select name='" . $name . "Day'>";
  $type .= getSelect ( $times->day, "7,0,1,2,3,4,5,6", "Immer,Mo.,Di.,Mi.,Do.,Fr.,Sa.,So." );
  $type .= "</select> ";
  
  $options = "";
  for($hour = 0; $hour < 24; $hour ++)
  {
    $myHour = $hour;
    if (strlen ( $myHour ) == 1) $myHour = "0" . $myHour;
    if ($times->hour == $hour) $selected = "selected";
    else $selected = "";
    $options .= "<option $selected value='$hour'>$myHour";
  }
  
  if ($times->hour == 31) $selected = "selected";
  else $selected = "";
  $type .= " &nbsp;&nbsp; Stunde: <select name='" . $name . "Hour'><option $selected value='31'>Immer$options</select> ";
  
  $options = "";
  for($minute = 0; $minute < 60; $minute ++)
  {
    $myMinute = $minute;
    if (strlen ( $myMinute ) == 1) $myMinute = "0" . $myMinute;
    if ($times->minute == $minute) $selected = "selected";
    else $selected = "";
    $options .= "<option $selected value='$minute'>$myMinute";
  }
  
  if ($times->minute == 255) $selected = "selected";
  else $selected = "";
  $type .= " &nbsp;&nbsp; Minute: <select name='" . $name . "Minute'><option $selected value='255'>Immer$options</select> ";
  return $type;
}

function twoDigits($number)
{
  if (strlen ( $number ) == 1) $number = "0" . $number;
  return $number;
}

function generateAndCheckRules()
{
  ob_end_flush ();
  ob_start ();
  
  // echo "Regeln werden generiert und geprüft.<br><br>";
  global $dimmerClassesId, $rolloClassesId, $ledClassesId, $schalterClassesId, $irClassesId, $tasterClassesId, $logicalButtonClassesId, $ethernetClassesId, $pcServerClassesId;
  global $startFunctionId, $stopFunctionId, $moveToPositionFunctionId, $paramToOpen, $paramToClose, $paramToToggle, $paramPosition;
  global $functionTemplates;
  global $ledStatusBrightness;
  global $ledLogicalButtonBrightness;
  global $serverInstances;
  global $raspberryFeatureInstanceId;
  
  $raspberryFeatureInstanceId = getFirstSoftwareController()->featureInstanceId;
  
  $scriptStart = microtime ( TRUE );
  
  $dimmerClassesId = getClassesIdByName ( "Dimmer" );
  $rolloClassesId = getClassesIdByName ( "Rollladen" );
  $ledClassesId = getClassesIdByName ( "Led" );
  $schalterClassesId = getClassesIdByName ( "Schalter" );
  $irClassesId = getClassesIdByName ( "IR-Sensor" );
  $tasterClassesId = getClassesIdByName ( "Taster" );
  $ethernetClassesId = getClassesIdByName ( "Ethernet" );
  $logicalButtonClassesId = getClassesIdByName ( "LogicalButton" );
  $pcServerClassesId = getClassesIdByName ( "PC-Server" );
  
  $startFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "start" );
  $stopFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "stop" );
  $moveToPositionFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "moveToPosition" );
  $paramToOpen = getFunctionParamEnumValueForClassesIdByName ( $rolloClassesId, "start", "direction", "TO_OPEN" );
  $paramToClose = getFunctionParamEnumValueForClassesIdByName ( $rolloClassesId, "start", "direction", "TO_CLOSE" );
  $paramToToggle = getFunctionParamEnumValueForClassesIdByName ( $rolloClassesId, "start", "direction", "TOGGLE" );
  $paramPosition = getClassesIdFunctionParamIdByName ( $rolloClassesId, "moveToPosition", "position" );
  
  $start = microtime ( TRUE );
  
  $functionTemplates = array();
  $erg = QUERY ( "select `signal`,classesId,function,name from functionTemplates" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $functionTemplates [$obj->classesId . "-" . $obj->function . "-" . $obj->name] = $obj->signal;
  }

  $ledStatusBrightness = 100;
  $erg = QUERY ( "select paramValue from basicConfig where paramKey = 'ledStatusBrightness' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) $ledStatusBrightness = $row [0];
  
  $ledLogicalButtonBrightness = 50;
  $erg = QUERY ( "select paramValue from basicConfig where paramKey = 'ledLogicalBrightness' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) $ledLogicalButtonBrightness = $row [0];
    
    // Alte generierten Sachen löschen
  QUERY ( "DELETE from groupSyncHelper" );
  QUERY ( "DELETE from systemVariableHelper" );
  QUERY ( "DELETE from groupfeatures where `generated`='1'" );
  QUERY ( "DELETE from groups where `generated`='1'" );
  QUERY ( "DELETE from groupstates where `generated`='1'" );
  QUERY ( "DELETE from ruleactionparams where `generated`='1'" );
  QUERY ( "DELETE from ruleactions where `generated`='1'" );
  QUERY ( "DELETE from rules where `generated`='1'" );
  QUERY ( "DELETE from rulesignalparams where `generated`='1'" );
  QUERY ( "DELETE from rulesignals where `generated`='1'" );
  QUERY ( "DELETE from basicrulegroupsignals where `generated`='1'" );
  QUERY ( "update basicrulegroupsignals set controllerInstanceId='0', groupIndex='0'");
  
  checkRemoveUnusedHeatingRules();
  
  // Referenzielle Integrität 
  checkDatabaseIntegrity();
  liveOut ( "- Aufräumen " . round ( microtime ( TRUE ) - $start, 2 ) . " Sekunden" );
  
  // BaseRules erzeugen
  $start = microtime ( TRUE );
  $erg = QUERY ( "select distinct(groupId) from basicRules order by groupId" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    $groupId = $row [0];
    generateBaseRulesForGroup ( $groupId );
  }
  liveOut ( "- Basisregeln generieren " . round ( microtime ( TRUE ) - $start, 2 ) . " Sekunden" );

  generateHeatingSignalGroups();

  // Synchronisationsevents für erstellte Gruppen
  $start = microtime ( TRUE );
  $erg = QUERY ( "SELECT groups.id, COUNT( groupFeatures.featureInstanceId ) AS myCount FROM groups JOIN groupFeatures ON ( groupFeatures.groupId = groups.id ) WHERE single =0 AND groups.generated =0 and groups.groupType='' GROUP BY groups.id" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    if ($row [1] > 1)
    {
      $erg2 = QUERY ( "select max(ledStatus) from basicRules where groupId='$row[0]'" );
      $row2 = MYSQLi_FETCH_ROW ( $erg2 );
      if ($row2 [0] > 1)
      {
        $manualGroup [$row [0]] = 1;
        
        if ($row2 [0] == 3) $completeGroupFeedback = 1;
        else if ($row2 [0] == 2) $completeGroupFeedback = 2;
        else $completeGroupFeedback = 0;
        
        generateSyncEvents ( $row [0], $completeGroupFeedback );
      }
    }
  }

  liveOut ( "- Synchronisationsevents für Gruppen erzeugen " . round ( microtime ( TRUE ) - $start, 2 ) . " Sekunden" );
  
  // Multigruppen erzeugen
  $start = microtime ( TRUE );
  generateMultiGroups ();
  liveOut ( "- Multigruppen erzeugen " . round ( microtime ( TRUE ) - $start, 2 ) . " Sekunden" );

  $erg = QUERY ( "select id from controller where size='999' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
  {
    $erg = QUERY ( "select id from featureInstances where controllerId='$row[0]'" );
    while ( $row = MYSQLi_FETCH_ROW ($erg)) $serverInstances [$row [0]] = 1;
  }
  
  // LED Feedback erzeugen
  $start = microtime ( TRUE );
  $erg = QUERY ( "select distinct(groupId) from basicRules order by groupId" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    generateLedFeedbackForGroup ( $row [0], $manualGroup [$row [0]] );
  }

  liveOut ( "- LED Feedback erzeugen " . round ( microtime ( TRUE ) - $start, 2 ) . " Sekunden" );
  
  // Signalgruppen generieren
  $start = microtime ( TRUE );
  $erg = QUERY ( "select id from groups where groupType!='' and (`generated`='0' or name like 'Generated Heating Signals%' or name like 'Generated Ventilation Signals%')" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    generateSignalGroup ( $row [0] );
  }

  liveOut ( "- Signalgruppen generieren " . round ( microtime ( TRUE ) - $start, 2 ) . " Sekunden" );
  
  // Pr�b es Gruppen mit nur DummySignalen gibt
  $start = microtime ( TRUE );
  removeDummyGroups ();
  liveOut ( "- Dummysignale löschen " . round ( microtime ( TRUE ) - $start, 2 ) . " Sekunden" );
  // liveOut("- Dummysignale l�en deaktiviert");
  
  // Pr�b es in Gruppen Signale gibt, die mit StartState ALLE und auch AN oder AUS vorkommen und dann ersetzen
  $start = microtime ( TRUE );
  mixStateAllSignals ();
  liveOut ( "- Regeln optimieren " . round ( microtime ( TRUE ) - $start, 2 ) . " Sekunden" );
  
  // Subgruppen in die Vatergruppe mischen
  $start = microtime ( TRUE );
  mixSubGroups ();
  liveOut ( "- Subgruppen mischen " . round ( microtime ( TRUE ) - $start, 2 ) . " Sekunden" );
  
  // Regeln pr�  $start = microtime ( TRUE );
  checkRuleConsistency ();
  
  // Controllerleichen ohne features l�en
  $erg = QUERY ( "select controller.id from controller left join featureInstances on (featureInstances.controllerId=controller.id) where featureInstances.id is null and bootloader=0 and online=0 limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) QUERY ( "delete from controller where id='$row[0]' limit 1" );

  // Referenzielle Integrit㲠 
  checkDatabaseIntegrity();

  
  liveOut ( "- Abschlussprüfung " . round ( microtime ( TRUE ) - $start, 2 ) . " Sekunden" );
  liveOut ( "- Gesamtdauer " . round ( microtime ( TRUE ) - $scriptStart, 2 ) . " Sekunden" );
}

function checkRuleConsistency()
{
  // Pr�b Aktoren in mehreren Gruppen durch das gleiche Signal getriggert werden
  /*
   * $erg = QUERY("SELECT group_concat(distinct rulesignalparams.featureFunctionParamsId order by rulesignalparams.featureFunctionParamsId) as con1, group_concat(distinct rulesignalparams.paramValue order by rulesignalparams.paramValue) as con2, rules.groupid as groupId, rulesb.groupid as groupbId, rulesignals.featureinstanceid as mySignal,signalsb.featureinstanceid,ruleactions.featureinstanceid,actionsb.featureinstanceid as myAction,rulesignals.featurefunctionid as myFunction,signalsb.featurefunctionid,rulesignalparams.featureFunctionParamsId,signalparamsb.featureFunctionParamsId,rulesignalparams.paramValue,signalparamsb.paramvalue,ruleactions.featurefunctionid,actionsb.featurefunctionid FROM (rules join groups on (groups.id=rules.groupId) JOIN rulesignals ON ( rulesignals.ruleid = rules.id ) JOIN ruleactions ON ( ruleactions.ruleid = rules.id ) left join rulesignalparams on (rulesignalparams.rulesignalid=rulesignals.id) ) JOIN ( rules AS rulesb join groups as groupsb on (groupsb.id=rulesb.groupId) JOIN rulesignals AS signalsb ON ( signalsb.ruleid = rulesb.id ) JOIN ruleactions AS actionsb ON ( actionsb.ruleid = rulesb.id ) left join rulesignalparams as signalparamsb on (signalparamsb.rulesignalid=signalsb.id) ) WHERE rulesignals.featureinstanceid = signalsb.featureinstanceid AND ruleactions.featureinstanceid = actionsb.featureinstanceid AND rules.id != rulesb.id AND rules.activationstateid =0 AND rulesb.activationstateid =0 AND rulesignals.featurefunctionid = signalsb.featurefunctionid and rulesignals.groupalias=0 and signalsb.groupalias=0 and ruleactions.featureFunctionId!=169 and groups.subOf=0 and groupsb.subOf=0 group by rulesignals.id having (rulesignalparams.featureFunctionParamsId is null or group_concat(distinct rulesignalparams.featureFunctionParamsId order by rulesignalparams.featureFunctionParamsId)=group_concat(distinct signalparamsb.featureFunctionParamsId order by signalparamsb.featureFunctionParamsId)) and (rulesignalparams.paramvalue is null or group_concat(distinct rulesignalparams.paramvalue)=group_concat(distinct signalparamsb.paramvalue))"); while ( $obj = MYSQLi_FETCH_object($erg) ) { if ($obj->mySignal < 0) continue; $key = $obj->myAction . "-" . $obj->mySignal . "-" . $obj->myFunction . "-" . $obj->con1 . "-" . $obj->con2; if ($dones[$key] == 1) continue; $dones[$key] = 1; echo "Warnung: Doppelansteuerung von Aktor " . formatInstance($obj->myAction) . " in <a href='editRules.php?groupId=$obj->groupId' target='_blank'>dieser Gruppe</a> [<a href='editBaseConfig.php?groupId=$obj->groupId' target='_blank'>Basisregeln</a>] sowie in <a href='editRules.php?groupId=$obj->groupbId' target='_blank'>dieser Gruppe</a> [<a href='editBaseConfig.php?groupId=$obj->groupbId' target='_blank'>Basisregeln</a>]<br>"; }
   */
  
  // Pr�b in einer Gruppe vom gleichen Signar sowohl covered als auch DoubleClicked enthalten sind
  $lastGroupId = - 1;
  $foundCovered = "";
  $foundDoubleClick = "";
  
  $tasterClassesId = getClassesIdByName ( "Taster" );
  $evCoveredFunctionId = getClassesIdFunctionsIdByName ( $tasterClassesId, "evCovered" );
  $evClickedFunctionId = getClassesIdFunctionsIdByName ( $tasterClassesId, "evClicked" );
  $evDoubleClickFunctionId = getClassesIdFunctionsIdByName ( $tasterClassesId, "evDoubleClick" );
  $debug = 0;
  
  $erg = QUERY ( "select rules.id,rules.groupId,ruleSignals.featureFunctionId,ruleSignals.featureInstanceId from rules join ruleSignals on (ruleSignals.ruleId=rules.id) join groups on (groups.id = rules.groupId) join groupFeatures on (groupFeatures.groupId = rules.groupId) where groupAlias='0' order by groupId" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    if ($obj->groupId != $lastGroupId && $lastGroupId != - 1)
    {
      if ($debug == 1)
        echo "<br>Gruppe: " . $obj->groupId . "<br>";
      foreach ( ( array ) $foundCovered as $coveredInstance => $dummy )
      {
        foreach ( ( array ) $foundDoubleClick as $doubleClickInstance => $dummy )
        {
          if ($coveredInstance == $doubleClickInstance)
          {
            $erg2 = QUERY ( "select ruleSignals.id from rules join ruleSignals on (ruleSignals.ruleId=rules.id) where groupId='$lastGroupId' and featureInstanceId='$doubleClickInstance' and featureFunctionId='$evCoveredFunctionId'" );
            while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
            {
              // echo $obj2->id." -- ".$lastGroupId." - ".$coveredInstance."<br>";
              QUERY ( "UPDATE ruleSignals set featureFunctionId='$evClickedFunctionId' where id='$obj2->id' limit 1" );
            }
          }
        }
      }
      unset ( $foundCovered );
      unset ( $foundDoubleClick );
    }
    
    $lastGroupId = $obj->groupId;
    
    if ($obj->featureFunctionId == $evCoveredFunctionId)
      $foundCovered [$obj->featureInstanceId] = 1;
    if ($obj->featureFunctionId == $evDoubleClickFunctionId)
      $foundDoubleClick [$obj->featureInstanceId] = 1;
  }
}

// Pr�b es in Gruppen Signale gibt, die mit StartState ALLE und auch AN oder AUS vorkommen und dann ersetzen
function mixStateAllSignals()
{
  $erg = QUERY ( "select id from groups where single!=1 and groupType=''");
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    $groupId = $row [0];
    
    $erg2 = QUERY ( "select featureInstanceId,featureFunctionId,rules.id,groupAlias,ruleSignals.id as ruleSignalId from ruleSignals join rules on (rules.id = ruleSignals.ruleId) left join ruleSignalParams on (ruleSignalParams.ruleSignalId = ruleSignals.id) where activationStateId='0' and groupId='$groupId' and ruleSignals.featureFunctionId!='129'");
    while ( $row2 = MYSQLi_FETCH_ROW ( $erg2 ) )
    {
      $firstInstance = $row2 [0];
      $firstFunction = $row2 [1];
      $firstRuleId = $row2 [2];
      $firstGroupAlias = $row2 [3];
      $firstRuleSignalId = $row2 [4];
      
      $firstInsert = - 1;
      $secondInsert = - 1;

      $erg3 = QUERY ( "select rules.id from ruleSignals join rules on (rules.id = ruleSignals.ruleId) where activationStateId!='0' and groupId='$groupId' and ruleSignals.featureInstanceId='$firstInstance' and ruleSignals.featureFunctionId='$firstFunction' and ruleSignals.featureFunctionId!='129' limit 2");
      while ( $row3 = MYSQLi_FETCH_ROW ( $erg3 ) )
      {
        if ($firstInsert == - 1) $firstInsert = $row3 [0];
        else $secondInsert = $row3 [0];
      }
      
      if ($firstInsert != - 1 && $secondInsert != - 1)
      {
        // Signale löschen
        QUERY ( "DELETE from ruleSignals where id='$firstRuleSignalId' limit 1" );
        
        // Actions verschieben
        $erg3 = QUERY ( "select * from ruleActions where ruleId='$firstRuleId'" );
        while ( $obj = MYSQLi_FETCH_OBJECT ( $erg3 ) )
        {
          QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$secondInsert','$obj->featureInstanceId','$obj->featureFunctionId','1')" );
          $newActionId = query_insert_id ();
          
          $erg4 = QUERY ( "select * from ruleActionParams where ruleActionId='$obj->id' order by id" );
          while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg4 ) )
          {
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`)
              	                         values('$newActionId','$obj2->featureFunctionParamsId','$obj2->paramValue','1')" );
          }
        }
        QUERY ( "UPDATE ruleActions set ruleId='$firstInsert',`generated`='1' where ruleId='$firstRuleId'" );
        
        $erg3 = QUERY ( "select count(*) from ruleSignals where ruleId='$firstRuleId'" );
        $row3 = MYSQLi_FETCH_ROW ( $erg3 );
        if ($row3 [0] == 0) deleteRule ( $firstRuleId, 0 );
      }
    }
  }
}

// Subgruppen in die Vatergruppe mischen
function mixSubGroups()
{
  // Alle Subgroupen durchgehen, die einen Vater haben
  $erg = QUERY ( "select id,subOf from groups where subOf>0" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    $groupId = $row [0];
    $newGroupId = $row [1]; // Deren Regeln mischen wir beim Vater rein
    
    $ergA = QUERY ( "select ruleSignals.id from ruleSignals join rules on (ruleSignals.ruleId=rules.id) where groupId='$groupId'" );
    while ( $rowA = MYSQLi_FETCH_ROW ( $ergA ) )
    {
      $signalId = $rowA [0];
      
      $erg2 = QUERY ( "select rules.*, activation.basics as startBasics, resulting.basics as resultingBasics from rules join ruleSignals on (ruleSignals.ruleId=rules.id) left join groupStates as activation on (activation.id = rules.activationStateId) left join groupStates as resulting on (resulting.id = rules.resultingStateId) where ruleSignals.id='$signalId' limit 1" );
      if ($obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ))
      {
        $myActivationStateId = "0";
        $myResultingStateId = "0";
        $erg4 = QUERY ( "select id,basics from groupStates where basics>0 and groupId='$newGroupId'" );
        while ( $row4 = MYSQLi_FETCH_ROW ( $erg4 ) )
        {
          if ($obj2->startBasics == $row4 [1])
            $myActivationStateId = $row4 [0];
          if ($obj2->resultingBasics == $row4 [1])
            $myResultingStateId = $row4 [0];
        }
        
        // if ($myActivationStateId=="0" || $myResultingStateId=="0") echo "Fehler in Subgruppe. States nicht supported <br>";
        
        $erg3 = QUERY ( "select * from ruleSignals where id='$signalId' limit 1" );
        if ($obj3 = MYSQLi_FETCH_OBJECT ( $erg3 ))
        {
          $signalId = $obj3->id;
          $signalFeatureInstanceId = $obj3->featureInstanceId;
          $signalFeatureFunctionId = $obj3->featureFunctionId;
          
          // Beim Taster aus evCovered evClicked machen, wenn verschiedene Classes angesteuert werden
          if ($convertCoveredEvent == 1 && $signalFeatureFunctionId == 43)
            $signalFeatureFunctionId = 2;
        } else
          die ( "Signal ID $signalId nicht gefunden" );
          
          // Pr�b es die passende Regel schon gibt
          // echo "select rules.id from rules join ruleSignals on (ruleSignals.ruleId=rules.id) where activationStateId='$myActivationStateId' and groupId='$newGroupId' and featureInstanceId='$signalFeatureInstanceId' and featureFunctionId='$signalFeatureFunctionId' limit 1 <br>";
        $erg3 = QUERY ( "select rules.id from rules join ruleSignals on (ruleSignals.ruleId=rules.id) where activationStateId='$myActivationStateId' and groupId='$newGroupId' and featureInstanceId='$signalFeatureInstanceId' and featureFunctionId='$signalFeatureFunctionId' limit 1" );
        if ($row3 = MYSQLi_FETCH_ROW ( $erg3 ))
        {
          $newRuleId = $row3 [0];
          // echo "gefunden $newRuleId <br>";
        } else
        {
          QUERY ( "INSERT into rules (groupId,activationStateId,resultingStateId,startDay,startHour,startMinute,endDay,endHour,endMinute,signalType,baseRule,`generated`,intraDay)
        	                     values('$newGroupId','$myActivationStateId','$myResultingStateId','$obj2->startDay','$obj2->startHour','$obj2->startMinute','$obj2->endDay','$obj2->endHour','$obj2->endMinute','$obj2->signalType','$obj2->baseRule','1','$obj2->intraDay')" );
          $newRuleId = query_insert_id ();
          
          // echo "neu $newRuleId <br>";
          
          // Signale eintragen
          QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$newRuleId','$signalFeatureInstanceId','$signalFeatureFunctionId','1')" );
          $newSignalId = query_insert_id ();
		  
          $erg4 = QUERY ( "select * from ruleSignalParams where ruleSignalId='$signalId' order by id" );
          while ( $obj4 = MYSQLi_FETCH_OBJECT ( $erg4 ) )
          {
            QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$newSignalId','$obj4->featureFunctionParamsId','$obj4->paramValue','1')" );
          }
        }
        
        // Actions eintragen, wenns sie nicht schon gibt
        $erg3 = QUERY ( "select * from ruleActions where ruleId='$obj2->id' order by id" );
        while ( $obj3 = MYSQLi_FETCH_OBJECT ( $erg3 ) )
        {
          // if ($obj2->offRule==1) $myOffInstances[$obj3->featureInstanceId]=1;
          $erg4 = QUERY ( "select id from ruleActions where ruleId='$newRuleId' and featureInstanceId='$obj3->featureInstanceId' and featureFunctionId='$obj3->featureFunctionId' limit 1" );
          if ($row4 = MYSQLi_FETCH_ROW ( $erg4 ))
          {
          } else
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$newRuleId','$obj3->featureInstanceId','$obj3->featureFunctionId','1')" );
            $newSignalId = query_insert_id ();
            
            $erg4 = QUERY ( "select * from ruleActionParams where ruleActionId='$obj3->id' order by id" );
            while ( $obj4 = MYSQLi_FETCH_OBJECT ( $erg4 ) )
            {
              QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newSignalId','$obj4->featureFunctionParamsId','$obj4->paramValue','1')" );
            }
          }
        }
      }
    }
  }
}

// Pr�b es Gruppen mit nur DummySignalen gibt
function removeDummyGroups()
{
  // 46 19 dimmer evOn,evOff
  // 106 107 led evOn, evOff
  // 56,175 rollo evClosed, evOpen
  // 62, 63 schalter evOn, evOff
  $lastGroupId = - 1;
  $lastGroupEmpty = 1;
  $deleteRules = "";
  $ledClassesId = getClassesIdByName ( "Led" );
  $debug = 0;
  
  $erg = QUERY ( "select activationStateId, resultingStateId, rules.id, rules.groupId,ruleSignals.featureFunctionId,groupFeatures.featureInstanceId,ruleactions.featureInstanceId as actionInstanceId from rules join ruleSignals on (ruleSignals.ruleId=rules.id) join groups on (groups.id = rules.groupId) join groupFeatures on (groupFeatures.groupId = rules.groupId) left join ruleactions on(ruleactions.ruleId=rules.id) where groupAlias='0' and single=1 and groupType='' order by groupId" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    if (getClassesIdByFeatureInstanceId ( $obj->featureInstanceId ) == $ledClassesId) continue;
    
    if ($obj->groupId != $lastGroupId)
    {
      if ($debug == 1) echo "<br>".time().": Gruppe: " . $obj->groupId . "<br>";
      if ($lastGroupId != - 1)
      {
        if ($lastGroupEmpty == 1)
        {
          // echo $lastGroupId."<br>";
          // Gruppe: 12340
          // 435: 556829,556830
          // 12340: 555950
          
          if ($deleteRules != "")
          {
            if ($debug == 1) echo $lastGroupId . ": " . $deleteRules . "<br>";
            $ids = explode ( ",", $deleteRules );
            foreach ( $ids as $actId )
            {
              deleteRule ( $actId ,0 );
            }
          }
        }
        $lastGroupEmpty = 1;
        $deleteRules = "";
      }
    }
    
    $lastGroupId = $obj->groupId;
    
    if ($obj->actionInstanceId !=0) // && $obj->activationStateId!=0)
    {
      if ($debug == 1) echo "A, ";
      $lastGroupEmpty = 0;
    } 
    else if ($obj->actionInstanceId == null || $obj->featureFunctionId == 46 || $obj->featureFunctionId == 19 || $obj->featureFunctionId == 106 || $obj->featureFunctionId == 107 || $obj->featureFunctionId == 56 || $obj->featureFunctionId == 175 || $obj->featureFunctionId == 62 || $obj->featureFunctionId == 63)
    {
      if ($debug == 1) echo "D" . $obj->id . ", ";
      if ($deleteRules != "") $deleteRules .= ",";
      $deleteRules .= $obj->id;
    } 
    else
    {
      if ($debug == 1) echo "B, ";
    }
  }
}

function generateBaseRulesForGroup($groupId)
{
	global $testNewRolloRules;

  // echo "Generiere Basisregeln für Gruppe $groupId <br>";
  global $CONTROLLER_CLASSES_ID;
  global $signalParamWildcard,$signalParamWildcardWord;
  global $dimmerClassesId, $rolloClassesId, $ledClassesId, $schalterClassesId, $irClassesId, $tasterClassesId, $logicalButtonClassesId, $ethernetClassesId;
  global $startFunctionId, $stopFunctionId, $moveToPositionFunctionId, $paramToOpen, $paramToClose, $paramToToggle, $paramPosition;
  global $functionTemplates;
  global $ledLogicalButtonBrightness;
  global $configCache;
  global $raspberryFeatureInstanceId;
  
  unset ( $configCache );
  
  $myInstanceCount = 0;
  $doSkipSyncEventsForHeating=false;
  $doSkipSyncEventsForVentilation=false;
  
  unset ( $diffFeatureClasses );
  $erg = QUERY ( "select featureClassesId,featureInstanceId,objectId from groupFeatures join featureInstances on (featureInstances.id=featureInstanceId) where groupId='$groupId'" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    $myClassesId = $row [0];
    $diffFeatureClasses [$myClassesId] = 1;
    $myInstanceId = $row [1];
    $myInstances [$myInstanceCount] ["myClassesId"] = $myClassesId;
    $myInstances [$myInstanceCount] ["myInstanceId"] = $myInstanceId;
    $myObjectIds [$myInstanceId] = $row [2];
    $myInstanceCount ++;
  }
  
  // Wenn wir verschiedene Aktortypen in einer Gruppe haben, werden die Schalterfunktionen als kleinster gemeinsamer Nenner angeboten
  if (count ( $diffFeatureClasses ) > 1) $isMixedGroup = 1;

  // Bewegungsmeldersonderfunktion
  $erg = QUERY ( "select id from basicRules where groupId='$groupId' and extras='Heizungssteuerung' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) $hasHeizung = TRUE;
  else $hasHeizung = FALSE;

  if ($isMixedGroup!=1 && $myClassesId == $rolloClassesId && $testNewRolloRules==1 && !$hasHeizung)
  {
  	generateBaseRulesRollladen($groupId);
	return;
  }
  
  // echo "Generiere Basisregeln für Gruppe $groupId $myInstanceCount <br>";
 
  // Bewegungsmeldersonderfunktion
  $erg = QUERY ( "select id from basicRules where groupId='$groupId' and extras='Bewegungsmelder' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) $hasBewegung = TRUE;
  else $hasBewegung = FALSE;
    
    // Standardstates ggf. anlegen
  $erg = QUERY ( "select id from groupStates where groupId='$groupId' and basics='1' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) $firstState = $row [0];
  else
  {
    $basicStateNames = getBasicStateNames ( $myClassesId );
    $offName = $basicStateNames->offName;
    $onName = $basicStateNames->onName;
    QUERY ( "INSERT into groupStates (groupId, name,basics,`generated`) values('$groupId','$offName','1','1')" );
    $firstState = query_insert_id ();
  }
  
  $erg = QUERY ( "select id from groupStates where groupId='$groupId' and basics='2' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) $secondState = $row [0];
  else
  {
    QUERY ( "INSERT into groupStates (groupId, name,basics,`generated`) values('$groupId','$onName','2','1')" );
    $secondState = query_insert_id ();
  }
  
  if ($hasBewegung)
  {
    $erg = QUERY ( "select id from groupStates where groupId='$groupId' and basics='5' limit 1" );
    if ($row = MYSQLi_FETCH_ROW ( $erg )) $bewegungsState = $row [0];
    else
    {
      QUERY ( "INSERT into groupStates (groupId, name,basics,`generated`) values('$groupId','Bewegung','5','1')" );
      $bewegungsState = query_insert_id ();
    }
  }


  $foundStateChange = 0; // Wenn wir keine Statechanges haben, brauchen wir später auch keine Synchronisationsevents
  $erg = QUERY ( "select * from basicRules where groupId='$groupId' and active='1'" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $erg2 = QUERY ( "select count(*) from basicRuleSignals where ruleId='$obj->id'" );
    $row = MYSQLi_FETCH_ROW ( $erg2 );
    if ($row [0] == 0) continue;
    
    if ($obj->extras == "Bewegungsmelder") $isBewegungsMelder = TRUE;
    else $isBewegungsMelder = FALSE;

    if ($obj->extras == "Heizungssteuerung")
    {
    	$doSkipSyncEventsForHeating = generateHeizungsSteuerung($groupId, $obj, $firstState, $secondState);
    	continue;
    }
	
	if ($obj->extras == "Lüftungssteuerung")
    {
    	$doSkipSyncEventsForVentilation = generateLueftungsSteuerung($groupId, $obj, $firstState, $secondState, $obj->id);
    	continue;
    }

    // Wenn in einer Gruppe LED Feedback Gruppenstatus angewählt wurde, dann will man dass beim Event groupUndefined die Gruppe als aus gilt und sonst als an
    if ($obj->ledStatus == 3) $completeGroupFeedback = 1;
    else if ($obj->ledStatus == 2) $completeGroupFeedback = 2;
    else $completeGroupFeedback = 0;
    
    if ($obj->fkt1 == "true" && ($myClassesId == $dimmerClassesId || $myClassesId == $ledClassesId))
    {
      echo "Repariere true Fehler in Gruppe $groupId <br>";
      QUERY ( "update basicRules set fkt1='-' where id ='$obj->id' limit 1" );
      $obj->fkt1 = "-";
    }
    
    if (((int)$obj->fkt1)>0 && $myClassesId == $rolloClassesId)
    {
      echo "Repariere Wertefehler in Gruppe $groupId <br>";
      QUERY ( "update basicRules set fkt1='-' where id ='$obj->id' limit 1" );
      $obj->fkt1 = "-";
    }

    if ($obj->fkt3!='' && $obj->fkt3!='-' && $myClassesId == $schalterClassesId)
    {
      echo "Repariere Wertefehler in Gruppe $groupId <br>";
      QUERY ( "update basicRules set fkt3='-' where id ='$obj->id' limit 1" );
      $obj->fkt3 = "-";
    }
	
    // Templates zu default reparieren, die mittlerweile nicht mehr existieren, weil sie mal gelöscht wurden
    if ($obj->template!="" && !isset($functionTemplates [$myClassesId . "-1-" . $obj->template]) && !isset($functionTemplates ["-1-1-" . $obj->template]))
	{
        echo "Repariere nicht mehr vorhandenes Funktionstemplate ".$obj->template." in Gruppe $groupId <br>";
        QUERY ( "update basicRules set template='' where id ='$obj->id' limit 1" );
		$obj->template="";
	}

    
    $zweiTastenModus=false;
    if (((isFunctionActive ($obj->fkt1) && !isFunctionActive ($obj->fkt2)) || (isFunctionActive ($obj->fkt2) && !isFunctionActive ($obj->fkt1)))
      && ($functionTemplates [$myClassesId . "-1-" . $obj->template] == $functionTemplates [$myClassesId . "-2-" . $obj->template])) 
    {
      $zweiTastenModus=true;
    }  
    
    // Standardstates ggf. umbenennen
    if ($myClassesId == $rolloClassesId)
    {
      if ($zweiTastenModus)
      {
      	QUERY ( "update groupStates set name='steht' where groupId='$groupId' and basics='1' limit 1" );
      	QUERY ( "update groupStates set name='fährt' where groupId='$groupId' and basics='2' limit 1" );
      }
      else
      {
       	QUERY ( "update groupStates set name='zu' where groupId='$groupId' and basics='1' limit 1" );
      	QUERY ( "update groupStates set name='auf' where groupId='$groupId' and basics='2' limit 1" );
      }
    }
    
    // AN Regeln
    if (isFunctionActive ( $obj->fkt1 ))
    {

      $signalType = $functionTemplates [$myClassesId . "-1-" . $obj->template];
      if ($signalType == "") $signalType = $functionTemplates ["-1-1-" . $obj->template];
      if ($signalType == "") die ( "A: Templatekonfiguration fehlt für $myClassesId Fkt 1 Gruppe $groupId template $obj->template ! ID ".$obj->id );
      
      if ($signalType != "-")
      {
        $ruleSignalType = $signalType;
        if ($signalType == "hold") $ruleSignalType = "holdStart";
        
        $signalType2 = $functionTemplates [$myClassesId . "-2-" . $obj->template];
        if ($signalType2 == "") $signalType2 = $functionTemplates ["-1-2-" . $obj->template];
        if ($signalType2 == "") die ( "B: Templatekonfiguration fehlt für $myClassesId Fkt 2 Gruppe $groupId!" );
        
        if ($isBewegungsMelder)
        {
          $startState = $firstState;
          $endState = $bewegungsState;
          $foundStateChange = 1;
        }         
        else if ($zweiTastenModus && $myClassesId == $rolloClassesId)
        {
          $startState = $firstState;
          $endState = $secondState;
          $foundStateChange = 1;
        }
        // Wenn die Signale für AN und AUS gleich sind, brauchen wir die States
        else if (($signalType == $signalType2 && isFunctionActive ( $obj->fkt2 )) || $hasBewegung)
        {
          $startState = $firstState;
          $endState = $secondState;
          $foundStateChange = 1;
        } 
        else
        {
          $startState = "0";
          $endState = "0";
        }

        for($u = 0; $u < 2; $u ++)
        {
          if (!$hasBewegung) $u = 1; // || $isBewegungsMelder)
          else if ($u == 1) $startState = $bewegungsState;
            
            // Regel anlegen
          QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,extras,intraDay, groupLock) 
                            values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','$startState','$endState','$ruleSignalType','1','1','$obj->extras','$obj->intraDay','$hasBewegung')" );
          $ruleId = query_insert_id ();
          
          // Actions ergäzen
          foreach ( $myInstances as $arr )
          {
            $myClassesId = $arr ["myClassesId"];
            $myInstanceId = $arr ["myInstanceId"];
            
            if ($myClassesId == $dimmerClassesId)
            {
              if ($isMixedGroup == 1)
              {
                $obj->fkt1DauerOrig = $obj->fkt1Dauer;
                $obj->fkt1Orig = $obj->fkt1;
                
                $obj->fkt1Dauer = $obj->fkt1;
                $obj->fkt1 = "100";
              }
              
              QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','25','1')" );
              $newRuleActionId = query_insert_id ();
              
              QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','90','$obj->fkt1','1')" );
              
              $dauer = "0";
              if ($obj->fkt1Dauer > 0) $dauer = $obj->fkt1Dauer;
              QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','91','$dauer','1')" );
              
              if ($isMixedGroup == 1)
              {
                $obj->fkt1Dauer = $obj->fkt1DauerOrig;
                $obj->fkt1 = $obj->fkt1Orig;
              }
            } 
            else if ($myClassesId == $rolloClassesId)
            {
              QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','$startFunctionId','1')" );
              $newRuleActionId = query_insert_id ();
              
              QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','142','$paramToOpen','1')" );
            } 
            else if ($myClassesId == $ledClassesId)
            {
              QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','101','1')" );
              $newRuleActionId = query_insert_id ();
              
              QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','165','$obj->fkt1','1')" );
              
              $dauer = "0";
              if ($obj->fkt1Dauer > 0) $dauer = readDauerWithTimebase ( $myObjectIds [$myInstanceId], $obj->fkt1Dauer );
              
              QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','166','$dauer','1')" );
            } 
            else if ($myClassesId == $logicalButtonClassesId)
            {
              QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','177','1')" );
              $newRuleActionId = query_insert_id ();
              
              if ($obj->fkt1 == "C" || $obj->fkt1 == "true") $brightness = $ledLogicalButtonBrightness;
              else $brightness = $obj->fkt1;
              QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','359','$brightness','1')" );
              
              $dauer = "0";
              if ($obj->fkt1Dauer > 0) $dauer = $obj->fkt1Dauer;
              QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','394','$dauer','1')" );
            } 
            else if ($myClassesId == $schalterClassesId)
            {
              QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','60','1')" );
              $newRuleActionId = query_insert_id ();
              
              $dauer = "0";
              if ($obj->fkt1 > 0) $dauer = readDauerWithTimebase ( $myObjectIds [$myInstanceId], $obj->fkt1 );
              
              QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','265','$dauer','1')" );
            } 
            else if ($myClassesId == $tasterClassesId)
            {
              QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','188','1')" );
              $newRuleActionId = query_insert_id ();
              QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','372','0','1')" ); // enable events off
            } else die ( "nicht implementierte class $myClassesId -3" );
          }
          
          // Signale ergänzen
          $erg2 = QUERY ( "select basicRuleSignals.id, featureInstanceId, controllerId, featureClassesId, featureInstances.id as checkId from basicRuleSignals left join featureInstances on (featureInstances.id=basicRuleSignals.featureInstanceId) where ruleId='$obj->id' order by basicRuleSignals.id" );
          while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
          {
            
            if ($obj2->featureInstanceId < 0) // Signalgruppe
            {
			  QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','0','1')" );
              continue;
            }
            
            if ($obj2->checkId == null) continue;
            
            $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
			if ($obj2->featureInstanceId==$raspberryFeatureInstanceId) // Systemvariable
			{
                QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                           values('$ruleId','$raspberryFeatureInstanceId','270','1')" );
	            $ruleSignalId = QUERY_insert_id();
				
				$erg99 = QUERY("select featureFunctionParamsId, paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id'");
				while($obj99=MYSQLi_fetch_object($erg99))
				{
                  QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                  values('$ruleSignalId','$obj99->featureFunctionParamsId','$obj99->paramValue','1')" );
				}
              
                continue;
			}
            // EV-Time vom Controller (Zeitsteuerung)
            else if ($signalClassesId == $CONTROLLER_CLASSES_ID)
            {
              // QUERY("UPDATE rules set activationStateId='0' where id='$ruleId' limit 1");
              
              $erg3 = QUERY ( "select id from featureInstances where controllerId='$obj2->controllerId' and featureClassesId='$CONTROLLER_CLASSES_ID' limit 1" );
              if ($row = MYSQLi_FETCH_ROW ( $erg3 )) $controllerInstanceId = $row [0];
              else die ( "ControllerInstanz zu controllerId $obj2->controllerId nicht gefunden" );
              
              $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' limit 1" );
              if ($row = MYSQLi_FETCH_ROW ( $erg3 )) $timeParamValue = $row [0];
              else showRuleError ( "Regel ohne g�n Parameterwert gefunden. RegelID = $obj2->id", $groupId );
              
              if ($timeParamValue == - 1) // evDay
              {
                $evDayFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evDay" );
                QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                        values('$ruleId','$controllerInstanceId','$evDayFunctionId','1')" );
              } else if ($timeParamValue == - 2) // evNight
              {
                $evNightFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evNight" );
                QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                        values('$ruleId','$controllerInstanceId','$evNightFunctionId','1')" );
              } else
              {
                $evTimeFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evTime" );
                QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                        values('$ruleId','$controllerInstanceId','$evTimeFunctionId','1')" );
                $ruleSignalId = query_insert_id ();
                
                $timeParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evTime", "weekTime" );
                QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$timeParamId','$timeParamValue','1')" );
              }
              
              continue;
            }
            
            if ($signalType == "click" || ($signalType == "covered" && $signalClassesId == $irClassesId))
            {
              $evClickedFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evClicked" );
              
              QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                      values('$ruleId','$obj2->featureInstanceId','$evClickedFunctionId','1','$completeGroupFeedback')" );
              $ruleSignalId = query_insert_id ();
              
              if ($signalClassesId == $irClassesId)
              {
                $param1Value = "";
                $i = 0;
                $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
                while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
                {
                  if ($param1Value == "") $param1Value = $row [0];
                  else $param2Value = $row [0];
                }
                
                if ($param1Value == "") die ( "Params zu ruleSignalId $obj2->id nicht gefunden -1" );
                
                $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evClicked", "address" );
                QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
                $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evClicked", "command" );
                QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
              }
            } else if ($signalType == "hold")
            {
              $evHoldStartFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evHoldStart" );
              
              QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                       values('$ruleId','$obj2->featureInstanceId','$evHoldStartFunctionId','1','$completeGroupFeedback')" );
              $ruleSignalId = query_insert_id ();
              
              if ($signalClassesId == $irClassesId)
              {
                $param1Value = "";
                $i = 0;
                $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
                while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
                {
                  if ($param1Value == "") $param1Value = $row [0];
                  else $param2Value = $row [0];
                }
                if ($param1Value == "") die ( "Params zu ruleSignalId $obj2->id nicht gefunden -2" );
                
                $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldStart", "address" );
                QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
                $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldStart", "command" );
                QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
              }
            } else if ($signalType == "doubleClick")
            {
              $evDoubleClickFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evDoubleClick" );
              
              QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                        values('$ruleId','$obj2->featureInstanceId','$evDoubleClickFunctionId','1','$completeGroupFeedback')" );
            } else if ($signalType == "covered")
            {
              $evCoveredFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evCovered" );
              
              QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                        values('$ruleId','$obj2->featureInstanceId','$evCoveredFunctionId','1','$completeGroupFeedback')" );
            } else if ($signalType == "free")
            {
              $evFreeFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evFree" );
              
              QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                        values('$ruleId','$obj2->featureInstanceId','$evFreeFunctionId','1','$completeGroupFeedback')" );
            }
          }
        }
      }
    }
    
    // AUS Regeln
    if (isFunctionActive ( $obj->fkt2 ))
    {
      $signalType = $functionTemplates [$myClassesId . "-2-" . $obj->template];
      if ($signalType == "") $signalType = $functionTemplates ["-1-2-" . $obj->template];
      if ($signalType == "") die ( "C: Templatekonfiguration fehlt f�ss $myClassesId Fkt 2 Gruppe $groupId!" );
      
      if ($signalType != "-")
      {
        $ruleSignalType = $signalType;
        if ($signalType == "hold") $ruleSignalType = "holdStart";
        
        $signalType2 = $functionTemplates [$myClassesId . "-1-" . $obj->template];
        if ($signalType2 == "") $signalType2 = $functionTemplates ["-1-1-" . $obj->template];
        if ($signalType2 == "") die ( "D: Templatekonfiguration fehlt f�ss $myClassesId Fkt 1 Gruppe $groupId!" );
        
        if ($isBewegungsMelder)
        {
          $startState = $bewegungsState;
          $endState = $firstState;
          $foundStateChange = 1;
        }         
        else if ($zweiTastenModus && $myClassesId == $rolloClassesId)
        {
          $startState = $firstState;
          $endState = $secondState;
          $foundStateChange = 1;
        }
        // Wenn die Signale f�und AUS gleich sind, brauchen wir die States
        else if (($signalType == $signalType2 && isFunctionActive ( $obj->fkt1 )) || $hasBewegung)
        {
          $startState = $secondState;
          $endState = $firstState;
          $foundStateChange = 1;
        } 
        else
        {
          $startState = "0";
          $endState = "0";
        }
        
        // Regel anlegen
        QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,offRule,extras,intraDay,groupLock) 
                            values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','$startState','$endState','$ruleSignalType','1','1','$offRule','$obj->extras','$obj->intraDay','$hasBewegung')" );
        $ruleId = query_insert_id ();

        // Actions für Kaskade zählen
		$nrSchalterDimmerActions=0;
        foreach ( ( array ) $myInstances as $arr )
        {
           $myClassesId = $arr ["myClassesId"];
		   if ($myClassesId == $dimmerClassesId || $myClassesId == $schalterClassesId) $nrSchalterDimmerActions++;
		}
		
		if ($nrSchalterDimmerActions>16) $kaskadeMode=true;
		else $kaskadeMode=false;
        
        // Actions ergänzen
		$actSchalterDimmerKaskadeCount=0;
		$restoreKaskadeRuleId = $ruleId;
		$kaskadeTriggerInstanzId=0;
		$kaskadeTriggerType="";
		
        foreach ( ( array ) $myInstances as $arr )
        {
		  if ($kaskadeMode) $actSchalterDimmerKaskadeCount++;
			
          $myClassesId = $arr ["myClassesId"];
          $myInstanceId = $arr ["myInstanceId"];
          
          if ($myClassesId == $dimmerClassesId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','25','1')" );
            $newRuleActionId = query_insert_id ();
            
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','90','0','1')" );
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','91','0','1')" );
			
			$kaskadeTriggerInstanzId = $myInstanceId;
			$kaskadeTriggerType = "dimmer";
          } else if ($myClassesId == $rolloClassesId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','$startFunctionId','1')" );
            $newRuleActionId = query_insert_id ();
            
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','142','$paramToClose','1')" );
          } else if ($myClassesId == $ledClassesId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','135','1')" );
          } else if ($myClassesId == $logicalButtonClassesId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','177','1')" );
            $newRuleActionId = query_insert_id();
            
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','359','0','1')" );
          } else if ($myClassesId == $schalterClassesId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','61','1')" );
			
			$kaskadeTriggerInstanzId = $myInstanceId;
			$kaskadeTriggerType = "schalter";
          } else if ($myClassesId == $tasterClassesId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','188','1')" );
            $newRuleActionId = query_insert_id();
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','372','1','1')" ); // enable events on
          } else
            die ( "nicht implementierte class $myClassesId -4" );
		
		  if ($kaskadeMode && $actSchalterDimmerKaskadeCount%16==0)
		  {
			  $kaskadeRound = (int)($actSchalterDimmerKaskadeCount/16);
			  
			  $erg77 = QUERY ( "select id from groupStates where groupId='$groupId' and name='Kaskade$kaskadeRound' limit 1" );
              if ($row77 = MYSQLi_FETCH_ROW ( $erg77 )) $kaskadeStartState = $row77[0];
			  else 
			  {
				  $erg77 = QUERY("select max(value) from groupstates where groupId='$groupId'");
				  $row77=MYSQLi_FETCH_ROW($erg77);
				  $value = $row77[0]+1;
				  
				  QUERY("Insert into groupStates (groupId, name, value, generated) values ('$groupId','Kaskade$kaskadeRound','$value','1')");
				  $kaskadeStartState = query_insert_id();
			  }

			  if ($kaskadeRound ==1) QUERY("UPDATE rules set resultingStateId='$kaskadeStartState' where id='$ruleId' limit 1");

			  $kaskadeRound++;
			  $erg77 = QUERY ("select id from groupStates where groupId='$groupId' and name='Kaskade$kaskadeRound' limit 1");
              if ($row77 = MYSQLi_FETCH_ROW ($erg77)) $kaskadeEndState = $row77[0];
			  else 
			  {
				  $erg77 = QUERY("select max(value) from groupstates where groupId='$groupId'");
				  $row77=MYSQLi_FETCH_ROW($erg77);
				  $value = $row77[0]+1;
				  
				  QUERY("Insert into groupStates (groupId, name, value, generated) values ('$groupId','Kaskade$kaskadeRound','$value','1')");
				  $kaskadeEndState = query_insert_id();
			  }

			  QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,offRule,extras,intraDay,groupLock) 
                                   values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','$kaskadeStartState','$kaskadeEndState','$ruleSignalType','1','1','$offRule','$obj->extras','$obj->intraDay','$hasBewegung')" );
              $ruleId = query_insert_id ();
			  
			  // Kaskadensignale ergänzen
			  if ($kaskadeTriggerType=="dimmer")
			  {
                QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                              values('$ruleId','$kaskadeTriggerInstanzId','46','1')" ); // 46 = evOff
			  }
			  else if ($kaskadeTriggerType=="schalter")
			  {
                QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                           values('$ruleId','$kaskadeTriggerInstanzId','63','1')" ); // 63 = evOff
			  }
			  
			  $kaskadeTriggerInstanzId=0;
			  $kaskadeTriggerType = "";
		  }
        }
		
        if ($kaskadeMode)
		{
			QUERY("UPDATE rules set resultingStateId='$firstState' where id='$ruleId' limit 1");
			$ruleId = $restoreKaskadeRuleId;
		}
			
        // Signale ergänzen
        $erg2 = QUERY ( "select basicRuleSignals.id, featureInstances.controllerId, featureInstanceId, featureClassesId, featureInstances.id as checkId from basicRuleSignals left join featureInstances on (featureInstances.id=basicRuleSignals.featureInstanceId) where ruleId='$obj->id' order by basicRuleSignals.id" );
        while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
        {
          if ($obj2->featureInstanceId < 0) // Signalgruppe
          {
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','0','1')" );
            continue;
          }
          
          if ($obj2->checkId == null) continue;
          
          $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
            
          // EV-Time vom Controller (Zeitsteuerung)
          if ($signalClassesId == $CONTROLLER_CLASSES_ID)
          {
            // QUERY("UPDATE rules set activationStateId='0' where id='$ruleId' limit 1");
            
            $erg3 = QUERY ( "select id from featureInstances where controllerId='$obj2->controllerId' and featureClassesId='$CONTROLLER_CLASSES_ID' limit 1" );
            if ($row = MYSQLi_FETCH_ROW ( $erg3 )) $controllerInstanceId = $row [0];
            else die ( "ControllerInstanz zu controllerId $obj2->controllerId nicht gefunden" );
            
            $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' limit 1" );
            if ($row = MYSQLi_FETCH_ROW ( $erg3 )) $timeParamValue = $row [0];
            else showRuleError ( "Regel ohne g�n Parameterwert gefunden. RegelID = $obj2->id", $groupId );
            
            if ($timeParamValue == - 1) // evDay
            {
              $evDayFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evDay" );
              QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                        values('$ruleId','$controllerInstanceId','$evDayFunctionId','1')" );
            } else if ($timeParamValue == - 2) // evNight
            {
              $evNightFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evNight" );
              QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                        values('$ruleId','$controllerInstanceId','$evNightFunctionId','1')" );
            } else
            {
              $evTimeFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evTime" );
              QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                        values('$ruleId','$controllerInstanceId','$evTimeFunctionId','1')" );
              $ruleSignalId = query_insert_id ();
              
              $timeParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evTime", "weekTime" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$timeParamId','$timeParamValue','1')" );
            }
			
            continue;
          }
          
          if ($signalType == "click" || ($signalType == "covered" && $signalClassesId == $irClassesId))
          {
            $evClickedFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evClicked" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                      values('$ruleId','$obj2->featureInstanceId','$evClickedFunctionId','1','$completeGroupFeedback')" );
            $ruleSignalId = query_insert_id ();
            
            if ($signalClassesId == $irClassesId)
            {
              $param1Value = "";
              $i = 0;
              $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
              while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
              {
                if ($param1Value == "")
                  $param1Value = $row [0];
                else
                  $param2Value = $row [0];
              }
              if ($param1Value == "")
                die ( "Params zu ruleSignalId $obj2->id nicht gefunden -3" );
              
              $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evClicked", "address" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
              $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evClicked", "command" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
            }
          } else if ($signalType == "hold")
          {
            $evHoldStartFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evHoldStart" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback	)
                                       values('$ruleId','$obj2->featureInstanceId','$evHoldStartFunctionId','1','$completeGroupFeedback')" );
            $ruleSignalId = query_insert_id ();
           
            if ($signalClassesId == $irClassesId)
            {
              $param1Value = "";
              $i = 0;
              $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
              while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
              {
                if ($param1Value == "")
                  $param1Value = $row [0];
                else
                  $param2Value = $row [0];
              }
              if ($param1Value == "")
                die ( "Params zu ruleSignalId $obj2->id nicht gefunden -4" );
              
              $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldStart", "address" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                  values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
              $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldStart", "command" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                  values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
            }
          } else if ($signalType == "doubleClick")
          {
            $evDoubleClickFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evDoubleClick" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                       values('$ruleId','$obj2->featureInstanceId','$evDoubleClickFunctionId','1','$completeGroupFeedback')" );
          } else if ($signalType == "covered")
          {
            $evCoveredFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evCovered" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                       values('$ruleId','$obj2->featureInstanceId','$evCoveredFunctionId','1','$completeGroupFeedback')" );
          } else if ($signalType == "free")
          {
            $evFreeFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evFree" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                        values('$ruleId','$obj2->featureInstanceId','$evFreeFunctionId','1','$completeGroupFeedback')" );
          }
        }
      }
    }
    
    // DIMM // STOP'N'GO Regeln
    if (isFunctionActive ( $obj->fkt3 ))
    {
      $signalType = $functionTemplates [$myClassesId . "-3-" . $obj->template];
      if ($signalType == "") $signalType = $functionTemplates ["-1-3-" . $obj->template];
      if ($signalType == "") die ( "E: Templatekonfiguration fehlt f�ss $myClassesId Fkt 3 Gruppe $groupId!" );
      
      if ($signalType != "-")
      {
        $ruleSignalType = $signalType;
        if ($signalType == "hold") $ruleSignalType = "holdStart";
        
        $startState = 0;
        $endState = 0;
        
        if ($isBewegungsMelder)
        {
          $startState = $firstState;
          $endState = $bewegungsState;
        } else if ($hasBewegung)
          $endState = $secondState;
          
          // Regel anlegen f�mmen / toogle
        QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,extras,intraDay, groupLock) 
                            values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','$startState','$endState','$ruleSignalType','1','1','$obj->extras','$obj->intraDay','$hasBewegung')" );
        $ruleId = query_insert_id ();
        
        // Actions ergänzen
        foreach ( $myInstances as $arr )
        {
          $myClassesId = $arr ["myClassesId"];
          $myInstanceId = $arr ["myInstanceId"];
          
          if ($myClassesId == $dimmerClassesId)
          {
            // TOGGLE
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','64','1')" );
            $newRuleActionId = query_insert_id ();
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','115','0','1')" );
          } 
          else if ($myClassesId == $rolloClassesId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','$startFunctionId','1')" );
            $newRuleActionId = query_insert_id ();
            
            // TOGGLE
            if ($zweiTastenModus)
            {
            	if (isFunctionActive ( $obj->fkt1 )) QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','142','$paramToOpen','1')" );
            	else QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','142','$paramToClose','1')" );
            }
            else QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','142','0','1')" );
          } else die ( "nicht implementierte class $myClassesId -5 in Gruppe $groupId" );
        }
        
        // Signale ergänzen
        $erg2 = QUERY ( "select basicRuleSignals.id, featureInstanceId, featureClassesId, featureInstances.id as checkId from basicRuleSignals left  join featureInstances on (featureInstances.id=basicRuleSignals.featureInstanceId) where ruleId='$obj->id' order by basicRuleSignals.id" );
        while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
        {
          if ($obj2->featureInstanceId < 0) // Signalgruppe
          {
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','0','1')" );
            continue;
          }
          
          if ($obj2->checkId == null) continue;
          
          $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
          
          if ($signalType == "click" || ($signalType == "covered" && $signalClassesId == $irClassesId))
          {
            $evClickedFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evClicked" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                      values('$ruleId','$obj2->featureInstanceId','$evClickedFunctionId','1','$completeGroupFeedback')" );
            $ruleSignalId = query_insert_id ();
            
            if ($signalClassesId == $irClassesId)
            {
              $param1Value = "";
              $i = 0;
              $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
              while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
              {
                if ($param1Value == "") $param1Value = $row [0];
                else $param2Value = $row [0];
              }
              
              if ($param1Value == "") die ( "Params zu ruleSignalId $obj2->id nicht gefunden -5" );
              
              $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evClicked", "address" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
              $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evClicked", "command" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
            }
          } else if ($signalType == "hold")
          {
            $evHoldStartFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evHoldStart" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                       values('$ruleId','$obj2->featureInstanceId','$evHoldStartFunctionId','1','$completeGroupFeedback')" );
            $ruleSignalId = query_insert_id ();
            
            if ($signalClassesId == $irClassesId)
            {
              $param1Value = "";
              $i = 0;
              $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
              while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
              {
                if ($param1Value == "") $param1Value = $row [0];
                else $param2Value = $row [0];
              }
              
              if ($param1Value == "") die ( "Params zu ruleSignalId $obj2->id nicht gefunden -6" );
              
              $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldStart", "address" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
              $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldStart", "command" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
            }
          } else if ($signalType == "doubleClick")
          {
            $evDoubleClickFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evDoubleClick" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                       values('$ruleId','$obj2->featureInstanceId','$evDoubleClickFunctionId','1','$completeGroupFeedback')" );
          } else if ($signalType == "covered")
          {
            $evCoveredFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evCovered" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                       values('$ruleId','$obj2->featureInstanceId','$evCoveredFunctionId','1','$completeGroupFeedback')" );
          } else if ($signalType == "free")
          {
            $evFreeFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evFree" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                        values('$ruleId','$obj2->featureInstanceId','$evFreeFunctionId','1','$completeGroupFeedback')" );
          }
        }
        
        // Bei Hold können wir noch holdEnd verwenden
        if ($signalType == "hold")
        {
          $ruleSignalType = "holdEnd";
          
          $startState = 0;
          $endState = 0;
          
          if ($isBewegungsMelder)
          {
            $startState = $firstState;
            $endState = $bewegungsState;
          } else if ($hasBewegung)
            $endState = $secondState;
          
          QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,extras,intraDay,groupLock) 
                               values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','$startState','$endState','$ruleSignalType','1','1','$obj->extras','$obj->intraDay','$hasBewegung')" );
          $ruleIdHoldEnd = query_insert_id ();
          
          // Actions erg㭺en
          foreach ( $myInstances as $arr )
          {
            $myClassesId = $arr ["myClassesId"];
            $myInstanceId = $arr ["myInstanceId"];
            
            if ($myClassesId == $dimmerClassesId) QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleIdHoldEnd','$myInstanceId','65','1')" );
            else if ($myClassesId == $rolloClassesId) QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleIdHoldEnd','$myInstanceId','$stopFunctionId','1')" );
            else die ( "nicht implementierte class $myClassesId -7" );
          }
          
          // Signale ergänzen
          $erg2 = QUERY ( "select basicRuleSignals.id, featureInstanceId, featureClassesId, featureInstances.id as checkId from basicRuleSignals left  join featureInstances on (featureInstances.id=basicRuleSignals.featureInstanceId) where ruleId='$obj->id' order by basicRuleSignals.id" );
          while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
          {
            if ($obj2->featureInstanceId < 0) // Signalgruppe
            {
              QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','0','1')" );
              continue;
            }
            
            if ($obj2->checkId == null) continue;
            
            $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
            
            $evHoldEndFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evHoldEnd" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                       values('$ruleIdHoldEnd','$obj2->featureInstanceId','$evHoldEndFunctionId','1','$completeGroupFeedback')" );
            $ruleSignalId = query_insert_id ();
            
            if ($signalClassesId == $irClassesId)
            {
              $param1Value = "";
              $i = 0;
              $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
              while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
              {
                if ($param1Value == "")
                  $param1Value = $row [0];
                else
                  $param2Value = $row [0];
              }
              if ($param1Value == "")
                die ( "Params zu ruleSignalId $obj2->id nicht gefunden -9" );
              
              $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldEnd", "address" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
         		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
              $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldEnd", "command" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
         		                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
            }
          }
        }
      }
    }
    
    // PRESET Regeln
    if (isFunctionActive ( $obj->fkt4 ))
    {
      $signalType = $functionTemplates [$myClassesId . "-4-" . $obj->template];
      if ($signalType == "")
        $signalType = $functionTemplates ["-1-4-" . $obj->template];
      if ($signalType == "")
        die ( "F: Templatekonfiguration fehlt f�ss $myClassesId Fkt 4 Gruppe $groupId!" );
      
      $ruleSignalType = $signalType;
      if ($signalType == "hold")
        $ruleSignalType = "holdStart";
      
      if ($signalType != "-")
      {
        $startState = 0;
        $endState = 0;
        
        if ($isBewegungsMelder)
        {
          $startState = $bewegungsState;
          $endState = $bewegungsState;
        } else if ($hasBewegung)
          $endState = $secondState;
          
          // Regel anlegen
        QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,extras,intraDay,groupLock) 
                              values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','$startState','$endState','$ruleSignalType','1','1','$obj->extras','$obj->intraDay','$hasBewegung')" );
        $ruleId = query_insert_id ();
        
        // Actions ergänzen
        foreach ( $myInstances as $arr )
        {
          $myClassesId = $arr ["myClassesId"];
          $myInstanceId = $arr ["myInstanceId"];
          
          if ($myClassesId == $dimmerClassesId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','25','1')" );
            $newRuleActionId = query_insert_id ();
            
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','90','$obj->fkt4','1')" );
            
            $dauer = "0";
            if ($obj->fkt4Dauer > 0)
              $dauer = $obj->fkt4Dauer;
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','91','$dauer','1')" );
          } 
          else if ($myClassesId == $rolloClassesId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','$moveToPositionFunctionId','1')" );
            $newRuleActionId = query_insert_id ();
            
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','$paramPosition','$obj->fkt4','1')" );
          } else if ($myClassesId == $ledClassesId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','101','1')" );
            $newRuleActionId = query_insert_id ();
            
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','165','$obj->fkt4','1')" );
            
            $dauer = "0";
            if ($obj->fkt4Dauer > 0) $dauer = readDauerWithTimebase ( $myObjectIds [$myInstanceId], $obj->fkt4Dauer );
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','166','$dauer','1')" );
          } 
          else if ($myClassesId == $logicalButtonClassesId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','137','1')" );
            $newRuleActionId = query_insert_id ();
            
            if ($obj->fkt4 == "C")
              $brightness = $ledLogicalButtonBrightness;
            else
              $brightness = $obj->fkt4;
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','243','$brightness','1')" );
            
            $dauer = "0";
            if ($obj->fkt4Dauer > 0)
              $dauer = $obj->fkt4Dauer;
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','244','$dauer','1')" );
          } else if ($myClassesId == $schalterClassesId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','60','1')" );
            $newRuleActionId = query_insert_id ();
            
            $dauer = "0";
            if ($obj->fkt4 > 0) $dauer = readDauerWithTimebase ( $myObjectIds [$myInstanceId], $obj->fkt4 );
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','265','$dauer','1')" );
          } 
          else die ( "nicht implementierte class $myClassesId -8" );
        }
        
        // Signale ergänzen
        $erg2 = QUERY ( "select basicRuleSignals.id, featureInstanceId, featureClassesId, featureInstances.id as checkId from basicRuleSignals left  join featureInstances on (featureInstances.id=basicRuleSignals.featureInstanceId) where ruleId='$obj->id' order by basicRuleSignals.id" );
        while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
        {
          if ($obj2->featureInstanceId < 0) // Signalgruppe
          {
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','0','1')" );
            continue;
          }
          
          if ($obj2->checkId == null)
            continue;
          
          $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
          
          if ($signalType == "click" || ($signalType == "covered" && $signalClassesId == $irClassesId))
          {
            $evClickedFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evClicked" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                        values('$ruleId','$obj2->featureInstanceId','$evClickedFunctionId','1','$completeGroupFeedback')" );
            $ruleSignalId = query_insert_id ();
            
            if ($signalClassesId == $irClassesId)
            {
              $param1Value = "";
              $i = 0;
              $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
              while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
              {
                if ($param1Value == "")
                  $param1Value = $row [0];
                else
                  $param2Value = $row [0];
              }
              if ($param1Value == "")
                die ( "Params zu ruleSignalId $obj2->id nicht gefunden -10" );
              
              $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evClicked", "address" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
          		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
              $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evClicked", "command" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
          		                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
            }
          } else if ($signalType == "hold")
          {
            $evHoldStartFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evHoldStart" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                         values('$ruleId','$obj2->featureInstanceId','$evHoldStartFunctionId','1','$completeGroupFeedback')" );
            $ruleSignalId = query_insert_id ();
            
            if ($signalClassesId == $irClassesId)
            {
              $param1Value = "";
              $i = 0;
              $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
              while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
              {
                if ($param1Value == "")
                  $param1Value = $row [0];
                else
                  $param2Value = $row [0];
              }
              if ($param1Value == "")
                die ( "Params zu ruleSignalId $obj2->id nicht gefunden -11" );
              
              $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldStart", "address" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
           		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
              $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldStart", "command" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
           		                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
            }
            
            // Regel dimmStop
            /*
             * $ruleSignalType="holdEnd"; QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule) values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','0','$secondState','$ruleSignalType','1')"); $ruleIdHoldEnd=query_insert_id(); $evHoldEndFunctionId = getClassesIdFunctionsIdByName($obj2->featureClassesId, "evHoldEnd"); QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId) values('$ruleIdHoldEnd','$obj2->featureInstanceId','$evHoldEndFunctionId')"); $ruleSignalId = query_insert_id(); if ($signalClassesId==$irClassesId) { $param1Value=""; $i=0; $erg = QUERY("select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2"); while($row=MYSQLi_FETCH_ROW($erg)) { if ($param1Value=="") $param1Value=$row[0]; else $param2Value=$row[0]; } if ($param1Value=="") die("Params zu ruleSignalId $obj2->id nicht gefunden -12"); $irParamAddressId = getClassesIdFunctionParamIdByName($irClassesId,"evHoldEnd","address"); QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) values('$ruleSignalId','$irParamAddressId','$param1Value')"); $irParamCommandId = getClassesIdFunctionParamIdByName($irClassesId,"evHoldEnd","command"); QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) values('$ruleSignalId','$irParamCommandId','$param2Value')"); } // Actions erg㭺en if ($myClassesId == $dimmerClassesId) { QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleIdHoldEnd','$myInstanceId','65')"); } else die("nicht implementierte class $myClassesId -9");
             */
          } else if ($signalType == "doubleClick")
          {
            $evDoubleClickFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evDoubleClick" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                         values('$ruleId','$obj2->featureInstanceId','$evDoubleClickFunctionId','1','$completeGroupFeedback')" );
          } else if ($signalType == "covered")
          {
            $evCoveredFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evCovered" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                         values('$ruleId','$obj2->featureInstanceId','$evCoveredFunctionId','1','$completeGroupFeedback')" );
          } else if ($signalType == "free")
          {
            $evFreeFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evFree" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                          values('$ruleId','$obj2->featureInstanceId','$evFreeFunctionId','1','$completeGroupFeedback')" );
          }
        }
      }
    }

    
    // Stop Regeln bei Rollos
    if ($myClassesId == $rolloClassesId)
    {
      $signalType = $functionTemplates [$myClassesId . "-5-" . $obj->template];
      if ($signalType == "") $signalType = $functionTemplates ["-1-5-" . $obj->template];
      if ($signalType == "") die ( "G: Templatekonfiguration fehlt f�ss $myClassesId Fkt 5 Gruppe $groupId!" );
      
      $ruleSignalType = $signalType;
      if ($signalType == "hold") $ruleSignalType = "holdEnd";
      
      if ($signalType != "-")
      {
      	if ($isBewegungsMelder)
        {
          $startState = $bewegungsState;
          $endState = $bewegungsState;
        } 
        else if ($hasBewegung) $endState = $secondState;
        else if ($zweiTastenModus && $myClassesId == $rolloClassesId)
        {
          $startState = $secondState;
          $endState = $firstState;
        }
        else
        {
          $startState = 0;
          $endState = 0;
        }
          
          // Regel anlegen
        QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,extras,intraDay, groupLock) 
                            values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','$startState','$endState','$ruleSignalType','1','1','$obj->extras','$obj->intraDay','$hasBewegung')" );
        $ruleId = query_insert_id ();

        // Actions erg㭺en
        foreach ( $myInstances as $arr )
        {
          $myClassesId = $arr ["myClassesId"];
          $myInstanceId = $arr ["myInstanceId"];
          
          QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','$stopFunctionId','1')" );
        }
        
        // Signale erg㭺en
        $erg2 = QUERY ( "select basicRuleSignals.id,featureInstanceId, featureClassesId, featureInstances.id as checkId from basicRuleSignals left  join featureInstances on (featureInstances.id=basicRuleSignals.featureInstanceId) where ruleId='$obj->id' order by basicRuleSignals.id" );
        while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
        {
          if ($obj2->featureInstanceId < 0) // Signalgruppe
          {
             QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','0','1')" );
            continue;
          }
          
          if ($obj2->checkId == null)
            continue;
          
          $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
          
          if ($signalType == "click" || ($signalType == "covered" && $signalClassesId == $irClassesId))
          {
            $evClickedFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evClicked" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                      values('$ruleId','$obj2->featureInstanceId','$evClickedFunctionId','1','$completeGroupFeedback')" );
            $ruleSignalId = query_insert_id ();
            
            if ($signalClassesId == $irClassesId)
            {
              $param1Value = "";
              $i = 0;
              $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
              while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
              {
                if ($param1Value == "") $param1Value = $row [0];
                else $param2Value = $row [0];
              }
              if ($param1Value == "") die ( "Params zu ruleSignalId $obj2->id nicht gefunden -13" );
              
              $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evClicked", "address" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
              $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evClicked", "command" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
            }
          } else if ($signalType == "hold")
          {
            $evHoldEndFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evHoldEnd" );
            
            QUERY ( "INSERT  ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                       values('$ruleId','$obj2->featureInstanceId','$evHoldEndFunctionId','1','$completeGroupFeedback')" );
            $ruleSignalId = query_insert_id ();
            
            if ($signalClassesId == $irClassesId)
            {
              $param1Value = "";
              $i = 0;
              $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
              while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
              {
                if ($param1Value == "")
                  $param1Value = $row [0];
                else
                  $param2Value = $row [0];
              }
              if ($param1Value == "")
                die ( "Params zu ruleSignalId $obj2->id nicht gefunden -14" );
              
              $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldEnd", "address" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
              $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldEnd", "command" );
              QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
        		                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
            }
          } else if ($signalType == "doubleClick")
          {
            $evDoubleClickFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evDoubleClick" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                       values('$ruleId','$obj2->featureInstanceId','$evDoubleClickFunctionId','1','$completeGroupFeedback')" );
          } else if ($signalType == "covered")
          {
            $evCoveredFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evCovered" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                       values('$ruleId','$obj2->featureInstanceId','$evCoveredFunctionId','1','$completeGroupFeedback')" );
          } else if ($signalType == "free")
          {
            $evFreeFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evFree" );
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                        values('$ruleId','$obj2->featureInstanceId','$evFreeFunctionId','1','$completeGroupFeedback')" );
          }
        }
      }
    }
  }
  
  // Dummyregeln ergänzen
  if (!$doSkipSyncEventsForHeating && !$doSkipSyncEventsForVentilation && $myClassesId != $ethernetClassesId && $myClassesId != $logicalButtonClassesId && $myClassesId != $tasterClassesId && $myInstanceCount == 1 && $myClassesId != 24) // Bei Gruppen mir mehreren Aktoren generieren wir keine Statewechseldummies
  {
    // Dummy für evOn
    if ($zweiTastenModus && $myClassesId == $rolloClassesId)
    {
    	$myFirstState=0;
    	$mySecondState=$firstState;
    }
    else
    {
    	$myFirstState=$firstState;
    	$mySecondState=$secondState;
    }

    QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,syncEvent,`generated`) 
                       values('$groupId','7','31','255','7','31','255','$myFirstState','$mySecondState','evOn','1','1','1')" );
    $ruleId = query_insert_id ();
    // QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$myInstanceId','-1')");
    
    if ($myClassesId == $dimmerClassesId)
    {
      $evOnFunctionId = getClassesIdFunctionsIdByName ( $dimmerClassesId, "evOn" );
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                values('$ruleId','$myInstanceId','$evOnFunctionId','1')" );
      $signalId = query_insert_id ();
      
      $dimmerParamBrightnessId = getClassesIdFunctionParamIdByName ( $dimmerClassesId, "evOn", "brightness" );
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                     values('$signalId','$dimmerParamBrightnessId','255','1')" );
    } else if ($myClassesId == $rolloClassesId)
    {
      $statusFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "evOpen" );
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                values('$ruleId','$myInstanceId','$statusFunctionId','1')" );
    } else if ($myClassesId == $schalterClassesId)
    {
      $evOnFunctionId = getClassesIdFunctionsIdByName ( $schalterClassesId, "evOn" );
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                values('$ruleId','$myInstanceId','$evOnFunctionId','1')" );
      $signalId = query_insert_id ();
      
      $schalterParamDurationId = getClassesIdFunctionParamIdByName ( $schalterClassesId, "evOn", "duration" );
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                     values('$signalId','$schalterParamDurationId','$signalParamWildcardWord','1')" );                         
                                
    } else if ($myClassesId == $ledClassesId)
    {
      $evOnFunctionId = getClassesIdFunctionsIdByName ( $ledClassesId, "evOn" );
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                values('$ruleId','$myInstanceId','$evOnFunctionId','1')" );
    } else
      die ( "nicht implementierte class $myClassesId -10" );
      

    // evOff
    $myDestinationState=$firstState;

    QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,syncEvent,`generated`) 
                       values('$groupId','7','31','255','7','31','255','0','$myDestinationState','evOff','1','1','1')" );
    $ruleId = query_insert_id ();
    // QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$myInstanceId','-1')");
    
    if ($myClassesId == $dimmerClassesId)
    {
      $evOffFunctionId = getClassesIdFunctionsIdByName ( $dimmerClassesId, "evOff" );
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                values('$ruleId','$myInstanceId','$evOffFunctionId','1')" );
    } else if ($myClassesId == $rolloClassesId)
    {
      $statusFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "evClosed" );
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                values('$ruleId','$myInstanceId','$statusFunctionId','1')" );
      $signalId = query_insert_id ();
      
      $positionParamId = getClassesIdFunctionParamIdByName ( $rolloClassesId, "evClosed", "position" );
      
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                     values('$signalId','$positionParamId','255')" );
    } else if ($myClassesId == $schalterClassesId)
    {
      $evOnFunctionId = getClassesIdFunctionsIdByName ( $schalterClassesId, "evOff" );
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                values('$ruleId','$myInstanceId','$evOnFunctionId','1')" );
                                
    } else if ($myClassesId == $ledClassesId)
    {
      $evOnFunctionId = getClassesIdFunctionsIdByName ( $ledClassesId, "evOff" );
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                values('$ruleId','$myInstanceId','$evOnFunctionId','1')" );
    } else die ( "nicht implementierte class $myClassesId -11" );
  }
}

function isFunctionActive($fkt)
{
  if ($fkt == "-") return FALSE;
  if ($fkt == "") return FALSE;
  if ($fkt == "false") return FALSE;
  return TRUE;
}

function generateLedFeedbackForGroup($groupId, $manualGroup = 0)
{
  // echo "Generiere Ledfeedback für Gruppe $groupId <br>";
  global $CONTROLLER_CLASSES_ID;
  global $signalParamWildcard,$signalParamWildcardWord;
  global $dimmerClassesId, $rolloClassesId, $ledClassesId, $schalterClassesId, $irClassesId, $tasterClassesId, $logicalButtonClassesId;
  global $ledStatusBrightness;
  global $serverInstances;
  
  $erg = QUERY ( "select featureInstanceId,featureClassesId from groupFeatures join featureInstances on (featureInstances.id=featureInstanceId) where groupId='$groupId' limit 1" );
  $obj = MYSQLi_FETCH_OBJECT ( $erg );
  $myInstanceId = $obj->featureInstanceId;
  $myClassesId = $obj->featureClassesId;
  
  // LED Feedback generieren
  $erg = QUERY ( "select * from basicRules where groupId='$groupId' and active='1'" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    // LED Feedback 0 = Kein, 1 = Aktor Einzeln, 2 = Teilszene, 3 = Komplettszene
    if ($obj->ledStatus == 0) {}// 0 = Kein Feedback
    else if ($obj->ledStatus == 2 || $obj->ledStatus == 3) // 2 = Teilszene 3 = Komplettszene
    {
      // $ledStatusBrightness="100";
      // $erg2 = QUERY("select paramValue from basicConfig where paramKey = 'ledStatusBrightness' limit 1");
      // if($row = MYSQLi_FETCH_ROW($erg2)) $ledStatusBrightness=$row[0];
      // $ledStatusBrightness = (int)($ledStatusBrightness*255/100);
      
      $evGroupOnFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOn" );
      $evGroupOffFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOff" );
      $evGroupUndefinedFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined" );
      
      // LEDs der Signale suchen
      $erg2 = QUERY ( "select featureInstanceId from basicRuleSignals where ruleId='$obj->id' order by id" );
      while ( $row = MYSQLi_FETCH_ROW ( $erg2 ) )
      {
        $actSignalInstanceId = $row [0];
        $signalClassesId = getClassesIdByFeatureInstanceId ( $actSignalInstanceId );
        
        if ($signalClassesId == $tasterClassesId)
        {
          // Erstmal alle bisherigen Feedbacks zu diesem Aktor-Signal-Paar l�en
          // $ledFeedbackIndent=$myInstanceId."-".$actSignalInstanceId;
          // $erg4 = QUERY("select id from rules where ledFeedbackIndent='$ledFeedbackIndent'");
          // while($row4=MYSQLi_FETCH_ROW($erg4)) deleteRule($row4[0]);
          
          $ledInstanceId = getLedForTaster ( $actSignalInstanceId );
          if (! showError ( $ledInstanceId, $obj->id, $groupId, $actSignalInstanceId ))
          {
            $erg3 = QUERY ( "select groups.id from groups join groupFeatures on (groupFeatures.groupId=groups.id) where featureInstanceId='$ledInstanceId' limit 1" );
            if ($row3 = MYSQLi_FETCH_ROW ( $erg3 )) $ledGroupId = $row3 [0];
            else die ( "B) LED Groupid nicht gefunden zu ledInstanceId $ledInstanceId" );
              
              // Standardstates auslesen
            $ledFirstState = "";
            $ledSecondState = "";
            $erg3 = QUERY ( "select id,basics from groupStates where groupId='$ledGroupId' and (basics='1' or basics='2') limit 2" );
            while ( $row3 = MYSQLi_FETCH_ROW ( $erg3 ) )
            {
              if ($row3 [1] == "1") $ledFirstState = $row3 [0];
              else if ($row3 [1] == "2") $ledSecondState = $row3 [0];
            }
            
            if ($ledFirstState == "" || $ledSecondState == "") die ( "Led States nicht gefunden zu Gruppe $groupId" );
            
            if ($manualGroup == 1) $szeneGroupId = $groupId;
            else
            {
              $szeneGroupId = getSyncGroupIdForSignalInstanceId ( $actSignalInstanceId );
              if ($szeneGroupId=="") echo "Gruppe $groupId <br>";
            }
            
            if ($szeneGroupId == - 2) // keine Gruppe->einzelmember generieren
            {
              // Member einzeln switch
              $obj->ledStatus = 1;
              // echo "Aktuell keine Szene vorhanden! ID = $actSignalInstanceId <br>";
            } else if ($szeneGroupId != "")
            {
              $erg3 = QUERY ( "select controllerId, groupIndex from groupSyncHelper where groupId='$szeneGroupId' order by id desc limit 1" );
              if ($row3 = MYSQLi_FETCH_ROW ( $erg3 ))
              {
                $controllerId = getControllerFeatureInstanceIdForControllerId ( $row3 [0] );
                $groupIndex = $row3 [1];
                
                // pr�b es diese regel bei der led nicht schon gibt
                $erg3 = QUERY ( "select ruleId from ruleSignals join rules on(ruleSignals.ruleId=rules.id) where featureInstanceId='$controllerId' and featureFunctionId='$evGroupUndefinedFunctionId' and groupId='$ledGroupId' limit 1" );
                if ($row3 = MYSQLi_FETCH_ROW ( $erg3 ))
                {
                  // echo "Szene schon vorhanden <br>";
                } else
                {
                  // Teilszene: Einschalten bei groupUndefined und groupOn
                  // Komplettszene: Einschalten bei groupOn
                  QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,ledFeedbackIndent,`generated`) 
                                        values('$ledGroupId','7','31','255','7','31','255','$ledFirstState','$ledSecondState','evOn','1','$ledFeedbackIndent','1')" );
                  $actRuleId = query_insert_id ();
                  
                  if ($obj->ledStatus == 2)
                  {
                    QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                                values('$actRuleId','$controllerId','$evGroupUndefinedFunctionId','1')" );
                    $actSignalId = query_insert_id ();
                    $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined", "index" );
                    QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                                     values('$actSignalId','$indexParamId','$groupIndex','1')" );
                  }
                  
                  QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                              values('$actRuleId','$controllerId','$evGroupOnFunctionId','1')" );
                  $actSignalId = query_insert_id ();
                  $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOn", "index" );
                  QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                                   values('$actSignalId','$indexParamId','$groupIndex','1')" );
                  
                  $ledFunctionIdOn = getClassesIdFunctionsIdByName ( $ledClassesId, "on" );
                  
                  QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`)
        		                                  values('$actRuleId','$ledInstanceId','$ledFunctionIdOn','1')" );
                  $newRuleActionId = query_insert_id ();
                  
                  $ledParamBrightnessId = getClassesIdFunctionParamIdByName ( $ledClassesId, "on", "brightness" );
                  $ledParamDurationId = getClassesIdFunctionParamIdByName ( $ledClassesId, "on", "duration" );
				  
				  $myBrightness = $ledStatusBrightness;
				  $ergBright = QUERY("select brightness from brightnessExceptions join featureInstances on (featureInstances.objectId=brightnessExceptions.objectId) where featureinstances.id='$ledInstanceId' limit 1");
			      if ($rowBright = MYSQLi_FETCH_ROW($ergBright)) $myBrightness = $rowBright[0];
				  
                  QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) 
        	                                         values('$newRuleActionId','$ledParamBrightnessId','$myBrightness','1')" );
                  QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) 
        	                                         values('$newRuleActionId','$ledParamDurationId','0','1')" );
                  
                  // Ausschalten bei groupOff
                  // Bei Komplettszene auch ausschalten bei undefined
                  QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,ledFeedbackIndent,`generated`) 
                                        values('$ledGroupId','7','31','255','7','31','255','$ledSecondState','$ledFirstState','evOff','1','$ledFeedbackIndent','1')" );
                  $actRuleId = query_insert_id ();
                  
                  if ($obj->ledStatus == 3) // Komplettszene
                  {
                    QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                                values('$actRuleId','$controllerId','$evGroupUndefinedFunctionId','1')" );
                    $actSignalId = query_insert_id ();
                    $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined", "index" );
                    QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                                     values('$actSignalId','$indexParamId','$groupIndex','1')" );
                  }
                  
                  QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                              values('$actRuleId','$controllerId','$evGroupOffFunctionId','1')" );
                  $actSignalId = query_insert_id ();
                  $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOff", "index" );
                  QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                                   values('$actSignalId','$indexParamId','$groupIndex','1')" );
                  
                  $ledFunctionIdOff = getClassesIdFunctionsIdByName ( $ledClassesId, "off" );
                  QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`)
        		                                  values('$actRuleId','$ledInstanceId','$ledFunctionIdOff','1')" );
                }
              } else
                die ( "ControllerId und GroupIndex zu Szenengruppe $szeneGroupId nicht gefunden. actSignalInstanceId = $actSignalInstanceId groupId = $groupId ruleId=" . $obj->id );
            } else
              die ( "SzeneGroupId nicht gefunden zu Signal $actSignalInstanceId  " );
          } else
            echo "A Kein LogicalButton $actSignalInstanceId in Gruppe $groupId<br>";
        }
      }
    }
    
    if ($obj->ledStatus == 1) // 1 = Aktor Einzeln
    {
      // $ledStatusBrightness="100";
      // $erg2 = QUERY("select paramValue from basicConfig where paramKey = 'ledStatusBrightness' limit 1");
      // if($row = MYSQLi_FETCH_ROW($erg2)) $ledStatusBrightness=$row[0];
      // $ledStatusBrightness = (int)($ledStatusBrightness*255/100);
      
      // LEDs der Signale suchen
      $erg2 = QUERY ( "select featureInstanceId from basicRuleSignals where ruleId='$obj->id' order by id" );
      while ( $row = MYSQLi_FETCH_ROW ( $erg2 ) )
      {
        $actSignalInstanceId = $row [0];
        if ($serverInstances [$actSignalInstanceId] == 1) continue; // Kein Feedback f�tuelle Servertaster
        
        $signalClassesId = getClassesIdByFeatureInstanceId ( $actSignalInstanceId );
        
        if ($signalClassesId == $tasterClassesId)
        {
          $ledInstanceId = getLedForTaster ( $actSignalInstanceId );
          
          if (! showError ( $ledInstanceId, $obj->id, $groupId, $actSignalInstanceId ))
          {
            // Erstmal alle bisherigen Feedbacks zu diesem Aktor-Signal-Paar l�en
            // $ledFeedbackIndent=$myInstanceId."-".$actSignalInstanceId;
            // $erg4 = QUERY("select id from rules where ledFeedbackIndent='$ledFeedbackIndent'");
            // while($row4=MYSQLi_FETCH_ROW($erg4)) deleteRule($row4[0]);
            
            $erg3 = QUERY ( "select groups.id from groups join groupFeatures on (groupFeatures.groupId=groups.id) where featureInstanceId='$ledInstanceId' limit 1" );
            if ($row3 = MYSQLi_FETCH_ROW ( $erg3 )) $ledGroupId = $row3 [0];
            else die ( "A) LED Groupid nicht gefunden zu ledInstanceId $ledInstanceId" );
              
              // Standardstates auslesen
            $ledFirstState = "";
            $ledSecondState = "";
            $erg3 = QUERY ( "select id,basics from groupStates where groupId='$ledGroupId' and (basics='1' or basics='2') limit 2" );
            while ( $row3 = MYSQLi_FETCH_ROW ( $erg3 ) )
            {
              if ($row3 [1] == "1") $ledFirstState = $row3 [0];
              else if ($row3 [1] == "2") $ledSecondState = $row3 [0];
            }
            
            if ($ledFirstState == "" || $ledSecondState == "") die ( "Led States nicht gefunden zu Gruppe $ledGroupId" );
              // $myClassesId
              // $dimmerClassesId
              // $rolloClassesId
              // $ledClassesId
              // $schalterClassesId

            $paramBrightnessId = - 1; 
            $paramDurationId = -1;             
            
            // dimmer events
            if ($myClassesId == $dimmerClassesId)
            {
              $evOnFunctionId = getClassesIdFunctionsIdByName ( $dimmerClassesId, "evOn" );
              $evOffFunctionId = getClassesIdFunctionsIdByName ( $dimmerClassesId, "evOff" );
              $paramBrightnessId = getClassesIdFunctionParamIdByName ( $dimmerClassesId, "evOn", "brightness" );
              $paramBrightnessValue = $signalParamWildcard;
            } 
            else if ($myClassesId == $schalterClassesId)
            {
              $evOnFunctionId = getClassesIdFunctionsIdByName ( $schalterClassesId, "evOn" );
              $evOffFunctionId = getClassesIdFunctionsIdByName ( $schalterClassesId, "evOff" );
              $paramDurationId = getClassesIdFunctionParamIdByName ( $schalterClassesId, "evOn", "duration" );
              $paramDurationValue = $signalParamWildcardWord;
            } 
            else if ($myClassesId == $ledClassesId || $myClassesId == $logicalButtonClassesId)
            {
              $evOnFunctionId = getClassesIdFunctionsIdByName ( $ledClassesId, "evOn" );
              $evOffFunctionId = getClassesIdFunctionsIdByName ( $ledClassesId, "evOff" );
              $paramBrightnessId = getClassesIdFunctionParamIdByName ( $ledClassesId, "evOn", "brightness" );
              $paramBrightnessValue = $signalParamWildcard;
            } 
            else if ($myClassesId == $rolloClassesId) die ( "LED-FEEDBACK NOCH NICHT IMPLEMENTIERT FüR ROLLOS" );
            
            // Einschalten bei evOn
            QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,baseRule,ledFeedbackIndent,`generated`) 
                                 values('$ledGroupId','7','31','255','7','31','255','$ledFirstState','$ledSecondState','1','$ledFeedbackIndent','1')" );
            $ruleId = query_insert_id ();
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                       values('$ruleId','$myInstanceId','$evOnFunctionId','1')" );
            $signalId = query_insert_id ();
            
            if ($paramBrightnessId != - 1) QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                              values('$signalId','$paramBrightnessId','$paramBrightnessValue','1')" );

            if ($paramDurationId != - 1) QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                              values('$signalId','$paramDurationId','$paramDurationValue','1')" );
            
            
            $ledFunctionIdOn = getClassesIdFunctionsIdByName ( $ledClassesId, "on" );
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                       values('$ruleId','$ledInstanceId','$ledFunctionIdOn','1')" );
            $newRuleActionId = query_insert_id ();
            $ledParamBrightnessId = getClassesIdFunctionParamIdByName ( $ledClassesId, "on", "brightness" );
            $ledParamDurationId = getClassesIdFunctionParamIdByName ( $ledClassesId, "on", "duration" );
			
		    $myBrightness = $ledStatusBrightness;
			$ergBright = QUERY("select brightness from brightnessExceptions join featureInstances on (featureInstances.objectId=brightnessExceptions.objectId) where featureinstances.id='$ledInstanceId' limit 1");
			if ($rowBright = MYSQLi_FETCH_ROW($ergBright)) $myBrightness = $rowBright[0];

            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) 
                                            values('$newRuleActionId','$ledParamBrightnessId','$myBrightness','1')" );
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) 
                                            values('$newRuleActionId','$ledParamDurationId','0','1')" );
            
            // Ausschalten bei evOff
            QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,baseRule,ledFeedbackIndent,`generated`) 
                                 values('$ledGroupId','7','31','255','7','31','255','$ledSecondState','$ledFirstState','1','$ledFeedbackIndent','1')" );
            $ruleId = query_insert_id ();
            
            QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
         	                             values('$ruleId','$myInstanceId','$evOffFunctionId','1')" );
            
            $ledFunctionIdOff = getClassesIdFunctionsIdByName ( $ledClassesId, "off" );
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`)
            		                       values('$ruleId','$ledInstanceId','$ledFunctionIdOff','1')" );
          } else echoError("Kein LogicalButton: ".substr($ledInstanceId,1), $groupId, $obj->id, $actSignalInstanceId);
        }
      }
    }
    
    /*
     * if ($obj->ledStatus==4) // 4 = Gruppenstatus { // LEDs der Signale suchen $erg2=QUERY("select featureInstanceId from basicRuleSignals where ruleId='$obj->id' order by id"); while ($row=MYSQLi_FETCH_ROW($erg2)) { $actSignalInstanceId = $row[0]; if ($serverInstances[$actSignalInstanceId]==1) continue; // Kein Feedback f�tuelle Servertaster $signalClassesId = getClassesIdByFeatureInstanceId($actSignalInstanceId); if ($signalClassesId == $tasterClassesId) { $ledInstanceId = getLedForTaster($actSignalInstanceId); if (!showError($ledInstanceId, $obj->id, $groupId, $actSignalInstanceId)) { // Standardstates auslesen $firstState=""; $secondState=""; $erg3 = QUERY("select id,basics from groupStates where groupId='$groupId' and (basics='1' or basics='2') limit 2"); while ($row3=MYSQLi_FETCH_ROW($erg3)) { if ($row3[1]=="1") $firstState=$row3[0]; else if ($row3[1]=="2") $secondState=$row3[0]; } if ($firstState=="" || $secondState=="") die("States nicht gefunden zu Gruppe $groupId"); // ON Regeln finden $ledFunctionIdOn = getClassesIdFunctionsIdByName($ledClassesId,"on"); $ledParamBrightnessId = getClassesIdFunctionParamIdByName($ledClassesId,"on","brightness"); $ledParamDurationId = getClassesIdFunctionParamIdByName($ledClassesId,"on","duration"); $erg3 = QUERY("select id from rules where groupId='$groupId' and resultingStateId='$secondState'"); while ($row3=MYSQLi_FETCH_ROW($erg3)) { $ruleId=$row3[0]; QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$ledInstanceId','$ledFunctionIdOn','1')"); $newRuleActionId=query_insert_id(); QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','$ledParamBrightnessId','$ledStatusBrightness','1')"); QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','$ledParamDurationId','0','1')"); } // OFF Regeln finden $ledFunctionIdOff = getClassesIdFunctionsIdByName($ledClassesId,"off"); $erg3 = QUERY("select id from rules where groupId='$groupId' and resultingStateId='$firstState'"); while ($row3=MYSQLi_FETCH_ROW($erg3)) { $ruleId=$row3[0]; QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$ledInstanceId','$ledFunctionIdOff','1')"); } } else echo "Kein LogicalButton $actSignalInstanceId <br>"; } } }
     */
  }
}

function echoError($message, $groupId, $basicRuleId, $signalInstanceId)
{
	$erg = QUERY("select name from groups where id='$groupId' limit 1");
	$row=MYsqli_fetch_row($erg);
	$groupName = $row[0];

	$erg = QUERY("select name from featureInstances where id='$signalInstanceId' limit 1");
	$row=MYsqli_fetch_row($erg);
	$instanceName = $row[0];
	
	echo "Fehler: ".$message." in <a href='editBaseConfig.php?groupId=$groupId' target='_blank'>Basisregel $groupId</a> und Instanz $instanceName <br>";
}

function getControllerFeatureInstanceIdForControllerId($controllerId)
{
  $erg = QUERY ( "select featureInstances.id from featureInstances join controller on (controller.objectId=featureInstances.objectId) where controller.id='$controllerId' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
    return $row [0];
}

/*
 Idee: Wenn mehrere Signale einen Aktor steuern, kann man das problemlos die Gruppe des Aktors regeln, 
       auch wenn ein Signal später in eine Multigruppen rausgezogen wird, wird alles über die Synchronisationsevents synchronisiert. 
       Wenn aber ein Signal mehrere Aktoren ansteuern, muss es ein Statechart für Aktoren gemeinsam gehen. 
       Dafür wird dann eine eigene Multigruppe erstellt. Kriterium ist also ob ein Signal mehrere Aktoren schaltet. 
       Zusätzlich zu den DummyEvents gibt es dann die GroupStates über die der Gruppenzustand und das LED-Feedback gesteuert wird
 */
function generateMultiGroups()
{
  global $CONTROLLER_CLASSES_ID;
  global $debug;
  global $signalParamWildcard,$signalParamWildcardWord;
  global $dimmerClassesId, $rolloClassesId, $ledClassesId, $schalterClassesId, $irClassesId, $tasterClassesId, $logicalButtonClassesId, $pcServerClassesId;
  
  //$debug=1;
  
  if ($debug == 1) echo "Generiere Autogruppen <br>";
  
  // Regeln mit Gruppensignalen oder Systemvariablen nicht gruppieren
  $erg = QUERY("select rules.id from rules join rulesignals on (rulesignals.ruleId=rules.id) where featureInstanceId < 0");
  while($row=MYSQLi_FETCH_ROW($erg))
  {
  	 $groupSignalRules[$row[0]]=1;
  }
  
  $erg = QUERY ("select activationStateId,resultingStateId,
                        ruleactions.featureInstanceId as actionFeatureId, 
                        rulesignals.id as ruleSignalId, rulesignals.featureInstanceId as signalFeatureId, rulesignals.featureFunctionId as signalFunctionId,
                        groups.id as groupId, completeGroupFeedback,
                        rules.id as ruleId
                        from ruleactions 
                        join rules on (rules.id=ruleactions.ruleId) 
                        join ruleSignals on (ruleSignals.ruleId = rules.id) 
                        join groups on (rules.groupId=groups.id) 
                        where  single=1 and groups.generated=0 and groupLock=0 order by groups.id" ); // single=1 and and groupLock=0
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
  	if ($groupSignalRules[$obj->ruleId]==1) continue;
  	
    $key = $obj->signalFeatureId;
    
    $myClassesId = getClassesIdByFeatureInstanceId ( $obj->signalFeatureId );
    if ($myClassesId == $CONTROLLER_CLASSES_ID) continue; // Controllerevents nicht gruppieren (EvTime usw)
    if ($myClassesId == $dimmerClassesId) continue; // Dimmerevents nicht gruppieren
    if ($myClassesId == $ledClassesId) continue; // Ledevents nicht gruppieren
    
    // wenns unabhängig vom state ist und kein Feedback benötigt wird -> nicht rausziehen
    if ($obj->activationStateId == 0 && $obj->completeGroupFeedback==0) continue; 
      
    // TODO hier nur Taster und IR zulassen ?
    
    $actionClassesId = getClassesIdByFeatureInstanceId ( $obj->actionFeatureId );
    
    if ($actionClassesId == $ledClassesId) continue; // LEDs als Aktor nicht gruppieren
    if ($actionClassesId == $logicalButtonClassesId) continue; // LogicalButtons nicht gruppieren
    if ($actionClassesId == $pcServerClassesId) continue; // PcServer nicht gruppieren
    if ($actionClassesId == $tasterClassesId) continue; // Taster als Aktor nicht gruppieren
    //if ($actionClassesId == $rolloClassesId) continue; // Testweise auch Rollos als Aktor rausgenommen
    
    // TODO hier ggf. auch generell nur Schalter und Dimmer zulassen ?
      
      // Bei Infrarot müssen zur Unterscheidung noch die Parameter rein
    if ($myClassesId == $irClassesId)
    {
      $signalParams = "";
      $erg2 = QUERY ( "select paramValue from ruleSignalParams where ruleSignalId='$obj->ruleSignalId' order by featureFunctionParamsId" );
      while ( $row = MYSQLi_FETCH_ROW ( $erg2 ) )
      {
        if ($signalParams != "") $signalParams .= "-";
        $signalParams .= $row [0];
      }
      
      $key = $obj->signalFeatureId . "-" . $signalParams;
    }
    
    // ID: 0000051 Rollos nur mit Rollos gruppieren
    if ($actionClassesId == $rolloClassesId) $key .= "-rollo";
    
    //if ($key == "237") $aktor [$key] [$obj->actionFeatureId] .= $obj->ruleSignalId . "(" . $obj->groupId . "),";
    //else 
    $aktor [$key] [$obj->actionFeatureId] .= $obj->ruleSignalId . ",";
    $aktorView [$obj->actionFeatureId] [$obj->signalFeatureId] = 1;
    
    $actionClassForSignal [$obj->ruleSignalId] = $actionClassesId;
  }
  
  QUERY ( "update ruleSignals set groupAlias='0'" );
  
  
  // Jetzt noch prüfen, ob beide Aktoren nicht nur mit dem gleichen Sensor, sondern auch mit dem gleichen eventType angesteuert werden
  foreach ( $aktor as $signalFeatureId => $actionIdArr )
  {
    if (count ( $actionIdArr ) > 1)
    {
      if ($debug == 1) echo "Signal $signalFeatureId steuert ";
      foreach ( $actionIdArr as $actionFeatureId => $ruleSignalId )
      {
        if ($debug == 1) echo "$actionFeatureId in Gruppe $groupId [$ruleSignalId] , ";
        if ($debug == 1) echo "anzahl signalFeatures = " . count ( $aktorView [$actionFeatureId] ) . ", ";
        if ($debug == 1) echo "Groupalias: $groupAlias <br>";
        
        $parts = explode ( ",", $ruleSignalId );
        foreach ( $parts as $actRuleSignalId )
        {
          if ($actRuleSignalId > 0)
          {
          	$erg77 = QUERY("select featureFunctionId from ruleSignals where id='$actRuleSignalId' limit 1");
          	$row77 = MYSQLi_FETCH_ROW($erg77);
          	
          	if ($row77[0]==43) $mySignalTypes[$actionFeatureId][]=2; // covered wie clicked behandeln
          	else $mySignalTypes[$actionFeatureId][]=$row77[0];
          }
          // QUERY("update rules set groupAlias='$groupAlias' where groupId='$groupId'");
        }
      }

      $foundDuplicate=0;
      foreach ($mySignalTypes as $actionFeature => $signals)
      {
         foreach ($mySignalTypes as $otherActionFeature => $otherSignals)
         {
         	  if ($actionFeature == $otherActionFeature) continue;
         	  
         	  foreach ($signals as $signal)
         	  {
 	         	  foreach ($otherSignals as $otherSignal)
         	    {
         	    	  if ($signal == $otherSignal)
         	    	  {
         	    	  	$foundDuplicate=1;
         	    	  	break;
         	    	  }
         	    }
         	    if ($foundDuplicate==1) break;
         	  }
         	  if ($foundDuplicate==1) break;
         }
         if ($foundDuplicate==1) break;
      }

      if ($foundDuplicate!=1)
      {
      	 if ($debug == 1) 
      	 {
      	 	  echo "Keine Duplicate !";
      	 	  print_r($mySignalTypes);
      	 }
      	 
      	 unset($aktor[$signalFeatureId]);
      }
      
      unset($mySignalTypes);
      
      if ($debug == 1) echo "<br>";
    }
  }
 
  $groupAlias = 1;
  foreach ( $aktor as $signalFeatureId => $actionIdArr )
  {
    if (count ( $actionIdArr ) > 1)
    {
      if ($debug == 1) echo "Signal $signalFeatureId steuert ";
      foreach ( $actionIdArr as $actionFeatureId => $ruleSignalId )
      {
        if ($debug == 1) echo "$actionFeatureId in Gruppe $groupId [$ruleSignalId] , ";
        if ($debug == 1) echo "anzahl signalFeatures = " . count ( $aktorView [$actionFeatureId] ) . ", ";
        if ($debug == 1) echo "Groupalias: $groupAlias <br>";
        
        $parts = explode ( ",", $ruleSignalId );
        foreach ( $parts as $actRuleSignalId )
        {
          if ($actRuleSignalId > 0)
          {
            QUERY ( "update ruleSignals set groupAlias='$groupAlias' where id='$actRuleSignalId' limit 1" );
            $aliasActionClass [$groupAlias] = $actionClassForSignal [$actRuleSignalId];
          }
          // QUERY("update rules set groupAlias='$groupAlias' where groupId='$groupId'");
        }
      }
      
      if ($debug == 1) echo "<br>";
      $groupAlias ++;
    }
  }
  
  $erg = QUERY ( "select distinct groupAlias from ruleSignals where groupAlias>0 order by id" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    $basicStateNames = getBasicStateNames ( $aliasActionClass [$obj->groupAlias] ); // einfach einen aktor nehmen
    $offName = $basicStateNames->offName;
    $onName = $basicStateNames->onName;
    
    $isRolloGroup = $aliasActionClass [$obj->groupAlias]==$rolloClassesId;
    
    if ($isRolloGroup)
    {
    	  $offName="steht";
    	  $onName="fährt";
    }

    // Wenn wir keine Statewechsel und kein LED Feedback haben, brauchen wir auch keine Synchronisationsevents
    $foundStateChanges = 0;
    
    // Neue Gruppe erstellen
    if ($debug == 1) echo "Erstelle Gruppe Generated $obj->groupAlias <br>";
    QUERY ( "INSERT into groups (name,single,`generated`) values('Generated $obj->groupAlias','0','1')" );
    $newGroupId = query_insert_id ();
    
    QUERY ( "INSERT into groupStates (groupId,name, value,basics,`generated`) values ('$newGroupId','$offName','1','1','1')" );
    $activationStateId = query_insert_id ();
    QUERY ( "INSERT into groupStates (groupId,name, value,basics,`generated`) values ('$newGroupId','$onName','2','2','1')" );
    $resultingStateId = query_insert_id ();
    
    // Wenn es in der neuen Gruppe verschiedene Classes existieren, machen wir aus evCovered ein evClicked
    $convertCoveredEvent = 0;
    $lastClass = "";
    $erg7 = QUERY ( "select distinct (ruleActions.featureInstanceId) from ruleActions join rules on (rules.id=ruleActions.ruleId) join ruleSignals on (ruleSignals.ruleId=rules.id) where ruleSignals.groupAlias='$obj->groupAlias'" );
    while ( $row = MYSQLi_FETCH_ROW ( $erg7 ) )
    {
      $actClassesId = getClassesIdByFeatureInstanceId ( $row [0] );
      if ($lastClass == "") $lastClass = $actClassesId;
      else if ($lastClass != $actClassesId)
      {
        // echo "$lastClass , $actClassesId <br>";
        $lastClass = $actClassesId;
        $convertCoveredEvent = 1;
      }
    }
    // echo "---- <br>";
    
    // unset($myOffInstances);
    
    $erg7 = QUERY ( "select id from ruleSignals where groupAlias='$obj->groupAlias' order by id" );
    while ( $row = MYSQLi_FETCH_ROW ( $erg7 ) )
    {
      $signalId = $row [0];
      
      // Features eintragen
      $erg2 = QUERY ( "select ruleActions.featureInstanceId from ruleActions join rules on (ruleActions.ruleId = rules.id) join ruleSignals on (ruleSignals.ruleId = rules.id) where ruleSignals.id='$signalId' limit 1" );
      if ($obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ))
      {
        QUERY ( "delete from groupFeatures where featureInstanceId='$obj2->featureInstanceId' and groupId='$newGroupId' limit 1" );
        QUERY ( "INSERT into groupFeatures (featureInstanceId, groupId,`generated`) values('$obj2->featureInstanceId','$newGroupId','1')" );
      } else die ( "Fehler: ActionInstance nicht gefunden zu GroupAlias $obj->groupAlias" );
        
        // Regeln mischen die für die signals markiert wurden
      $extras = "";
      $erg2 = QUERY ( "select rules.*, activation.basics as startBasics, resulting.basics as resultingBasics from rules join ruleSignals on (ruleSignals.ruleId=rules.id) left join groupStates as activation on (activation.id = rules.activationStateId) left join groupStates as resulting on (resulting.id = rules.resultingStateId) where ruleSignals.id='$signalId' limit 1" );
      if ($obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ))
      {
        if ($obj2->extras != "") $extras = $obj2->extras;
        
        $myActivationStateId = "0";
        if ($obj2->startBasics == "1") $myActivationStateId = $activationStateId;
        else if ($obj2->startBasics == "2") $myActivationStateId = $resultingStateId;
        
        $myResultingStateId = "0";
        if ($obj2->resultingBasics == "1") $myResultingStateId = $activationStateId;
        else if ($obj2->resultingBasics == "2") $myResultingStateId = $resultingStateId;
        
        $erg3 = QUERY ( "select * from ruleSignals where id='$signalId' limit 1" );
        if ($obj3 = MYSQLi_FETCH_OBJECT ( $erg3 ))
        {
          $signalId = $obj3->id;
          $signalFeatureInstanceId = $obj3->featureInstanceId;
          $signalFeatureFunctionId = $obj3->featureFunctionId;
          
          $completeGroupFeedback = $obj3->completeGroupFeedback;
          
          // Beim Taster aus evCovered evClicked machen, wenn verschiedene Classes angesteuert werden
          if ($convertCoveredEvent == 1 && $signalFeatureFunctionId == 43) $signalFeatureFunctionId = 2;
        } else die ( "Signal ID $signalId nicht gefunden" );
          
        // Prüfen ob es die passende Regel schon gibt
        // echo "select rules.id from rules join ruleSignals on (ruleSignals.ruleId=rules.id) where activationStateId='$myActivationStateId' and groupId='$newGroupId' and featureInstanceId='$signalFeatureInstanceId' and featureFunctionId='$signalFeatureFunctionId' limit 1 <br>";
        $erg3 = QUERY ( "select rules.id from rules join ruleSignals on (ruleSignals.ruleId=rules.id) where activationStateId='$myActivationStateId' and groupId='$newGroupId' and featureInstanceId='$signalFeatureInstanceId' and featureFunctionId='$signalFeatureFunctionId' limit 1" );
        if ($row3 = MYSQLi_FETCH_ROW ( $erg3 ))
        {
          $newRuleId = $row3 [0];
          // echo "gefunden $newRuleId <br>";
        } 
        else
        {
          QUERY ( "INSERT into rules (groupId,activationStateId,resultingStateId,startDay,startHour,startMinute,endDay,endHour,endMinute,signalType,baseRule,`generated`, intraDay)
    	                     values('$newGroupId','$myActivationStateId','$myResultingStateId','$obj2->startDay','$obj2->startHour','$obj2->startMinute','$obj2->endDay','$obj2->endHour','$obj2->endMinute','$obj2->signalType','$obj2->baseRule','1','$obj2->intraDay')" );
          $newRuleId = query_insert_id ();
          if ($myResultingStateId != 0 || $myActivationStateId != 0) $foundStateChanges = 1;
            
            // echo "neu $newRuleId <br>";
            
          // Signale eintragen
          
          QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$newRuleId','$signalFeatureInstanceId','$signalFeatureFunctionId','1')" );
          $newSignalId = query_insert_id ();
          
          $erg4 = QUERY ( "select * from ruleSignalParams where ruleSignalId='$signalId' order by id" );
          while ( $obj4 = MYSQLi_FETCH_OBJECT ( $erg4 ) )
          {
            QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$newSignalId','$obj4->featureFunctionParamsId','$obj4->paramValue','1')" );
          }
        }
        
        // Actions eintragen
        $erg3 = QUERY ( "select * from ruleActions where ruleId='$obj2->id' order by id" );
        while ( $obj3 = MYSQLi_FETCH_OBJECT ( $erg3 ) )
        {
          // if ($obj2->offRule==1) $myOffInstances[$obj3->featureInstanceId]=1;
          
          QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$newRuleId','$obj3->featureInstanceId','$obj3->featureFunctionId','1')" );
          $newSignalId = query_insert_id ();
          
          $erg4 = QUERY ( "select * from ruleActionParams where ruleActionId='$obj3->id' order by id" );
          while ( $obj4 = MYSQLi_FETCH_OBJECT ( $erg4 ) )
          {
            QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newSignalId','$obj4->featureFunctionParamsId','$obj4->paramValue','1')" );
          }
        }
      } else die ( "FEhler: keine regel zu signal id $signalId gefunden" );
    }
    
    if ($extras == "Rotation") generateRotation ( $newGroupId );
      
      // Synchronisationsevents generieren
    if ($isRolloGroup) generateSyncEventsRollladen($newGroupId, $activationStateId, $resultingStateId);
    else if ($foundStateChanges == 1 || $completeGroupFeedback > 0) generateSyncEvents ( $newGroupId, $completeGroupFeedback );
  } // For multigroups
}

function generateSignalGroup($groupId)
{
  // echo "generateSignalGroup $groupId <br>";
  global $CONTROLLER_CLASSES_ID;
  global $debug;
  global $signalParamWildcard,$signalParamWildcardWord;
  global $dimmerClassesId, $rolloClassesId, $ledClassesId, $schalterClassesId, $irClassesId, $tasterClassesId, $logicalButtonClassesId;
  
  $evGroupOnFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOn" );
  $evGroupOffFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOff" );
  $evGroupUndefinedFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined" );
  
  QUERY ( "DELETE from groupSyncHelper where groupId='$groupId'" );
  
  $erg = QUERY ( "select name,groupType, forcedController from groups where id='$groupId' limit 1" );
  $row = MYSQLi_FETCH_row ( $erg );
  $myGroupName = $row [0];
  $name = query_real_escape_string ( "Generated " . $row [0] );
  $groupType = $row [1];
  $forcedController = $row[2];
  
  QUERY ( "insert into groups (single,name,`generated`,forcedController) values('0','$name','1','$forcedController')" );
  $newGroupId = query_insert_id ();
  
  // Jedem Status der Gruppe einen Index zuweisen
  $index = 0;
  $groupThreshold = 0;
  unset ( $indexList );
  $erg2 = QUERY ( "select activationStateId from rules join ruleSignals on(ruleSignals.ruleId = rules.id) where groupId='$groupId' order by activationStateId" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg2 ) )
  {
    if ($row [0] % 2 == 0)
    {
      $nr = ( int ) ($row [0] / 10);
      $indexList [$nr] = $index ++;
    }
  }
  
  $totalMembers = count ( $indexList );
  if ($totalMembers == 0)
  {
	if (strpos($myGroupName,"Nachtabsenkung")===FALSE) echo "Keine Member in $groupId <br>";
    return;
  }
  
  if ($totalMembers > 256) die ( "Fehler: Signalgruppe mit mehr als 256 Membern gefunden -> $totalMembers" );
    
  // Passende Gruppe(n) erstellen
  // Wenn mehr als 16 Member beteiligt sind müssen wir Gruppen kaskadieren
  // Zum Verwalten des Gruppenstatus wird dann eine Obergruppe verwendet, die als Member so viele Untergruppen verwendet, wie Aktoren im Spiel sind.
  // Maximal also 16x16 = 256 Member
  $nrNeededGroups = ( int ) ($totalMembers / 16);
  if ($totalMembers % 16 > 0) $nrNeededGroups ++;
  if ($nrNeededGroups > 1) $schachtelNeeded = 1;
  
  unset ( $syncGroups );
  for($i = 0; $i < $nrNeededGroups; $i ++)
  {
    // Freie Gruppe auf irgend einem Controller suchen.
	$resultObj = registerSignalGroupOnController($groupId, "sensor");
	$controllerId = $resultObj->controllerId;
	$myGroupIndex = $resultObj->groupIndex;
	$controllerInstanceId = $resultObj->controllerInstanceId;
	
	/*
    $controllerId = - 1;
    $erg2 = QUERY ( "select id from controller where size!='999' and online='1'" );
    while ( $row = MYSQLi_FETCH_ROW ( $erg2 ) )
    {
      $controllerId = $row [0];
      
      $myGroupIndex = 0;
      $erg3 = QUERY ( "select groupIndex from groupSyncHelper where controllerId='$controllerId' order by groupIndex desc limit 1" );
      if ($row3 = MYSQLi_FETCH_ROW ( $erg3 )) $myGroupIndex = $row3 [0] + 1;
        
        // Controller ist schon voll.
      if ($myGroupIndex > 15)
      {
        $controllerId = - 1;
        continue;
      } else break;
    }
    
    if ($controllerId == - 1) die ( "Fehler: Keine freie Gruppe fü Signalgruppe gefunden" );
    
    // Gruppe reservieren
    QUERY ( "INSERT into groupSyncHelper (controllerId, groupIndex, groupId) values('$controllerId','$myGroupIndex','$newGroupId')" );
    
    // Controller als Instanz suchen
    $erg2 = QUERY ( "select id from featureInstances where controllerId='$controllerId' and featureClassesId='$CONTROLLER_CLASSES_ID' limit 1" );
    if ($row = MYSQLi_FETCH_ROW ( $erg2 )) $controllerInstanceId = $row [0];
    else die ( "Fehler: Controller Instance zu controllerId $controllerId nicht gefunden" );
      */
	  
	//echo "HERM C: $groupId - $controllerId - $myGroupIndex <br>";
	 
      // Threshold der Gruppe anhand der Member definieren
    if ($i < $nrNeededGroups - 1) $groupThreshold = 16;
    else $groupThreshold = $totalMembers % 16;
    
    $syncGroups [$i] ["controllerId"] = $controllerId;
    $syncGroups [$i] ["controllerInstanceId"] = $controllerInstanceId;
    $syncGroups [$i] ["groupThreshold"] = $groupThreshold;
    $syncGroups [$i] ["myGroupIndex"] = $myGroupIndex;
    
    // Nach der letzten normalen Gruppe erstellen wir noch die Schaltungsgruppe
    if ($i == $nrNeededGroups - 1 && $schachtelNeeded == 1)
    {
      $schachtelNeeded = 0;
      $nrNeededGroups ++;
    }
  }
  
  // Pro Status die Aktivierungs und Deaktivierungsevents eintragen und damit Bit in Syncgruppe schalten
  $syncActorDone = "";
  $erg2 = QUERY ( "select rules.id, activationStateId from rules join ruleSignals on(ruleSignals.ruleId = rules.id) where groupId='$groupId' order by activationStateId" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg2 ) )
  {
    $ruleId = $row [0];
    if ($syncActorDone [$ruleId] == 1) continue;
    $syncActorDone [$ruleId] = 1;
    
    $activationStateId = $row [1];
    
    $nr = ( int ) ($activationStateId / 10);
    if ($activationStateId % 2 == 0) $active = 1;
    else $active = 0;
    
    $actFeatureIndex = $indexList [$nr];
    $actGroupIndex = ( int ) ($actFeatureIndex / 16);
    $controllerId = $syncGroups [$actGroupIndex] ["controllerId"];
    $controllerInstanceId = $syncGroups [$actGroupIndex] ["controllerInstanceId"];
    $groupThreshold = $syncGroups [$actGroupIndex] ["groupThreshold"];
    $myGroupIndex = $syncGroups [$actGroupIndex] ["myGroupIndex"];
    $actFeatureIndex = $indexList [$nr] % 16;
    
    // Regel
    QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,syncEvent,`generated`) 
                      values('$newGroupId','7','31','255','7','31','255','0','0','evOn','1','1','1')" );
    $newRuleId = query_insert_id ();
    
    QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$newRuleId','$controllerInstanceId','169','1')" );
    $newRuleActionId = query_insert_id ();
    QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','326','$myGroupIndex','1')" );
    QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','327','$actFeatureIndex','1')" );
    QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','328','$active','1')" );
    QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','349','$groupThreshold','1')" );
    
    // Signale ergänzen
    $erg3 = QUERY ( "select * from ruleSignals where ruleId='$ruleId'" );
    while ( $obj3 = MYSQLi_FETCH_object ( $erg3 ) )
    {
      // EV-Time vom Controller (Zeitsteuerung)
      if ($obj3->featureInstanceId==0) $obj3->featureInstanceId = $controllerInstanceId;
 		
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                               values('$newRuleId','$obj3->featureInstanceId','$obj3->featureFunctionId','1')" );
      $signalId = query_insert_id ();
      
      $erg4 = QUERY ( "select * from ruleSignalParams where ruleSignalId='$obj3->id'" );
      while ( $obj4 = MYSQLi_FETCH_object ( $erg4 ) )
      {
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`)
                                 values('$signalId','$obj4->featureFunctionParamsId','$obj4->paramValue','1')" );
      }
    }
  }
  
  // Wenn mehr als eine Gruppe nötig war, haben wir geschachtelt.
  // Dann hier die Subevents eintragen und später die Gesamtgruppe als Eventgeber eintragen
  if ($nrNeededGroups > 1)
  {
    $globalGroupIndex = $syncGroups [$nrNeededGroups - 1] ["myGroupIndex"];
    $globalControllerInstanceId = $syncGroups [$nrNeededGroups - 1] ["controllerInstanceId"];
    
    $myGroupIndex = $globalGroupIndex;
    $controllerInstanceId = $globalControllerInstanceId;
    
    $nrSubGroups = $nrNeededGroups - 1;
    
    // Synchronisationsevents für Subgruppen eintragen
    for($i = 0; $i < $nrSubGroups; $i ++)
    {
      $myGroupIndex = $syncGroups [$i] ["myGroupIndex"];
      $controllerInstanceId = $syncGroups [$i] ["controllerInstanceId"];
      
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`) values('$newGroupId','7','31','255','7','31','255','0','0','evOn','1','1')" );
      $actRuleId = query_insert_id ();
      
      if ($groupType == "SIGNALS-OR")
      {
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupUndefinedFunctionId','1')" );
        $actSignalId = query_insert_id ();
        $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined", "index" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      }
      
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupOnFunctionId','1')" );
      $actSignalId = query_insert_id ();
      $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOn", "index" );
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$globalControllerInstanceId','169','1')" );
      $newRuleActionId = query_insert_id ();
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','326','$globalGroupIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','327','$i','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','328','1','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','349','$nrSubGroups','1')" );
      
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`) values('$newGroupId','7','31','255','7','31','255','0','0','evOff','1','1')" );
      $actRuleId = query_insert_id ();
      
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupOffFunctionId','1')" );
      $actSignalId = query_insert_id ();
      $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOff", "index" );
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      
      if ($groupType == "SIGNALS-AND")
      {
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupUndefinedFunctionId','1')" );
        $actSignalId = query_insert_id ();
        $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined", "index" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      }
      
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$globalControllerInstanceId','169','1')" );
      $newRuleActionId = query_insert_id ();
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','326','$globalGroupIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','327','$i','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','328','0','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','349','$nrSubGroups','1')" );
    }
  }
  
  if ($nrNeededGroups > 1)
  {
    $myGroupIndex = $globalGroupIndex;
    $controllerInstanceId = $globalControllerInstanceId;
  }

  // Hier jetzt noch eine Heizungsaktorgruppe generieren, damit die anschließend auch mit den Gruppenevents gefüllt wird
  if (strpos($name,"Generated Generated Heating Signals")!==FALSE)
  {
   	 $actPump = substr($name, strrpos($name," ")+1);
  	 
   	 QUERY("INSERT into groups (name,single,`generated`, active)
                         values('Generated Heating Pump $actPump','0','1','1')");
     $pumpGroupId = query_insert_id();
	 
	 QUERY("INSERT into groupfeatures (groupId, featureInstanceId, generated) values('$pumpGroupId', '$actPump','1')");
    
     // anschalten   
     QUERY("INSERT into rules (groupId, activationStateId,startDay,startHour,startMinute,endDay,endHour,endMinute,`generated`)
                        values('$pumpGroupId','0','7','31','255','7','31','255','1')"); 
     $pumpRuleId = query_insert_id();
                               
     QUERY("INSERT into basicRuleGroupSignals (ruleId, groupId, eventType,`generated`) values('-$pumpRuleId','$groupId','ACTIVE','1')");
     $pumpGroupSignalId = query_insert_id();
          
     QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$pumpRuleId','-$pumpGroupSignalId','0','1')");
       
     QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$pumpRuleId','$actPump','60','1')" );
     $newRuleActionId = query_insert_id ();
     QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','265','0','1')" );
    
    
     // ausschalten   
     QUERY("INSERT into rules (groupId, activationStateId,startDay,startHour,startMinute,endDay,endHour,endMinute,`generated`)
                        values('$pumpGroupId','0','7','31','255','7','31','255','1')"); 
     $pumpRuleId = query_insert_id();
                               
     QUERY("INSERT into basicRuleGroupSignals (ruleId, groupId, eventType,`generated`) 
                                       values('-$pumpRuleId','$groupId','DEACTIVE','1')");
     $pumpGroupSignalId = query_insert_id();
          
     QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                              values('$pumpRuleId','-$pumpGroupSignalId','0','1')");
       
     QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$pumpRuleId','$actPump','61','1')" );
  }
  
  // Hier jetzt noch eine Lüftungsaktorgruppe generieren, damit die anschließend auch mit den Gruppenevents gefüllt wird
  if (strpos($name,"Generated Generated Ventilation Signals")!==FALSE)
  {
   	 $actPump = substr($name, strrpos($name," ")+1);
  	 
   	 QUERY("INSERT into groups (name,single,`generated`, active)
                         values('Generated Ventilation Ventilator $actPump','0','1','1')");
     $pumpGroupId = query_insert_id();
    
     // anschalten   
     QUERY("INSERT into rules (groupId, activationStateId,startDay,startHour,startMinute,endDay,endHour,endMinute,`generated`)
                        values('$pumpGroupId','0','7','31','255','7','31','255','1')"); 
     $pumpRuleId = query_insert_id();
                               
     QUERY("INSERT into basicRuleGroupSignals (ruleId, groupId, eventType,`generated`) 
                                        values('-$pumpRuleId','$groupId','ACTIVE','1')");
     $pumpGroupSignalId = query_insert_id();
          
     QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                              values('$pumpRuleId','-$pumpGroupSignalId','0','1')");
       
     QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$pumpRuleId','$actPump','60','1')" );
     $newRuleActionId = query_insert_id ();
     QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','265','0','1')" );
    
    
     // ausschalten   
     QUERY("INSERT into rules (groupId, activationStateId,startDay,startHour,startMinute,endDay,endHour,endMinute,`generated`)
                        values('$pumpGroupId','0','7','31','255','7','31','255','1')"); 
     $pumpRuleId = query_insert_id();
                               
     QUERY("INSERT into basicRuleGroupSignals (ruleId, groupId, eventType,`generated`) 
                                       values('-$pumpRuleId','$groupId','DEACTIVE','1')");
     $pumpGroupSignalId = query_insert_id();
          
     QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                              values('$pumpRuleId','-$pumpGroupSignalId','0','1')");
       
     QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$pumpRuleId','$actPump','61','1')" );
   }


  // Dann die Gruppenevents in allen Regeln eintragen, die auf diese Gruppen referenzieren
  // $evGroupOnFunctionId $evGroupOffFunctionId $evGroupUndefinedFunctionId
  $erg = QUERY ( "select id,eventType,andCondition from basicrulegroupsignals where groupId='$groupId'" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
  	QUERY("update basicrulegroupsignals set controllerInstanceId='$controllerInstanceId', groupIndex='$myGroupIndex' where id='$row[0]' limit 1");
  	
  	// Bei AND Conditions generieren wir hier nichts
  	if ($row[2]==1) continue;
  	
    $searchFeatureInstanceId = $row [0] * - 1;
    
    $eventType = $row [1];
    // echo "SearchInstance = " . $searchFeatureInstanceId . "<br>";
    
    $erg2 = QUERY ( "select ruleId from ruleSignals where featureInstanceId='$searchFeatureInstanceId' order by id" );
    while ( $row2 = MYSQLi_FETCH_ROW ( $erg2 ) )
    {
      $actRuleId = $row2 [0];
      // echo $searchFeatureInstanceId . " in " . $actRuleId . "<br>";
      
      // Group-ON
      if ($eventType == "ACTIVE")
      {
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                            values('$actRuleId','$controllerInstanceId','$evGroupOnFunctionId','1')" );
        $actSignalId = query_insert_id ();
        $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOn", "index" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                            values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      }
      
      // Group-OFF
      if ($eventType == "DEACTIVE")
      {
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                          values('$actRuleId','$controllerInstanceId','$evGroupOffFunctionId','1')" );
        $actSignalId = query_insert_id ();
        $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOff", "index" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                   values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      }
      
      // Group-UNDEFINED
      // Was Signal undefined bewirken soll, hängt davon ab welches feedback gewählt war
      if (($eventType == "ACTIVE" && $groupType == "SIGNALS-OR") || ($eventType == "DEACTIVE" && $groupType == "SIGNALS-AND"))
      {
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                   values('$actRuleId','$controllerInstanceId','$evGroupUndefinedFunctionId','1')" );
        $actSignalId = query_insert_id ();
        $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined", "index" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                   values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      }
    }
  }
  
  // Dann noch den Controller als Feature der Gruppe eintragen, sonst wird sie nirgens gehostet
  QUERY ( "INSERT into groupFeatures (groupId, featureInstanceId,`generated`) values('$newGroupId','$controllerInstanceId','1')" );
}

function generateSyncEvents($groupId, $completeGroupFeedback)
{
  // echo "generateSyncEvents $groupId - $completeGroupFeedback <br>";
  global $CONTROLLER_CLASSES_ID;
  global $debug;
  global $signalParamWildcard,$signalParamWildcardWord;
  global $dimmerClassesId, $rolloClassesId, $ledClassesId, $schalterClassesId, $irClassesId, $tasterClassesId, $logicalButtonClassesId;
  
  $evGroupOnFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOn" );
  $evGroupOffFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOff" );
  $evGroupUndefinedFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined" );
  
  QUERY ( "DELETE from groupSyncHelper where groupId='$groupId'" );
  
  // Jedem Aktor der Gruppe einen Index zuweisen
  $index = 0;
  $groupThreshold = 0;
  unset ( $indexList );
  $erg2 = QUERY ( "select distinct featureInstanceId from ruleActions join rules on (rules.id=ruleActions.ruleId) where groupId='$groupId' order by featureInstanceId" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg2 ) )
  {
    $myClassesId = getClassesIdByFeatureInstanceId ( $row [0] );
    if ($myClassesId == $logicalButtonClassesId) continue;
    
    $indexList [$row [0]] = $index ++;
  }
  
  $totalMembers = count ( $indexList );
  if ($totalMembers == 0) return;
  if ($totalMembers > 256) die ( "Fehler: Gruppe mit mehr als 64 Membern gefunden -> $totalMembers" );
    
    // Passende Gruppe(n) erstellen
    // Wenn mehr als 16 Member beteiligt sind müssen wir Gruppen kaskadieren
    // Zum Verwalten des Gruppenstatus wird dann eine Obergruppe verwendet, die als Member so viele Untergruppen verwendet, wie Aktoren im Spiel sind.
    // Maximal also 16x16 = 256 Member
  $nrNeededGroups = ( int ) ($totalMembers / 16);
  if ($totalMembers % 16 > 0) $nrNeededGroups ++;
  if ($nrNeededGroups > 1) $schachtelNeeded = 1;
  
    
    // Wir versuchen den ersten Aktor als Host f� Gruppe zu verwenden
  //$erg4 = QUERY ( "select featureInstanceId from groupFeatures where groupId='$groupId' order by id limit 1" );
  //$row4 = MYSQLi_FETCH_ROW ( $erg4 );
  //$firstAktor = $row4 [0];
  
  unset ( $syncGroups );
  for($i = 0; $i < $nrNeededGroups; $i ++)
  {
	$resultObj = registerSignalGroupOnController($groupId, "aktor");
	$controllerId = $resultObj->controllerId;
	$myGroupIndex = $resultObj->groupIndex;
	$controllerInstanceId = $resultObj->controllerInstanceId;
	
	/*
    // Freie Gruppe auf Controller der beteiligten Aktoren suchen.
    // Dabei zun㢨st versuchen, das erste Feature zu treffen, weil submitRules die Regel auf den Controller des ersten Features packt.
    // Dadurch werden die setGroupState Aufrufe alle intern abgehandelt
    $controllerId = - 1;
    $erg2 = QUERY ( "select distinct(controllerId) from featureInstances join groupFeatures on (groupFeatures.featureInstanceId=featureInstances.id) join controller on (featureInstances.controllerId=controller.id) where groupId='$groupId' and size!=999 order by featureInstances.id='$firstAktor', featureInstances.id" );
    while ( $row = MYSQLi_FETCH_ROW ( $erg2 ) )
    {
      $controllerId = $row [0];
      
      $myGroupIndex = 0;
      $erg3 = QUERY ( "select groupIndex from groupSyncHelper where controllerId='$controllerId' order by groupIndex desc limit 1" );
      if ($row3 = MYSQLi_FETCH_ROW ( $erg3 )) $myGroupIndex = $row3 [0] + 1;
        
        // Controller ist schon voll.
      if ($myGroupIndex > 7)
      {
        $controllerId = - 1;
        continue;
      } else
        break;
    }
    
    if ($controllerId == - 1)
    {
    	echo "select distinct(controllerId) from featureInstances join groupFeatures on (groupFeatures.featureInstanceId=featureInstances.id) join controller on (featureInstances.controllerId=controller.id) where groupId='$groupId' and size!=999 order by featureInstances.id='$firstAktor', featureInstances.id<br>";
      echo "select distinct(controllerId) from featureInstances join groupFeatures on (groupFeatures.featureInstanceId=featureInstances.id) where groupId='$groupId' <br>";
      die ( "Fehler: keine freie Gruppe gefunden" );
    }
    
    // Gruppe reservieren
    QUERY ( "INSERT into groupSyncHelper (controllerId, groupIndex, groupId) values('$controllerId','$myGroupIndex','$groupId')" );
    
	
    // Controller als Instanz suchen
    $erg2 = QUERY ( "select id from featureInstances where controllerId='$controllerId' and featureClassesId='$CONTROLLER_CLASSES_ID' limit 1" );
    if ($row = MYSQLi_FETCH_ROW ( $erg2 )) $controllerInstanceId = $row [0];
    else die ( "Fehler: Controller Instance zu controllerId $controllerId nicht gefunden" );
      */
	  
  	//echo "HERM A: $groupId - $controllerId - $myGroupIndex <br>";

      // Threshold der Gruppe anhand der Member definieren
    if ($i < $nrNeededGroups - 1) $groupThreshold = 16;
    else $groupThreshold = $totalMembers % 16;
    
    $syncGroups [$i] ["controllerId"] = $controllerId;
    $syncGroups [$i] ["controllerInstanceId"] = $controllerInstanceId;
    $syncGroups [$i] ["groupThreshold"] = $groupThreshold;
    $syncGroups [$i] ["myGroupIndex"] = $myGroupIndex;
    
    // Nach der letzten normalen Gruppe erstellen wir noch die Schaltungsgruppe
    if ($i == $nrNeededGroups - 1 && $schachtelNeeded == 1)
    {
      $schachtelNeeded = 0;
      $nrNeededGroups ++;
    }
  }
  
  // Synchronisationsevents in Regeln eintragen
  $myOnStateId = "";
  $myOffStateId = "";
  
  // On und Off State aus der Gruppe auslesen
  $erg2 = QUERY ( "select ruleId,featureInstanceId,groupStates.basics as resultingBasics,groupStates.id as resultingStateId from ruleActions join rules on (rules.id=ruleActions.ruleId) left join groupStates on (groupStates.id=rules.resultingStateId) where rules.groupId='$groupId' order by ruleActions.id" );
  while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
  {
    if ($obj2->resultingBasics == "2") $myOnStateId = $obj2->resultingStateId;
    else if ($obj2->resultingBasics == "1") $myOffStateId = $obj2->resultingStateId;
  }
  /*
  $erg2 = QUERY ( "select value,basic from groupstates where groupId='$groupId' " );
  while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
  {
    if ($obj2->basic == "2") $myOnStateId = $obj->value;
    else if ($obj2->basic == "1") $myOffStateId = $obj->value;
  }
  */

  
  // Pro Aktor ein Event f� On-Zustand und Off-Zustand generieren und damit Bit in Syncgruppe schalten
  $syncActorDone = "";
  $erg2 = QUERY ( "select ruleId,featureInstanceId,groupStates.name as resultingStateName,groupStates.id as resultingStateId from ruleActions join rules on (rules.id=ruleActions.ruleId) left join groupStates on (groupStates.id=rules.resultingStateId) where rules.groupId='$groupId' order by ruleActions.id" );
  while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
  {
    $myClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
    if ($myClassesId == $logicalButtonClassesId) continue;
    
    $actFeatureIndex = $indexList [$obj2->featureInstanceId];
    $actGroupIndex = ( int ) ($actFeatureIndex / 16);
    $controllerId = $syncGroups [$actGroupIndex] ["controllerId"];
    $controllerInstanceId = $syncGroups [$actGroupIndex] ["controllerInstanceId"];
    $groupThreshold = $syncGroups [$actGroupIndex] ["groupThreshold"];
    $myGroupIndex = $syncGroups [$actGroupIndex] ["myGroupIndex"];
    $actFeatureIndex = $indexList [$obj2->featureInstanceId] % 16;
    
    if ($syncActorDone [$obj2->featureInstanceId] != 1)
    {
      $syncActorDone [$obj2->featureInstanceId] = 1;
      
      // Dummy f�n
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,syncEvent,`generated`) values('$groupId','7','31','255','7','31','255','0','0','evOn','1','1','1')" );
      $ruleId = query_insert_id ();
      
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$controllerInstanceId','169','1')" );
      $newRuleActionId = query_insert_id ();
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','326','$myGroupIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','327','$actFeatureIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','328','1','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','349','$groupThreshold','1')" );
      
      if ($myClassesId == $dimmerClassesId)
      {
        $evOnFunctionId = getClassesIdFunctionsIdByName ( $dimmerClassesId, "evOn" );
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','$evOnFunctionId','1')" );
        $signalId = query_insert_id ();
        
        $dimmerParamBrightnessId = getClassesIdFunctionParamIdByName ( $dimmerClassesId, "evOn", "brightness" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$signalId','$dimmerParamBrightnessId','$signalParamWildcard','1')" );
      } else if ($myClassesId == $rolloClassesId)
      {
        $statusFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "evOpen" );
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','$statusFunctionId','1')" );
      } else if ($myClassesId == $ledClassesId)
      {
        $evOnFunctionId = getClassesIdFunctionsIdByName ( $ledClassesId, "evOn" );
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','$evOnFunctionId','1')" );
        $signalId = query_insert_id ();
        
        $dimmerParamBrightnessId = getClassesIdFunctionParamIdByName ( $ledClassesId, "evOn", "brightness" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$signalId','$dimmerParamBrightnessId','$signalParamWildcard','1')" );
      } else if ($myClassesId == $schalterClassesId)
      {
        $evOnFunctionId = getClassesIdFunctionsIdByName ( $schalterClassesId, "evOn" );
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','$evOnFunctionId','1')" );
        
        $signalId = query_insert_id ();
        
        $schalterParamDurationId = getClassesIdFunctionParamIdByName ( $schalterClassesId, "evOn", "duration" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$signalId','$schalterParamDurationId','$signalParamWildcardWord','1')" );

      } else if ($myClassesId == 21)
      {
        echo "dummy f�ver fehlt noch <br>";
      } else if ($myClassesId == $tasterClassesId)
      {
        // hat keine Events
      } else
        die ( "nicht implementierte class $myClassesId -1 -> $obj2->ruleId" );
        
        // Dummy f�ff
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,syncEvent,`generated`) values('$groupId','7','31','255','7','31','255','0','0','evOff','1','1','1')" );
      $ruleId = query_insert_id ();
      
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$controllerInstanceId','169','1')" );
      $newRuleActionId = query_insert_id ();
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','326','$myGroupIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','327','$actFeatureIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','328','0','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','349','$groupThreshold','1')" );
      
      if ($myClassesId == $dimmerClassesId)
      {
        $evOffFunctionId = getClassesIdFunctionsIdByName ( $dimmerClassesId, "evOff" );
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','$evOffFunctionId','1')" );
      } else if ($myClassesId == $rolloClassesId)
      {
        $statusFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "evClosed" );
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','$statusFunctionId','1')" );
        $signalId = query_insert_id ();
        
        $dimmerParamBrightnessId = getClassesIdFunctionParamIdByName ( $rolloClassesId, "evClosed", "position" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$signalId','$dimmerParamBrightnessId','$signalParamWildcard','1')" );
      } else if ($myClassesId == $ledClassesId)
      {
        $evOffFunctionId = getClassesIdFunctionsIdByName ( $ledClassesId, "evOff" );
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','$evOffFunctionId','1')" );
      } else if ($myClassesId == $schalterClassesId)
      {
        $evOffFunctionId = getClassesIdFunctionsIdByName ( $ledClassesId, "evOff" );
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','$evOffFunctionId','1')" );
      } else if ($myClassesId == 21)
      {
        echo "dummy f�ver fehlt noch <br>";
      } else if ($myClassesId == $tasterClassesId)
      {
        // hat keine Events
      } else
        die ( "nicht implementierte class $myClassesId -2" );
    }
  }
  
  // Wenn mehr als eine Gruppe n� war, haben wir geschachtelt.
  // Dann hier die Subevents eintragen und sp㳥r die Gesamtgruppe als Eventgeber eintragen
  if ($nrNeededGroups > 1)
  {
    $globalGroupIndex = $syncGroups [$nrNeededGroups - 1] ["myGroupIndex"];
    $globalControllerInstanceId = $syncGroups [$nrNeededGroups - 1] ["controllerInstanceId"];
    
    $nrSubGroups = $nrNeededGroups - 1;
    
    // Synchronisationsevents f� Subgruppen eintragen
    for($i = 0; $i < $nrSubGroups; $i ++)
    {
      $myGroupIndex = $syncGroups [$i] ["myGroupIndex"];
      $controllerInstanceId = $syncGroups [$i] ["controllerInstanceId"];
      
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`) values('$groupId','7','31','255','7','31','255','0','0','evOn','1','1')" );
      $actRuleId = query_insert_id ();
      
      if ($completeGroupFeedback != 1)
      {
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupUndefinedFunctionId','1')" );
        $actSignalId = query_insert_id ();
        $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined", "index" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      }
      
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupOnFunctionId','1')" );
      $actSignalId = query_insert_id ();
      $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOn", "index" );
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$globalControllerInstanceId','169','1')" );
      $newRuleActionId = query_insert_id ();
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','326','$globalGroupIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','327','$i','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','328','1','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','349','$nrSubGroups','1')" );
      
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`) values('$groupId','7','31','255','7','31','255','0','0','evOff','1','1')" );
      $actRuleId = query_insert_id ();
      
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupOffFunctionId','1')" );
      $actSignalId = query_insert_id ();
      $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOff", "index" );
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      
      if ($completeGroupFeedback == 1)
      {
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupUndefinedFunctionId','1')" );
        $actSignalId = query_insert_id ();
        $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined", "index" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      }
      
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$globalControllerInstanceId','169','1')" );
      $newRuleActionId = query_insert_id ();
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','326','$globalGroupIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','327','$i','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','328','0','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','349','$nrSubGroups','1')" );
    }
    
    $myGroupIndex = $globalGroupIndex;
    $controllerInstanceId = $globalControllerInstanceId;
  }

  // Dann die Events der Synchronisationsgruppe eintragen um damit die Gruppenstates zu schalten
  // $evGroupOnFunctionId $evGroupOffFunctionId $evGroupUndefinedFunctionId
  QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`) 
                       values('$groupId','7','31','255','7','31','255','$myOffStateId','$myOnStateId','evOn','1','1')" );
  $actRuleId = query_insert_id ();
  $onRuleId = $actRuleId;
  
  QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                       values('$actRuleId','$controllerInstanceId','$evGroupOnFunctionId','1')" );
  $actSignalId = query_insert_id ();
  $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOn", "index" );
  QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
  
  QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`) values('$groupId','7','31','255','7','31','255','$myOnStateId','$myOffStateId','evOff','1','1')" );
  $actRuleId = query_insert_id ();
  $offRuleId = $actRuleId;
  
  QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupOffFunctionId','1')" );
  $actSignalId = query_insert_id ();
  $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOff", "index" );
  QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
  
  // Was Signal undefined bewirken soll, h㭧t davon ab welches feedback gew� war
  if ($completeGroupFeedback == 1) $actRuleId = $offRuleId;
  else $actRuleId = $onRuleId;
  
  QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupUndefinedFunctionId','1')" );
  $actSignalId = query_insert_id ();
  $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined", "index" );
  QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
  
}

function generateRotation($groupId)
{
  $firstInstance = "";
  $secondInstance = "";
  $erg = QUERY ( "select featureInstanceId from groupFeatures where groupId='$groupId' order by id" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    if ($firstInstance == "") $firstInstance = $row [0];
    else if ($secondInstance == "") $secondInstance = $row [0];
    else
    {
      echo "Rotation momentan nur mit 2 Aktoren implementiert. Ignoriere Rotation in Gruppe $groupId ... <br>";
      return;
    }
  }
  
  if ($firstInstance == "" || $secondInstance == "")
  {
    echo "Rotation momentan nur mit 2 Aktoren implementiert. Ignoriere Rotation in Gruppe $groupId ... <br>";
    return;
  }
  
  QUERY ( "INSERT into groupStates (groupId,name, value,basics,`generated`) values ('$groupId','1an','3','0','1')" );
  $firstOnStateId = query_insert_id ();
  QUERY ( "INSERT into groupStates (groupId,name, value,basics,`generated`) values ('$groupId','2an','4','0','1')" );
  $secondOnStateId = query_insert_id ();
  
  // Standardstates suchen
  $erg = QUERY ( "select id from groupStates where groupId='$groupId' and basics='1' limit 1" );
  $row = MYSQLi_FETCH_ROW ( $erg );
  $ausState = $row [0];
  
  $erg = QUERY ( "select id from groupStates where groupId='$groupId' and basics='2' limit 1" );
  $row = MYSQLi_FETCH_ROW ( $erg );
  $anState = $row [0];
  
  // Regel suchen, die anschaltet
  $erg = QUERY ( "select rules.id from rules join groupStates as activation on (activation.id = rules.activationStateId) left join groupStates as resulting on (resulting.id = rules.resultingStateId) where activation.basics=1 and resulting.basics=2 and rules.groupId='$groupId' limit 1" );
  $row = MYSQLi_FETCH_ROW ( $erg );
  $origRuleId = $row [0];
  
  QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,syncEvent,`generated`) 
                     values('$groupId','7','31','255','7','31','255','$ausState','$firstOnStateId','on','1','0','1')" );
  $ruleId = query_insert_id ();
  
  $erg = QUERY ( "select * from ruleSignals where ruleId='$origRuleId' order by id" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
  	                        values('$ruleId','$obj->featureInstanceId','$obj->featureFunctionId','1')" );
    $newSignalId = query_insert_id ();
    
    $erg2 = QUERY ( "select * from ruleSignalParams where ruleSignalId='$obj->id' order by id" );
    while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
    {
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                    values('$newSignalId','$obj2->featureFunctionParamsId','$obj2->paramValue','1')" );
    }
  }
  
  $erg = QUERY ( "select * from ruleActions where ruleId='$origRuleId' order by id" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    if ($obj->featureInstanceId == $firstInstance)
    {
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                values('$ruleId','$obj->featureInstanceId','$obj->featureFunctionId','1')" );
      $newActionId = query_insert_id ();
      
      $erg2 = QUERY ( "select * from ruleActionParams where ruleActionId='$obj->id' order by id" );
      while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
      {
        QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) 
                                      values('$newActionId','$obj2->featureFunctionParamsId','$obj2->paramValue','1')" );
      }
    }
  }
  
  QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,syncEvent,`generated`) 
                     values('$groupId','7','31','255','7','31','255','$firstOnStateId','$secondOnStateId','on','1','0','1')" );
  $ruleId = query_insert_id ();
  
  $erg = QUERY ( "select * from ruleSignals where ruleId='$origRuleId' order by id" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
  	                        values('$ruleId','$obj->featureInstanceId','$obj->featureFunctionId','1')" );
							
    $newSignalId = query_insert_id ();
    
    $erg2 = QUERY ( "select * from ruleSignalParams where ruleSignalId='$obj->id' order by id" );
    while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
    {
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                    values('$newSignalId','$obj2->featureFunctionParamsId','$obj2->paramValue','1')" );
    }
  }
  
  $erg = QUERY ( "select * from ruleActions where ruleId='$origRuleId' order by id" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
    if ($obj->featureInstanceId == $secondInstance)
    {
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                values('$ruleId','$obj->featureInstanceId','$obj->featureFunctionId','1')" );
      $newActionId = query_insert_id ();
      
      $erg2 = QUERY ( "select * from ruleActionParams where ruleActionId='$obj->id' order by id" );
      while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
      {
        QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) 
                                      values('$newActionId','$obj2->featureFunctionParamsId','$obj2->paramValue','1')" );
      }
    }
  }
  
  // States der originalRegel auswechseln
  QUERY ( "update rules set activationStateId='$secondOnStateId' where id='$origRuleId'" );
  
  // Zweiten Aktor rauswerfen
  $erg = QUERY ( "select id from ruleActions where ruleId='$origRuleId' and featureInstanceId='$secondInstance' limit 1" );
  $row = MYSQLi_FETCH_ROW ( $erg );
  deleteRuleAction ( $row [0], 0);
  
  // ersten Aktor ausschalten
  // Regel suchen, die ausschaltet
  $erg = QUERY ( "select rules.id from rules join groupStates as activation on (activation.id = rules.activationStateId) left join groupStates as resulting on (resulting.id = rules.resultingStateId) where activation.basics=2 and resulting.basics=1 and rules.groupId='$groupId' limit 1" );
  $row = MYSQLi_FETCH_ROW ( $erg );
  $origRuleId = $row [0];
  
  $erg = QUERY ( "select * from ruleActions where ruleId='$origRuleId' and featureInstanceId='$firstInstance' limit 1" );
  $obj = MYSQLi_FETCH_OBJECT ( $erg );
  
  QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                           values('$ruleId','$obj->featureInstanceId','$obj->featureFunctionId','1')" );
  $newActionId = query_insert_id ();
  
  $erg2 = QUERY ( "select * from ruleActionParams where ruleActionId='$obj->id' order by id" );
  while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
  {
    QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) 
                                     values('$newActionId','$obj2->featureFunctionParamsId','$obj2->paramValue','1')" );
  }
}
function showError($ledInstanceId, $basicRuleId, $groupId, $signalInstanceId)
{
  if (substr ( $ledInstanceId, 0, 1 ) == "E")
  {
    $error = substr ( $ledInstanceId, 1 );
    if (strpos ( $error, "offline" ) === FALSE)
    {
      echo "<hr>";
      echo "Fehler beim erstellen des LED-Feedbacks eines Aktors<br>";
      echo "Fehler: $error <br>";
      echo "Problemsignal: " . formatInstance ( $signalInstanceId ) . "<br>";
      echo "Problemregel: <a href='editBaseConfig.php?groupId=$groupId' target='_blank'>anzeigen</a> <br>";
      echo "<hr>";
    }
    return TRUE;
  }
  return FALSE;
}
function showRuleError($message, $groupId)
{
  echo "<hr>";
  echo "Fehler: $message <br>";
  echo "Problemregel: <a href='editBaseConfig.php?groupId=$groupId' target='_blank'>anzeigen</a> <br>";
  echo "<hr>";
}
function formatInstance($instanceId)
{
  $obj = getFeatureInstanceData ( $instanceId );
  if ($obj->roomName != "")
    $result = $obj->roomName . " » ";
  else
    $result = "Controller " . $obj->controllerName . " » ";
  $result .= $obj->featureInstanceName;
  return $result;
}

function getFeatureInstanceData($instanceId)
{
  $erg = QUERY ( "select featureInstances.name as featureInstanceName, featureInstances.objectId as objectId,
	                      controller.name as controllerName, controller.id as controllerId, 
	                      rooms.name as roomName,
	                      featureClasses.name as featureClassName
	                      from featureInstances 
	                      join controller on (controller.id = featureInstances.controllerId)
	                      join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
	                      left join roomFeatures on (roomFeatures.featureInstanceId = featureInstances.id)
	                      left join rooms on (rooms.id = roomFeatures.roomId)
	                      where featureInstances.id = $instanceId limit 1" );
  if ($obj = MYSQLi_FETCH_OBJECT ( $erg )) return $obj;
  else return "Fehler: Instance $instanceId unbekannt";
}
function getLedForTaster($actInstanceId)
{
  global $lastLogId;
  
  $ledClassesId = getClassesIdByName ( "Led" );
  
  // Zum Taster den LogicalButton suchen
  $erg2 = QUERY ( "select parentInstanceId,objectid from featureInstances where id = '$actInstanceId' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg2 ))
  {
    $parentInstanceId = $row [0];
    if ($parentInstanceId == 0) return "EKeine ParentInstanceId gefunden zu Tasterinstanz $actInstanceId. (Kein LogicalButton erstellt?)";
    $myObjectId = $row [1];
    $myInstance = getInstanceId ( $myObjectId );
    
    $erg2 = QUERY ( "select featureInstances.objectid,online,controller.name from featureInstances join controller on (controller.id=featureInstances.controllerId) where featureInstances.id = '$parentInstanceId' limit 1" );
    if ($row = MYSQLi_FETCH_ROW ( $erg2 ))
    {
      if ($row [1] == 0)
        return "EController " . $row [2] . " offline";
      
      $parentObjectId = $row [0];
      
      // Dann schauen bei welchem Index in der Konfiguration die InstanzId des Tasters eingetragen ist
      $erg2 = QUERY ( "select functionData from lastReceived where senderObj = '$parentObjectId' and function='Configuration' and type='RESULT' order by id desc limit 1" );
      if ($row = MYSQLi_FETCH_ROW ( $erg2 ))
        $functionData = unserialize ( $row [0] );
      else
      {
        callObjectMethodByName ( $parentObjectId, "getConfiguration" );
        $result = waitForObjectResultByName ( $parentObjectId, 5, "Configuration", $lastLogId, "funtionDataParams", 0 );
        if ($result == - 1)
        {
          echo "Keine Antwort beim lesen der LogicalButton-Konfiguration zu Objekt $parentObjectId. Wiederhole....";
          callObjectMethodByName ( $parentObjectId, "getConfiguration" );
          $result = waitForObjectResultByName ( $parentObjectId, 5, "Configuration", $lastLogId, "funtionDataParams", 0 );
          if ($result == - 1)
            return "EKeine Antwort beim lesen der LogicalButton-Konfiguration zu Objekt $parentObjectId. Abbruch!";
        }
        $erg2 = QUERY ( "select functionData from lastReceived where senderObj = '$parentObjectId' and function='Configuration' and type='RESULT' order by id desc limit 1" );
        $row = MYSQLi_FETCH_ROW ( $erg2 );
        $functionData = unserialize ( $row [0] );
      }
      
      $buttonIndex = 0;
      foreach ( $functionData->paramData as $actSearchParam )
      {
        if (strpos ( $actSearchParam->name, "button" ) !== FALSE && $actSearchParam->dataValue == $myInstance)
        {
          $buttonIndex = substr ( $actSearchParam->name, strpos ( $actSearchParam->name, "button" ) + 6 );
          break;
        }
      }
      
      // Dann InstanzId der LED zum zugeh�en Index suchen
      if ($buttonIndex > 0)
      {
        $ledInstance = 0;
        foreach ( $functionData->paramData as $actSearchParam )
        {
          if ($actSearchParam->name == "led" . $buttonIndex)
          {
            $ledInstance = $actSearchParam->dataValue;
            break;
          }
        }
        
        if ($ledInstance > 0)
        {
          $erg2 = QUERY ( "select id,objectId from featureInstances where parentInstanceId='$parentInstanceId'" );
          while ( $row = MYSQLi_FETCH_ROW ( $erg2 ) )
          {
            $actClassesId = getClassesIdByFeatureInstanceId ( $row [0] );
            
            // echo $actClassesId." -> ".$ledClassesId.", ".getInstanceId($row[1])." -> $ledInstance <br>";
            
            // Dann die LED als Feedback verwenden
            if ($actClassesId == $ledClassesId && getInstanceId ( $row [1] ) == $ledInstance)
              return $row [0];
          }
          return "ELED Instanz $ledInstance nicht in featureInstances gefunden.";
        } else
          return "ELED Instanz nicht gefunden <br>Pr�ie die Konfiguration des LogicalButton: <a href='editFeatureInstance.php?id=$parentInstanceId' target='_blank'>" . formatInstance ( $parentInstanceId ) . "</a>";
      } else
        return "EButton Index nicht gefunden. <br>Pr�ie die Konfiguration des LogicalButton: <a href='editFeatureInstance.php?id=$parentInstanceId' target='_blank'>" . formatInstance ( $parentInstanceId ) . "</a>";
    } else
      return "EObjectId zum Taster Id $parentInstanceId nicht gefunden <br>Pr�ie die Konfiguration des LogicalButton: <a href='editFeatureInstance.php?id=$parentInstanceId' target='_blank'>" . formatInstance ( $parentInstanceId ) . "</a>";
  } else
    return "ESignal ID $actInstanceId nicht gefunden <br>";
}

function getSyncGroupIdForSignalInstanceId($actSignalInstanceId)
{
  // Member raussuchen, die von diesem Signal getriggert werden
  // Aber Regeln mit SignalGruppen oder UND Bedingungen müssen ausgeklammert sein, weil diese nicht gruppiert werden
  $andNotRule="";
  $erg = QUERY("select rules.id from rules join rulesignals on (rulesignals.ruleId = rules.id) where ruleSignals.featureInstanceId<0");
  while($row=MYSQLi_FETCH_ROW($erg))
  {
  	  $andNotRule.=" and rules.id!=".$row[0];
  }
  
  $myMembers = "";
  $first = "";
  $ledClassesId = getClassesIdByName ( "Led" );
  $erg = QUERY ( "select distinct ruleActions.featureInstanceId,featureClasses.id from ruleActions join rules on (rules.id=ruleActions.ruleId) join ruleSignals on (ruleSignals.ruleId=rules.id) join featureInstances on (featureInstances.id=ruleActions.featureInstanceId) join featureClasses on (featureClasses.id=featureInstances.featureClassesId) where ruleSignals.featureInstanceId='$actSignalInstanceId' and not (activationStateId=0 and resultingStateId=0 and completeGroupFeedback=0) $andNotRule order by ruleActions.featureInstanceId" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    if ($row [1] == $ledClassesId) continue; // leds werden nicht gruppiert und nicht synchronisiert
    
    if ($first == "") $first = $row [0];
    $myMembers .= $row [0] . "-";
  }
  if ($myMembers == $first . "-" || $myMembers == "") return - 2;
    
    // Gruppe zu diesen Features suchen
  $myGroup = - 1;
  $erg = QUERY ( "select groupId from groupFeatures join groups on (groups.id=groupFeatures.groupId) where featureInstanceId='$first'" ); // and groups.generated=1
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    $actGroupMembers = "";
    $erg2 = QUERY ( "select featureInstanceId from groupFeatures where groupId='$row[0]' order by featureInstanceId" );
    while ( $row2 = MYSQLi_FETCH_ROW ( $erg2 ) )
    {
      $actGroupMembers .= $row2 [0] . "-";
    }
    
    //$debug.=$actGroupMembers."<br>";
    
    if ($myMembers == $actGroupMembers)
    {
      $myGroup = $row [0];
      break;
    }
  }
  
  if ($myGroup != - 1) return $myGroup;
  else echo "Fehler: Multigruppe zu SignalId $actSignalInstanceId und Members $myMembers nicht gefunden. <br>select distinct ruleActions.featureInstanceId,featureClasses.id from ruleActions join rules on (rules.id=ruleActions.ruleId) join ruleSignals on (ruleSignals.ruleId=rules.id) join featureInstances on (featureInstances.id=ruleActions.featureInstanceId) join featureClasses on (featureClasses.id=featureInstances.featureClassesId) where ruleSignals.featureInstanceId='$actSignalInstanceId' and not (activationStateId=0 and resultingStateId=0 and completeGroupFeedback=0) order by ruleActions.featureInstanceId <br>"; //.$debug;
}
function getBasicStateNames($featureClassesId)
{
  $dimmerClassesId = getClassesIdByName ( "Dimmer" );
  $rolloClassesId = getClassesIdByName ( "Rollladen" );
  $ledClassesId = getClassesIdByName ( "Led" );
  $schalterClassesId = getClassesIdByName ( "Schalter" );
  
  $result = "";
  
  if ($featureClassesId == $dimmerClassesId)
  {
    $result->offName = "aus";
    $result->onName = "an";
  } else if ($featureClassesId == $rolloClassesId)
  {
    $result->offName = "zu";
    $result->onName = "auf";
  } else if ($featureClassesId == $ledClassesId)
  {
    $result->offName = "aus";
    $result->onName = "an";
  } else if ($featureClassesId == $schalterClassesId)
  {
    $result->offName = "aus";
    $result->onName = "an";
  } else
  {
    $result->offName = "aus";
    $result->onName = "an";
  }
  
  return $result;
}

// getWeather( getWoeID( "Deutschland", 33758 ) ); // SHS
// getWeather( 12833587 ); // SHS
// @deprecated
function getWeather($woeid)
{
  $cxContext = getStreamContext ();
  $api = simplexml_load_string ( str_replace ( "yweather:", "", utf8_encode ( file_get_contents ( "http://weather.yahooapis.com/forecastrss?w=" . $woeid . "&u=c", false, getStreamContext () ) ) ) );
  $json = json_encode ( $api );
  $arr = json_decode ( $json, TRUE );
  
  $wetter = array ();
  $wetter ['humidity'] = $arr ["channel"] ["atmosphere"] ["@attributes"] ["humidity"];
  $wetter ['visibility'] = $arr ["channel"] ["atmosphere"] ["@attributes"] ["visibility"];
  $wetter ['pressure'] = $arr ["channel"] ["atmosphere"] ["@attributes"] ["pressure"];
  $wetter ['sunrise'] = $arr ["channel"] ["astronomy"] ["@attributes"] ["sunrise"];
  $wetter ['sunset'] = $arr ["channel"] ["astronomy"] ["@attributes"] ["sunset"];
  $wetter ['text'] = $arr ["channel"] ["item"] ["condition"] ["@attributes"] ["text"];
  $wetter ['temp'] = $arr ["channel"] ["item"] ["condition"] ["@attributes"] ["temp"];
  return $wetter;
}

// @deprecated
function getWoeID()
{
  $erg = QUERY ( "select paramValue, paramKey from basicConfig where paramKey='locationZipCode' or paramKey='locationCountry' limit 2" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    if ($row [1] == "locationZipCode")
      $zipCode = $row [0];
    if ($row [1] == "locationCountry")
      $country = $row [0];
  }
  
  if ($country == "")
    return - 1;
  
  $req = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20geo.places%20where%20text%3D%22" . $country . "%20" . $zipCode . "%22&format=xml";
  $api = simplexml_load_string ( utf8_encode ( file_get_contents ( $req, false, getStreamContext () ) ) );
  return $api->results->place->woeid;
}

function isWindows()
{
  // if (!isset($_SESSION["myOS"]))
  {
    ob_start ();
    phpInfo ( INFO_GENERAL );
    $pinfo = ob_get_contents ();
    ob_end_clean ();
    $pos = strpos ( $pinfo, "System" );
    if ($pos === "FALSE")
      echo "Betriebssystem konnte nicht ermittelt werden <br>";
    else
    {
      $check = strtolower ( substr ( $pinfo, $pos, strpos ( $pinfo, "</tr>", $pos ) - $pos ) );
      if (strpos ( $check, "windows" ) !== FALSE)
        $_SESSION ["myOS"] = "windows";
      else
        $_SESSION ["myOS"] = "not_windows";
    }
  }
  
  if ($_SESSION ["myOS"] == "windows")
    return TRUE;
  return FALSE;
}

function triggerTreeUpdate()
{
  forceTreeUpdate();
}

function liveOut($newOut, $withBr=1)
{
  echo "<script>document.getElementById('updateArea').innerHTML=document.getElementById('updateArea').innerHTML+'$newOut'";
  if ($withBr==1) echo "+'<br>'";
  echo ";</script>";
    flushIt ();
}

function treeStatusOut($newOut)
{
  echo "<script>top.frames[0].document.getElementById('treeUpdateArea').innerHTML='$newOut';</script>";
  flushIt ();
}

function checkAndSetTimeZone()
{
  $erg = QUERY ( "select paramValue from basicConfig where paramKey = 'timeZone' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) date_default_timezone_set ( $row [0] );
  else if (strcmp ( date_default_timezone_get (), ini_get ( 'date.timezone' ) )) echo "Zeitzone muss eingestellt werden unter Weitere Einstellungen -> Standorteinstellungen ! \n\n\n";
}

function readDauerWithTimebase($objectId, $dauer)
{
  global $configCache;
  
  if ($configCache [$objectId] == "")
  {
    $erg = QUERY ( "select functionData from lastReceived  where senderObj='$objectId' and type='RESULT' and function='Configuration' order by id desc limit 1" );
    if ($row = MYSQLi_FETCH_ROW ( $erg )) $configCache [$objectId] = unserialize ( $row [0] )->paramData [2]->dataValue;
    else
    {
      $erg = QUERY ( "select online from controller join featureInstances on (featureInstances.controllerId=controller.id) where featureInstances.objectId='$objectId' limit 1" );
      if (($row = MYSQLi_FETCH_ROW ( $erg )) && $row[0]==0)
      {
    	  echo "Controller for objectId $objectId ist offline. Verwendet 1000 als Timebase <br>";
        $configCache [$objectId] = 1000;
      }
      else
      {
        $result = callObjectMethodByNameAndRecover ( $objectId, "getConfiguration", "", "Configuration", 3, 2, 0 );
        if ($result == - 1)
        {
          echo "Controller offline. Verwendet 1000 als Timebase <br>";
          $configCache [$objectId] = 1000;
        } else
        {
          $configCache [$objectId] = $result [2]->dataValue;
          if ($configCache [$objectId] == 0)
          {
            echo "Achtung: Timebase von 0 gefunden bei ObjectId: $objectId -> verwendet 1000 stattdesse <br>";
            $configCache [$objectId] = 1000;
          }
        }
      }
    }
  }
  
  $timebase = $configCache [$objectId];
  $dauer = $dauer * 1000 / $timebase;
  
  return $dauer;
}

function isFirmwareVersionGreaterOrEqual($majorRelease, $minorRelease, $controllerObjectId)
{
  $erg = QUERY("select majorRelease, minorRelease from controller where objectId='$controllerObjectId' limit 1");
  $obj = mysqli_fetch_OBJECT($erg);
  if ($obj->majorRelease>$majorRelease) return true;
  if ($obj->majorRelease==$majorRelease && $obj->minorRelease>=$minorRelease) return true;
  return false;
}

function getStreamContext()
{
  $erg = QUERY ( "select paramValue from basicConfig where paramKey = 'proxy' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
    $myProxy = $row [0];
  
  $erg = QUERY ( "select paramValue from basicConfig where paramKey = 'proxyPort' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
    $myProxyPort = $row [0];
  
  if ($con = @fsockopen ( $myProxy, $myProxyPort, $eroare, $eroare_str, 3 ))
  {
    $uri = 'tcp://' . $myProxy . ':' . $myProxyPort;
    $aContext = array (
        'http' => array (
            'proxy' => $uri,
            'request_fulluri' => true 
        ) 
    );
    fclose ( $con ); // Close the socket handle
    return stream_context_create ( $aContext );
  }
  return null;
}

function getNetworkIp()
{
  global $UDP_NETWORK_IP;
  
  $erg = QUERY ( "select paramValue from basicConfig where paramKey = 'networkIp' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) $myNetwork = $row [0];
  
  if (filter_var ( $myNetwork, FILTER_VALIDATE_IP )) $UDP_NETWORK_IP = $myNetwork;
  
  return $UDP_NETWORK_IP;
}

function getNetworkMask()
{
  global $UDP_NETWORK_MASK;
  
  $erg = QUERY ( "select paramValue from basicConfig where paramKey = 'networkMask' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) $myNetworkMask = $row [0];
  
  if (filter_var ( $myNetworkMask, FILTER_VALIDATE_IP )) $UDP_NETWORK_MASK = $myNetworkMask;
  
  return $UDP_NETWORK_MASK;
}

function getNetworkPort()
{
  global $UDP_PORT;
  
  $erg = QUERY ( "select paramValue from basicConfig where paramKey = 'networkPort' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg )) $UDP_PORT = $row [0];
}

function setupNetwork()
{
  global $UDP_NETWORK_IP;
  global $UDP_NETWORK_MASK;
  global $UDP_BCAST_IP;
  
  // update config from database
  getNetworkIp();
  getNetworkPort();
  getNetworkMask();
  
  // calculate UDP_BCAST_IP
  // Network is a logical AND between the address and netmask
  $netmask_int = ip2long($UDP_NETWORK_MASK);
  $network_int = ip2long($UDP_NETWORK_IP) & $netmask_int;
  // Broadcast is a logical OR between the address and the NOT netmask
  $broadcast_int = $network_int | (~ $netmask_int);
  $UDP_BCAST_IP = long2ip($broadcast_int);  
}


function forceTreeUpdate()
{
	 QUERY("INSERT into treeUpdateHelper values('1')");
}

function generateHeizungsSteuerung($groupId, $basicRuleObj, $offState, $onState)
{
	global $rolloClassesId, $CONTROLLER_CLASSES_ID, $singleControllerId;
	//echo "Heizung $groupId <br>";
	  
	$basicRuleId = $basicRuleObj->id;
	  
	$erg = QUERY("select featureInstanceId from groupFeatures where groupId='$groupId' limit 1");
	if ($row=MYSQLi_FETCH_ROW($erg)) $myInstanceId = $row[0];
	else
	{
		 echo "Kein Aktor bei Heizungssteuerungsregeln in Gruppe $groupId gefunden<br>";
	 	 return;
	}

	$data = getFeatureInstanceData($myInstanceId);
	$myControllerId = $data->controllerId;

	global $lastEventParamId;
	global $celsiusParamId;
	global $centiCelsiusParamId;
	global $evStatusLastEventParamId;
	global $evStatusCelsiusParamId;
	global $evStatusCentiCelsiusParamId;
	global $moveToPositionFunctionId;
	global $tempStatusEvent;
	global $evStatusTempStatusEvent;
	global $coldEnumValue;
	global $evStatusColdEnumValue;
	global $hotEnumValue;
	global $evStatusHotEnumValue;
	global $evStatusWarmEnumValue;
	global $warmEnumValue;

	$evStatusTempStatusEvent = getClassesIdFunctionsIdByName(2, "evStatus");
    $evStatusLastEventParamId = getClassesIdFunctionParamIdByName ( 2, "evStatus", "lastEvent" );
    $evStatusCelsiusParamId = getClassesIdFunctionParamIdByName ( 2, "evStatus", "celsius" );
    $evStatusCentiCelsiusParamId = getClassesIdFunctionParamIdByName ( 2, "evStatus", "centiCelsius" );
    $evStatusColdEnumValue = getFunctionParamEnumValueForClassesIdByName ( 2, "evStatus", "lastEvent", "COLD" );
    $evStatusWarmEnumValue = getFunctionParamEnumValueForClassesIdByName ( 2, "evStatus", "lastEvent", "WARM" );
    $evStatusHotEnumValue = getFunctionParamEnumValueForClassesIdByName ( 2, "evStatus", "lastEvent", "HOT" );
	
	$tempStatusEvent = getClassesIdFunctionsIdByName(2, "Status");
    $lastEventParamId = getClassesIdFunctionParamIdByName ( 2, "Status", "lastEvent" );
    $celsiusParamId = getClassesIdFunctionParamIdByName ( 2, "Status", "celsius" );
    $centiCelsiusParamId = getClassesIdFunctionParamIdByName ( 2, "Status", "centiCelsius" );
    $coldEnumValue = getFunctionParamEnumValueForClassesIdByName ( 2, "Status", "lastEvent", "COLD" );
    $warmEnumValue = getFunctionParamEnumValueForClassesIdByName ( 2, "Status", "lastEvent", "WARM" );
    $hotEnumValue = getFunctionParamEnumValueForClassesIdByName ( 2, "Status", "lastEvent", "HOT" );

	$moveToPositionFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "moveToPosition" );
 
	// Ventilsteuerung
	if ($data->featureClassName=="Rollladen")
	{
	  // Zusatzstates erstellen
	  $geschlossenState = generateGroupState($groupId, "0 Prozent", 0, 0);
	  $firstState = generateGroupState($groupId, "25 Prozent", 0, 25);
	  $secondState = generateGroupState($groupId, "50 Prozent", 0, 50);
	  $thirdState = generateGroupState($groupId, "75 Prozent", 0, 75);
	  
	  // 1. von Aus nach 25 %
	  generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $geschlossenState, $firstState, 75, "cold");
      
      // 2. von 25% nach 50 %
	  generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $firstState, $secondState, 50, "cold");
      
      // 3. von 50% nach 75 %
	  generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $secondState, $thirdState, 75, "cold");
        
      // 4. von 75% nach 100 %
	  generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $thirdState, $onState, 0, "cold");
      
      // 5. von 100% nach 75%
	  generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $onState, $thirdState, 25, "hot");
      
      // 6. von 75% nach 50%
	  generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $thirdState, $secondState, 50, "hot");
      
      // 7. von 50% nach 25%
	  generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $secondState, $firstState, 75, "hot");
      
      // 8. von 25% nach 0%
	  generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $firstState, $geschlossenState, 100, "hot");
      
      // 9. Initialzustand von zu -> geschlossen
	  generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $offState, $geschlossenState, 100, "*");
      
	  // Abschaltung für Nachtabsenkung
	  if (($basicRuleObj->startHour!=$basicRuleObj->endHour || $basicRuleObj->startMinute!=$basicRuleObj->endMinute) && $basicRuleObj->intraDay==1)
	  {
         $erg3 = QUERY("select featureInstanceId from groupFeatures where groupId='$groupId' order by id limit 1");
         if ($row3 = mysqli_fetch_ROW($erg3)) $firstAktor = $row3[0];
         else die("Kein Aktor gefunden in Gruppe $groupId");

         $erg = QUERY("select id from featureInstances where controllerId='$myControllerId' and featureClassesId='$CONTROLLER_CLASSES_ID' limit 1");
         if ($row = mysqli_fetch_ROW($erg)) $controllerInstanceId = $row[0];
         else die("controllerInstanceId zu controllerId $controllerId nicht gefunden");
  
	     $nachtGeschlossenState = generateGroupState($groupId, "Nacht 0 Prozent", 0, 0);
	     $nachtFirstState = generateGroupState($groupId, "Nacht 25 Prozent", 0, 25);
	     $nachtSecondState = generateGroupState($groupId, "Nacht 50 Prozent", 0, 50);
	     $nachtThirdState = generateGroupState($groupId, "Nacht 75 Prozent", 0, 75);

         // Wechsel von Tag zu Nacht
		 $ruleId = generateDefaultRule($groupId, 0, $nachtGeschlossenState);
	     generateDefaultAction($ruleId, $myInstanceId, $moveToPositionFunctionId, array("102" => "100"));
  
         // evTime zum Start der Absenkung
		 $evTimeFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evTime" );
         $evTimeEvent = getClassesIdFunctionsIdByName($CONTROLLER_CLASSES_ID, "evTime");
         $erg = QUERY("select id from featureFunctionParams where featureFunctionId='$evTimeFunctionId' and name='weekTime' limit 1");
         $row = mysqli_fetch_ROW($erg);
         $paramsId = $row[0];
	     $value = toWeekTime(7, $basicRuleObj->endHour, 255);
 		 generateDefaultSignal($ruleId, $controllerInstanceId, $evTimeEvent, array($paramsId => $value));

		 
		 // Wechsel von Nacht zu Tag
		 $ruleId = generateDefaultRule($groupId, 0, $geschlossenState);
	     generateDefaultAction($ruleId, $myInstanceId, $moveToPositionFunctionId, array("102" => "100"));

         // evTime zum Ende der Absenkung
	     $value = toWeekTime(7, $basicRuleObj->startHour, 255);
 		 generateDefaultSignal($ruleId, $controllerInstanceId, $evTimeEvent, array($paramsId => $value));

	     // 1. Nacht von Aus nach 25 %
	     generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $nachtGeschlossenState, $nachtFirstState, 75, "cold");
      
         // 2. Nacht von 25% nach 50 %
	     generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $nachtFirstState, $nachtSecondState, 50, "cold");
      
         // 3. Nacht von 50% nach 75 %
	     generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $nachtSecondState, $nachtThirdState, 75, "cold");
        
         // 4. Nacht von 75% nach 100 %
	     generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $nachtThirdState, $nachtOnState, 0, "cold");
      
         // 5. Nacht von 100% nach 75%
	     generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $nachtOnState, $nachtThirdState, 25, "hot");
      
         // 6. Nacht von 75% nach 50%
	     generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $nachtThirdState, $nachtSecondState, 50, "hot");
      
         // 7. Nacht von 50% nach 25%
	     generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $nachtSecondState, $nachtFirstState, 75, "hot");
      
         // 8. Nacht von 25% nach 0%
	     generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $nachtFirstState, $nachtGeschlossenState, 100, "hot");
      
         // 9. Initialzustand von zu -> geschlossen
	     generateRolloHeizungRules($groupId, $myInstanceId, $basicRuleId, $nachtOffState, $nachtGeschlossenState, 100, "*");

	     return true; // Syncevents überspringen
	   }
	   else
	   {
          // Dummy für evOn
   	      $ruleId = generateDefaultRule($groupId, 0, $onState);
          $evOpenFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "evOpen" );
	      generateDefaultSignal($ruleId, $myInstanceId, $evOpenFunctionId);
	  
          // Dummy für off
   	      $ruleId = generateDefaultRule($groupId, 0, $geschlossenState);
          $evClosedFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "evClosed" );
	      $positionParamId = getClassesIdFunctionParamIdByName ( $rolloClassesId, "evClosed", "position" );
	      generateDefaultSignal($ruleId, $myInstanceId, $evClosedFunctionId, array($positionParamId => 100), true);
	   }
      
      return true; // Syncevents überspringen
	}
	// Schaltersteuerung
	else if ($data->featureClassName=="Schalter")
	{
	   // AUS (TAG_AUS)  AN (TAG_AN)  NACHT_AUS  NACHT_AN
	   $ausState = getOrGenerateGroupDefaultState($groupId,0, "Aus");
	   $anState = getOrGenerateGroupDefaultState($groupId, 1, "Ein");
	   
  	   // 1. von Tag aus nach Tag an
	   $ruleId = generateDefaultRule($groupId, $ausState, $anState);
	   generateDefaultAction($ruleId, $myInstanceId, 60, array("265" => "0"));

       // Signale
       $erg2 = QUERY ("select featureInstanceId from basicRuleSignals where ruleId='$basicRuleId'");
       while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
       {
          if ($obj2->featureInstanceId < 0)  continue;
          $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
          if ($signalClassesId!=2) continue;

          // Statusevent cold und warm
		  generateDefaultSignal($ruleId, $obj2->featureInstanceId, $tempStatusEvent, array($lastEventParamId => $coldEnumValue, $celsiusParamId => 255, $centiCelsiusParamId => 255));
		  generateDefaultSignal($ruleId, $obj2->featureInstanceId, $tempStatusEvent, array($lastEventParamId => $warmEnumValue, $celsiusParamId => 255, $centiCelsiusParamId => 255));
		  
		  // evStatus cold und warm
		  generateDefaultSignal($ruleId, $obj2->featureInstanceId, $evStatusTempStatusEvent, array($evStatusLastEventParamId => $evStatusColdEnumValue, $evStatusCelsiusParamId => 255, $evStatusCentiCelsiusParamId => 255));
		  generateDefaultSignal($ruleId, $obj2->featureInstanceId, $evStatusTempStatusEvent, array($evStatusLastEventParamId => $evStatusWarmEnumValue, $evStatusCelsiusParamId => 255, $evStatusCentiCelsiusParamId => 255));
								
          break;
       }	 
      
       // 2. von Tag an nach aus
	   $ruleId = generateDefaultRule($groupId, $anState, $ausState);
	   generateDefaultAction($ruleId, $myInstanceId, 61);

       // Signale
       $erg2 = QUERY ("select featureInstanceId from basicRuleSignals where ruleId='$basicRuleId'");
       while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
       {
         if ($obj2->featureInstanceId < 0)  continue;
         $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
         if ($signalClassesId!=2) continue;

         // Statusevent hot
		 generateDefaultSignal($ruleId, $obj2->featureInstanceId, $tempStatusEvent, array($lastEventParamId => $hotEnumValue, $celsiusParamId => 255, $centiCelsiusParamId => 255));
										  
         // evStatus hot
		 generateDefaultSignal($ruleId, $obj2->featureInstanceId, $evStatusTempStatusEvent, array($evStatusLastEventParamId => $evStatusHotEnumValue, $evStatusCelsiusParamId => 255, $evStatusCentiCelsiusParamId => 255));

         break;
      }	 
	  
	  // Nachtabsenkung
	  if (($basicRuleObj->startHour!=$basicRuleObj->endHour || $basicRuleObj->startMinute!=$basicRuleObj->endMinute) && $basicRuleObj->intraDay==1)
	  {
         $erg3 = QUERY("select featureInstanceId from groupFeatures where groupId='$groupId' order by id limit 1");
         if ($row3 = mysqli_fetch_ROW($erg3)) $firstAktor = $row3[0];
         else die("Kein Aktor gefunden in Gruppe $groupId");

         $erg = QUERY("select id from featureInstances where controllerId='$myControllerId' and featureClassesId='$CONTROLLER_CLASSES_ID' limit 1");
         if ($row = mysqli_fetch_ROW($erg)) $controllerInstanceId = $row[0];
         else die("controllerInstanceId zu controllerId $controllerId nicht gefunden");

         $erg = QUERY("select userKey, userValue from userData where userKey='Gruppe_Nachtabsenkung' or userKey='Gruppe_Nachtabsenkung_Dauer_An' limit 2");
		 while($row=mysqli_fetch_row($erg))
		 {
			 if ($row[0]=="Gruppe_Nachtabsenkung") $groupIdDeaktivierung = $row[1];
			 else if ($row[0]=="Gruppe_Nachtabsenkung_Dauer_An") $groupIdDauerAktivierung = $row[1];
		 }
		 
  	     $nachtAusStateId = generateGroupState($groupId, "Nacht aus", 1);
	     $nachtAnStateId = generateGroupState($groupId, "Nacht an", 2);

         $evTimeEvent = getClassesIdFunctionsIdByName($CONTROLLER_CLASSES_ID, "evTime");
         $erg = QUERY("select id from featureFunctionParams where featureFunctionId='$evTimeEvent' and name='weekTime' limit 1");
         $row = mysqli_fetch_ROW($erg);
         $paramsId = $row[0];

         // Wechsel von Tag zu Nacht
		 $ruleId = generateDefaultRule($groupId, $ausState, $nachtAusStateId);
	     //generateDefaultAction($ruleId, $myInstanceId, 61);
 		 generateDefaultSignal($ruleId, $controllerInstanceId, $evTimeEvent, array($paramsId => toWeekTime(7, $basicRuleObj->endHour, 0)));
		 generateAndCondition($ruleId,  0, 0);

		 $ruleId = generateDefaultRule($groupId, $anState, $nachtAnStateId);
	     //generateDefaultAction($ruleId, $myInstanceId, 61);
 		 generateDefaultSignal($ruleId, $controllerInstanceId, $evTimeEvent, array($paramsId => toWeekTime(7, $basicRuleObj->endHour, 0)));
		 generateAndCondition($ruleId,  0, 0);
		 
		 // Wechsel von Nacht zu Tag
		 $ruleId = generateDefaultRule($groupId, $nachtAusStateId, $ausState);
	     //generateDefaultAction($ruleId, $myInstanceId, 61);
 		 generateDefaultSignal($ruleId, $controllerInstanceId, $evTimeEvent, array($paramsId => toWeekTime(7, $basicRuleObj->startHour, 0)));
		 generateAndCondition($ruleId,  1, 0);

		 $ruleId = generateDefaultRule($groupId, $nachtAnStateId, $anState);
	     //generateDefaultAction($ruleId, $myInstanceId, 61);
 		 generateDefaultSignal($ruleId, $controllerInstanceId, $evTimeEvent, array($paramsId => toWeekTime(7, $basicRuleObj->startHour, 0)));
		 generateAndCondition($ruleId,  1, 0);

         $controllerThatHoldsSystemProperties = getFirstNormalOnlineController()->featureInstanceId;
		 
		 // Regel zum direkten Aktivieren des Tagmodus, falls jemand den Nachtmodus deaktiviert
		 $ruleId = generateDefaultRule($groupId, $nachtAusStateId, $ausState);
 		 generateDefaultSignal($ruleId, $controllerThatHoldsSystemProperties, 363, array(810 => 0, 811 => 0, 812 => 1)); // evSystemVariableChanged type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1

		 $ruleId = generateDefaultRule($groupId, $nachtAnStateId, $anState);
 		 generateDefaultSignal($ruleId, $controllerThatHoldsSystemProperties, 363, array(810 => 0, 811 => 0, 812 => 1)); // evSystemVariableChanged type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1


		 // Regel zum direkten Aktivieren des Nachtmodus, falls jemand den Nachtmodusdeaktivierung wieder aktiviert
		 $ruleId = generateDefaultRule($groupId, $anState, $nachtAnStateId, 1, 7, $basicRuleObj->endHour, 0, 7, $basicRuleObj->startHour, 0, 0);
 		 generateDefaultSignal($ruleId, $controllerThatHoldsSystemProperties, 363, array(810 => 0, 811 => 0, 812 => 0)); // evSystemVariableChanged type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1

		 $ruleId = generateDefaultRule($groupId, $ausState, $nachtAusStateId, 1, 7, $basicRuleObj->endHour, 0, 7, $basicRuleObj->startHour, 0, 0);
 		 generateDefaultSignal($ruleId, $controllerThatHoldsSystemProperties, 363, array(810 => 0, 811 => 0, 812 => 0)); // evSystemVariableChanged type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1


		 // Regel zum direkten Aktivieren des Nachtmodus, falls jemand den Nachtmodus daueraktiviert
		 $ruleId = generateDefaultRule($groupId, $anState, $nachtAnStateId);
 		 generateDefaultSignal($ruleId, $controllerThatHoldsSystemProperties, 363, array(810 => 0, 811 => 1, 812 => 1));

		 $ruleId = generateDefaultRule($groupId, $ausState, $nachtAusStateId);
 		 generateDefaultSignal($ruleId, $controllerThatHoldsSystemProperties, 363, array(810 => 0, 811 => 1, 812 => 1));

		 // Regel zum direkten Aktivieren des Tagmodus, falls jemand den Dauernachtmodus wieder deaktiviert
		 $ruleId = generateDefaultRule($groupId, $nachtAnStateId, $anState, 1, 7, $basicRuleObj->startHour, 0, 7, $basicRuleObj->endHour, 0, 0);
 		 generateDefaultSignal($ruleId, $controllerThatHoldsSystemProperties, 363, array(810 => 0, 811 => 1, 812 => 0)); // evSystemVariableChanged type = bit , index = systemvarialenindex 1=Dauernachtmodus Deaktivierung, value = 0

		 $ruleId = generateDefaultRule($groupId, $nachtAusStateId, $ausState, 1, 7, $basicRuleObj->startHour, 0, 7, $basicRuleObj->endHour, 0, 0);
 		 generateDefaultSignal($ruleId, $controllerThatHoldsSystemProperties, 363, array(810 => 0, 811 => 1, 812 => 0)); // evSystemVariableChanged type = bit , index = systemvarialenindex 1=Dauernachtmodus Deaktivierung, value = 0

  	     // 3. von Nacht aus nach Nacht an
		 $ruleId = generateDefaultRule($groupId, $nachtAusStateId, $nachtAnStateId);
	     generateDefaultAction($ruleId, $myInstanceId, 60, array(265 => 0));

         // Signal
         $erg2 = QUERY ("select featureInstanceId from basicRuleSignals where ruleId='$basicRuleId'");
         while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
         {
            if ($obj2->featureInstanceId < 0)  continue;
            $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
            if ($signalClassesId!=2) continue;

            // Statusevent cold 
		    generateDefaultSignal($ruleId, $obj2->featureInstanceId, $tempStatusEvent, array($lastEventParamId => $coldEnumValue, $celsiusParamId => 255, $centiCelsiusParamId => 255));
		    
		    // evStatus cold
		    generateDefaultSignal($ruleId, $obj2->featureInstanceId, $evStatusTempStatusEvent, array($evStatusLastEventParamId => $evStatusColdEnumValue, $evStatusCelsiusParamId => 255, $evStatusCentiCelsiusParamId => 255));
         
		    break;
		 }
      
         // 4. von Nach an nach Nacht aus
		 $ruleId = generateDefaultRule($groupId, $nachtAnStateId, $nachtAusStateId);
	     generateDefaultAction($ruleId, $myInstanceId, 61);

         // Signale
         $erg2 = QUERY ("select featureInstanceId from basicRuleSignals where ruleId='$basicRuleId'");
         while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
         {
            if ($obj2->featureInstanceId < 0)  continue;
            $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
            if ($signalClassesId!=2) continue;

            // Statusevent hot und warm
		    generateDefaultSignal($ruleId, $obj2->featureInstanceId, $tempStatusEvent, array($lastEventParamId => $hotEnumValue, $celsiusParamId => 255, $centiCelsiusParamId => 255));
		    generateDefaultSignal($ruleId, $obj2->featureInstanceId, $tempStatusEvent, array($lastEventParamId => $warmEnumValue, $celsiusParamId => 255, $centiCelsiusParamId => 255));
		  
		    // evStatus hot und warm
		    generateDefaultSignal($ruleId, $obj2->featureInstanceId, $evStatusTempStatusEvent, array($evStatusLastEventParamId => $evStatusHotEnumValue, $evStatusCelsiusParamId => 255, $evStatusCentiCelsiusParamId => 255));
		    generateDefaultSignal($ruleId, $obj2->featureInstanceId, $evStatusTempStatusEvent, array($evStatusLastEventParamId => $evStatusWarmEnumValue, $evStatusCelsiusParamId => 255, $evStatusCentiCelsiusParamId => 255));
         
            break;
	     }
   	   
	     // DummyEvents
	     // evOn
		 $ruleId = generateDefaultRule($groupId, $ausState, $anState);
         $evOnFunctionId = getClassesIdFunctionsIdByName ( 8, "evOn" );
         $schalterParamDurationId = getClassesIdFunctionParamIdByName ( 8, "evOn", "duration" );
	     generateDefaultSignal($ruleId, $myInstanceId, $evOnFunctionId, array($schalterParamDurationId => 255));

	     // evOff
		 $ruleId = generateDefaultRule($groupId, $anState, $ausState);
         $evOffFunctionId = getClassesIdFunctionsIdByName ( 8, "evOff" );
	     generateDefaultSignal($ruleId, $myInstanceId, $evOffFunctionId);
    

	     // evOn Nachts
		 $ruleId = generateDefaultRule($groupId, $nachtAusStateId, $nachtAnStateId);
	     generateDefaultSignal($ruleId, $myInstanceId, $evOnFunctionId, array($schalterParamDurationId => 255));
    
         // evOff Nachts
		 $ruleId = generateDefaultRule($groupId, $nachtAnStateId, $nachtAusStateId);
	     generateDefaultSignal($ruleId, $myInstanceId, $evOffFunctionId);
								 
	     return true; // Syncevents überspringen
	   }
	}
	else
	{
		echo "Unbekannter Aktor für Heizungssteuerung -> ".$data->featureClassName."<br>";
	  	return;
	}
}

function generateLueftungsSteuerung($groupId, $basicRuleObj, $offState, $onState, $ruleId)
{
	global $CONTROLLER_CLASSES_ID;

	$basicRuleId = $basicRuleObj->id;
	  
	$erg = QUERY("select featureInstanceId from groupFeatures where groupId='$groupId' limit 1");
	if ($row=MYSQLi_FETCH_ROW($erg)) $myInstanceId = $row[0];
	else
	{
		 echo "Kein Ventilator bei Lüftungssteuerungsregeln in Gruppe $groupId gefunden<br>";
	 	 return;
	}

	$erg = QUERY("select type from ventilation where basicRuleId='$ruleId' limit 1");
	if ($row=MYSQLi_FETCH_ROW($erg)) $type = $row[0];
	else $type="Entfeuchten";

	$data = getFeatureInstanceData($myInstanceId);

    $feuchteStatusEvent = getClassesIdFunctionsIdByName(23, "evStatus");
	$searchStatus = "evStatus";
	if ($feuchteStatusEvent==0)
	{
		$feuchteStatusEvent = getClassesIdFunctionsIdByName(23, "Status");
		$searchStatus = "Status";
	}
    $lastEventParamId = getClassesIdFunctionParamIdByName ( 23, $searchStatus, "lastEvent" );
    $humidityParamId = getClassesIdFunctionParamIdByName ( 23, $searchStatus, "relativeHumidity");
    $coldEnumValue = getFunctionParamEnumValueForClassesIdByName ( 23, $searchStatus, "lastEvent", "WET" );
    $warmEnumValue = getFunctionParamEnumValueForClassesIdByName ( 23, $searchStatus, "lastEvent", "CONFORTABLE" );
    $hotEnumValue = getFunctionParamEnumValueForClassesIdByName ( 23, $searchStatus, "lastEvent", "DRY" );
	  
	// Schaltersteuerung
	
	  	// 1. von aus nach an
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,extras,intraDay) 
                           values('$groupId','$basicRuleObj->startDay','$basicRuleObj->startHour','$basicRuleObj->startMinute','$basicRuleObj->endDay','$basicRuleObj->endHour','$basicRuleObj->endMinute','0','$onState','click','1','1','$basicRuleObj->extras','$basicRuleObj->intraDay')" );
      $ruleId = query_insert_id ();
      
      // actions
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','60','1')" );
      $newRuleActionId = query_insert_id ();
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','265','0','1')" );

      // signal
      $erg2 = QUERY ("select featureInstanceId from basicRuleSignals where ruleId='$basicRuleId'");
      while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
      {
         if ($obj2->featureInstanceId < 0)  continue;
         $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
         if ($signalClassesId!=23) continue;

         QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                    values('$ruleId','$obj2->featureInstanceId','$feuchteStatusEvent','1','')" );
         $ruleSignalId = query_insert_id ();
         
		 if ($type=="Befeuchten") $targetValue=$hotEnumValue;
		 else $targetValue= $coldEnumValue;
		 
         QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
   		                                  values('$ruleSignalId','$lastEventParamId','$targetValue','1')" );

         QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
   		                                  values('$ruleSignalId','$humidityParamId','65535','1')" );
         
         break;
      }	 
      
      // 2. von an nach aus
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,extras,intraDay) 
                           values('$groupId','$basicRuleObj->startDay','$basicRuleObj->startHour','$basicRuleObj->startMinute','$basicRuleObj->endDay','$basicRuleObj->endHour','$basicRuleObj->endMinute','$onState','0','click','1','1','$basicRuleObj->extras','$basicRuleObj->intraDay')" );
      $ruleId = query_insert_id ();
      
      // actions
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','61','1')" );

      // signal
      $erg2 = QUERY ("select featureInstanceId from basicRuleSignals where ruleId='$basicRuleId'");
      while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
      {
         if ($obj2->featureInstanceId < 0)  continue;
         $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
         if ($signalClassesId!=23) continue;

         // warm
         QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                    values('$ruleId','$obj2->featureInstanceId','$feuchteStatusEvent','1','')" );
         $ruleSignalId = query_insert_id ();
                 
         QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
   		                                  values('$ruleSignalId','$lastEventParamId','$warmEnumValue','1')" );

         QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
   		                                  values('$ruleSignalId','$humidityParamId','65535','1')" );

         // hot
         QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                    values('$ruleId','$obj2->featureInstanceId','$feuchteStatusEvent','1','')" );
         $ruleSignalId = query_insert_id ();

         if ($type=="Befeuchten") $targetValue=$coldEnumValue;
		 else $targetValue= $hotEnumValue;
                  
         QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
   		                                  values('$ruleSignalId','$lastEventParamId','$targetValue','1')" );

         QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
   		                                  values('$ruleSignalId','$humidityParamId','65535','1')" );
   		                                  
         break;
      }	 

	  // Abschaltung für Nachtabsenkung
	  if (($basicRuleObj->startHour!=$basicRuleObj->endHour || $basicRuleObj->startMinute!=$basicRuleObj->endMinute) && $basicRuleObj->intraDay==1)
	  {
		  
		  // Abschalten zum Endzeitpunkt
          $erg = QUERY("select featureInstanceId from groupFeatures where groupId='$groupId' order by id limit 1");
          if ($row = mysqli_fetch_ROW($erg)) $firstAktor = $row[0];
          else die("Kein Aktor gefunden in Gruppe $groupId");
  
          $erg = QUERY("select controllerId from featureInstances where id='$firstAktor' limit 1");
          if ($row = mysqli_fetch_ROW($erg)) $controllerId = $row[0];
          else die("Controller zu featureId $firstAktor nicht gefunden");

          $erg = QUERY("select size from controller where id='$controllerId' limit 1");
          $row = mysqli_fetch_ROW($erg);
		  if ($row[0]==999) // Diese Controller schicken kein EvTime
		  {
            $erg = QUERY("select featureInstances.id, controllerId, groups.id from featureInstances join controller on (controller.id=featureInstances.controllerId) join groupFeatures on (featureInstances.id=groupfeatures.featureInstanceId) join groups on (groups.id=groupfeatures.groupId)  where size>4500 and featureClassesId='27' and single=1 order by controller.id desc limit 1");
            $row = mysqli_fetch_ROW($erg);
			$gatewayFeatureInstanceId=$row[0];
			$controllerId = $row[1];
			$groupId = $row[2];
			//die("TEST: $controllerId - $gatewayFeatureInstanceId - $groupId");
		  }
  
          $erg = QUERY("select id from featureInstances where controllerId='$controllerId' and featureClassesId='$CONTROLLER_CLASSES_ID' limit 1");
          if ($row = mysqli_fetch_ROW($erg)) $controllerInstanceId = $row[0];
          else die("controllerInstanceId zu controllerId $controllerId nicht gefunden");
  
          QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,`generated`,baseRule) 
                            values('$groupId','7','31','255','7','31','255','0','0','1','1')");
          $ruleId = query_insert_id();
  
          $myEvent = getClassesIdFunctionsIdByName($CONTROLLER_CLASSES_ID, "evTime");
  
          QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                                   values('$ruleId','$controllerInstanceId','$myEvent')");
          $ruleSignalId = query_insert_id();
  
          $evTimeFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evTime" );
		  
          $erg = QUERY("select id from featureFunctionParams where featureFunctionId='$evTimeFunctionId' and name='weekTime' limit 1");
          $row = mysqli_fetch_ROW($erg);
          $paramsId = $row[0];
		  
		  //echo "Hour ".$basicRuleObj->endHour.", Minute ".$basicRuleObj->endMinute."<br>"; 
		  $value = toWeekTime(7, $basicRuleObj->endHour, $basicRuleObj->endMinute);
		  //die("Value: $value");
          QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                        values('$ruleSignalId','$paramsId','$value')");
		  
          // actions
          QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','61','1')" );
	  }
}

function checkRemoveUnusedHeatingRules()
{
	 $erg = QUERY("select basicRules.id as basicRuleId, featureInstanceId, heating.id 
	              from basicRules 
	              join groupFeatures on (groupFeatures.groupId=basicRules.groupId) 
	              left join heating on (heating.relay = groupFeatures.featureInstanceId) 
	              where extras='Heizungssteuerung'");
	 while($obj=MYSQLi_FETCH_OBJECT($erg))
	 {
	 	  if ($obj->id==null) deleteBaseRule($obj->basicRuleId,0);
	 }
}


function generateHeatingSignalGroups()
{
  $erg = QUERY("select distinct pump from heating where pump>0 order by id");
  while($row=MYSQLi_FETCH_ROW($erg)) $pumps[]=$row[0]; 
  $totalPumps = count((array)$pumps);
  if ($totalPumps==0) return;

  $evOnFunctionId = getFunctionsIdByNameForClassName("Schalter", "evOn");
  $evOffFunctionId = getFunctionsIdByNameForClassName("Schalter", "evOff");
  
  for($a=0;$a<$totalPumps;$a++)
  {
  	$actPump = $pumps[$a];
  	
    QUERY("INSERT into groups (name,single,groupType, `generated`, active)
                        values('Generated Heating Signals $actPump','0','SIGNALS-OR','1','1')");
    $groupId = query_insert_id();
  
    $schalterClassesId = getClassesIdByName("Schalter");
    $basicStateNames = getBasicStateNames($schalterClassesId); 
    $offName = $basicStateNames->offName;
    $onName = $basicStateNames->onName; 
    QUERY("INSERT into groupStates (groupId, name,basics,`generated`) 
                             values('$groupId','$offName','1','1')"); 
                             
    QUERY("INSERT into groupStates (groupId, name,basics,`generated`)
                             values('$groupId','$onName','2','1')");
  
    $i=-1;
    $erg = QUERY("select relay from heating where pump='$actPump' order by id");
    while($row=MYSQLi_FETCH_ROW($erg))
    { 
    	$i++;
    	
    	$featureInstanceId = $row[0];
    	
    	$activationState = $i * 10; 
    	QUERY("INSERT into rules (groupId, activationStateId,startDay,startHour,startMinute,endDay,endHour,endMinute,`generated`)
                         values('$groupId','$activationState','7','31','255','7','31','255','1')"); 
      $ruleId = query_insert_id();
  
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                               values('$ruleId','$featureInstanceId','$evOnFunctionId','1')");
  
      $deactivationState = ($i * 10) + 1; 
      QUERY("INSERT into rules (groupId,activationStateId,startDay,startHour,startMinute,endDay,endHour,endMinute,`generated`)
                         values('$groupId','$deactivationState','7','31','255','7','31','255','1')");
      $ruleId = query_insert_id();
      
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                               values('$ruleId','$featureInstanceId','$evOffFunctionId','1')");
    }
  }
} 

function makeDownload($file, $dir, $type) 
{
  header("Content-Type: $type");
  header("Content-Disposition: attachment; filename=\"$file\"");
  readfile($dir.$file);
} 

function generateBaseRulesRollladen($groupId)
{
  // echo "Generiere Basisregeln für Gruppe $groupId <br>";
  global $CONTROLLER_CLASSES_ID;
  global $signalParamWildcard,$signalParamWildcardWord;
  global $dimmerClassesId, $rolloClassesId, $ledClassesId, $schalterClassesId, $irClassesId, $tasterClassesId, $logicalButtonClassesId, $ethernetClassesId;
  global $startFunctionId, $stopFunctionId, $moveToPositionFunctionId, $paramToOpen, $paramToClose, $paramToToggle, $paramPosition;
  global $functionTemplates;
  global $ledLogicalButtonBrightness;
  global $configCache;
  
  unset ( $configCache );
  
  $myInstanceCount = 0;
  $erg = QUERY ( "select featureInstanceId from groupFeatures join featureInstances on (featureInstances.id=featureInstanceId) where groupId='$groupId'" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
  {
    $myInstances[$myInstanceCount++] = $row[0];
  }
  
  // Standardstates ggf. anlegen oder umbenennen. Wir arbeiten bei Rollos nur noch mit "steht" und "fährt"
  $erg = QUERY ( "select id, name from groupStates where groupId='$groupId' and basics='1' limit 1");
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
  {
  	$firstState = $row [0];
  	if ($row [1]!="steht") QUERY("UPDATE groupStates set name='steht' where groupId='$groupId' and basics='1' limit 1");
  }
  else
  {
    QUERY ( "INSERT into groupStates (groupId, name,basics,`generated`) values('$groupId','steht','1','1')" );
    $firstState = query_insert_id ();
  }
  
  $erg = QUERY ( "select id from groupStates where groupId='$groupId' and basics='2' limit 1" );
  if ($row = MYSQLi_FETCH_ROW ( $erg ))
  {
  	 $secondState = $row [0];
  	 if ($row [1]!="fährt") QUERY("UPDATE groupStates set name='fährt' where groupId='$groupId' and basics='2' limit 1");
  }
  else
  {
    QUERY ( "INSERT into groupStates (groupId, name,basics,`generated`) values('$groupId','fährt','2','1')" );
    $secondState = query_insert_id ();
  }
  
  // Dann alle Basisregeln abarbeiten und Regeln für die einzelnen Funktionen für alle Instanzen generieren
  $erg = QUERY ( "select * from basicRules where groupId='$groupId' and active='1'" );
  while ( $obj = MYSQLi_FETCH_OBJECT ( $erg ) )
  {
  	// Regeln überspringen, die keine Signale haben
    $erg2 = QUERY ( "select count(*) from basicRuleSignals where ruleId='$obj->id'" );
    $row = MYSQLi_FETCH_ROW ( $erg2 );
    if ($row [0] == 0) continue;
     
    // Wenn in einer Gruppe LED Feedback Gruppenstatus angewählt wurde, dann will man dass beim Event groupUndefined die Gruppe als aus gilt und sonst als an
    // Feedback wird bei den Signals gespeichert und dort später für LED Feedback ausgewertet
    if ($obj->ledStatus == 3) $completeGroupFeedback = 1;
    else if ($obj->ledStatus == 2) $completeGroupFeedback = 2;
    else $completeGroupFeedback = 0;

    // Bugfix    
    if (((int)$obj->fkt1)>0)
    {
      echo "Repariere Wertefehler in Gruppe $groupId <br>";
      QUERY ( "update basicRules set fkt1='-' where id ='$obj->id' limit 1" );
      $obj->fkt1 = "-";
    }
	
	// Templates zu default reparieren, die mittlerweile nicht mehr existieren, weil sie mal gelöscht wurden
    if ($obj->template!="" && !isset($functionTemplates [$rolloClassesId . "-1-" . $obj->template]) && !isset($functionTemplates ["-1-1-" . $obj->template]))
	{
        echo "Repariere nicht mehr vorhandenes Funktionstemplate ".$obj->template." in Gruppe $groupId <br>";
        QUERY ( "update basicRules set template='' where id ='$obj->id' limit 1" );
		$obj->template="";
	}

    // active signalType ruleSignalType toggle stopAndGo
    $functions = createFunctionsArray($obj, $rolloClassesId);
    
    /*if ($groupId==49 && $obj->id==586)
    {
    	 print_r($functions);
    	 exit;
    }*/

    $timeTriggeredRule = isTimeTriggeredRule($obj->id);
    // Per Zeitsteuerung nicht zwei Aktionen gleichzeitig zulassen
    if ($timeTriggeredRule && $functions[1]["active"] && $functions[2]["active"]) $functions[1]["active"]=false;

    // AN Regeln
    if ($functions[1]["active"])
    {
      $signalType = $functions[1]["signalType"];
      $ruleSignalType = $functions[1]["ruleSignalType"];
      $toggleMode = $functions[1]["toggle"];   // toggle bedeutet, dass an und aus mit kleinem Event gesteuert werden
      $stopAndGo = $functions[1]["stopAndGo"]; // StopAndGo bedeutet, dass An und/oder Aus mit dem gleichen Signal wie Stop angesteuert wird
      
      // Wenn AN und AUS mit dem gleichen Signal geschaltet wird, verwenden wir toggle und stop und die States steht und fährt, ansonsten keine states
	  // Oder wenn wir mit dem selben Signal fahren und stoppen
	  if ($timeTriggeredRule)
	  {
      	$startState = 0;
      	$actionParamValue = $paramToOpen;
	  }
	  else if ($toggleMode)
      {
		if ($stopAndGo)	$startState = $firstState;
		else $startState = 0;
      	$actionParamValue = $paramToToggle;
      }
      else if ($stopAndGo)
      {
      	$startState = $firstState;
      	$actionParamValue = $paramToOpen;
      }
      else
      {
      	$startState = 0;
      	$actionParamValue = $paramToOpen;
      }
         
      // Regel anlegen
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,extras,intraDay, groupLock) 
                           values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','$startState','0','$ruleSignalType','1','1','$obj->extras','$obj->intraDay','0')" );
      $ruleId = query_insert_id ();
          
      // Actions und Signale ergänzen pro Instanz
      foreach ($myInstances as $myInstanceId)
      {
        QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$myInstanceId','$startFunctionId','1')" );
        $newRuleActionId = query_insert_id ();

        QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','142','$actionParamValue','1')" );
      }
        
      // Signale ergänzen
      $generatedAnySignales = generateSignalsForGeneratedGroup($obj->id, $ruleId, $completeGroupFeedback, $signalType, $groupId);
      if (!$generatedAnySignales) deleteRule($ruleId);
    }
    
    // AUS Regeln
    if ($functions[2]["active"] && !$functions[2]["toggle"]) // Wenn wir hoch und runter gleich ansteuern (toggle) brauchen wir hier nichts generieren
    {
      $signalType = $functions[2]["signalType"];
      $ruleSignalType = $functions[2]["ruleSignalType"];
      $stopAndGo = $functions[2]["stopAndGo"];
	  
	  // Wenn wir mit dem selben Signal fahren und stoppen
	    if ($timeTriggeredRule) $startState = 0;
      else if ($stopAndGo) $startState = $firstState;
      else $startState = 0;

      // Regel anlegen
      $offRule=""; // TODO wird scheinbar nirgends verwendet und könnte entfernt werden ???
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,offRule,extras,intraDay,groupLock) 
                           values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','$startState','0','$ruleSignalType','1','1','$offRule','$obj->extras','$obj->intraDay','0')" );
      $ruleId = query_insert_id ();
        
      // Actions ergänzen
      foreach ($myInstances as $myInstanceId)
      {
        QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                   values('$ruleId','$myInstanceId','$startFunctionId','1')" );
        $newRuleActionId = query_insert_id ();
            
        QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) 
                                        values('$newRuleActionId','142','$paramToClose','1')" );
      }
        
      // Signale ergänzen
      $generatedAnySignales = generateSignalsForGeneratedGroup($obj->id, $ruleId, $completeGroupFeedback, $signalType, $groupId);
      if (!$generatedAnySignales) deleteRule($ruleId);
    }
    
    if (!$timeTriggeredRule)
    {
      // DIMM // STOP'N'GO Regeln
      if ($functions[3]["active"])
      {
        $signalType = $functions[3]["signalType"];
        $ruleSignalType = $functions[3]["ruleSignalType"];
        
        // Wenn wir hoch und runter fahren können, muss stop&go togglen
        if ($functions[1]["active"] && $functions[2]["active"]) $actionParamValue = $paramToToggle;
        else if ($functions[1]["active"]) $actionParamValue = $paramToOpen;
        else if ($functions[2]["active"]) $actionParamValue = $paramToClose;
        else $actionParamValue = $paramToToggle; //keine funktion gewählt toggled
          
        // Regel anlegen für holdStart
        QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,extras,intraDay, groupLock) 
                             values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','0','0','$ruleSignalType','1','1','$obj->extras','$obj->intraDay','0')" );
        $ruleId = query_insert_id ();
          
        // Actions ergänzen
        foreach ($myInstances as $myInstanceId)
        {
           QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                      values('$ruleId','$myInstanceId','$startFunctionId','1')" );
           $newRuleActionId = query_insert_id ();
              
           QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) 
                                           values('$newRuleActionId','142','$actionParamValue','1')" );
        }
  
        // Signale ergänzen
        $generatedAnySignales = generateSignalsForGeneratedGroup($obj->id, $ruleId, $completeGroupFeedback, $signalType, $groupId);
        if (!$generatedAnySignales) deleteRule($ruleId);
          
        // Bei Hold können wir noch holdEnd verwenden
        if ($signalType == "hold")
        {
          $ruleSignalType = "holdEnd";
           
          QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,extras,intraDay,groupLock) 
                               values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','0','0','$ruleSignalType','1','1','$obj->extras','$obj->intraDay','0')" );
          $ruleId = query_insert_id ();
          
          // Actions ergänzen
          foreach ($myInstances as $myInstanceId)
          {
            QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                       values('$ruleId','$myInstanceId','$stopFunctionId','1')" );
          }  
            
          // Signale ergänzen
          $erg2 = QUERY ( "select basicRuleSignals.id, featureInstanceId, featureClassesId, featureInstances.id as checkId 
                           from basicRuleSignals 
                           left join featureInstances on (featureInstances.id=basicRuleSignals.featureInstanceId) 
                           where ruleId='$obj->id' order by basicRuleSignals.id" );
          while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
          {
             if ($obj2->checkId == null) continue;
             $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
             $evHoldEndFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evHoldEnd" );
              
             QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                        values('$ruleId','$obj2->featureInstanceId','$evHoldEndFunctionId','1','$completeGroupFeedback')" );
             $ruleSignalId = query_insert_id ();
              
             if ($signalClassesId == $irClassesId)
             {
               $param1Value = "";
               $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
               while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
               {
                  if ($param1Value == "") $param1Value = $row [0];
                  else $param2Value = $row [0];
               }
               
               if ($param1Value == "") die ( "Params zu ruleSignalId $obj2->id nicht gefunden -9" );
                
               $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldEnd", "address" );
               QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
           		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
               $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldEnd", "command" );
               QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
           		                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
             }
          }
        }
      }
      
      // PRESET Regeln
      if ($functions[4]["active"])
      {
        $signalType = $functions[4]["signalType"];
        $ruleSignalType = $functions[4]["ruleSignalType"];
            
        // Regel anlegen
        QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,extras,intraDay,groupLock) 
                             values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','0','0','$ruleSignalType','1','1','$obj->extras','$obj->intraDay','0')" );
        $ruleId = query_insert_id ();
          
        // Actions ergänzen
        foreach ($myInstances as $myInstanceId)
        {
          QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                     values('$ruleId','$myInstanceId','$moveToPositionFunctionId','1')" );
          $newRuleActionId = query_insert_id ();
              
          QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) 
                                          values('$newRuleActionId','$paramPosition','$obj->fkt4','1')" );
        }
        
        // Signale ergänzen
        $generatedAnySignales = generateSignalsForGeneratedGroup($obj->id, $ruleId, $completeGroupFeedback, $signalType, $groupId);
        if (!$generatedAnySignales) deleteRule($ruleId);
      }
  
      
      // Stop Regeln bei Rollos
      if ($functions[5]["active"])
      {
        $signalType = $functions[5]["signalType"];
        $ruleSignalType = $functions[5]["ruleSignalType"];
        $stopAndGo = $functions[5]["stopAndGo"];
        
		/*if ($groupId==307)
		{
			 echo "<pre>"; print_r($functions); echo "<br></pre>";
		}*/
		
        if ($stopAndGo) $startState = $secondState;
        else $startState =0;
  
        QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`,extras,intraDay, groupLock) 
                             values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','$startState','0','$ruleSignalType','1','1','$obj->extras','$obj->intraDay','0')" );
        $ruleId = query_insert_id ();
  
        // Actions ergänzen
        foreach ($myInstances as $myInstanceId)
        {
          QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                     values('$ruleId','$myInstanceId','$stopFunctionId','1')" );
        }
        
        // Signale ergänzen
        $generatedAnySignales = generateSignalsForGeneratedGroup($obj->id, $ruleId, $completeGroupFeedback, $signalType, $groupId);
        if (!$generatedAnySignales) deleteRule($ruleId);
      }
    }
  } //über alle BaseRules
  
  // Dummyregeln ergänzen
  // Bei Gruppen mir mehreren Aktoren generieren wir keine Statewechseldummies
  if ($myInstanceCount == 1) 
  {
    // Dummy für evOn und evOff
    QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,syncEvent,`generated`) 
                       values('$groupId','7','31','255','7','31','255','0','$firstState','evOn','1','1','1')" );
    $ruleId = query_insert_id ();
    
    $statusFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "evOpen" );
    QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                               values('$ruleId','$myInstanceId','$statusFunctionId','1')" );
    
    $statusFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "evClosed" );
    QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                               values('$ruleId','$myInstanceId','$statusFunctionId','1')" );
    $signalId = query_insert_id ();
      
    $positionParamId = getClassesIdFunctionParamIdByName ( $rolloClassesId, "evClosed", "position" );
      
    QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                    values('$signalId','$positionParamId','255')" );
									
    // evStart
    QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,syncEvent,`generated`) 
                         values('$groupId','7','31','255','7','31','255','0','$secondState','evOn','1','1','1')" );
    $ruleId = query_insert_id ();
    
    $statusFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "evStart" );
    $positionParamId = getClassesIdFunctionParamIdByName ( $rolloClassesId, "evStart", "direction" );

    QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                               values('$ruleId','$myInstanceId','$statusFunctionId','1')" );
    $signalId = query_insert_id ();
       
    QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                    values('$signalId','$positionParamId','255')" );

/*
    QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                               values('$ruleId','$myInstanceId','$statusFunctionId','1')" );
    $signalId = query_insert_id ();
       
    QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                    values('$signalId','$positionParamId','0')" );

    QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                               values('$ruleId','$myInstanceId','$statusFunctionId','1')" );
    $signalId = query_insert_id ();
       
    QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                    values('$signalId','$positionParamId','1')" );
									*/
  } 
  else
  {
  	generateSyncEventsRollladen($groupId, $firstState, $secondState);
  }
}

function generateSyncEventsRollladen($groupId, $firstState, $secondState)
{
  global $CONTROLLER_CLASSES_ID, $rolloClassesId;
  global $debug;
  global $signalParamWildcard,$signalParamWildcardWord;
  
  $evGroupOnFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOn" );
  $evGroupOffFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOff" );
  $evGroupUndefinedFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined" );
  
  $completeGroupFeedback=2;
  
  QUERY ( "DELETE from groupSyncHelper where groupId='$groupId'" );
  
  // Jedem Aktor der Gruppe einen Index zuweisen
  $index = 0;
  $groupThreshold = 0;
  unset ( $indexList );
  $erg2 = QUERY ( "select distinct featureInstanceId from ruleActions join rules on (rules.id=ruleActions.ruleId) where groupId='$groupId' order by featureInstanceId" );
  while ( $row = MYSQLi_FETCH_ROW ( $erg2 ) )
  {
    $indexList [$row [0]] = $index ++;
  }
  
  $totalMembers = count ( $indexList );
  if ($totalMembers == 0) return;
  if ($totalMembers > 64) die ( "Fehler: Gruppe mit mehr als 64 Membern gefunden -> $totalMembers" );
    
  // Passende Gruppe(n) erstellen
  // Wenn mehr als 8 Member beteiligt sind müssen wir Gruppen kaskadieren
  // Zum Verwalten des Gruppenstatus wird dann eine Obergruppe verwendet, die als Member so viele Untergruppen verwendet, wie Aktoren im Spiel sind.
  // Maximal also 8x8 = 64 Member
  $nrNeededGroups = ( int ) ($totalMembers / 16);
  if ($totalMembers % 16 > 0) $nrNeededGroups ++;
  if ($nrNeededGroups > 1) $schachtelNeeded = 1;
    
    // Wir versuchen den ersten Aktor als Host für Gruppe zu verwenden
  //$erg4 = QUERY ( "select featureInstanceId from groupFeatures where groupId='$groupId' order by id limit 1" );
  //$row4 = MYSQLi_FETCH_ROW ( $erg4 );
  //$firstAktor = $row4 [0];
  
  unset ( $syncGroups );
  for($i = 0; $i < $nrNeededGroups; $i ++)
  {
	$resultObj = registerSignalGroupOnController($groupId, "aktor");
	$controllerId = $resultObj->controllerId;
	$myGroupIndex = $resultObj->groupIndex;
	$controllerInstanceId = $resultObj->controllerInstanceId;
	
	/*
    // Freie Gruppe auf Controller der beteiligten Aktoren suchen.
    // Dabei zun㢨st versuchen, das erste Feature zu treffen, weil submitRules die Regel auf den Controller des ersten Features packt.
    // Dadurch werden die setGroupState Aufrufe alle intern abgehandelt
    $controllerId = - 1;
    $erg2 = QUERY ( "select distinct(controllerId) from featureInstances join groupFeatures on (groupFeatures.featureInstanceId=featureInstances.id) join controller on (featureInstances.controllerId=controller.id) where groupId='$groupId' and size!=999 order by featureInstances.id='$firstAktor', featureInstances.id" );
    while ( $row = MYSQLi_FETCH_ROW ( $erg2 ) )
    {
      $controllerId = $row [0];
      
      $myGroupIndex = 0;
      $erg3 = QUERY ( "select groupIndex from groupSyncHelper where controllerId='$controllerId' order by groupIndex desc limit 1" );
      if ($row3 = MYSQLi_FETCH_ROW ( $erg3 )) $myGroupIndex = $row3 [0] + 1;
        
        // Controller ist schon voll.
      if ($myGroupIndex > 7)
      {
        $controllerId = - 1;
        continue;
      } else
        break;
    }
    
    if ($controllerId == - 1)
    {
      echo "select distinct(controllerId) from featureInstances join groupFeatures on (groupFeatures.featureInstanceId=featureInstances.id) join controller on (featureInstances.controllerId=controller.id) where groupId='$groupId' and size!=999 order by featureInstances.id='$firstAktor', featureInstances.id<br>";
      echo "select distinct(controllerId) from featureInstances join groupFeatures on (groupFeatures.featureInstanceId=featureInstances.id) where groupId='$groupId' <br>";
      die ( "Fehler: keine freie Gruppe gefunden" );
    }
    
    // Gruppe reservieren
    QUERY ( "INSERT into groupSyncHelper (controllerId, groupIndex, groupId) values('$controllerId','$myGroupIndex','$groupId')" );
    

    // Controller als Instanz suchen
    $erg2 = QUERY ( "select id from featureInstances where controllerId='$controllerId' and featureClassesId='$CONTROLLER_CLASSES_ID' limit 1" );
    if ($row = MYSQLi_FETCH_ROW ( $erg2 )) $controllerInstanceId = $row [0];
    else die ( "Fehler: Controller Instance zu controllerId $controllerId nicht gefunden" );
      */

    //echo "HERM B: $groupId - $controllerId - $myGroupIndex <br>";
	  
      // Threshold der Gruppe anhand der Member definieren
    if ($i < $nrNeededGroups - 1) $groupThreshold = 16;
    else $groupThreshold = $totalMembers % 16;
    
    $syncGroups [$i] ["controllerId"] = $controllerId;
    $syncGroups [$i] ["controllerInstanceId"] = $controllerInstanceId;
    $syncGroups [$i] ["groupThreshold"] = $groupThreshold;
    $syncGroups [$i] ["myGroupIndex"] = $myGroupIndex;
    
    // Nach der letzten normalen Gruppe erstellen wir noch die Schaltungsgruppe
    if ($i == $nrNeededGroups - 1 && $schachtelNeeded == 1)
    {
      $schachtelNeeded = 0;
      $nrNeededGroups ++;
    }
  }
  
  // Pro Aktor ein Event für On-Zustand und Off-Zustand generieren und damit Bit in Syncgruppe schalten
  $syncActorDone = "";
  $erg2 = QUERY ( "select ruleId,featureInstanceId,groupStates.name as resultingStateName,groupStates.id as resultingStateId from ruleActions join rules on (rules.id=ruleActions.ruleId) left join groupStates on (groupStates.id=rules.resultingStateId) where rules.groupId='$groupId' order by ruleActions.id" );
  while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
  {
    $actFeatureIndex = $indexList [$obj2->featureInstanceId];
    $actGroupIndex = ( int ) ($actFeatureIndex / 16);
    $controllerId = $syncGroups [$actGroupIndex] ["controllerId"];
    $controllerInstanceId = $syncGroups [$actGroupIndex] ["controllerInstanceId"];
    $groupThreshold = $syncGroups [$actGroupIndex] ["groupThreshold"];
    $myGroupIndex = $syncGroups [$actGroupIndex] ["myGroupIndex"];
    $actFeatureIndex = $indexList [$obj2->featureInstanceId] % 16;
    
    if ($syncActorDone [$obj2->featureInstanceId] != 1)
    {
      $syncActorDone [$obj2->featureInstanceId] = 1;
      
      // Dummy für fährt
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,syncEvent,`generated`) 
                           values('$groupId','7','31','255','7','31','255','0','0','evOn','1','1','1')" );
      $ruleId = query_insert_id ();
      
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                 values('$ruleId','$controllerInstanceId','169','1')" );
      $newRuleActionId = query_insert_id ();
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','326','$myGroupIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','327','$actFeatureIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','328','1','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','349','$groupThreshold','1')" );
      
      $statusFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "evStart" );
      $positionParamId = getClassesIdFunctionParamIdByName ( $rolloClassesId, "evStart", "direction" );
	  
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                 values('$ruleId','$obj2->featureInstanceId','$statusFunctionId','1')" );
      $signalId = query_insert_id ();
       
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                      values('$signalId','$positionParamId','255')" );

      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                 values('$ruleId','$obj2->featureInstanceId','$statusFunctionId','1')" );
      $signalId = query_insert_id ();
       
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                      values('$signalId','$positionParamId','0')" );

      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                                 values('$ruleId','$obj2->featureInstanceId','$statusFunctionId','1')" );
      $signalId = query_insert_id ();
       
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                      values('$signalId','$positionParamId','1')" );
									  
        // Dummy für steht
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,syncEvent,`generated`) 
                           values('$groupId','7','31','255','7','31','255','0','0','evOff','1','1','1')" );
      $ruleId = query_insert_id ();
      
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                 values('$ruleId','$controllerInstanceId','169','1')" );
      $newRuleActionId = query_insert_id ();
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','326','$myGroupIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','327','$actFeatureIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','328','0','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','349','$groupThreshold','1')" );
      
      $statusFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "evClosed" );
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','$statusFunctionId','1')" );
      $signalId = query_insert_id ();
        
      $dimmerParamBrightnessId = getClassesIdFunctionParamIdByName ( $rolloClassesId, "evClosed", "position" );
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$signalId','$dimmerParamBrightnessId','$signalParamWildcard','1')" );
      
      $statusFunctionId = getClassesIdFunctionsIdByName ( $rolloClassesId, "evOpen" );
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$obj2->featureInstanceId','$statusFunctionId','1')" );
    }
  }
  
  // Wenn mehr als eine Gruppe nötig war, haben wir geschachtelt.
  // Dann hier die Subevents eintragen und später die Gesamtgruppe als Eventgeber eintragen
  if ($nrNeededGroups > 1)
  {
    $globalGroupIndex = $syncGroups [$nrNeededGroups - 1] ["myGroupIndex"];
    $globalControllerInstanceId = $syncGroups [$nrNeededGroups - 1] ["controllerInstanceId"];
    
    $nrSubGroups = $nrNeededGroups - 1;
    
    // Synchronisationsevents f� Subgruppen eintragen
    for($i = 0; $i < $nrSubGroups; $i ++)
    {
      $myGroupIndex = $syncGroups [$i] ["myGroupIndex"];
      $controllerInstanceId = $syncGroups [$i] ["controllerInstanceId"];
      
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`) 
                           values('$groupId','7','31','255','7','31','255','0','0','evOn','1','1')" );
      $actRuleId = query_insert_id ();
      
      if ($completeGroupFeedback != 1)
      {
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                   values('$actRuleId','$controllerInstanceId','$evGroupUndefinedFunctionId','1')" );
        $actSignalId = query_insert_id ();
        $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined", "index" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      }
      
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupOnFunctionId','1')" );
      $actSignalId = query_insert_id ();
      $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOn", "index" );
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$globalControllerInstanceId','169','1')" );
      $newRuleActionId = query_insert_id ();
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','326','$globalGroupIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','327','$i','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','328','1','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','349','$nrSubGroups','1')" );
      
      QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`) values('$groupId','7','31','255','7','31','255','0','0','evOff','1','1')" );
      $actRuleId = query_insert_id ();
      
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupOffFunctionId','1')" );
      $actSignalId = query_insert_id ();
      $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOff", "index" );
      QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      
      if ($completeGroupFeedback == 1)
      {
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupUndefinedFunctionId','1')" );
        $actSignalId = query_insert_id ();
        $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined", "index" );
        QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
      }
      
      QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$globalControllerInstanceId','169','1')" );
      $newRuleActionId = query_insert_id ();
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','326','$globalGroupIndex','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','327','$i','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','328','0','1')" );
      QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newRuleActionId','349','$nrSubGroups','1')" );
    }
    
    $myGroupIndex = $globalGroupIndex;
    $controllerInstanceId = $globalControllerInstanceId;
  }
  
  // Dann die Events der Synchronisationsgruppe eintragen um damit die Gruppenstates zu schalten
  // $evGroupOnFunctionId $evGroupOffFunctionId $evGroupUndefinedFunctionId
  QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`) 
                       values('$groupId','7','31','255','7','31','255','0','$secondState','evOn','1','1')" );
  $actRuleId = query_insert_id ();
  $onRuleId = $actRuleId;
  
  QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupOnFunctionId','1')" );
  $actSignalId = query_insert_id ();
  $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOn", "index" );
  QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
  
  QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`) 
                       values('$groupId','7','31','255','7','31','255','0','$firstState','evOff','1','1')" );
  $actRuleId = query_insert_id ();
  $offRuleId = $actRuleId;
  
  QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$actRuleId','$controllerInstanceId','$evGroupOffFunctionId','1')" );
  $actSignalId = query_insert_id ();
  $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupOff", "index" );
  QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
  
  // Was Signal undefined bewirken soll, h㭧t davon ab welches feedback gew� war
  if ($completeGroupFeedback == 1) $actRuleId = $offRuleId;
  else $actRuleId = $onRuleId;
  
  QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                             values('$actRuleId','$controllerInstanceId','$evGroupUndefinedFunctionId','1')" );
  $actSignalId = query_insert_id ();
  $indexParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evGroupUndefined", "index" );
  QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                  values('$actSignalId','$indexParamId','$myGroupIndex','1')" );
}

function createFunctionsArray($obj, $classesId)
{
	 global $functionTemplates;
	 
	 $functions = array();
	 
	 for ($i=1;$i<=5;$i++)
	 {
	   $fktFromObject = "fkt".$i;
	 	 
	   $functions[$i]["active"] = isFunctionActive ($obj->$fktFromObject) || $i==5;
	   
	   if ($functions[$i]["active"])
	   {
        $signalType = $functionTemplates [$classesId . "-$i-" . $obj->template];
        if ($signalType == "") $signalType = $functionTemplates ["-1-$i-" . $obj->template];
        if ($signalType == "" || $signalType == "-") $functions[$i]["active"]=false;
      
        $functions[$i]["signalType"]=$signalType;
        $ruleSignalType = $signalType;
        if ($signalType == "hold") $ruleSignalType = "holdStart";
        $functions[$i]["ruleSignalType"]=$ruleSignalType;
	    }
	 }

   if (($functions[1]["active"] && $functions[2]["active"] && $functions[1]["signalType"] == $functions[2]["signalType"]))
   {
   	  $functions[1]["toggle"]=true;
   	  $functions[2]["toggle"]=true;
   }
   
   if (($functions[5]["active"] && $functions[1]["active"] && $functions[1]["signalType"]==$functions[5]["signalType"]))
   {
	   $functions[1]["stopAndGo"]=true;
	   $functions[5]["stopAndGo"]=true;
   }
   if (($functions[5]["active"] && $functions[2]["active"] && $functions[2]["signalType"]==$functions[5]["signalType"]))
   {
	   $functions[2]["stopAndGo"]=true;
	   $functions[5]["stopAndGo"]=true;
   }
   
   return $functions;
}

function generateSignalsForGeneratedGroup($baseRuleId, $ruleId, $completeGroupFeedback, $signalType, $groupId)
{
  global $CONTROLLER_CLASSES_ID;
  global $signalParamWildcard,$signalParamWildcardWord;
  global $dimmerClassesId, $rolloClassesId, $ledClassesId, $schalterClassesId, $irClassesId, $tasterClassesId, $logicalButtonClassesId, $ethernetClassesId;
  global $startFunctionId, $stopFunctionId, $moveToPositionFunctionId, $paramToOpen, $paramToClose, $paramToToggle, $paramPosition;
  global $functionTemplates;
  global $ledLogicalButtonBrightness;
  global $configCache;

  $generatedSignals=false;

  $erg2 = QUERY ( "select basicRuleSignals.id, featureInstanceId, controllerId, featureClassesId, featureInstances.id as checkId 
                   from basicRuleSignals 
                   left join featureInstances on (featureInstances.id=basicRuleSignals.featureInstanceId) 
                   where ruleId='$baseRuleId' order by basicRuleSignals.id" );
  while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
  {
    if ($obj2->featureInstanceId < 0) // Signalgruppe
    {
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                   values('$ruleId','$obj2->featureInstanceId','0','1')" );
        continue;
    }
            
    if ($obj2->checkId == null) continue;
            
    $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );

    if ($signalClassesId == $irClassesId && $signalType != "click" && $signalType != "covered" && $signalType != "hold") continue;         
    
    $generatedSignals=true;
         
    // EV-Time vom Controller (Zeitsteuerung)
    if ($signalClassesId == $CONTROLLER_CLASSES_ID)
    {
       $erg3 = QUERY ( "select id from featureInstances where controllerId='$obj2->controllerId' and featureClassesId='$CONTROLLER_CLASSES_ID' limit 1" );
       if ($row = MYSQLi_FETCH_ROW ( $erg3 )) $controllerInstanceId = $row [0];
       else die ( "ControllerInstanz zu controllerId $obj2->controllerId nicht gefunden" );
              
       $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' limit 1" );
       if ($row = MYSQLi_FETCH_ROW ( $erg3 )) $timeParamValue = $row [0];
       else showRuleError ( "Regel ohne gültigen Parameterwert gefunden. RegelID = $obj2->id", $groupId );
              
       if ($timeParamValue == - 1) // evDay
       {
         $evDayFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evDay" );
         QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                          values('$ruleId','$controllerInstanceId','$evDayFunctionId','1')" );
       } 
       else if ($timeParamValue == - 2) // evNight
       {
         $evNightFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evNight" );
         QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                    values('$ruleId','$controllerInstanceId','$evNightFunctionId','1')" );
       } 
       else
       {
         $evTimeFunctionId = getClassesIdFunctionsIdByName ( $CONTROLLER_CLASSES_ID, "evTime" );
         QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) 
                                    values('$ruleId','$controllerInstanceId','$evTimeFunctionId','1')" );
         $ruleSignalId = query_insert_id ();
                
         $timeParamId = getClassesIdFunctionParamIdByName ( $CONTROLLER_CLASSES_ID, "evTime", "weekTime" );
         QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
                                         values('$ruleSignalId','$timeParamId','$timeParamValue','1')" );
       }
              
       return true;
    }
            
    if ($signalType == "click" || ($signalType == "covered" && $signalClassesId == $irClassesId))
    {
      $evClickedFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evClicked" );
              
      QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                 values('$ruleId','$obj2->featureInstanceId','$evClickedFunctionId','1','$completeGroupFeedback')" );
      $ruleSignalId = query_insert_id ();
              
      if ($signalClassesId == $irClassesId)
      {
         $param1Value = "";
         $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
         while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
         {
           if ($param1Value == "") $param1Value = $row [0];
           else $param2Value = $row [0];
         }
                
         if ($param1Value == "") die ( "Params zu ruleSignalId $obj2->id nicht gefunden -1" );
                
         $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evClicked", "address" );
         QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
     		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
         $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evClicked", "command" );
         QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
      	                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
       }
     } 
     else if ($signalType == "hold")
     {
       $evHoldStartFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evHoldStart" );
              
       QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                  values('$ruleId','$obj2->featureInstanceId','$evHoldStartFunctionId','1','$completeGroupFeedback')" );
       $ruleSignalId = query_insert_id ();
              
       if ($signalClassesId == $irClassesId)
       {
         $param1Value = "";
         $erg3 = QUERY ( "select paramValue from basicRuleSignalParams where ruleSignalId='$obj2->id' order by id limit 2" );
         while ( $row = MYSQLi_FETCH_ROW ( $erg3 ) )
         {
           if ($param1Value == "") $param1Value = $row [0];
           else $param2Value = $row [0];
         }
         if ($param1Value == "") die ( "Params zu ruleSignalId $obj2->id nicht gefunden -2" );
               
         $irParamAddressId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldStart", "address" );
         QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
    		                                 values('$ruleSignalId','$irParamAddressId','$param1Value','1')" );
         $irParamCommandId = getClassesIdFunctionParamIdByName ( $irClassesId, "evHoldStart", "command" );
         QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
     		                                 values('$ruleSignalId','$irParamCommandId','$param2Value','1')" );
       }
     } 
     else if ($signalType == "doubleClick")
     {
        $evDoubleClickFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evDoubleClick" );
            
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                   values('$ruleId','$obj2->featureInstanceId','$evDoubleClickFunctionId','1','$completeGroupFeedback')" );
     } 
     else if ($signalType == "covered")
     {
        $evCoveredFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evCovered" );
              
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                   values('$ruleId','$obj2->featureInstanceId','$evCoveredFunctionId','1','$completeGroupFeedback')" );
     } 
     else if ($signalType == "free")
     {
        $evFreeFunctionId = getClassesIdFunctionsIdByName ( $obj2->featureClassesId, "evFree" );
            
        QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`,completeGroupFeedback)
                                   values('$ruleId','$obj2->featureInstanceId','$evFreeFunctionId','1','$completeGroupFeedback')" );
     }
  }
  
  return $generatedSignals;
}

function isTimeTriggeredRule($baseRuleId)
{
	 global $CONTROLLER_CLASSES_ID;
                   
	 $erg2 = QUERY ( "select featureInstanceId, featureClassesId, featureInstances.id as checkId 
                   from basicRuleSignals 
                   left join featureInstances on (featureInstances.id=basicRuleSignals.featureInstanceId) 
                   where ruleId='$baseRuleId' order by basicRuleSignals.id" );
  while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
  {
    if ($obj2->featureInstanceId < 0) continue;
    if ($obj2->checkId == null) continue;
            
    $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );

    // EV-Time vom Controller (Zeitsteuerung)
    if ($signalClassesId == $CONTROLLER_CLASSES_ID) return true;
  }
  
  return false;
}

function registerSignalGroupOnController($groupId, $mode="any")
{
	global $CONTROLLER_CLASSES_ID;
	
	$erg = QUERY("select forcedController from groups where id='$groupId' limit 1");
	$obj=MYSQLi_FETCH_OBJECT($erg);
	
	
	/*
	 Mode any: irgend einen freien online Controller auswählen
	 Mode aktor: Controller der beteiligen Gruppenaktoren auswählen und dabei den ersten Aktor bevorzugen
	 Mode sensor: Controller der beteiligen Gruppensensoren auswählen und dabei den ersten Sensor bevorzugen
	 */
    $controllerId = - 1;
	$query="";
	if ($obj->forcedController>0) $query="select id from controller where id='$obj->forcedController' limit 1";
	else if ($mode=="aktor")
	{
       // Wir versuchen den ersten Aktor als Host für die Gruppe zu verwenden
       $erg = QUERY ( "select featureInstanceId from groupFeatures where groupId='$groupId' order by id limit 1" );
       $row = MYSQLi_FETCH_ROW ($erg);
       $firstAktor = $row[0];

	   $query="select distinct(controllerId) from featureInstances join groupFeatures on (groupFeatures.featureInstanceId=featureInstances.id) join controller on (featureInstances.controllerId=controller.id) where groupId='$groupId' and size!=999 order by featureInstances.id='$firstAktor', featureInstances.id";
	}
	else if ($mode=="sensor")
	{
       // Wir versuchen den ersten Sensor als Host für die Gruppe zu verwenden
	   $firstSensor="";
	   $sensorInstances="(1=2 ";
       $erg = QUERY ( "select distinct(featureInstanceId) from ruleSignals join rules on (rules.id=ruleSignals.ruleId) where groupId='$groupId' order by rules.id" );
       while ($row = MYSQLi_FETCH_ROW ($erg))
	   {
		 if ($row[0]==0) continue; // Zeitevents in Signalgruppen haben an dieser Stelle noch kein Instanz, weil der Controller noch nicht bekannt ist
		 
		 if ($firstSensor=="") $firstSensor=$row[0];
		 $sensorInstances.=" or featureInstances.id='".$row[0]."'";
	   }
	   $sensorInstances.=")";
	   
	   // Kann in Signalgruppen passieren, wo BITS nur durch Zeitevents gesteuert werden. Dann nehmen wir irgend einen Controller
	   if ($firstSensor=="") $query="select id from controller where size!='999' and online='1'";
	   else $query="select distinct(controllerId) from featureInstances join controller on (controller.id = featureInstances.controllerId) where $sensorInstances and size!=999 order by featureInstances.id='$firstSensor', featureInstances.id";
	}
	else
	{
	   $query="select id from controller where size!='999' and online='1'";
	}
	
    $erg = QUERY ($query);
	$num_rows = mysqli_num_rows($erg);
	if ($num_rows==0)
	{
		$erg = QUERY ("select id from controller where size!='999' and online='1'");
	}
	
    while ($row = MYSQLi_FETCH_ROW ($erg))
    {
      $controllerId = $row[0];
      
      $myGroupIndex = 0;
      $erg2 = QUERY ( "select groupIndex from groupSyncHelper where controllerId='$controllerId' order by groupIndex desc limit 1" );
      if ($row2 = MYSQLi_FETCH_ROW ($erg2)) $myGroupIndex = $row2[0] + 1;
        
      // Controller ist schon voll.
      if ($myGroupIndex > 7)
      {
        $controllerId = - 1;
        continue;
      } 
	  else
        break;
    }
    
    if ($controllerId == - 1)
    {
      echo "$query <br>";
      die ( "Fehler: keine freie Gruppe gefunden. GroupID = $groupId Mode = $mode" );
    }
    
    // Gruppe reservieren
    QUERY ( "INSERT into groupSyncHelper (controllerId, groupIndex, groupId) 
	                               values('$controllerId','$myGroupIndex','$groupId')" );
  
    // Controller als Instanz suchen
    $erg2 = QUERY ( "select id from featureInstances where controllerId='$controllerId' and featureClassesId='$CONTROLLER_CLASSES_ID' limit 1" );
    if ($row = MYSQLi_FETCH_ROW ( $erg2 )) $controllerInstanceId = $row [0];
    else die ( "Fehler: Controller Instance zu controllerId $controllerId nicht gefunden" );


    $resultObj = new stdClass();
	$resultObj->controllerId = $controllerId;
	$resultObj->groupIndex = $myGroupIndex;
	$resultObj->controllerInstanceId = $controllerInstanceId;
	
	return $resultObj;
}

function getNameForObjectId($objectId)
{
	$erg = QUERY("select featureInstances.name as featureInstanceName, 
	                     featureClasses.name as featureClassName, 
						 controller.name as controllerName
						 from featureInstances
						 join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
						 join controller on ( controller.id = featureInstances.controllerId)
						 where featureInstances.objectId = '$objectId' limit 1");
    if ($obj = MYSQLi_FETCH_OBJECT($erg))
	{
		if ($obj->featureClassName=="") {$add="<font color=#bb0000><b>";$add2="</b></font>";}
		$result = $add.$obj->controllerName.":".$obj->featureClassName;
		if ($obj->featureInstanceName!="") $result.=" -> ".$obj->featureInstanceName;
		$result.=$add2;
		return $result;
	}		
	return "";
}

function getNameForFunctionOrEvent($senderOrReceiver, $functionId)
{
	$classId = getClassId($senderOrReceiver);
						 
	$erg = QUERY("select featureFunctions.name as featureFunctionsName
						 from featureFunctions
						 join featureClasses on (featureClasses.id = featureFunctions.featureClassesId)
						 where featureFunctions.functionId = '$functionId' and featureClasses.classId = '$classId' limit 1");
						 
    if ($obj = MYSQLi_FETCH_OBJECT($erg))
	{
		return $obj->featureFunctionsName;
	}		
	return "";
}

//////////// LOXONE   ////////////
/*
  // 1 = 16-Relais 
  // 2 = 8-Rollos (weg)
  // 3 = 6-Taster
  // 4 = IO-Modul
  // 5 = 8-Relais
  // 6 = 8-Dimmer
  // 7 = 4-Taster
  // 8 = 2-Taster
  // 9 = 1-Taster
  // 10 = 16-Relais V2
  // 11 = 32-IO
  // 12 = 4-fach Dimmer 0-10 / 1-10V
  // 13 = 6-Relais V2
  // 14 = 8-Dimmer V2
  // 15 = Brückenmodul
  // 16 = RGB Dimmer
*/
function getLoxoneDeviceType($elements, $sender)
{
  $deviceId = getDeviceId($sender);
  $controllerObj = getController($sender);
  $controllerId = $controllerObj->id;
  $fcke = $controllerObj->fcke;
  $firmwareId = $controllerObj->firmwareId;
  
  $myConfigId = $deviceId."laterConfig";
  
  $nrRelais=0;
  $nrTaster=0;
  $nrLeds=0;
  $nrRollos=0;
  $nrDimmer=0;
  foreach ($elements as $value)
  {
    $parts = explode(",",$value);
    $objectId = getObjectId($deviceId, $parts[1], $parts[0]);
    $featureClassesId = getFeatureClassesId($objectId);
    if ($featureClassesId==8) $nrRelais++;
    if ($featureClassesId==1) $nrTaster++;
    if ($featureClassesId==18) $nrLeds++;
    if ($featureClassesId==14) $nrRollos++;
	if ($featureClassesId==9) $nrDimmer++;
  }

  $renamed=0;
  $erg = QUERY("select renamed from controller where id='$controllerId' and renamed='1' limit 1");
  if ($row=MYSQLi_FETCH_ROW($erg)) $renamed=1;
  
  if ($nrRelais==17 && $nrTaster==16) // 32 IO Hutschienenmodul. 16 Ausgänge + rote LED und 16 Eingänge
  {
    $myName="32-IO Modul ID $deviceId";
    $myTemplate=11;
	if ($renamed==1) return $myTemplate;
	
    $erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
    if ($row=MYSQLi_FETCH_ROW($erg))
    {
      if ($_SESSION[$myConfigId]>0)
	  {
		  $_SESSION[$myConfigId]=0;
		  baseConfigureInputsAndOutputs($sender, $myTemplate);
	  }
  	}
  	else
  	{
  	  QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
      /*if (query_affected_rows()>0)
      {
        callInstanceMethodForObjectId(getObjectId($deviceId, 162, 1), 163);
        usleep(100000);
      }*/
	  
	  baseConfigureInputsAndOutputs($sender, $myTemplate);
  	}
  	  
  	return $myTemplate;
  }
  else if ($nrRelais==17) // 16-fach Relaismodul + 1 externer Schalter
  {
	if ($nrTaster==8)
	{
	  $myName="16x7A Relais V2 ID $deviceId";
      $myTemplate=10;
	  if ($renamed==1) return $myTemplate;
	
      $erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
      if ($row=MYSQLi_FETCH_ROW($erg))
      {
        if ($_SESSION[$myConfigId]>0)
        {
      	  if (time()-$_SESSION[$myConfigId]>1)
    	  {
    	    $_SESSION[$myConfigId]=0;
  	  	    baseConfigureInputsAndOutputs($sender, $myTemplate);
		  
            callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
 	  	  }
  	    }
	  }
	  else
  	  {
  	    QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
        if (query_affected_rows()>0)
        {
          callInstanceMethodForObjectId(getObjectId($deviceId, 162, 1), 163);
          usleep(100000);
        }
  	    
		baseConfigureInputsAndOutputs($sender, $myTemplate);
  	  }
  	  
  	  return $myTemplate;
	}
    else	
	{
      $myName="16x7A Relais ID $deviceId";
      $myTemplate=1;
	  if ($renamed==1) return $myTemplate;
	
      $erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
      if ($row=MYSQLi_FETCH_ROW($erg))
      {
        if ($_SESSION[$myConfigId]>0)
        {
      	  if (time()-$_SESSION[$myConfigId]>1)
    	  {
    	    $_SESSION[$myConfigId]=0;
  	  	    baseConfigureInputsAndOutputs($sender, $myTemplate);
		  
            callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
 	  	  }
  	    }
	  }
  	  else
  	  {
  	    QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
        if (query_affected_rows()>0)
        {
          callInstanceMethodForObjectId(getObjectId($deviceId, 162, 1), 163);
          usleep(100000);
        }

        if ($nrTaster!=12)
        {
    	    baseConfigureIoPorts($sender, $myTemplate);
  	 	  $_SESSION[$myConfigId]=time();
  	    }
  	    else baseConfigureInputsAndOutputs($sender, $myTemplate);
  	  }
  	  
  	  return $myTemplate;
	}
  }
  else if ($nrTaster==12 && $nrLeds==12 && $nrRelais==1) // IO-Modul
  {
  	$myName="IO-Modul ID $deviceId";
  	$myTemplate=4;
	if ($renamed==1) return $myTemplate;
	
  	$erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
  	if ($row=MYSQLi_FETCH_ROW($erg)) {}
  	else
  	{
  	  QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
	  if (query_affected_rows()>0)
      {
        callInstanceMethodForObjectId(getObjectId($deviceId, 162, 1), 163);
        usleep(100000);
      }

  	  baseConfigureInputsAndOutputs($sender, $myTemplate);
  	}
  	  
  	return $myTemplate;
  }
  else if ($nrTaster==4 && $nrLeds==4 && $nrRelais==1) // 4-fach Dimmer
  {
  	$myName="4-fach Dimmer ID $deviceId";
  	$myTemplate=12;
	if ($renamed==1) return $myTemplate;
	
    $erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
    if ($row=MYSQLi_FETCH_ROW($erg))
    {
      if ($_SESSION[$myConfigId]>0)
	  {
		  $_SESSION[$myConfigId]=0;
		  baseConfigureInputsAndOutputs($sender, $myTemplate);
	  }
  	}
  	else
  	{
  	  QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
	  
	  baseConfigureInputsAndOutputs($sender, $myTemplate);
  	}
  	  
  	return $myTemplate;
  }
  else if ($nrTaster==6 && $nrDimmer==6 && $nrRelais==1) // 6-fach RGB DIMMER
  {
  	$myName="6-fach RGB Dimmer ID $deviceId";
  	$myTemplate=16;
	if ($renamed==1) return $myTemplate;
	
    $erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
    if ($row=MYSQLi_FETCH_ROW($erg))
    {
      if ($_SESSION[$myConfigId]>0)
	  {
		  $_SESSION[$myConfigId]=0;
		  baseConfigureInputsAndOutputs($sender, $myTemplate);
	  }
  	}
  	else
  	{
  	  QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
	  
	  baseConfigureInputsAndOutputs($sender, $myTemplate);
  	}
  	  
  	return $myTemplate;
  }
  else if (($nrTaster==6 && $nrLeds==6) || ($nrTaster==12 && $nrLeds==12)) // 6-fach Taster
  {
  	$myName="6-fach Taster ID $deviceId";
  	$myTemplate=3;
	if ($renamed==1) return $myTemplate;
	
  	$erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
  	if ($row=MYSQLi_FETCH_ROW($erg)) {}
  	else
  	{
  	  QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
	  if (query_affected_rows()>0)
      {
        callInstanceMethodForObjectId(getObjectId($deviceId, 162, 1), 163);
        usleep(100000);
      }
	  
      if ($nrTaster!=12)
      {
  	    baseConfigureIoPorts($sender, $myTemplate);
  	 	$_SESSION[$myConfigId]=time();
  	  }
  	  else baseConfigureInputsAndOutputs($sender, $myTemplate);
  	}
  	  
  	return $myTemplate;
  }
  else if (($nrTaster==4 && $nrLeds==4) || ($nrTaster==10 && $nrLeds==10)) // 4-fach Taster
  {
  	$myName="4-fach Taster ID $deviceId";
  	$myTemplate=7;
	if ($renamed==1) return $myTemplate;
	
  	$erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
  	if ($row=MYSQLi_FETCH_ROW($erg)) {}
  	else
  	{
  	  QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
	  if (query_affected_rows()>0)
      {
        callInstanceMethodForObjectId(getObjectId($deviceId, 162, 1), 163);
        usleep(100000);
      }
	  
      if ($nrTaster!=10)
      {
  	    baseConfigureIoPorts($sender, $myTemplate);
  	 	$_SESSION[$myConfigId]=time();
  	  }
  	  else baseConfigureInputsAndOutputs($sender, $myTemplate);

  	  baseConfigureInputsAndOutputs($sender, $myTemplate);
  	}
  	  
  	return $myTemplate;
  }
  else if (($nrTaster==2 && $nrLeds==2) || ($nrTaster==8 && $nrLeds==12)) // 2-fach Taster
  {
  	$myName="2-fach Taster ID $deviceId";
  	$myTemplate=8;
	if ($renamed==1) return $myTemplate;
	
  	$erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
  	if ($row=MYSQLi_FETCH_ROW($erg)) {}
  	else
  	{
  	  QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
	  if (query_affected_rows()>0)
      {
        callInstanceMethodForObjectId(getObjectId($deviceId, 162, 1), 163);
        usleep(100000);
      }
	  
	  if ($nrTaster!=8)
      {
  	    baseConfigureIoPorts($sender, $myTemplate);
  	 	$_SESSION[$myConfigId]=time();
  	  }
  	  else baseConfigureInputsAndOutputs($sender, $myTemplate);
  	}
  	  
  	return $myTemplate;
  }
  else if (($nrTaster==1 && $nrLeds==1) || ($nrTaster==7 && $nrLeds==12)) // 1-fach Taster
  {
  	$myName="1-fach Taster ID $deviceId";
  	$myTemplate=9;
	if ($renamed==1) return $myTemplate;
	
  	$erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
  	if ($row=MYSQLi_FETCH_ROW($erg)) {}
  	else
  	{
  	  QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
	  if (query_affected_rows()>0)
      {
        callInstanceMethodForObjectId(getObjectId($deviceId, 162, 1), 163);
        usleep(100000);
      }
	  
	  if ($nrTaster!=7)
      {
  	    baseConfigureIoPorts($sender, $myTemplate);
  	 	$_SESSION[$myConfigId]=time();
  	  }
  	  else baseConfigureInputsAndOutputs($sender, $myTemplate);
  	}
  	  
  	return $myTemplate;
  }
  else if ($nrRelais==9 && $fcke==8) // 8-fach Relaismodul + 1 externer Schalter
  {
  	$myName="8x16A Relais V2 ID $deviceId";
  	$myTemplate=13;
    if ($renamed==1) return $myTemplate;
	
  	$erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
  	if ($row=MYSQLi_FETCH_ROW($erg)){}
  	else QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
  	  
  	return $myTemplate;
  }
  else if ($nrRelais==1 && $nrTaster==12) // Brückenmodul
  {
  	$myName="Brückenmodul ID $deviceId";
  	$myTemplate=15;
	if ($renamed==1) return $myTemplate;
	
  	$erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
  	if ($row=MYSQLi_FETCH_ROW($erg))
  	{
  	  if ($_SESSION[$myConfigId]>0)
  	  {
  	    if (time()-$_SESSION[$myConfigId]>1)
  	  	{
    	  $_SESSION[$myConfigId]=0;
  	  	  baseConfigureInputsAndOutputs($sender, $myTemplate);
 	  	}
  	  }
	}
  	else
  	{
  	  QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
	  if (query_affected_rows()>0)
      {
        callInstanceMethodForObjectId(getObjectId($deviceId, 162, 1), 163);
        usleep(100000);
      }

  	  if ($nrTaster!=12)
  	  {
  	    baseConfigureIoPorts($sender, $myTemplate);
  	  	$_SESSION[$myConfigId]=time();
  	  }
  	  else baseConfigureInputsAndOutputs($sender, $myTemplate);
  	}
	
	return $myTemplate;
  }
  else if ($nrRelais==9) // 8-fach Relaismodul + 1 externer Schalter
  {
  	$myName="8x16A Relais ID $deviceId";
  	$myTemplate=5;
	if ($renamed==1) return $myTemplate;
	
  	$erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
  	if ($row=MYSQLi_FETCH_ROW($erg))
  	{
  	  if ($_SESSION[$myConfigId]>0)
  	  {
  	    if (time()-$_SESSION[$myConfigId]>1)
  	  	{
    	  $_SESSION[$myConfigId]=0;
  	  	  baseConfigureInputsAndOutputs($sender, $myTemplate);
 	  	}
  	  }
	}
  	else
  	{
  	  QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
	  if (query_affected_rows()>0)
      {
        callInstanceMethodForObjectId(getObjectId($deviceId, 162, 1), 163);
        usleep(100000);
      }

  	  if ($nrTaster!=12)
  	  {
  	    baseConfigureIoPorts($sender, $myTemplate);
  	  	$_SESSION[$myConfigId]=time();
  	  }
  	  else baseConfigureInputsAndOutputs($sender, $myTemplate);
  	}
  	  
  	return $myTemplate;
  }
  else if ($nrDimmer==8 && $nrRelais==1 && $fcke==40) // 8-fach Dimmermodul + 1 externer Schalter
  {
  	$myName="8x Dimmer V2 ID $deviceId";
  	$myTemplate=14;
    if ($renamed==1) return $myTemplate;
	
  	$erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
    if ($row=MYSQLi_FETCH_ROW($erg))
    {
      if ($_SESSION[$myConfigId]>0)
      {
    	if (time()-$_SESSION[$myConfigId]>1)
    	{
    	  $_SESSION[$myConfigId]=0;
  	  	  baseConfigureInputsAndOutputs($sender, $myTemplate);
 	  	}
  	  }
  	}
  	else
  	{
  	  QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
      if (query_affected_rows()>0)
      {
        callInstanceMethodForObjectId(getObjectId($deviceId, 162, 1), 163);
        usleep(100000);
      }
	  
      baseConfigureInputsAndOutputs($sender, $myTemplate);
  	}
  	  
  	return $myTemplate;
  }

  else if ($nrDimmer==8 && $nrRelais==1) // 8-fach Dimmermodul + 1 externer Schalter
  {
    $myName="8x Dimmer ID $deviceId";
    $myTemplate=6;
	if ($renamed==1) return $myTemplate;
	
    $erg = QUERY("select id from controller where name='$myName' and id='$controllerId' limit 1");
    if ($row=MYSQLi_FETCH_ROW($erg))
    {
      if ($_SESSION[$myConfigId]>0)
      {
    	if (time()-$_SESSION[$myConfigId]>1)
    	{
    	  $_SESSION[$myConfigId]=0;
  	  	  baseConfigureInputsAndOutputs($sender, $myTemplate);
 	  	}
  	  }
  	}
  	else
  	{
  	  QUERY("UPDATE controller set name='$myName' where id='$controllerId' limit 1");
      if (query_affected_rows()>0)
      {
        callInstanceMethodForObjectId(getObjectId($deviceId, 162, 1), 163);
        usleep(100000);
      }
	  
      if ($nrTaster!=12)
      {
  	    baseConfigureIoPorts($sender, $myTemplate);
  	 	$_SESSION[$myConfigId]=time();
  	  }
  	  else baseConfigureInputsAndOutputs($sender, $myTemplate);
  	}
  	  
  	return $myTemplate;
  }
}

function baseConfigureModules($objectId, $configTemplate)
{
    $deviceId = getDeviceId($objectId);
    $controllerObj = getController($objectId);
    $controllerId = $controllerObj->id;
    $fcke = $controllerObj->fcke;

	if ($configTemplate==1 || $configTemplate==10) // 16 Relais
	{
  	 // SlotTypes auf Controller configurieren 40 = setConfiguration
  	 callInstanceMethodForObjectId($objectId, 40, array("startupDelay" => 0,"logicalButtonMask" => 0,"deviceId" => $deviceId,"reportMemoryStatusTime" => 0,"slotType0" => 7,"slotType1" => 7,"slotType2" => 7,"slotType3" => 7,"slotType4" => 7,"slotType5" => 7,"slotType6" => 7,"slotType7" => 7));
	}	
	else if ($configTemplate==2) // 8 Rollos
	{
  	 // SlotTypes auf Controller configurieren 40 = setConfiguration
  	 callInstanceMethodForObjectId($objectId, 40, array("startupDelay" => 0,"logicalButtonMask" => 0,"deviceId" => $deviceId,"reportMemoryStatusTime" => 0,"slotType0" => 3,"slotType1" => 3,"slotType2" => 3,"slotType3" => 3,"slotType4" => 3,"slotType5" => 3,"slotType6" => 3,"slotType7" => 3));
	}	
    else if ($configTemplate==5) // 8 Relais
	{
  	 // SlotTypes auf Controller configurieren 40 = setConfiguration
  	 callInstanceMethodForObjectId($objectId, 40, array("startupDelay" => 0,"logicalButtonMask" => 0,"deviceId" => $deviceId,"reportMemoryStatusTime" => 0,"slotType0" => 2,"slotType1" => 2,"slotType2" => 2,"slotType3" => 2,"slotType4" => 2,"slotType5" => 2,"slotType6" => 2,"slotType7" => 2));
	}	
    else if ($configTemplate==3 || $configTemplate==7 || $configTemplate==8 || $configTemplate==9 ) // 6-fach, 4-fach, 2-fach, 1-fach Taster
	{
      // Viktor legt jetzt immer einen LogicalButton selbst an bzw bei alten Tastern ist der bereits vorhanden
  	 // LogicalButton auf Controller configurieren 40 = setConfiguration
  	 // callInstanceMethodForObjectId($objectId, 40, array("startupDelay" => 0,"logicalButtonMask" => 1,"deviceId" => $deviceId,"reportMemoryStatusTime" => 0,"slotType0" => 0,"slotType1" => 0,"slotType2" => 0,"slotType3" => 0,"slotType4" => 0,"slotType5" => 0,"slotType6" => 0,"slotType7" => 0));
	}	
}

/*
  // 1 = 16-Relais 
  // 2 = 8-Rollos (weg)
  // 3 = 6-Taster
  // 4 = IO-Modul
  // 5 = 8-Relais
  // 6 = 8-Dimmer
  // 7 = 4-Taster
  // 8 = 2-Taster
  // 9 = 1-Taster
  // 10 = 16-Relais V2
  // 11 = 32-IO
  // 12 = 4-fach Dimmer
  // 13 = 8-Relais V2
  // 14 = 8-Dimmer V2
  // 15 = Brückenmodul
  // 16 = RGB Dimmer
*/
function baseConfigureIoPorts($objectId, $configTemplate, $force=false)
{
	global $lastLogId;
    $deviceId = getDeviceId($objectId);
    $controllerObj = getController($objectId);
    $controllerId = $controllerObj->id;
    $fcke = $controllerObj->fcke;
    $firmwareId = $controllerObj->firmwareId;

    if ($force)
	{
		
    // 1 = 16 Relais // 2 = 8 Rollos // 6 = 8 Dimmer  //  15 = Brückenmodul
	if ($configTemplate==1 || $configTemplate==2 || $configTemplate==6 || $configTemplate==15) 
	{
  	  // Taster an Port 80 und 96 konfigurieren 1 // 75 = setConfiguration auf IO Port
  	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 80), 75, array("pin0" => 255,"pin1" => 255,"pin2" => 255,"pin3" => 255,"pin4" => 16,"pin5" => 16,"pin6" => 16,"pin7" => 16));
  	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 96), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 16,"pin3" => 16,"pin4" => 16,"pin5" => 16,"pin6" => 16,"pin7" => 16));
  	  callInstanceMethodForObjectId($objectId,38); // 38 = reset auf controller
	}	
	// 16 Relais V2
    else if ($configTemplate==10) 
	{
  	  // Taster an Port 80 und 96 konfigurieren 1 // 75 = setConfiguration auf IO Port
  	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 96), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 16,"pin3" => 16,"pin4" => 16,"pin5" => 16,"pin6" => 16,"pin7" => 16));
  	  callInstanceMethodForObjectId($objectId,38); // 38 = reset auf controller
	}
	// 3 = 6-fach Taster
	else if ($configTemplate==3) 
	{
	  if ($firmwareId==8) // HBC
	  {
		// Port 16 und 48 sind nur noch intern und vorkonfiguriert
  	    // Externe Taster und LEDs an Port 32, 64 konfigurieren // 75 = setConfiguration auf IO Port
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 16,"pin3" => 16,"pin4" => 16,"pin5" => 16,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 75, array("pin0" => 21,"pin1" => 21,"pin2" => 21,"pin3" => 21,"pin4" => 21,"pin5" => 21,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId($objectId,38); // 38 = reset auf controller
	  }
	  else
	  {
  	    // Taster und LEDs an Port 16, 32, 48, 64 konfigurieren // 75 = setConfiguration auf IO Port
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 16), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 16,"pin3" => 16,"pin4" => 16,"pin5" => 16,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 16,"pin3" => 16,"pin4" => 255,"pin5" => 255,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 48), 75, array("pin0" => 21,"pin1" => 21,"pin2" => 21,"pin3" => 21,"pin4" => 21,"pin5" => 21,"pin6" => 16,"pin7" => 16));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 75, array("pin0" => 21,"pin1" => 21,"pin2" => 21,"pin3" => 21,"pin4" => 21,"pin5" => 21,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId($objectId,38); // 38 = reset auf controller
	  }
	}	
	// 7 = 4-fach Taster
	else if ($configTemplate==7) 
	{
      if ($firmwareId==8) // HBC
	  {
		// Port 16 und 48 sind nur noch intern und vorkonfiguriert
  	    // Externe Taster und LEDs an Port 32, 64 konfigurieren // 75 = setConfiguration auf IO Port
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 255,"pin3" => 255,"pin4" => 16,"pin5" => 16,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 75, array("pin0" => 21,"pin1" => 21,"pin2" => 21,"pin3" => 21,"pin4" => 21,"pin5" => 21,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId($objectId,38); // 38 = reset auf controller
	  }
	  else
	  {
  	     // Taster und LEDs an Port 16, 32, 48, 64 konfigurieren // 75 = setConfiguration auf IO Port
  	     callInstanceMethodForObjectId(getObjectId($deviceId, 15, 16), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 255,"pin3" => 255,"pin4" => 16,"pin5" => 16,"pin6" => 255,"pin7" => 255));
  	     callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 16,"pin3" => 16,"pin4" => 255,"pin5" => 255,"pin6" => 255,"pin7" => 255));
  	     callInstanceMethodForObjectId(getObjectId($deviceId, 15, 48), 75, array("pin0" => 21,"pin1" => 21,"pin2" => 255,"pin3" => 255,"pin4" => 21,"pin5" => 21,"pin6" => 16,"pin7" => 16));
  	     callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 75, array("pin0" => 21,"pin1" => 21,"pin2" => 21,"pin3" => 21,"pin4" => 21,"pin5" => 21,"pin6" => 255,"pin7" => 255));
  	     callInstanceMethodForObjectId($objectId,38); // 38 = reset auf controller
	  }
	}	
	// 8 = 2-fach Taster
	else if ($configTemplate==8) 
	{
      if ($firmwareId==8) // HBC
	  {
		// Port 16 und 48 sind nur noch intern und vorkonfiguriert
  	    // Externe Taster und LEDs an Port 32, 64 konfigurieren // 75 = setConfiguration auf IO Port
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 75, array("pin0" => 255,"pin1" => 255,"pin2" => 16,"pin3" => 16,"pin4" => 255,"pin5" => 255,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 75, array("pin0" => 21,"pin1" => 21,"pin2" => 21,"pin3" => 21,"pin4" => 21,"pin5" => 21,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId($objectId,38); // 38 = reset auf controller
	  }
	  else
	  {
 	    // Taster und LEDs an Port 16, 32, 48, 64 konfigurieren // 75 = setConfiguration auf IO Port
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 16), 75, array("pin0" => 255,"pin1" => 255,"pin2" => 16,"pin3" => 16,"pin4" => 255,"pin5" => 255,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 16,"pin3" => 16,"pin4" => 255,"pin5" => 255,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 48), 75, array("pin0" => 255,"pin1" => 255,"pin2" => 21,"pin3" => 21,"pin4" => 255,"pin5" => 255,"pin6" => 16,"pin7" => 16));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 75, array("pin0" => 21,"pin1" => 21,"pin2" => 21,"pin3" => 21,"pin4" => 21,"pin5" => 21,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId($objectId,38); // 38 = reset auf controller
	  }
	}	
	// 9 = 1-fach Taster
	else if ($configTemplate==9) 
	{
      if ($firmwareId==8) // HBC
	  {
		// Port 16 und 48 sind nur noch intern und vorkonfiguriert
  	    // Externe Taster und LEDs an Port 32, 64 konfigurieren // 75 = setConfiguration auf IO Port
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 75, array("pin0" => 255,"pin1" => 255,"pin2" => 16,"pin3" => 255,"pin4" => 255,"pin5" => 255,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 75, array("pin0" => 21,"pin1" => 21,"pin2" => 21,"pin3" => 21,"pin4" => 21,"pin5" => 21,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId($objectId,38); // 38 = reset auf controller
	  }
	  else
	  {
  	    // Taster und LEDs an Port 16, 32, 48, 64 konfigurieren // 75 = setConfiguration auf IO Port
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 16), 75, array("pin0" => 255,"pin1" => 255,"pin2" => 16,"pin3" => 255,"pin4" => 255,"pin5" => 255,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 16,"pin3" => 16,"pin4" => 255,"pin5" => 255,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 48), 75, array("pin0" => 255,"pin1" => 255,"pin2" => 21,"pin3" => 255,"pin4" => 255,"pin5" => 255,"pin6" => 16,"pin7" => 16));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 75, array("pin0" => 21,"pin1" => 21,"pin2" => 21,"pin3" => 21,"pin4" => 21,"pin5" => 21,"pin6" => 255,"pin7" => 255));
  	    callInstanceMethodForObjectId($objectId,38); // 38 = reset auf controller
	  }
	}	
	// 5 = 8 Relais
	else if ($configTemplate==5) 
	{
  	  // Taster an Port 96 konfigurieren 1 // 75 = setConfiguration auf IO Port
  	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 96), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 16,"pin3" => 16,"pin4" => 16,"pin5" => 16,"pin6" => 16,"pin7" => 16));
  	  callInstanceMethodForObjectId($objectId,38); // 38 = reset auf controller
	}		
    // 11 = 32-io (NUR FORCE)
	else if ($configTemplate==11 && $force) 
	{	
  	    // Taster und Schalter an Port 16, 32, 48, 64 konfigurieren // 75 = setConfiguration auf IO Port
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 16), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 16,"pin3" => 16,"pin4" => 16,"pin5" => 16,"pin6" => 16,"pin7" => 16));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 75, array("pin0" => 16,"pin1" => 16,"pin2" => 16,"pin3" => 16,"pin4" => 16,"pin5" => 16,"pin6" => 16,"pin7" => 16));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 48), 75, array("pin0" => 19,"pin1" => 19,"pin2" => 19,"pin3" => 19,"pin4" => 19,"pin5" => 19,"pin6" => 19,"pin7" => 19));
  	    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 75, array("pin0" => 19,"pin1" => 19,"pin2" => 19,"pin3" => 19,"pin4" => 19,"pin5" => 19,"pin6" => 19,"pin7" => 19));
  	    callInstanceMethodForObjectId($objectId,38); // 38 = reset auf controller
	}
	}
}

function baseConfigureWaitForReset($objectId)
{
	global $lastLogId;
	return waitForObjectEventByName($objectId, 15, "evStarted", $lastLogId, "funtionDataParams", 0);
}

function baseConfigureInputsAndOutputs($objectId, $configTemplate, $force=false)
{
  $deviceId = getDeviceId($objectId);
  $controllerObj = getController($objectId);
  $controllerId = $controllerObj->id;
  $fcke = $controllerObj->fcke;
  $firmwareId = $controllerObj->firmwareId;

  // 1 = 16 Relais // 2 = 8 Rollos   // 6 = 8 Dimmer  // 15 = Brückenmodul
  if ($configTemplate==1 || $configTemplate==2 || $configTemplate==6 || $configTemplate==15)  
  {
	// Tasterevents und activeLow aktivieren // 16 = setConfiguration auf Taster
    $taster = array(104,87,86,85,103,102,101,100,99,98,97,88);
    foreach ($taster as $instance)
    {
       callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 16, array("holdTimeout" => "100","waitForDoubleClickTimeout" => "50","eventMask" => "33", "optionMask" => "0"));
  	   usleep(100000);
    }

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 15 = getConfiguration auf Taster
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
  	  usleep(100000);
    } 

    // Konfiguration der IO-Ports einlesen  	  	 
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 80), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 96), 74);
  }	
  // 10 = 16 Relais V2
  else if ($configTemplate==10) 
  {
	// Tasterevents und activeHigh aktivieren // 16 = setConfiguration auf Taster
    $taster = array(97,98,99,100,101,102,103,104);
    foreach ($taster as $instance)
    {
       callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 16, array("holdTimeout" => "100","waitForDoubleClickTimeout" => "50","eventMask" => "33", "optionMask" => "3"));
  	   usleep(100000);
    }

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 15 = getConfiguration auf Taster
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
  	  usleep(100000);
    } 

    // Konfiguration der IO-Ports einlesen  	  	 
    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 96), 74);
  }	  
  else if ($configTemplate==13) // 8-fach Relais V2
  {
    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 15 = getConfiguration auf Taster
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
  	  usleep(100000);
    } 

    // Konfiguration der IO-Ports einlesen  	  	 
    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 96), 74);
  }
  else if ($configTemplate==5)  // 8-fach Relaismodul
  {
	 // Tasterevents und activeLow aktivieren // 16 = setConfiguration auf Taster
 	 // Options 3 = pulldown inverted für activeHigh
     $taster = array(104,103,102,101,100,99,98,97);
     foreach ($taster as $instance)
     {
        callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 16, array("holdTimeout" => "100","waitForDoubleClickTimeout" => "50","eventMask" => "97", "optionMask" => "3"));
  	    usleep(100000);
     }

     // Konfiguration einlesen  	  	 
     foreach ($taster as $instance) // 15 = getConfiguration auf Taster
     {
       callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
  	   usleep(100000);
     } 

     // Konfiguration der IO-Ports einlesen  	  	 
 	 callInstanceMethodForObjectId(getObjectId($deviceId, 15, 96), 74);
  }	  
  // 6-fach Taster 
  else if ($configTemplate==3) 
  {
	// HBC
	if ($firmwareId==8)  $taster = array(17,18,19,20,21,22,33,34,35,36,37,38);
    else $taster = array(17,18,19,20,21,22,33,34,35,36,55,56);

    // Tasterevents und activeLow aktivieren // 16 = setConfiguration auf Taster
    foreach ($taster as $instance)
    {
       callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 16, array("holdTimeout" => "100","waitForDoubleClickTimeout" => "50","eventMask" => "161", "optionMask" => "0"));
  	   usleep(100000);
    }

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 15 = getConfiguration auf Taster
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
  	  usleep(100000);
    } 

	// LEDs onBoard = 7 (driveOn, driveOff, inverted) // 99 = setConfiguration auf LED
    $taster = array(49,50,51,52,53,54);
    foreach ($taster as $instance)
    {
       callInstanceMethodForObjectId(getObjectId($deviceId, 21, $instance), 99, array("dimmOffset" => "0","minBrightness" => "0","timeBase" => "100", "options" => "7"));
  	   usleep(100000);
    }

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 98 = getConfiguration auf LED
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 21, $instance), 98);
  	  usleep(100000);
    } 

	// LEDs extern = 6 (driveOn, driveOff) // 99 = setConfiguration auf LED
    $taster = array(65,66,67,68,69,70);
    foreach ($taster as $instance)
    {
       callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 99, array("dimmOffset" => "0","minBrightness" => "0","timeBase" => "100", "options" => "6"));
  	   usleep(100000);
    }

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 98 = getConfiguration auf LED
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 21, $instance), 98);
  	  usleep(100000);
    } 
	
    // Konfiguration der IO-Ports einlesen  	  	 
	if ($firmwareId==8)
	{
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 74);
	}
	else
	{
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 16), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 48), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 74);
	}
	usleep(100000);

	// Bei HBC ist der LogicalButton automatisch da, also nur Konfiguration einlesen
	if ($firmwareId==8)
	{
	  // LogicalButton Konfiguration einlesen // 95 = getConfiguration auf LogicalButton
      callInstanceMethodForObjectId(getObjectId($deviceId, 20, 16), 95);
	}
	else
	{
	  // LogicalButton konfigurieren // 96 = setConfiguration auf LogicalButton
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 20, 1), 96, array("button1" => "17","button2" => "18","button3" => "19", "button4" => "20", "button5" => "21", "button6" => "22", "led1" => "49", "led2" => "50", "led3" => "51", "led4" => "52", "led5" => "53", "led6" => "54"));
	  usleep(100000);
	  
  	  // LogicalButton Konfiguration einlesen // 95 = getConfiguration auf LogicalButton
      callInstanceMethodForObjectId(getObjectId($deviceId, 20, 1), 95);
	}
  }
  // 4-fach Taster 
  else if ($configTemplate==7) 
  {
	// Tasterevents und activeLow aktivieren // 16 = setConfiguration auf Taster
	// HBC
	if ($firmwareId==8)  $taster = array(17,18,21,22,33,34,35,36,37,38);
    else $taster = array(17,18,21,22,33,34,35,36,55,56);
	
    foreach ($taster as $instance)
    {
       callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 16, array("holdTimeout" => "100","waitForDoubleClickTimeout" => "50","eventMask" => "161", "optionMask" => "0"));
  	   usleep(100000);
    }

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 15 = getConfiguration auf Taster
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
  	  usleep(100000);
    } 

    // LEDs onBoard = 7 (driveOn, driveOff, inverted) // 99 = setConfiguration auf LED
    $taster = array(49,50,53,54);
    foreach ($taster as $instance)
    {
       callInstanceMethodForObjectId(getObjectId($deviceId, 21, $instance), 99, array("dimmOffset" => "0","minBrightness" => "0","timeBase" => "100", "options" => "7"));
  	   usleep(100000);
    }

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 98 = getConfiguration auf LED
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 21, $instance), 98);
  	  usleep(100000);
    } 

	// LEDs extern = 6 (driveOn, driveOff) // 99 = setConfiguration auf LED
    $taster = array(65,66,67,68,69,70);
    foreach ($taster as $instance)
    {
       callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 99, array("dimmOffset" => "0","minBrightness" => "0","timeBase" => "100", "options" => "6"));
  	   usleep(100000);
    }

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 98 = getConfiguration auf LED
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 21, $instance), 98);
  	  usleep(100000);
    } 
    
	if ($firmwareId==8)
	{
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 74);
	}
	else
	{
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 16), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 48), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 74);
	}
	usleep(100000);
	
	// Bei HBC ist der LogicalButton automatisch da, also nur Konfiguration einlesen
	if ($firmwareId==8)
	{
	  // LogicalButton Konfiguration einlesen // 95 = getConfiguration auf LogicalButton
      callInstanceMethodForObjectId(getObjectId($deviceId, 20, 16), 95);
	}
	else
	{
	  // LogicalButton konfigurieren // 96 = setConfiguration auf LogicalButton
	  callInstanceMethodForObjectId(getObjectId($deviceId, 20, 1), 96, array("button1" => "17","button2" => "18","button3" => "21", "button4" => "22", "button5" => "0", "button6" => "9", "led1" => "49", "led2" => "50", "led3" => "53", "led4" => "54", "led5" => "0", "led6" => "0"));
	  usleep(100000);
	  
  	  // LogicalButton Konfiguration einlesen // 95 = getConfiguration auf LogicalButton
      callInstanceMethodForObjectId(getObjectId($deviceId, 20, 1), 95);
	}
  }
  // 2-fach Taster 
  else if ($configTemplate==8) 
  {
	// Tasterevents und activeLow aktivieren // 16 = setConfiguration auf Taster
    // HBC
	if ($firmwareId==8)  $taster = array(19,20,33,34,35,36,37,38);
    else $taster = array(19,20,33,34,35,36,55,56);
	
    foreach ($taster as $instance)
    {
       callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 16, array("holdTimeout" => "100","waitForDoubleClickTimeout" => "50","eventMask" => "161", "optionMask" => "0"));
  	   usleep(100000);
    }

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 15 = getConfiguration auf Taster
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
  	  usleep(100000);
    } 

    if ($firmwareId==8)
	{
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 74);
	}
	else
	{
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 16), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 48), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 74);
	}

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 98 = getConfiguration auf LED
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 21, $instance), 98);
  	  usleep(100000);
    }
	
	// Bei HBC ist der LogicalButton automatisch da, also nur Konfiguration einlesen
	if ($firmwareId==8)
	{
	  // LogicalButton Konfiguration einlesen // 95 = getConfiguration auf LogicalButton
      callInstanceMethodForObjectId(getObjectId($deviceId, 20, 16), 95);
	}
	else
	{
	  // LogicalButton konfigurieren // 96 = setConfiguration auf LogicalButton
	  callInstanceMethodForObjectId(getObjectId($deviceId, 20, 1), 96, array("button1" => "19","button2" => "20","button3" => "0", "button4" => "0", "button5" => "0", "button6" => "9", "led1" => "51", "led2" => "52", "led3" => "0", "led4" => "0", "led5" => "0", "led6" => "0"));
	  usleep(100000);
	  
  	  // LogicalButton Konfiguration einlesen // 95 = getConfiguration auf LogicalButton
      callInstanceMethodForObjectId(getObjectId($deviceId, 20, 1), 95);
	}
  }
  // 1-fach Taster 
  else if ($configTemplate==9) 
  {
	// Tasterevents und activeLow aktivieren // 16 = setConfiguration auf Taster
	// HBC
	if ($firmwareId==8)  $taster = array(19,33,34,35,36,37,38);
    else $taster = array(19,33,34,35,36,55,56);
    foreach ($taster as $instance)
    {
       callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 16, array("holdTimeout" => "100","waitForDoubleClickTimeout" => "50","eventMask" => "161", "optionMask" => "0"));
  	   usleep(100000);
    }

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 15 = getConfiguration auf Taster
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
  	  usleep(100000);
    } 

    if ($firmwareId==8)
	{
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 74);
	}
	else
	{
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 16), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 48), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 64), 74);
	}
	
	// Bei HBC ist der LogicalButton automatisch da, also nur Konfiguration einlesen
	if ($firmwareId==8)
	{
	  // LogicalButton Konfiguration einlesen // 95 = getConfiguration auf LogicalButton
      callInstanceMethodForObjectId(getObjectId($deviceId, 20, 16), 95);
	}
	else
	{
	  // LogicalButton konfigurieren // 96 = setConfiguration auf LogicalButton
	  callInstanceMethodForObjectId(getObjectId($deviceId, 20, 1), 96, array("button1" => "19","button2" => "0","button3" => "0", "button4" => "0", "button5" => "0", "button6" => "9", "led1" => "51", "led2" => "0", "led3" => "0", "led4" => "0", "led5" => "0", "led6" => "0"));
	  usleep(100000);
	  
  	  // LogicalButton Konfiguration einlesen // 95 = getConfiguration auf LogicalButton
      callInstanceMethodForObjectId(getObjectId($deviceId, 20, 1), 95);
	}
  }
  // IO-Modul
  else if ($configTemplate==4) 
  {
	// Tasterevents und activeLow aktivieren // 16 = setConfiguration auf Taster
    $taster = array(17,18,19,20,21,22,33,34,35,36,55,56);
    foreach ($taster as $instance)
    {
       callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 16, array("holdTimeout" => "100","waitForDoubleClickTimeout" => "50","eventMask" => "161", "optionMask" => "0"));
  	   usleep(100000);
    }

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 15 = getConfiguration auf Taster
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
  	  usleep(100000);
    } 

    // Konfiguration der IO-Ports einlesen  	  	 
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 80), 74);
 	  callInstanceMethodForObjectId(getObjectId($deviceId, 15, 96), 74);
  }	
  // 32-IO-Modul
  else if ($configTemplate==11) 
  {
  	 // Tasterevents und activeLow aktivieren // 16 = setConfiguration auf Taster
 	 // Options 3 = pulldown inverted für activeHigh
	 if ($force)
	 {
       $taster = array(17,18,19,20,21,22,23,24,33,34,35,36,37,38,39,40);
       foreach ($taster as $instance)
       {
          callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 16, array("holdTimeout" => "100","waitForDoubleClickTimeout" => "50","eventMask" => "97", "optionMask" => "3"));
  	      usleep(100000);
       }

      // Konfiguration einlesen  	  	 
      $taster = array(17,18,19,20,21,22,23,24,33,34,35,36,37,38,39,40);
      foreach ($taster as $instance) // 15 = getConfiguration auf Taster
      {
        callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
  	    usleep(100000);
      } 
	}

    // Konfiguration der IO-Ports einlesen  	  	 
 	callInstanceMethodForObjectId(getObjectId($deviceId, 15, 16), 74);
	usleep(100000);
 	callInstanceMethodForObjectId(getObjectId($deviceId, 15, 32), 74);
	usleep(100000);
 	callInstanceMethodForObjectId(getObjectId($deviceId, 15, 48), 74);
	usleep(100000);
 	callInstanceMethodForObjectId(getObjectId($deviceId, 15, 96), 74);
	usleep(100000);
  }	  
  // 4-fach Dimmer
  else if ($configTemplate==12) 
  {
    // Konfiguration einlesen  	  	 
    $taster = array(17,18,19,20);
    foreach ($taster as $instance) // 15 = getConfiguration auf Taster
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
  	  usleep(100000);
    } 

    // Konfiguration der IO-Ports einlesen  	  	 
 	callInstanceMethodForObjectId(getObjectId($deviceId, 15, 16), 74);
	usleep(100000);
  }
  // 14 = 8 Dimmer V2
  else if ($configTemplate==14) 
  {
	// Tasterevents und activeHigh aktivieren // 16 = setConfiguration auf Taster
    $taster = array(71,72,99,100,101,102,103,104);
    foreach ($taster as $instance)
    {
       callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 16, array("holdTimeout" => "100","waitForDoubleClickTimeout" => "50","eventMask" => "33", "optionMask" => "3"));
  	   usleep(100000);
    }

    // Konfiguration einlesen  	  	 
    foreach ($taster as $instance) // 15 = getConfiguration auf Taster
    {
      callInstanceMethodForObjectId(getObjectId($deviceId, 16, $instance), 15);
  	  usleep(100000);
    } 

    // Konfiguration der IO-Ports einlesen  	  	 
    callInstanceMethodForObjectId(getObjectId($deviceId, 15, 96), 74);
  }	  
}

  	  	 
/*
  // 1 = 16-Relais 
  // 2 = 8-Rollos (weg)
  // 3 = 6-Taster
  // 4 = IO-Modul
  // 5 = 8-Relais
  // 6 = 8-Dimmer
  // 7 = 4-Taster
  // 8 = 2-Taster
  // 9 = 1-Taster
  // 10 = 16-Relais 2020
  // 11 = 32-IO Modul
  // 12 = 4-fach Dimmer
  // 13 = 8-Relais V2
  // 14 = 8-Dimmer V2
  // 15 = Brückenmodul
  // 16 = RGB Dimmer
*/
function getLoxoneFeatureName($objectId, $loxoneType)
{
  $featureClasses = readFeatureClasses();
  $featureClassesId = getFeatureClassesId($objectId);
  $instanceId=getInstanceId($objectId);

  $controllerObj = getController($objectId);
  $controllerId = $controllerObj->id;
  $fcke = $controllerObj->fcke;
  $firmwareId = $controllerObj->firmwareId;

  // Schalter = 8 
  // Taster = 1
  // Leds = 18
  // Rollos = 14
  // Temp = 2
  // Dimmer = 9
	
	if ($loxoneType==1) // 16-Relais
	{
	  if ($featureClassesId==8) //Schalter  -> Relais
      {
     	  if ($instanceId==1) return "Relais 2";
     	  if ($instanceId==2) return "Relais 4";
     	  if ($instanceId==3) return "Relais 6";
     	  if ($instanceId==4) return "Relais 8";
     	  if ($instanceId==5) return "Relais 9";
     	  if ($instanceId==6) return "Relais 11";
     	  if ($instanceId==7) return "Relais 13";
     	  if ($instanceId==8) return "Relais 15";
     	  if ($instanceId==9) return "Relais 1";
     	  if ($instanceId==10) return "Relais 3";
     	  if ($instanceId==11) return "Relais 5";
     	  if ($instanceId==12) return "Relais 7";
     	  if ($instanceId==13) return "Relais 10";
     	  if ($instanceId==14) return "Relais 12";
     	  if ($instanceId==15) return "Relais 14";
     	  if ($instanceId==16) return "Relais 16";
     	  if ($instanceId==210) return "Rote Modul LED";
      }
     
      if ($featureClassesId==1) //Taster -> Eingänge
      {
    	  if ($instanceId==104) return "Eingang 1";
    	  if ($instanceId==103) return "Eingang 2";
    	  if ($instanceId==102) return "Eingang 3";
    	  if ($instanceId==101) return "Eingang 4";
    	  if ($instanceId==100) return "Eingang 5";
    	  if ($instanceId==99) return "Eingang 6";
    	  if ($instanceId==98) return "Eingang 7";
    	  if ($instanceId==98) return "Eingang 7";
    	  if ($instanceId==97) return "Eingang 8";
    	  if ($instanceId==88) return "Eingang 9";
    	  if ($instanceId==87) return "Eingang 10";
    	  if ($instanceId==86) return "Eingang 11";
    	  if ($instanceId==85) return "Eingang 12";
      }
	}
	
	if ($loxoneType==10) // 16-Relais V2
	{
	  if ($featureClassesId==8) //Schalter  -> Relais
      {
     	  if ($instanceId==17) return "Relais 1";
     	  if ($instanceId==18) return "Relais 2";
     	  if ($instanceId==19) return "Relais 3";
     	  if ($instanceId==20) return "Relais 4";
     	  if ($instanceId==21) return "Relais 5";
     	  if ($instanceId==22) return "Relais 6";
     	  if ($instanceId==23) return "Relais 7";
     	  if ($instanceId==24) return "Relais 8";
     	  if ($instanceId==33) return "Relais 9";
     	  if ($instanceId==34) return "Relais 10";
     	  if ($instanceId==35) return "Relais 11";
     	  if ($instanceId==36) return "Relais 12";
     	  if ($instanceId==37) return "Relais 13";
     	  if ($instanceId==38) return "Relais 14";
     	  if ($instanceId==39) return "Relais 15";
     	  if ($instanceId==40) return "Relais 16";
     	  if ($instanceId==210) return "Rote Modul LED";
      }
     
      if ($featureClassesId==1) //Taster -> Eingänge
      {
    	  if ($instanceId==97) return "Eingang 1";
    	  if ($instanceId==98) return "Eingang 2";
    	  if ($instanceId==99) return "Eingang 3";
    	  if ($instanceId==100) return "Eingang 4";
    	  if ($instanceId==101) return "Eingang 5";
    	  if ($instanceId==102) return "Eingang 6";
    	  if ($instanceId==103) return "Eingang 7";
    	  if ($instanceId==104) return "Eingang 8";
      }
	}
	
	if ($loxoneType==5) // 8-Relais
	{
	  if ($featureClassesId==8) //Schalter  -> Relais
      {
     	  if ($instanceId==1) return "Relais 1";
     	  if ($instanceId==2) return "Relais 2";
     	  if ($instanceId==3) return "Relais 3";
     	  if ($instanceId==4) return "Relais 4";
     	  if ($instanceId==5) return "Relais 5";
     	  if ($instanceId==6) return "Relais 6";
     	  if ($instanceId==7) return "Relais 7";
     	  if ($instanceId==8) return "Relais 8";
     	  if ($instanceId==210) return "Rote Modul LED";
      }
     
      if ($featureClassesId==1) //Taster -> Eingänge
      { 
    	  if ($instanceId==104) return "Eingang 7";
    	  if ($instanceId==103) return "Eingang 8";
    	  if ($instanceId==102) return "Eingang 5";
    	  if ($instanceId==101) return "Eingang 6";
    	  if ($instanceId==100) return "Eingang 3";
    	  if ($instanceId==99) return "Eingang 4";
    	  if ($instanceId==98) return "Eingang 1";
    	  if ($instanceId==97) return "Eingang 2";
      }
	}
	
	if ($loxoneType==13) // 8-Relais V2
	{
	  if ($featureClassesId==8) //Schalter  -> Relais
      {
     	  if ($instanceId==17) return "Relais 1";
     	  if ($instanceId==18) return "Relais 2";
     	  if ($instanceId==19) return "Relais 3";
     	  if ($instanceId==20) return "Relais 4";
     	  if ($instanceId==21) return "Relais 5";
     	  if ($instanceId==22) return "Relais 6";
     	  if ($instanceId==23) return "Relais 7";
     	  if ($instanceId==24) return "Relais 8";
     	  if ($instanceId==210) return "Rote Modul LED";
      }
     
      if ($featureClassesId==1) //Taster -> Eingänge
      { 
    	  if ($instanceId==97) return "Eingang 1";
    	  if ($instanceId==98) return "Eingang 2";
    	  if ($instanceId==99) return "Eingang 3";
    	  if ($instanceId==100) return "Eingang 4";
    	  if ($instanceId==101) return "Eingang 5";
    	  if ($instanceId==102) return "Eingang 6";
    	  if ($instanceId==103) return "Eingang 7";
    	  if ($instanceId==104) return "Eingang 8";
      }
	}
	
	if ($loxoneType==2) // 8-Rollos
	{
	  if ($featureClassesId==14) //Rollos
      {
     	  if ($instanceId==1) return "Rollo 1";
     	  if ($instanceId==2) return "Rollo 2";
     	  if ($instanceId==3) return "Rollo 3";
     	  if ($instanceId==4) return "Rollo 4";
     	  if ($instanceId==5) return "Rollo 5";
     	  if ($instanceId==6) return "Rollo 6";
     	  if ($instanceId==7) return "Rollo 7";
     	  if ($instanceId==8) return "Rollo 8";
      }
      
      if ($featureClassesId==1) //Taster -> Eingänge
      {
    	  if ($instanceId==104) return "Eingang 1";
    	  if ($instanceId==103) return "Eingang 2";
    	  if ($instanceId==102) return "Eingang 3";
    	  if ($instanceId==101) return "Eingang 4";
    	  if ($instanceId==100) return "Eingang 5";
    	  if ($instanceId==99) return "Eingang 6";
    	  if ($instanceId==98) return "Eingang 7";
    	  if ($instanceId==97) return "Eingang 8";
    	  if ($instanceId==88) return "Eingang 9";
    	  if ($instanceId==87) return "Eingang 10";
    	  if ($instanceId==86) return "Eingang 11";
    	  if ($instanceId==85) return "Eingang 12";
      }
	}
	
	if ($loxoneType==15) // Brückenmodul
	{
	  if ($featureClassesId==8) //Relais
      {
     	  if ($instanceId==1) return "Dummy 1";
     	  if ($instanceId==2) return "Dummy 2";
     	  if ($instanceId==3) return "Dummy 3";
     	  if ($instanceId==4) return "Dummy 4";
     	  if ($instanceId==5) return "Dummy 5";
     	  if ($instanceId==6) return "Dummy 6";
     	  if ($instanceId==7) return "Dummy 7";
     	  if ($instanceId==8) return "Dummy 8";
      	  if ($instanceId==210) return "Rote Modul LED";
      }
      
      if ($featureClassesId==1) //Taster -> Eingänge
      {
    	  if ($instanceId==104) return "Eingang 1";
    	  if ($instanceId==103) return "Eingang 2";
    	  if ($instanceId==102) return "Eingang 3";
    	  if ($instanceId==101) return "Eingang 4";
    	  if ($instanceId==100) return "Eingang 5";
    	  if ($instanceId==99) return "Eingang 6";
    	  if ($instanceId==98) return "Eingang 7";
    	  if ($instanceId==97) return "Eingang 8";
    	  if ($instanceId==88) return "Eingang 9";
    	  if ($instanceId==87) return "Eingang 10";
    	  if ($instanceId==86) return "Eingang 11";
    	  if ($instanceId==85) return "Eingang 12";
      }
	}
	
	if ($loxoneType=="3") // 6-fach Taster
	{
	  if ($featureClassesId==1) //Taster
      {
     	  if ($instanceId==17) return "Taster 1";
     	  if ($instanceId==18) return "Taster 2";
     	  if ($instanceId==19) return "Taster 3";
     	  if ($instanceId==20) return "Taster 4";
     	  if ($instanceId==21) return "Taster 5";
     	  if ($instanceId==22) return "Taster 6";
		  
		  if ($firmwareId==8) //HBC
		  {
     	    if ($instanceId==33) return "Extern Taster 1";
     	    if ($instanceId==34) return "Extern Taster 2";
     	    if ($instanceId==35) return "Extern Taster 3";
     	    if ($instanceId==36) return "Extern Taster 4";
     	    if ($instanceId==37) return "Extern Taster 5";
     	    if ($instanceId==38) return "Extern Taster 6";
		  }
		  else
		  {
     	    if ($instanceId==33) return "Extern Taster 1";
     	    if ($instanceId==34) return "Extern Taster 2";
     	    if ($instanceId==35) return "Extern Taster 3";
     	    if ($instanceId==36) return "Extern Taster 4";
     	    if ($instanceId==55) return "Extern Taster 5";
     	    if ($instanceId==56) return "Extern Taster 6";
		  }
      }
     
      if ($featureClassesId==18) //LEDs
      {
    	  if ($instanceId==49) return "Led 1";
    	  if ($instanceId==50) return "Led 2";
    	  if ($instanceId==51) return "Led 3";
    	  if ($instanceId==52) return "Led 4";
    	  if ($instanceId==53) return "Led 5";
    	  if ($instanceId==54) return "Led 6";
    	  if ($instanceId==65) return "Extern Led 1";
    	  if ($instanceId==66) return "Extern Led 2";
    	  if ($instanceId==67) return "Extern Led 3";
    	  if ($instanceId==68) return "Extern Led 4";
    	  if ($instanceId==69) return "Extern Led 5";
    	  if ($instanceId==70) return "Extern Led 6";
      }
     
      if ($featureClassesId==17) // LogicalButton
      {
    	  if ($instanceId==1 || $instanceId==16) return "Hintergrundbeleuchtung";
      }
     
      if ($featureClassesId==2) // Temp
      {
    	  if ($instanceId==1) return "Temperatursensor";
      }
	  
      if ($featureClassesId==34) // Helligkeitssensor
      {
    	  if ($instanceId==1 || $instanceId==23) return "Helligkeitssensor";
      }
	  
      if ($featureClassesId==23) // Feuchtesensor 
      {
    	  if ($instanceId==1) return "Feuchtesensor";
      }
	}

    if ($loxoneType=="7") // 4-fach Taster
	{
	  if ($featureClassesId==1) //Taster
      {
     	  if ($instanceId==17) return "Taster 1";
     	  if ($instanceId==18) return "Taster 2";
     	  if ($instanceId==21) return "Taster 3";
     	  if ($instanceId==22) return "Taster 4";
		  
		  if ($firmwareId==8) //HBC
		  {
     	    if ($instanceId==33) return "Extern Taster 1";
     	    if ($instanceId==34) return "Extern Taster 2";
     	    if ($instanceId==35) return "Extern Taster 3";
     	    if ($instanceId==36) return "Extern Taster 4";
     	    if ($instanceId==37) return "Extern Taster 5";
     	    if ($instanceId==38) return "Extern Taster 6";
		  }
		  else
		  {
     	    if ($instanceId==33) return "Extern Taster 1";
     	    if ($instanceId==34) return "Extern Taster 2";
     	    if ($instanceId==35) return "Extern Taster 3";
     	    if ($instanceId==36) return "Extern Taster 4";
     	    if ($instanceId==55) return "Extern Taster 5";
     	    if ($instanceId==56) return "Extern Taster 6";
		  }
      }
     
      if ($featureClassesId==18) //LEDs
      {
    	  if ($instanceId==49) return "Led 1";
    	  if ($instanceId==50) return "Led 2";
    	  if ($instanceId==53) return "Led 3";
    	  if ($instanceId==54) return "Led 4";
    	  if ($instanceId==65) return "Extern Led 1";
    	  if ($instanceId==66) return "Extern Led 2";
    	  if ($instanceId==67) return "Extern Led 3";
    	  if ($instanceId==68) return "Extern Led 4";
    	  if ($instanceId==69) return "Extern Led 5";
    	  if ($instanceId==70) return "Extern Led 6";
      }
     
      if ($featureClassesId==17) // LogicalButton
      {
    	  if ($instanceId==1 || $instanceId==16) return "Hintergrundbeleuchtung";
      }
     
      if ($featureClassesId==2) // Temp
      {
    	  if ($instanceId==1) return "Temperatursensor";
      }
	  
      if ($featureClassesId==34) // Helligkeitssensor
      {
    	  if ($instanceId==1 || $instanceId==23) return "Helligkeitssensor";
      }
	  
      if ($featureClassesId==23) // Feuchtesensor 
      {
    	  if ($instanceId==1) return "Feuchtesensor";
      }
	}
	
	if ($loxoneType=="8") // 2-fach Taster
	{
	  if ($featureClassesId==1) //Taster
      {
     	  if ($instanceId==19) return "Taster 1";
     	  if ($instanceId==20) return "Taster 2";
		  
     	  if ($firmwareId==8) //HBC
		  {
     	    if ($instanceId==33) return "Extern Taster 1";
     	    if ($instanceId==34) return "Extern Taster 2";
     	    if ($instanceId==35) return "Extern Taster 3";
     	    if ($instanceId==36) return "Extern Taster 4";
     	    if ($instanceId==37) return "Extern Taster 5";
     	    if ($instanceId==38) return "Extern Taster 6";
		  }
		  else
		  {
     	    if ($instanceId==33) return "Extern Taster 1";
     	    if ($instanceId==34) return "Extern Taster 2";
     	    if ($instanceId==35) return "Extern Taster 3";
     	    if ($instanceId==36) return "Extern Taster 4";
     	    if ($instanceId==55) return "Extern Taster 5";
     	    if ($instanceId==56) return "Extern Taster 6";
		  }
      }
     
      if ($featureClassesId==18) //LEDs
      {
		  if ($instanceId==49) return "Led 1";
    	  if ($instanceId==50) return "Led 2";
    	  if ($instanceId==51) return "Led 3";
    	  if ($instanceId==52) return "Led 4";
    	  if ($instanceId==53) return "Led 5";
    	  if ($instanceId==54) return "Led 6";
    	  if ($instanceId==65) return "Extern Led 1";
    	  if ($instanceId==66) return "Extern Led 2";
    	  if ($instanceId==67) return "Extern Led 3";
    	  if ($instanceId==68) return "Extern Led 4";
    	  if ($instanceId==69) return "Extern Led 5";
    	  if ($instanceId==70) return "Extern Led 6";
      }
     
      if ($featureClassesId==17) // LogicalButton
      {
 		  if ($firmwareId == 8 && $instanceId==1) return "Hintergrundbeleuchtung Extern";
    	  if ($instanceId==1 || $instanceId==16) return "Hintergrundbeleuchtung";
      }
     
      if ($featureClassesId==2) // Temp
      {
    	  if ($instanceId==1) return "Temperatursensor";
      }
	  
      if ($featureClassesId==34) // Helligkeitssensor
      {
    	  if ($instanceId==1 || $instanceId==23) return "Helligkeitssensor";
      }
	  
      if ($featureClassesId==23) // Feuchtesensor 
      {
    	  if ($instanceId==1) return "Feuchtesensor";
      }
	}
	
	if ($loxoneType=="9") // 1-fach Taster
	{
	  if ($featureClassesId==1) //Taster
      {
     	  if ($instanceId==19) return "Taster 1";
		  
     	  if ($firmwareId==8) //HBC
		  {
     	    if ($instanceId==33) return "Extern Taster 1";
     	    if ($instanceId==34) return "Extern Taster 2";
     	    if ($instanceId==35) return "Extern Taster 3";
     	    if ($instanceId==36) return "Extern Taster 4";
     	    if ($instanceId==37) return "Extern Taster 5";
     	    if ($instanceId==38) return "Extern Taster 6";
		  }
		  else
		  {
     	    if ($instanceId==33) return "Extern Taster 1";
     	    if ($instanceId==34) return "Extern Taster 2";
     	    if ($instanceId==35) return "Extern Taster 3";
     	    if ($instanceId==36) return "Extern Taster 4";
     	    if ($instanceId==55) return "Extern Taster 5";
     	    if ($instanceId==56) return "Extern Taster 6";
		  }
      }
     
      if ($featureClassesId==18) //LEDs
      {
    	  if ($instanceId==49) return "Led 1";
    	  if ($instanceId==50) return "Led 2";
    	  if ($instanceId==51) return "Led 3";
    	  if ($instanceId==52) return "Led 4";
    	  if ($instanceId==53) return "Led 5";
    	  if ($instanceId==54) return "Led 6";

    	  if ($instanceId==65) return "Extern Led 1";
    	  if ($instanceId==66) return "Extern Led 2";
    	  if ($instanceId==67) return "Extern Led 3";
    	  if ($instanceId==68) return "Extern Led 4";
    	  if ($instanceId==69) return "Extern Led 5";
    	  if ($instanceId==70) return "Extern Led 6";
      }
     
      if ($featureClassesId==17) // LogicalButton
      {
    	  if ($instanceId==1 || $instanceId==16) return "Hintergrundbeleuchtung";
      }
     
      if ($featureClassesId==2) // Temp
      {
    	  if ($instanceId==1) return "Temperatursensor";
      }
	  
      if ($featureClassesId==34) // Helligkeitssensor
      {
    	  if ($instanceId==1 || $instanceId==23) return "Helligkeitssensor";
      }
	  
      if ($featureClassesId==23) // Feuchtesensor 
      {
    	  if ($instanceId==1) return "Feuchtesensor";
      }
	}

	if ($loxoneType=="4") // IO-Modul
	{
	  if ($featureClassesId==1) //Taster
      {
    	if ($instanceId==17) return "Eingang 1";
     	if ($instanceId==18) return "Eingang 2";
     	if ($instanceId==19) return "Eingang 3";
     	if ($instanceId==20) return "Eingang 4";
     	if ($instanceId==21) return "Eingang 5";
     	if ($instanceId==22) return "Eingang 6";
     	if ($instanceId==33) return "Eingang 7";
  	    if ($instanceId==34) return "Eingang 8";
  	    if ($instanceId==35) return "Eingang 9";
  	    if ($instanceId==36) return "Eingang 10";
  	    if ($instanceId==37) return "Eingang 11";
  	    if ($instanceId==38) return "Eingang 12";
      }
     
      if ($featureClassesId==18) //LEDs
      {
    	  if ($instanceId==49) return "Ausgang 1";
    	  if ($instanceId==50) return "Ausgang 2";
    	  if ($instanceId==51) return "Ausgang 3";
    	  if ($instanceId==52) return "Ausgang 4";
    	  if ($instanceId==53) return "Ausgang 5";
    	  if ($instanceId==54) return "Ausgang 6";
    	  if ($instanceId==65) return "Ausgang 7";
    	  if ($instanceId==66) return "Ausgang 8";
    	  if ($instanceId==67) return "Ausgang 9";
    	  if ($instanceId==68) return "Ausgang 10";
    	  if ($instanceId==69) return "Ausgang 11";
    	  if ($instanceId==70) return "Ausgang 12";
      }

      if ($featureClassesId==8) //Schalter
      {
        if ($instanceId==23) return "Rote Modul LED";
      }
	}
	
	if ($loxoneType==6) // 8-Dimmer
	{
	  if ($featureClassesId==9) //Dimmer
      {
     	  if ($instanceId==1) return "Dimmer 1";
     	  if ($instanceId==2) return "Dimmer 2";
     	  if ($instanceId==3) return "Dimmer 3";
     	  if ($instanceId==4) return "Dimmer 4";
     	  if ($instanceId==5) return "Dimmer 5";
     	  if ($instanceId==6) return "Dimmer 6";
     	  if ($instanceId==7) return "Dimmer 7";
     	  if ($instanceId==8) return "Dimmer 8";
     	  if ($instanceId==210) return "Rote Modul LED";
      }
     
 	  if ($featureClassesId==8) //Schalter  -> Relais
      {
      	  if ($instanceId==210) return "Rote Modul LED";
      }
	  
      if ($featureClassesId==1) //Taster -> Eingänge
      { 
		  if ($instanceId==104) return "Eingang 1";
    	  if ($instanceId==103) return "Eingang 2";
    	  if ($instanceId==102) return "Eingang 3";
    	  if ($instanceId==101) return "Eingang 4";
    	  if ($instanceId==100) return "Eingang 5";
    	  if ($instanceId==99) return "Eingang 6";
    	  if ($instanceId==98) return "Eingang 7";
    	  if ($instanceId==97) return "Eingang 8";
    	  if ($instanceId==88) return "Eingang 9";
    	  if ($instanceId==87) return "Eingang 10";
    	  if ($instanceId==86) return "Eingang 11";
    	  if ($instanceId==85) return "Eingang 12";

      }
	}
	
	if ($loxoneType==14) // 8-Dimmer V2
	{
	  if ($featureClassesId==9) //Dimmer
      {
     	  if ($instanceId==1) return "Dimmer 1";
     	  if ($instanceId==2) return "Dimmer 2";
     	  if ($instanceId==3) return "Dimmer 3";
     	  if ($instanceId==4) return "Dimmer 4";
     	  if ($instanceId==5) return "Dimmer 5";
     	  if ($instanceId==6) return "Dimmer 6";
     	  if ($instanceId==7) return "Dimmer 7";
     	  if ($instanceId==8) return "Dimmer 8";
     	  if ($instanceId==210) return "Rote Modul LED";
      }
     
 	  if ($featureClassesId==8) //Schalter  -> Relais
      {
      	  if ($instanceId==210) return "Rote Modul LED";
      }
	  
      if ($featureClassesId==1) //Taster -> Eingänge
      { 
		  if ($instanceId==71) return "Eingang 1";
    	  if ($instanceId==72) return "Eingang 2";
    	  if ($instanceId==99) return "Eingang 3";
    	  if ($instanceId==100) return "Eingang 4";
    	  if ($instanceId==101) return "Eingang 5";
    	  if ($instanceId==102) return "Eingang 6";
    	  if ($instanceId==103) return "Eingang 7";
    	  if ($instanceId==104) return "Eingang 8";
      }
	}
	
	if ($loxoneType==11) // 32-IO
	{
 	  if ($featureClassesId==8) //Schalter  -> Relais
      {
      	  if ($instanceId==210) return "Rote Modul LED";
      	  if ($instanceId==49) return "Ausgang 1";
      	  if ($instanceId==50) return "Ausgang 2";
      	  if ($instanceId==51) return "Ausgang 3";
      	  if ($instanceId==52) return "Ausgang 4";
      	  if ($instanceId==53) return "Ausgang 5";
      	  if ($instanceId==54) return "Ausgang 6";
      	  if ($instanceId==55) return "Ausgang 7";
      	  if ($instanceId==56) return "Ausgang 8";
      	  if ($instanceId==97) return "Ausgang 9";
      	  if ($instanceId==98) return "Ausgang 10";
      	  if ($instanceId==99) return "Ausgang 11";
      	  if ($instanceId==100) return "Ausgang 12";
      	  if ($instanceId==101) return "Ausgang 13";
      	  if ($instanceId==102) return "Ausgang 14";
      	  if ($instanceId==103) return "Ausgang 15";
      	  if ($instanceId==104) return "Ausgang 16";
      }
	  
      if ($featureClassesId==1) //Taster -> Eingänge
      { 
		  if ($instanceId==17) return "Eingang 1";
    	  if ($instanceId==18) return "Eingang 2";
    	  if ($instanceId==19) return "Eingang 3";
    	  if ($instanceId==20) return "Eingang 4";
    	  if ($instanceId==21) return "Eingang 5";
    	  if ($instanceId==22) return "Eingang 6";
    	  if ($instanceId==23) return "Eingang 7";
    	  if ($instanceId==24) return "Eingang 8";
    	  if ($instanceId==33) return "Eingang 9";
    	  if ($instanceId==34) return "Eingang 10";
    	  if ($instanceId==35) return "Eingang 11";
    	  if ($instanceId==36) return "Eingang 12";
    	  if ($instanceId==37) return "Eingang 13";
    	  if ($instanceId==38) return "Eingang 14";
    	  if ($instanceId==39) return "Eingang 15";
    	  if ($instanceId==40) return "Eingang 16";

      }
	}
	
	if ($loxoneType==12) // 4-fach Dimmmer
	{
 	  if ($featureClassesId==1) //Taster
      {
    	if ($instanceId==17) return "Eingang 1";
     	if ($instanceId==18) return "Eingang 2";
     	if ($instanceId==19) return "Eingang 3";
     	if ($instanceId==20) return "Eingang 4";
      }
     
      if ($featureClassesId==18) //LEDs
      {
    	  if ($instanceId==49) return "Dimmer 1";
    	  if ($instanceId==50) return "Dimmer 2";
    	  if ($instanceId==51) return "Dimmer 3";
    	  if ($instanceId==52) return "Dimmer 4";
      }

      if ($featureClassesId==8) //Schalter
      {
        if ($instanceId==210) return "Rote Modul LED";
      }
	}
	
	if ($loxoneType==16) // RGB Dimmer
	{
	  if ($featureClassesId==9) //Dimmer
      {
     	  if ($instanceId==1) return "Dimmer 1";
     	  if ($instanceId==2) return "Dimmer 2";
     	  if ($instanceId==3) return "Dimmer 3";
     	  if ($instanceId==4) return "Dimmer 4";
     	  if ($instanceId==5) return "Dimmer 5";
     	  if ($instanceId==6) return "Dimmer 6";
      }
     
 	  if ($featureClassesId==8) //Schalter  -> Relais
      {
      	  if ($instanceId==210) return "Rote Modul LED";
      }
	  
      if ($featureClassesId==1) //Taster -> Eingänge
      { 
		  if ($instanceId==17) return "Eingang 1";
    	  if ($instanceId==18) return "Eingang 2";
    	  if ($instanceId==19) return "Eingang 3";
    	  if ($instanceId==20) return "Eingang 4";
    	  if ($instanceId==21) return "Eingang 5";
    	  if ($instanceId==22) return "Eingang 6";
      }
	}
	
  return $featureClasses[$featureClassesId]->name." ".$instanceId;
}

function generateDefaultRule($groupId, $startState, $resultState, $generated=1, $startDay=7, $startHour=31, $startMinute=255, $endDay=7, $endHour=31, $endMinute=255, $intraDay=0)
{
    QUERY ( "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType,baseRule,`generated`, intraDay) 
                         values('$groupId','$startDay','$startHour','$startMinute','$endDay','$endHour','$endMinute','$startState','$resultState','click','0','$generated','$intraDay')" );
	return query_insert_id();
}

function generateDefaultAction($ruleId, $instanceId, $functionId, $params="", $generated=1)
{
   QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','$instanceId','$functionId','$generated')");
   $newId = query_insert_id ();
   
   if ($params!="")
   {
	 foreach ($params as $paramsId=>$paramsValue)
	 {	 
        QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue,`generated`) values('$newId','$paramsId','$paramsValue','$generated')" );
	 }
   }
   
   return $newId;
}

function generateDefaultSignal($ruleId, $featureInstanceId, $functionId, $params="", $debug=false, $generated=1)
{
    QUERY ( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                               values('$ruleId','$featureInstanceId','$functionId','$generated')" );
    $newId = query_insert_id ();

    if ($params!="")
    {
	  foreach ($params as $paramsId=>$paramsValue)
	  {	
         QUERY ( "INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue,`generated`) 
   	                                    values('$newId','$paramsId','$paramsValue','$generated')" );
	  }
	}
										 
    return $newId;
}

function generateRolloHeizungRules($groupId, $instanceId, $basicRuleId,  $startState, $endStart, $rolloPosition, $eventType)
{
	global $lastEventParamId;
	global $celsiusParamId;
	global $centiCelsiusParamId;
	global $evStatusLastEventParamId;
	global $evStatusCelsiusParamId;
	global $evStatusCentiCelsiusParamId;
	global $moveToPositionFunctionId;
	global $tempStatusEvent;
	global $evStatusTempStatusEvent;
	global $coldEnumValue;
	global $evStatusColdEnumValue;
	global $hotEnumValue;
	global $evStatusHotEnumValue;
	global $evStatusWarmEnumValue;
	global $warmEnumValue;
	
 	$ruleId = generateDefaultRule($groupId, $startState, $endStart);
    generateDefaultAction($ruleId, $instanceId, $moveToPositionFunctionId, array("102" => $rolloPosition));
	  
    // Signale
    $erg2 = QUERY ("select featureInstanceId from basicRuleSignals where ruleId='$basicRuleId'");
    while ( $obj2 = MYSQLi_FETCH_OBJECT ( $erg2 ) )
    {
       if ($obj2->featureInstanceId < 0)  continue;
       $signalClassesId = getClassesIdByFeatureInstanceId ( $obj2->featureInstanceId );
       if ($signalClassesId!=2) continue;
		 
	   if ($eventType=="cold")
	   {
		 // Statusevent cold
		 generateDefaultSignal($ruleId, $obj2->featureInstanceId, $tempStatusEvent, array($lastEventParamId => $coldEnumValue, $celsiusParamId => 255, $centiCelsiusParamId => 255));
	     // evStatus cold
		 generateDefaultSignal($ruleId, $obj2->featureInstanceId, $evStatusTempStatusEvent, array($evStatusLastEventParamId => $evStatusColdEnumValue, $evStatusCelsiusParamId => 255, $evStatusCentiCelsiusParamId => 255));
	   }
	   else if ($eventType=="hot")
	   {
          // Statusevent hot
		  generateDefaultSignal($ruleId, $obj2->featureInstanceId, $tempStatusEvent, array($lastEventParamId => $hotEnumValue, $celsiusParamId => 255, $centiCelsiusParamId => 255));
										   
          // evStatus hot
		  generateDefaultSignal($ruleId, $obj2->featureInstanceId, $evStatusTempStatusEvent, array($evStatusLastEventParamId => $evStatusHotEnumValue, $evStatusCelsiusParamId => 255, $evStatusCentiCelsiusParamId => 255));
	   }
	   else if ($eventType=="*")
	   {
          // Statusevent hot
		  generateDefaultSignal($ruleId, $obj2->featureInstanceId, $tempStatusEvent, array($lastEventParamId => $hotEnumValue, $celsiusParamId => 255, $centiCelsiusParamId => 255));
										   
          // evStatus hot
		  generateDefaultSignal($ruleId, $obj2->featureInstanceId, $evStatusTempStatusEvent, array($evStatusLastEventParamId => 255, $evStatusCelsiusParamId => 255, $evStatusCentiCelsiusParamId => 255));
	   }
	   return;
    }
}

function getSystemvariableIndex($controllerId, $variableType)
{
	$myIndex=0;
	$erg = QUERY("select nr from systemVariableHelper where controllerId='$controllerId' and variableType='$variableType' limit 1");
	if ($row=mysqli_fetch_row($erg))
	{
		$myIndex=$row[0];
		QUERY("update systemVariableHelper set nr=nr+1  where controllerId='$controllerId' and variableType='$variableType' limit 1");
	}
	else QUERY ("INSERT into systemVariableHelper (controllerId,variableType,nr) values('$controllerId', '$variableType','1')"); 
	
	return $myIndex;
}

function generateGroupToSetSystemVariableForSignalGroup($controllerInstanceId, $systemVariable, $groupId, $name)
{
	QUERY ( "INSERT into groups (name,single,`generated`) values('$name','0','1')");
    $newGroupId = query_insert_id ();
	$ruleId = generateDefaultRule($newGroupId, 0, 0);
	generateDefaultAction($ruleId, $controllerInstanceId, 268, array(540 => 738, 541 => $systemVariable, 542 => 1)); // SetSystemVariable type = bit , index = systemvarialenindex, value = 0
	
    QUERY("INSERT into basicRuleGroupSignals (ruleId, groupId, eventType,`generated`) values('-$ruleId','$groupId','ACTIVE','1')");
    $groupSignalId = query_insert_id();
          
    QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','-$groupSignalId','0','1')");
    

	$ruleId = generateDefaultRule($newGroupId, 0, 0);
	generateDefaultAction($ruleId, $controllerInstanceId, 268, array(540 => 738, 541 => $systemVariable, 542 => 0)); // SetSystemVariable type = bit , index = systemvarialenindex, value = 0
                               
     QUERY("INSERT into basicRuleGroupSignals (ruleId, groupId, eventType,`generated`) values('-$ruleId','$groupId','DEACTIVE','1')");
     $groupSignalId = query_insert_id();
          
     QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`) values('$ruleId','-$groupSignalId','0','1')");
}

function generateAndCondition($ruleId, $systemVariableIndex, $systemVariableValue)
{
	$raspberryFeatureInstanceId= getFirstSoftwareController()->featureInstanceId;
    $functionId=270;
	
	generateDefaultSignal($ruleId, $raspberryFeatureInstanceId, $functionId, array(545 => 0, 546 => $systemVariableIndex, 547=>$systemVariableValue));
}

function getFirstNormalOnlineController()
{
	$result = new stdClass;
	$erg = QUERY("select featureInstances.id as featureInstanceId, controller.objectId, controller.id as controllerId from controller join featureInstances on (featureInstances.objectId=controller.objectId) where size!=999 and online=1 and firmwareId!=2 and firmwareId!=3 and firmwareId!=4 order by controller.id limit 1");
	$row = mysqli_fetch_row($erg);
	$result->featureInstanceId=$row[0];
	$result->objectId=$row[1];
	$result->controllerId=$row[2];
	return $result;
}

function getFirstSoftwareController()
{
  $result = new stdClass;
  $erg = QUERY("select featureInstances.id as featureInstanceId,featureInstances.objectId, controller.id as controllerId from featureInstances join controller on (controller.id=featureInstances.controllerId) where controller.objectId='800980993' and featureClassesId= 12 limit 1");
  $row=mysqli_fetch_row($erg);
  $result->featureInstanceId = $row[0];
  $result->objectId = $row[1];
  $result->controllerId = $row[2];
  return $result;
}

function getSystemVariables()
{
  $systemVariables =  array();
  $erg = QUERY("select userkey, uservalue from userdata where userkey like 'SYSTEM_VARIABLE_%' order by uservalue");
  while($obj=MYSQLi_fetch_object($erg))
  {
	  $systemVariables[str_replace("SYSTEM_VARIABLE_","",$obj->userkey)]=$obj->uservalue;
  }
  
  return $systemVariables;
}

function getSystemVariableDataForSignal($signalId)
{
   $result = new stdClass;
   $erg = QUERY("select featureFunctionParamsId,paramValue from ruleSignalParams where ruleSignalId='$signalId'");
   while ( $obj = mysqli_fetch_object($erg) )
   {
	   $erg2=QUERY("select name,type from featureFunctionParams where id='$obj->featureFunctionParamsId' limit 1");
	   if ($row2=mysqli_fetch_row($erg2))
	   {
		   $name = $row2[0];
		   $type=$row2[1];
		   
		   if ($type=="ENUM")
		   {
	          $erg2=QUERY("select name from featureFunctionEnums where paramId='$obj->featureFunctionParamsId' and value='$obj->paramValue' limit 1");
	          if ($row2=mysqli_fetch_row($erg2)) $result->$name=$row2[0];
		   }
		   else $result->$name=$obj->paramValue;
	   }
   }
   
   $varName = "SYSTEM_VARIABLE_".$result->type.$result->index;
   $erg = QUERY("select userValue from userData where userKey='$varName' limit 1");
   if ($obj = mysqli_fetch_object($erg)) $result->name=$obj->userValue;

   return $result;
}

function getSystemVariableDataForBasicSignal($signalId)
{
   $result = new stdClass;
   $erg = QUERY("select featureFunctionParamsId,paramValue from basicRuleSignalParams where ruleSignalId='$signalId'");
   while ( $obj = mysqli_fetch_object($erg) )
   {
	   $erg2=QUERY("select name,type from featureFunctionParams where id='$obj->featureFunctionParamsId' limit 1");
	   if ($row2=mysqli_fetch_row($erg2))
	   {
		   $name = $row2[0];
		   $type=$row2[1];
		   
		   if ($type=="ENUM")
		   {
	          $erg2=QUERY("select name from featureFunctionEnums where paramId='$obj->featureFunctionParamsId' and value='$obj->paramValue' limit 1");
	          if ($row2=mysqli_fetch_row($erg2)) $result->$name=$row2[0];
		   }
		   else $result->$name=$obj->paramValue;
	   }
   }
   
   $varName = "SYSTEM_VARIABLE_".$result->type.$result->index;
   $erg = QUERY("select userValue from userData where userKey='$varName' limit 1");
   if ($obj = mysqli_fetch_object($erg)) $result->name=$obj->userValue;
   
   return $result;
}
?>