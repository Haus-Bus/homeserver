﻿<?php
$skipConfigurationCheck=false;

function activateBooter($controllerObjectId)
{
    liveOut ( "<b>Bootloader wird aktiviert ...</b>");
    flushIt ();
	
	for ($i=0;$i<3;$i++)
    {
      callObjectMethodByName ( $controllerObjectId, "reset" );
      $receiverObjectid = getBooterObjectId($controllerObjectId);
       
      for ($i=0;$i<5;$i++)
      {
        sleepMs(100);
        callObjectMethodByName($receiverObjectid, "ping");
      }
          
      sleepMs ( 500 );
      $result = callObjectMethodByNameAndRecover ( $receiverObjectid, "ping", "", "pong", 1, 2, 0,"senderData" );
      if ($result!=-1) 
	  {
        liveOut ( "Bootloader erfolgreich gestartet. ObjectID: " . getFormatedObjectId ( $result->objectId ));
        liveOut ( '');
	  }
	  
	  callObjectMethodByName ( $receiverObjectid, "getModuleId" );
	  
	  return $result->objectId;
	}
	
	die("Bootloader konnte nicht gestartet werden");
}

function updateFirmware($booterObjectId, $fwfile, $firmwareId, $showFilename, $verify=0)
{
	global $lastLogId;
	
     
    liveOut ( "<b>Firmware Update $showFilename ...</b>");
    liveOut ( "Während des Updates den Controller und den PC/RASPBERRY NICHT AUSSCHALTEN!");
    liveOut ( '' );
    flushIt ();

    if (strpos ( $fwfile, "BOOTER" ) !== FALSE) $isBooter = 1;
    else $isBooter = 0;
        
    $fileSize = filesize ( $fwfile );
        
    liveOut ( "Datei: " . substr ( $fwfile, strrpos ( $fwfile, "/" ) + 1 ) );
    liveOut ( "Größe: $fileSize Bytes" );
        
    if( $firmwareId == 7 )
    {
        $wifiId = getObjectId(getDeviceId($booterObjectId), getClassIdByName("WiFi"), 1);
        callObjectMethodByName ( $wifiId, "getCurrentIp" );
          
        $result = waitForObjectResultByName ( $wifiId, 5, "CurrentIp", $lastLogId );
        $ip = getResultDataValueByName ( "IP0", $result ).".".getResultDataValueByName ( "IP1", $result ).".".getResultDataValueByName ( "IP2", $result ).".".getResultDataValueByName ( "IP3", $result );
  
        $updateUrl = "http://".$ip."/updateFirmware";
        liveOut ( "IP vom Modul: $ip");
  
          /* Warum funktioniert das nicht ???
          $options = array(
              'http' => array(
                  'header'  => "Content-type: application/octet-stream\r\n",
                  'method'  => 'POST',
                  'content' => file_get_contents($fwfile)
              )
          );

          $context = stream_context_create($options);
          $result = file_get_contents($updateUrl, false, $context); */
        $result = curlPost( $updateUrl, $fwfile );
        if ($result === FALSE)  die ( "Fehler: FW konnte nicht übertragen werden" ); 

        liveOut( "Antwort: " . $result );
        if ( $result != "update successfull" ) die ( "Fehler: Controller update fehlgeschlagen" );
    }
    else
    {
        callObjectMethodByName ( $booterObjectId, "getConfiguration" );
        $result = waitForObjectResultByName ( $booterObjectId, 5, "Configuration", $lastLogId );
        $blockSize = getResultDataValueByName ( "dataBlockSize", $result );
          
        liveOut ( "Blockgröße: " . $blockSize . " Bytes" );
        liveOut ( '' );
        liveOut ( "<div id=\"status\">Updatestatus: 0/$fileSize Bytes - 0%</div>");
          
        $fd = fopen ( $fwfile, "r" );
        $ready = 0;
        $round = 0;
        $firstWriteId = - 1;
        while ( ! feof ( $fd ) )
        {
          $buffer = fread ( $fd, $blockSize );
          $data ["address"] = $ready;
          $data ["data"] = $buffer;
          if ($firstWriteId == - 1) $firstWriteId = $lastLogId;
            
          $result = callObjectMethodByNameAndRecover ( $booterObjectId, "writeMemory", $data, "MemoryStatus", 3, 2, 0 );
          if ($result == - 1)
          {
            updateControllerStatus ();
            die ( "Fehler: Controller antwortet nicht" );
          }
            
          if ($result [0]->dataValue != 0)
          {
            updateControllerStatus ();
            liveOut ( "Bootloader hat fehlerhaften MemoryStatus gemeldet: " . $result [0]->dataValue );
            exit ();
          }
        
		  $ready += strlen ( $buffer );
          if ($round % 5 == 0 || ($fileSize - $ready < 1500)) statusOut ( $ready, $fileSize, $blockSize );
            
          $round ++;
          $i ++;
          flushIt ();
            
          // Das hier wieder einkommentieren, falls es Probleme gibt
          //sleepMS(5); //TODO warum hilft das ?
        }
        fclose ( $fd );
    } 
        
    liveOut ( "Übertragung erfolgreich beendet" );
    liveOut ( '' );
        
    if ($verify == 1)
    {
       liveOut ( "<b>Firmware wird verifiziert...</b>" );
       $erg = QUERY ( "select functionData,receiverSubscriberData from udpCommandLog where function='writeMemory' and id>'$firstWriteId' order by id" );
       while ( $row = MYSQLi_FETCH_ROW ( $erg ) )
       {
          if (unserialize ( $row [1] )->objectId != $booterObjectId) continue;
            
          $fkt = unserialize ( $row [0] );
          $offset = $fkp->paramData [0]->dataValue;
          $crc = $fkp->paramData [1]->dataValue;
            
          callObjectMethodByName ( $booterObjectId, "readMemory", array ("address" => $offset,length => $blockSize) );
          $result = waitForObjectResultByName ( $booterObjectId, 5, "MemoryData", $lastLogId );
          $compareCrc = getResultDataValueByName ( "data", $result );
          if ($compareCrc != $crc)
          {
            liveOut ( "Fehler bei offset: $offset -> " . $compareCrc . " != " . $crc );
            exit ();
          }
       }
       liveOut ( 'OK!' );
       liveOut ( '' );
    }
	
	liveOut ( "<b>Starte Controller neu...</b>" );
    callObjectMethodByName ( $booterObjectId, "reset" );
    flush ();
        
    if ($isBooter != 1) $receiverObjectId = getFirmwareObjectId($booterObjectId);
    else $receiverObjectId = $booterObjectId;
        
    $result = waitForObjectEventByName($receiverObjectId, 15, "evStarted", $lastLogId, "funtionDataParams", 0);
    if ($result==-1)
    {
        updateControllerStatus();
        liveOut ( "Fehler! Controller antwortet nicht" );
        exit ();
    }

    // Wenn gerade die normale Firmware geladen wurde, nehmen wir anschließend den Booter offline, damit der nicht nochmal geladen wird
    if ($isBooter != 1) QUERY("update controller set online='0' where objectId='$booterObjectId' limit 1");
     
    liveOut ( "OK" );
}

function saveAndVerifyControllerConfiguration($controllerObjectId, $verify=false)
{
  global $CONTROLLER_CLASSES_ID;
  global $lastLogId;
  global $skipConfigurationCheck;
  
  if ($verify)
  {
    liveOut ( '' );
    liveOut ( "<b>Verifiziere Konfiguration...</b>" );
  }

  if ($skipConfigurationCheck)
  {
    if ($verify) liveOut ( "skipped");
	return;
  }
  
  $erg = QUERY("select id from featureFunctions where featureClassesId='$CONTROLLER_CLASSES_ID' and name='getConfiguration' limit 1");
  $row=mysqli_fetch_ROW($erg);
  $controllerConfigFktId = $row[0];

  $changes=false;	 	 
  $erg = QUERY("select controller.name as ControllerName, objectId from controller where objectId='$controllerObjectId' limit 1");
  $obj=mysqli_fetch_OBJECT($erg);
  $result = callObjectMethodByNameAndRecover($obj->objectId, "getConfiguration", "", "Configuration",3,1,0);
  if ($result==-1) liveOut ( "Warnung: Configuration vom Controller konnte nicht gelesen werden." );
  else
  {
	if ($verify)
    {
	   if (!isset($_SESSION["restore".$controllerObjectId])) liveOut ( "Warnung: Originalconfiguration vom Controller konnte nicht gelesen werden." );
       else if (configIsDifferent($result, $_SESSION["restore".$controllerObjectId]))
	   {
		 $changes=true;
		 liveOut("Restauriere Configuration vom Controller....");
		 restoreConfig($controllerObjectId, $_SESSION["restore".$controllerObjectId]);
	   }
	}
	else
	{
      $config = serialize($result);
  	  $_SESSION["restore".$controllerObjectId]=$config;
      sleepMS(10);
	}
  } 
    
  $erg = QUERY("select featureInstances.name as featureInstanceName, featureInstances.featureClassesId, featureInstances.objectId,
                       featureClasses.name as featureClassName,
                       featureFunctions.id  as featureFunctionId,featureFunctions.name  as featureFunctionName,
                       controller.name as ControllerName, controller.objectId as controllerObjectId
                       from featureInstances
                       join controller on (controller.id = featureInstances.controllerId)
                       join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
                       join featureFunctions on (featureInstances.featureClassesId=featureFunctions.featureClassesId) 
                       where featureFunctions.name='setConfiguration' and checked='1' and controller.objectId='$controllerObjectId' and featureClasses.name!='Gateway'");
  while($obj=mysqli_fetch_OBJECT($erg))
  {
    $result = callObjectMethodByNameAndRecover($obj->objectId, "getConfiguration", "", "Configuration",3,1,0);
    if ($result==-1) liveOut ( "Warnung: Configuration konnte nicht gelesen werden: ".$obj->featureClassName." - ".$obj->featureInstanceName);
    else
    {
	   if ($verify)
       {
   	     if (configIsDifferent($result, $_SESSION["restore".$obj->objectId]))
	     {
		   $changes=true;
		   liveOut("Restauriere Configuration von ".$obj->featureClassName.": ".$obj->featureInstanceName);
		   restoreConfig($obj->objectId, $_SESSION["restore".$obj->objectId]);
	     }
	   }
	   else
	   {
         $config = serialize($result);
	     $_SESSION["restore".$obj->objectId]=$config;
	   }
    }
  }
  
  if ($verify)
  {
	if ($changes) liveOut ( "OK, einige Konfigurationen wurden wiederhergestellt." );
	else liveOut ( "OK" );
  }
}

function configIsDifferent($new, $old)
{
	$actData=array();
    foreach ( $new as $key => $valueObj )
    {
       $actData[$valueObj->name] = $valueObj->dataValue;
    }
        
    $data=array();
	$obj = unserialize($old);

    foreach ( $obj as $key => $valueObj )
    {
       if ($valueObj->name=="timeCorrection") continue;
       if ($valueObj->name=="reserve") continue;

       $data[$valueObj->name] = $valueObj->dataValue;
       if ($actData[$valueObj->name] != $valueObj->dataValue)
       {
         // echo $valueObj->name . " von " . $actData[$valueObj->name] . " -> " . $valueObj->dataValue . "<br>";
		 return TRUE;
       }
    }
	
	return FALSE;
}

function restoreConfig($objectId, $config)
{
  $obj = unserialize($config);
  
  foreach ( $obj as $key => $valueObj )
  {
   	if ($valueObj->name=="timeCorrection") continue;
   	if ($valueObj->name=="reserve") continue;

    $data[$valueObj->name] = $valueObj->dataValue;
  }
  
  callObjectMethodByName($objectId, "setConfiguration", $data);
  sleepMs(100);
  callObjectMethodByNameAndRecover($objectId, "getConfiguration", "", "Configuration");
  sleepMs(100);
}

function statusOut($bytes, $fileSize, $blockSize)
{
  $percent = ( int ) ($bytes * 100 / $fileSize);
  echo "<script>document.getElementById(\"status\").innerHTML=\"Updatestatus: $bytes/$fileSize Bytes - $percent%\";</script>";
}

function getBooterObjectId($firmwareObjectId)
{
  global $BOOTLOADER_INSTANCE_ID;

  return getObjectId ( getDeviceId ( $firmwareObjectId ), getClassId ( $firmwareObjectId ), $BOOTLOADER_INSTANCE_ID );
}

function getFirmwareObjectId($booterObjectId)
{
  global $FIRMWARE_INSTANCE_ID;

  return getObjectId ( getDeviceId ( $booterObjectId ), getClassId ( $booterObjectId ), $FIRMWARE_INSTANCE_ID );
}

?>

