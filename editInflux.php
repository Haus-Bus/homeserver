<?php
include ($_SERVER["DOCUMENT_ROOT"] . "/homeserver/include/all.php");

if ($action == "submitGraph")
{
  if ($delete == 1)
  {
    QUERY("DELETE from influx where id='$id' limit 1");
    triggerTreeUpdate();
    header("Location: editInflux.php");
    exit;
  }
  else
  {
    if ($id == "")
    {
      QUERY("INSERT into influx (title) values('$name')");
      $id = query_insert_id();
      triggerTreeUpdate();
    }
    else QUERY("update influx set title='$name' where id='$id' limit 1");
  }
}
else if ($action=="submitDbData")
{
	QUERY("REPLACE into basicConfig (paramKey, paramValue) values('influxServer','$influxSserver')");
	QUERY("REPLACE into basicConfig (paramKey, paramValue) values('influxToken','$influxToken')");
	QUERY("REPLACE into basicConfig (paramKey, paramValue) values('influxOrg','$influxOrg')");
	QUERY("REPLACE into basicConfig (paramKey, paramValue) values('influxBucket','$influxBucket')");
}

if ($deleteSignal!="") 
{
	QUERY("delete from influxSignals where id='$deleteSignal' and influxId='$id' limit 1");
	QUERY("delete from influxSignalEvents where influxSignalsId='$deleteSignal'");	
}
else if( $deleteEvent!="" ) QUERY("delete from influxSignalEvents  where id='$deleteEvent' limit 1");	
else if ($nameSignal!="")
{
    if ($submitted==1) QUERY("update influxSignals set title='$name' where id='$signalId' and influxId='$id' limit 1");
    else 
    {
       setupTreeAndContent("editInfluxSignalName_design.html");
       $html = str_replace("%ID%", $id, $html);
       $html = str_replace("%SIGNAL_ID%", $nameSignal, $html);
       $erg = QUERY("select title from influxSignals where id='$nameSignal' and influxId='$id' limit 1");
       $row=mysqli_fetch_ROW($erg);
       $html = str_replace("%NAME%", $row[0], $html);
        
       show();
    }
}
else if ($action=="editFkt")
{
  if ($submitted==1) QUERY("update influxSignalEvents set fkt='$fkt' where influxSignalsId='$signalId' and id='$signalEventId' limit 1");
  else
  {
    setupTreeAndContent("editInfluxSignalFkt_design.html");
    $html = str_replace("%ID%", $id, $html);
    $html = str_replace("%SIGNAL_ID%", $signalId, $html);
    $html = str_replace("%SIGNAL_EVENT_ID%", $signalEventId, $html);
    
    $erg = QUERY("select functionId,fkt from influxSignalEvents where influxSignalsId='$signalId' and id='$signalEventId' limit 1");
    $row=mysqli_fetch_ROW($erg);
    $functionId = $row[0];
    $fkt = $row[1];
    $html = str_replace("%FKT%", $fkt, $html);
    
    $params="";
    $erg = QUERY("select name from featureFunctionParams where featureFunctionId='$functionId' order by id");
    while($row=mysqli_fetch_ROW($erg))
    {
       $params.="<li>".$row[0]."<br>";
    }
    
    if ($params=="") $params="<i>keine</i> (Bitte festen Wert eintragen)";
    $html = str_replace("%PARAMS%", $params, $html);
    

    show();
  }
}

setupTreeAndContent("editInflux_design.html");
if ($id != "")
{
  removeTag("%OPT_DB_DATA%", $html);
  $html = str_replace("%MODE%", "bearbeiten", $html);
  
  $erg = QUERY("select * from influx where id='$id' limit 1");
  $obj = mysqli_fetch_OBJECT($erg);
  
  $html = str_replace("%NAME%", $obj->title, $html);
  $html = str_replace("%SUBMIT_TITLE%", "Änderungen speichern", $html);
  
  chooseTag("%OPT_DELETE%", $html);
  chooseTag("%OPT_SIGNALS%",$html);
  
  $allFeatureInstances = readFeatureInstances();
  $allFeatureFunctions = readFeatureFunctions();
  
  $signals="";
  $signalTag = getTag("%SIGNALS%",$html);
  $signalEventsTag = getTag("%SIGNAL_EVENTS%",$signalTag);
  $signalFktTag = getTag("%SIGNAL_FKTS%",$signalTag);
    
  $erg = QUERY("select id,title,featureInstanceId,functionId,fkt from influxSignals where influxId='$id' order by id");
  while($obj=mysqli_fetch_OBJECT($erg))
  {
  	$actTag = $signalTag;
  	
    $signalEvents="";  
	  $signalFkts="";  
    $erg2 = QUERY("select id,featureInstanceId,functionId,fkt from influxSignalEvents where influxSignalsId='$obj->id' order by id");
    while($obj2=mysqli_fetch_OBJECT($erg2))    
    {
    	$actSignalFktTag = $signalFktTag;
    	if ($obj2->fkt=="") $obj2->fkt="[anlegen]";
	    $actSignalFktTag = str_replace("%FKT%",$obj2->fkt,$actSignalFktTag);
	        	
    	$actEventTag = $signalEventsTag;
    	
	    $roomName = getRoomForFeatureInstance($obj2->featureInstanceId)->name;
	    $actFeatureInstanceName = $allFeatureInstances[$obj2->featureInstanceId]->name;
	    //  $actClassName = $allFeatureClasses[$actFeatureInstance->featureClassesId]->name;
	    $functionName = $allFeatureFunctions[$obj2->functionId]->name;
	    $signalEvent=$roomName." » ".$actFeatureInstanceName." » ".$functionName;
	    $actEventTag = str_replace("%SIGNAL_EVENT%",$signalEvent,$actEventTag);
        $actEventTag = str_replace("%EVENT_ID%",$obj2->id,$actEventTag);
        $actSignalFktTag = str_replace("%EVENT_ID%",$obj2->id,$actSignalFktTag);
	    $signalEvents .= $actEventTag;
	    $signalFkts .= $actSignalFktTag;
    }  
	  
    $actTag = str_replace("%SIGNAL_EVENTS%",$signalEvents,$actTag); 
    $actTag = str_replace("%SIGNAL_FKTS%",$signalFkts,$actTag);
    $actTag = str_replace("%SIGNAL_ID%",$obj->id,$actTag);
    $actTag = str_replace("%ID%",$id,$actTag);
    $actTag = str_replace("%COLOR%",$obj->color,$actTag);
    if ($obj->title=="") $obj->title="[anlegen]";
    $actTag = str_replace("%SIGNAL_NAME%",$obj->title,$actTag);
    $signals.=$actTag;
  }
  
  $html = str_replace("%SIGNALS%",$signals,$html);
 
}
else
{
  chooseTag("%OPT_DB_DATA%", $html);
  
  $erg = QUERY("select paramKey, paramValue from basicconfig where paramKey='influxServer' or paramKey='influxToken' or  paramKey='influxOrg' or  paramKey='influxBucket' limit 4");
  while($obj=mysqli_fetch_object($erg))
  {
	  $key=$obj->paramKey;
	  $$key=$obj->paramValue;
  }
	  
  $html = str_replace("%SERVER%", $influxServer, $html);
  $html = str_replace("%TOKEN%", $influxToken, $html);
  $html = str_replace("%ORG%", $influxOrg, $html);
  $html = str_replace("%BUCKET%", $influxBucket, $html);
	
  $html = str_replace("%MODE%", "erstellen", $html);
  $html = str_replace("%NAME%", "", $html);
  $html = str_replace("%SUBMIT_TITLE%", "Datengruppe erstellen", $html);
  
  removeTag("%OPT_DELETE%", $html);
  removeTag("%OPT_SIGNALS%",$html);
}

$html = str_replace("%ERROR%", "", $html);
$html = str_replace("%ID%", $id, $html);

show();
?>