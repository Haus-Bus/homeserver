# Sicherstellen, dass alle Shell Scripte Unix Zeilenumbruch haben
apt-get install dos2unix
dos2unix *.sh

# Statische IP Adresse einstellen, falls kein DHCP verfuegbar
file="/etc/dhcpcd.conf"
sed -i -e 's/#profile static_eth0/profile static_eth0/' $file
sed -i -e 's/#static ip_address=192.168.1.23\/24/static ip_address=192.168.0.23\/24/' $file
sed -i -e 's/#static routers=192.168.1.1/static routers=192.168.0.1/' $file
sed -i -e 's/#static domain_name_servers=192.168.1.1/static domain_name_servers=192.168.0.1/' $file
sed -i -e 's/#fallback static_eth0/interface eth0\nfallback static_eth0/' $file
