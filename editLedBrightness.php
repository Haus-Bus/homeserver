<?php
include($_SERVER["DOCUMENT_ROOT"]."/homeserver/include/all.php");

if ($submitted==1)
{
  QUERY("DELETE from basicConfig where paramKey = 'ledStatusBrightness' limit 1");
  QUERY("INSERT into basicConfig (paramKey,paramValue) values('ledStatusBrightness','$brightness')");

  QUERY("DELETE from basicConfig where paramKey = 'ledLogicalButtonBrightness' limit 1");
  QUERY("INSERT into basicConfig (paramKey,paramValue) values('ledLogicalButtonBrightness','$logicalBrightness')");
  
  $message="Einstellung wurde gespeichert.<br>Änderungen sind erst nach Regelübermittlung aktiv.";
}
else if ($action=="addException")
{
	$erg = QUERY("select id from featureInstances where objectId='$exObjectId' limit 1");
	if ($row=mysqli_fetch_ROW($erg))
	{
		QUERY("DELEte from brightnessExceptions where objectId='$exObjectId'");
		QUERY("INSERT into brightnessExceptions (objectId, brightness) values('$exObjectId','$exBrightness')");
	}
	else $error="ObjectID $exObjectId nicht vorhanden";
}

setupTreeAndContent("editLedBrightness_design.html", $message);

$ledStatusBrightness="100";
$erg = QUERY("select paramValue from basicConfig where paramKey = 'ledStatusBrightness' limit 1");
if($row = mysqli_fetch_ROW($erg)) $ledStatusBrightness=$row[0];
$html = str_replace("%BRIGHTNESS%", $ledStatusBrightness, $html);

$logicalBrightness="50";
$erg = QUERY("select paramValue from basicConfig where paramKey = 'ledLogicalButtonBrightness' limit 1");
if($row = mysqli_fetch_ROW($erg)) $ledStatusBrightness=$row[0];
$html = str_replace("%LOGICAL_BRIGHTNESS%", $ledStatusBrightness, $html);

$exceptionTag = getTag("%OPT_EXCEPTION%",$html);
$exceptions="";
$erg = QUERY("select brightnessExceptions.id, brightnessExceptions.objectId,brightnessExceptions.brightness, name from brightnessExceptions left join featureInstances on (featureInstances.objectId=brightnessExceptions.objectId) order by id");
while ($obj=mysqli_fetch_object($erg))
{
	$actTag = $exceptionTag;
	$actTag = str_replace("%EX_OBJECTID%",$obj->name,$actTag);
	$actTag = str_replace("%EX_BRIGHTNESS%",$obj->brightness, $actTag);
	$actTag = str_replace("%EX_ID%",$obj->id, $actTag);
	$exceptions.=$actTag;
}
$html = str_replace("%OPT_EXCEPTION%", $exceptions, $html);

if ($error!="") $error="<br><font color=#bb0000><b>$error</b></font><br><br>";
$html = str_replace("%ERROR%", $error, $html);
show();

?>