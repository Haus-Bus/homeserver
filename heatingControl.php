<?php
include ($_SERVER["DOCUMENT_ROOT"] . "/homeserver/include/all.php");

// URL Variablen von addGenericFeature wieder in Variablen umwandeln
if ($params!="") urlToParams($params);

if ($action=="createNew") QUERY("INSERT into heating (sensor) values('0')");
else if ($action=="delete")
{
	 if ($confirm==1)
	 {
	 	 deleteGeneratedDiagram($id);
		 deleteGeneratedBaseRule($id);
	 	 QUERY("delete from heating where id='$id' limit 1");
	 }
	 else showMessage("Soll dieses Thermostat wirklich gelöscht werden?","Thermostat löschen?", $link = "heatingControl.php?action=delete&confirm=1&id=$id", $linkName = "Ja, löschen", $link2 = "heatingControl.php", $link2Name = "Nein, zurück");
}
else if ($action=="addSensor")
{
	QUERY("UPDATE heating set sensor='$featureInstanceId' where id='$id' limit 1");
	renewControl($id);
}
else if ($action=="deleteSensor")
{
	QUERY("UPDATE heating set sensor='0' where id='$id' limit 1");
	renewControl($id);
}
else if ($action=="addAktor")
{
	QUERY("UPDATE heating set relay='$featureInstanceId' where id='$id' limit 1");
	renewControl($id);
}
else if ($action=="deleteAktor")
{
	QUERY("UPDATE heating set relay='0' where id='$id' limit 1");
	renewControl($id);
}
else if ($action=="addPump") QUERY("UPDATE heating set pump='$featureInstanceId' where id='$id' limit 1");
else if ($action=="deletePump") QUERY("UPDATE heating set pump='0' where id='$id' limit 1");
else if ($action=="selectDiagram")
{
   $html = file_get_contents("templates/selectDiagram_design.html");
   $elementsTag = getTag("%ELEMENTS%",$html);
   $elements="";
   $erg = QUERY("select id,title from graphs order by id");
   while($obj=MYSQLi_FETCH_OBJECT($erg))
   {  
   	  $actTag = $elementsTag;
	  $actTag = str_replace("%DIAGRAM_ID%",$obj->id, $actTag);
	  $actTag = str_replace("%DIAGRAM_TITLE%",$obj->title, $actTag);
	  $elements.=$actTag;
   }
   
   $html = str_replace("%ELEMENTS%",$elements,$html);
   $html = str_replace("%ID%",$id, $html);
   
   $erg = QUERY("select diagram from heating where id='$id' limit 1");
   $row=MYSQLi_FETCH_ROW($erg);
   $html = str_replace("%GRAPH_ID%",$row[0], $html);

   die($html);
}
else if ($action=="deleteDiagram") QUERY("UPDATE heating set diagram='0' where id='$id' limit 1");
else if ($action=="setDiagram")
{
	deleteGeneratedDiagram($id);
	QUERY("UPDATE heating set diagram='$diagram' where id='$id' limit 1");
}
else if($action=="createDiagram")
{
	$erg = QUERY("select sensor, relay,diagram from heating where id='$id' limit 1");
	if ($obj=MYSQLi_FETCH_OBJECT($erg))
	{
		$data = getFeatureInstanceData($obj->sensor);
		$sensorName = $data->featureInstanceName;
		$relayData = getFeatureInstanceData($obj->relay);

    // generierte Diagramme löschen
  	deleteGeneratedDiagram($id);
		
  	QUERY("INSERT into graphs (title,timeMode,timeParam1,timeParam2,width,height,distValue,distType,theme,heightMode) 
	                    values('Thermostat $sensorName','days','7','','','700','','','','fixed')");
	  $graphId = query_insert_id();
    
    QUERY("TRUNCATE graphData");
    
    QUERY("INSERT into graphSignals (graphId, color, title) values('$graphId','FF2D0D','$sensorName')");
    $signalId = query_insert_id();
    QUERY("INSERT into graphSignalEvents (graphSignalsId, featureInstanceId, functionId,fkt) 
                                   values('$signalId','$obj->sensor','94','celsius+centiCelsius/100')"); // 94 ist FunctionId vom Temperatur Statusevent

    if ($diagramType==1 && $obj->relay>0)
    {
    	if ($relayData->featureClassName=="Rollladen")
    	{
 		  	$onId=175;  //175 ist FunctionId vom Rollo evOpen
 		  	$offId=56;  //56 ist FunctionId vom Rollo evClosed
  
        QUERY("INSERT into graphSignals (graphId, color, title, type) values('$graphId','086BFF','Heizungsschaltung','steps')");
        $signalId = query_insert_id();
        QUERY("INSERT into graphSignalEvents (graphSignalsId, featureInstanceId, functionId,fkt) 
                                         values('$signalId','$obj->relay','$onId','22')"); 
        QUERY("INSERT into graphSignalEvents (graphSignalsId, featureInstanceId, functionId,fkt) 
                                         values('$signalId','$obj->relay','$offId','22 - position/25')"); 
    	}
    	else
    	{ 
      	$onId=62;  //62 ist FunctionId vom Schalter evOn
      	$offId=63;  //63 ist FunctionId vom Schalter evOff
  
        QUERY("INSERT into graphSignals (graphId, color, title, type) values('$graphId','086BFF','Heizungsschaltung','steps')");
        $signalId = query_insert_id();
        QUERY("INSERT into graphSignalEvents (graphSignalsId, featureInstanceId, functionId,fkt) 
                                         values('$signalId','$obj->relay','$onId','20')"); 
        QUERY("INSERT into graphSignalEvents (graphSignalsId, featureInstanceId, functionId,fkt) 
                                         values('$signalId','$obj->relay','$offId','19.5')"); 
      }
    }
    
    QUERY("UPDATE heating set diagram='$graphId' where id='$id' limit 1");
    
	  triggerTreeUpdate();
	}
}
else if($action=="setConfig")
{
	$reduction = abs(str_replace(",",".",$reduction));
	QUERY("UPDATE heating set reduction='$reduction' where id='$id' limit 1");
	
	$erg = QUERY("select sensor, relay, startHour, endHour, reduction from heating where id='$id' limit 1");
	if ($row = MYSQLi_FETCH_ROW($erg))
	{
		 $data = getFeatureInstanceData($row[0]);
		 $objectId = $data->objectId;
		 $callArray = array();

         $erg2 = QUERY("select functionData from lastReceived where senderObj='".$objectId."' and function='Configuration' order by id desc limit 1");
         if ($row2=MYSQLi_FETCH_ROW($erg2))
         {
   	       $functionData = unserialize($row2[0]);
      	   $paramData = $functionData->paramData;
   	       foreach ($paramData as $object)
   	       {
			  $callArray[$object->name]=$object->dataValue;
     	   }
     	 }
		 else
		 {
			 $result = executeCommand($objectId, "getConfiguration", "", "Configuration");
			 foreach ($result as $key=>$value)
			 {
				 $callArray[$key]=$value;
			 }
		 }
		 
		 $newTemp="temp_1_$id";
	     $newTemp=trim($$newTemp);
	     $newTemp = str_replace(",",".",$newTemp);

 		 $upperThreshold = (int)$newTemp;
		 $upperThresholdFraction = round(($newTemp-$upperThreshold)*100);

		 $reduction = $row[4];
		 $newLowerValue = $newTemp-$reduction;
		 
 		 $lowerThreshold = (int)$newLowerValue;
		 $lowerThresholdFraction = round(($newLowerValue-$lowerThreshold)*100);
	 
		 $callArray["lowerThreshold"]=$lowerThreshold;
		 $callArray["lowerThresholdFraction"]=$lowerThresholdFraction;
		 
		 $callArray["upperThreshold"]=$upperThreshold;
		 $callArray["upperThresholdFraction"]=$upperThresholdFraction;
		 
 	     callObjectMethodByName($objectId, "setConfiguration", $callArray);
	     sleepMs(200);
	     callObjectMethodByName($objectId, "getConfiguration");
	     sleepMs(2000);
		 		 
	     $debug="<b>Einstellung wurde gespeichert</b><br>";

		 // Zeitraum in Regel eintragen
		 QUERY("UPDATE heating set startHour='$nightStart', endHour='$nightEnd' where id='$id' limit 1");
		 
		 $erg2= QUERY("select basicRules.id as ruleId, basicRules.startHour, basicRules.endHour, basicRules.startMinute, basicRules.endMinute, basicRules.intraDay
	 	                from groups 
	 	                join groupFeatures on (groupFeatures.groupId = groups.id)
	 	                join basicRules on (basicRules.groupId = groups.id)
	 	                join basicrulesignals on (basicrulesignals.ruleId=basicRules.id)
	 	                where groupFeatures.featureInstanceId='$row[1]' and groups.single=1
	 	                      and extras='Heizungssteuerung'
	 	                      and basicrulesignals.featureInstanceId='$row[0]'
	 	                ");
	 	 if ($obj2=MYSQLi_FETCH_OBJECT($erg2))
		 {
			$ruleId = $obj2->ruleId;
		    $actStartTime = $obj2->startHour;
		    $actEndTime = $obj2->endHour;
		    $actStartMinute = $obj2->startMinute;
		    $actEndMinute = $obj2->endMinute;
			$intraDay = 1;
			
			if ($nightStart==0 && $nightEnd==0)
			{
				$nightStart=31;
				$nightEnd=31;
				$nightStartMinute=255;
				$nightEndMinute=255;
			    $intraDay=0;
			}
			else 
			{
				$nightStartMinute=0;
				$nightEndMinute=0;
			}
			
			if ($actStartTime!=$nightEnd || $actEndTime!=$nightStart || $actStartMinute!=$nightStartMinute || $actEndMinute!=$nightEndMinute)
			{
			  QUERY("UPDATE basicRules set startDay='0', endDay='6', startHour='$nightEnd',startMinute='$nightStartMinute',endHour ='$nightStart',endMinute ='$nightEndMinute', intraDay='$intraDay' where id='$ruleId' limit 1");
			  
 			  $debug.="<b>Nach Änderung der Nachtabsenkung müssen die Regeln abschließend einmal übertragen werden!</b><br>";
			}
		 }
	}
	else $debug="Fehler: ungültige ID";
}
else if($action=="updateReduction")
{
   $controllerThatHoldsSystemProperties = getFirstNormalOnlineController()->objectId;	

   $result = executeCommand($controllerThatHoldsSystemProperties, "getSystemVariable", array("type" => "BIT", "index" => 0), "SystemVariable");
   if ($result["value"]==1) $mode="Aus";

   $result = executeCommand($controllerThatHoldsSystemProperties, "getSystemVariable", array("type" => "BIT", "index" => 1), "SystemVariable");
   if ($result["value"]==1)
   {
	   if ($mode=="Aus") $mode="Fehler!";
	   else $mode="Daueran";
   }
   
   if ($mode=="") $mode="Auto";
   
   $_SESSION["actReductionMode"] = $mode;
   
   $debug="";
}
else if($action=="setReductionOff")
{
	$controllerThatHoldsSystemProperties = getFirstNormalOnlineController()->objectId;	

	executeCommand($controllerThatHoldsSystemProperties, "setSystemVariable", array("type" => "BIT", "index" => 0, "value" => 1));
	executeCommand($controllerThatHoldsSystemProperties, "setSystemVariable", array("type" => "BIT", "index" => 1, "value" => 0));
	usleep(200000);
	header("Location: heatingControl.php?action=updateReduction");
	exit;
}
else if($action=="setReductionPermanent")
{
	$controllerThatHoldsSystemProperties = getFirstNormalOnlineController()->objectId;	
	
	executeCommand($controllerThatHoldsSystemProperties, "setSystemVariable", array("type" => "BIT", "index" => 1, "value" => 1));
	executeCommand($controllerThatHoldsSystemProperties, "setSystemVariable", array("type" => "BIT", "index" => 0, "value" => 0));
	usleep(200000);
	header("Location: heatingControl.php?action=updateReduction");
	exit;
}
else if($action=="setReductionAuto")
{
	$controllerThatHoldsSystemProperties = getFirstNormalOnlineController()->objectId;	
	
	executeCommand($controllerThatHoldsSystemProperties, "setSystemVariable", array("type" => "BIT", "index" => 1, "value" => 0));
	executeCommand($controllerThatHoldsSystemProperties, "setSystemVariable", array("type" => "BIT", "index" => 0, "value" => 0));
	usleep(200000);
	header("Location: heatingControl.php?action=updateReduction");
	exit;
}
		 
// GGF Logische Gruppe für Nachtabsenkung erstellen
$found=false;
$erg = QUERY("select userValue from userdata where userKey='Gruppe_Nachtabsenkung' limit 1");
if ($row=MYSQLi_FETCH_ROW($erg))
{
  $absenkungGroupId=$row[0];
  $erg = QUERY("select id from groups where id='$absenkungGroupId' limit 1");
  if ($row=mysqli_fetch_row($erg)) $found=true;
  else QUERY("delete from userdata where userKey='Gruppe_Nachtabsenkung' limit 1");
}

if (!$found)
{
  $softwareControllerInstanceId = getFirstSoftwareController()->featureInstanceId;

  QUERY("INSERT into groups (name, groupType,active) values('Nachtabsenkung Deaktivierung','','1')");
  $absenkungGroupId = QUERY_INSERT_ID();
  QUERY("INSERT into userdata (userKey, userValue) values('Gruppe_Nachtabsenkung','$absenkungGroupId')");
  
  $ausState = getOrGenerateGroupDefaultState($absenkungGroupId,0, "Aus");
  $anState = getOrGenerateGroupDefaultState($absenkungGroupId, 1, "Ein");

  $erg = QUERY("select id,objectId from featureInstances where objectId='800985121' limit 1");
  $row=mysqli_fetch_row($erg);
  $tasterDeaktivierung = $row[0];
  
  $ruleId = generateDefaultRule($absenkungGroupId, $ausState, $anState ,0);  
  generateDefaultSignal($ruleId, $tasterDeaktivierung, 2, array(568 => 807) , false, 0); // evClicked -> any
  generateDefaultAction($ruleId, $softwareControllerInstanceId, 268, array(540 => 0, 541 => 0, 542 => 1), 0); // SetSystemVariable type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1
  generateDefaultAction($ruleId, $softwareControllerInstanceId, 268, array(540 => 0, 541 => 1, 542 => 0), 0); // SetSystemVariable type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1

  $ruleId = generateDefaultRule($absenkungGroupId, $anState ,$ausState ,0);  
  generateDefaultSignal($ruleId, $tasterDeaktivierung, 2, array(568 => 807) , false, 0); // evClicked -> any
  generateDefaultAction($ruleId, $softwareControllerInstanceId, 268, array(540 => 0, 541 => 0, 542 => 0), 0); // SetSystemVariable type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1

  $ruleId = generateDefaultRule($absenkungGroupId, 0 ,$anState ,0);  
  generateDefaultSignal($ruleId, $softwareControllerInstanceId, 363, array(810 => 0, 811 => 0, 812 => 1), false, 0); // evSystemVariableChanged type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1

  $ruleId = generateDefaultRule($absenkungGroupId, 0 ,$ausState ,0);  
  generateDefaultSignal($ruleId, $softwareControllerInstanceId, 363, array(810 => 0, 811 => 0, 812 => 0), false, 0); // evSystemVariableChanged type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1

  $debug.="<b><font color=#bb0000>Es wurde eine Gruppe mit Namen 'Nachtabsenkung Deaktivierung' erstellt, mit der die Nachtabsenkung im aktiven Zustand ausgesetzt wird.</b></font><br><br>"; 
}

// GGF Logische Gruppe für Nachtabsenkung erstellen
$found=false;
$erg = QUERY("select userValue from userdata where userKey='Gruppe_Nachtabsenkung_Dauer_An' limit 1");
if ($row=MYSQLi_FETCH_ROW($erg))
{
  $absenkungDauerAnGroupId=$row[0];
  $erg = QUERY("select id from groups where id='$absenkungDauerAnGroupId' limit 1");
  if ($row=mysqli_fetch_row($erg)) $found=true;
  else QUERY("delete from userdata where userKey='Gruppe_Nachtabsenkung_Dauer_An' limit 1");
}

if (!$found)
{
  $softwareControllerInstanceId = getFirstSoftwareController()->featureInstanceId;

  QUERY("INSERT into groups (name, groupType,active) values('Nachtabsenkung Daueraktivierung','','1')");
  $absenkungDauerAnGroupId = QUERY_INSERT_ID();
  QUERY("INSERT into userdata (userKey, userValue) values('Gruppe_Nachtabsenkung_Dauer_An','$absenkungDauerAnGroupId')");
  
  $erg = QUERY("select id,objectId from featureInstances where objectId='800985122' limit 1");
  $row=mysqli_fetch_row($erg);
  if ($row[1]==800985122) $tasterDauerAktivierung = $row[0];

  
  $ausState = getOrGenerateGroupDefaultState($absenkungDauerAnGroupId,0, "Aus");
  $anState = getOrGenerateGroupDefaultState($absenkungDauerAnGroupId, 1, "Ein");
  
  $ruleId = generateDefaultRule($absenkungDauerAnGroupId, $ausState, $anState ,0);  
  generateDefaultSignal($ruleId, $tasterDauerAktivierung, 2, array(568 => 807) , false, 0); // evClicked -> any
  generateDefaultAction($ruleId, $softwareControllerInstanceId, 268, array(540 => 0, 541 => 1, 542 => 1), 0); // SetSystemVariable type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1
  generateDefaultAction($ruleId, $softwareControllerInstanceId, 268, array(540 => 0, 541 => 0, 542 => 0), 0); // SetSystemVariable type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1

  $ruleId = generateDefaultRule($absenkungDauerAnGroupId, $anState ,$ausState ,0);  
  generateDefaultSignal($ruleId, $tasterDauerAktivierung, 2, array(568 => 807) , false, 0); // evClicked -> any
  generateDefaultAction($ruleId, $softwareControllerInstanceId, 268, array(540 => 0, 541 => 1, 542 => 0), 0); // SetSystemVariable type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1
  
  $ruleId = generateDefaultRule($absenkungDauerAnGroupId, 0 ,$anState ,0);  
  generateDefaultSignal($ruleId, $softwareControllerInstanceId, 363, array(810 => 0, 811 => 1, 812 => 1), false, 0); // evSystemVariableChanged type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1

  $ruleId = generateDefaultRule($absenkungDauerAnGroupId, 0 ,$ausState ,0);  
  generateDefaultSignal($ruleId, $softwareControllerInstanceId, 363, array(810 => 0, 811 => 1, 812 => 0), false, 0); // evSystemVariableChanged type = bit , index = systemvarialenindex 0=Nachtabsenkung Deaktivierung, value = 1

  $debug.="<b><font color=#bb0000>Es wurde eine Gruppe mit Namen 'Nachtabsenkung Daueraktivierung' erstellt, mit der die Nachtabsenkung im aktiven Zustand auch tagsüber aktiviert werden kann.</b></font><br><br>"; 
}

QUERY("UPDATE featureInstances set name='Taster Deaktivierung Nachtabsenkung' where objectId='800985121' limit 1");
QUERY("UPDATE featureInstances set name='Taster Daueraktivierung Nachtabsenkung' where objectId='800985122' limit 1");

//else if ($action == "changeStartTime") QUERY("UPDATE heating set startHour='$startTime' where id='$id' limit 1");
//else if ($action == "changeEndTime") QUERY("UPDATE heating set endHour='$endTime' where id='$id' limit 1");


$html = file_get_contents("templates/heatingControl_design.html");

// Sekunden =  Sekunden minReportTime * 1s baseTime
$namesDauer = "deaktiviert";
$valsDauer = "0-0";
for($i = 10; $i < 60; $i+=10)
{
  $namesDauer .= "," . $i . " Sek";
  $valsDauer .= "," . $i."-1";
}

// Minuten =  Minuten*2 minReportTime * 30s baseTime
for($i = 1; $i <= 10; $i++)
{
  $namesDauer .= "," . $i . " Min";
  $valsDauer .= "," . ($i * 2)."-30";
}

// 10er Minuten = Minuten minReportTime * 60s baseTime
for($i = 20; $i <= 50; $i+=10)
{
  $namesDauer .= "," . $i . " Min";
  $valsDauer .= "," .$i."-60";
}

// Stunden = Stunden * 60 minReportTime * 60s baseTime
for($i = 1; $i <= 4; $i++)
{
  $namesDauer .= "," . $i . " Std";
  $valsDauer .= "," . ($i * 60)."-60";
}


$elementsTag = getTag("%ELEMENTS%",$html);
$elements="";
//$foundInavtive=0;
$generated=0;
$count=0;
$erg = QUERY("select heating.id, heating.sensor, heating.relay, heating.diagram, heating.pump, heating.startHour, heating.endHour, heating.reduction,
                     graphs.title as graphTitle, group_concat(rooms.name) as roomName
                     from heating 
                     left join graphs on(graphs.id=heating.diagram)
                     left join roomFeatures on(roomFeatures.featureInstanceId=heating.sensor)
                     left join rooms on(rooms.id=roomFeatures.roomId)
                     where diagram!=-1
                     group by roomFeatures.featureInstanceId,heating.id
                     order by heating.id");
while($obj=MYSQLi_FETCH_OBJECT($erg))
{
	 $actTag = $elementsTag;
	 $actTag = str_replace("%ID%",$obj->id, $actTag);
	 
	 $count++;
	 if ($count%2==0) $actTag = str_replace("%BG_COLOR%","#eeeeee", $actTag);
	 else $actTag = str_replace("%BG_COLOR%","", $actTag);
	 
	 if ($obj->sensor==0)
	 {
	 	$mySensor="eintragen";
	 	removeTag("%OPT_SENSOR%",$actTag);
	 	removeTag("%OPT_DELETE_SENSOR%",$actTag);
	 }
	 else
	 {
	 	chooseTag("%OPT_SENSOR%",$actTag);
	 	chooseTag("%OPT_DELETE_SENSOR%",$actTag);
	 	$data = getFeatureInstanceData($obj->sensor);
	 	$mySensor = $data->roomName." > ".$data->featureInstanceName;

    	$minReportTime=2;
     	$reportTimeBase=60;
	 	  
	 	$erg2 = QUERY("select functionData from lastReceived where senderObj='".$data->objectId."' and function='Configuration' order by id desc limit 1");
        if ($row2=MYSQLi_FETCH_ROW($erg2))
        {
     	  $functionData = unserialize($row2[0]);
          $paramData = $functionData->paramData;
   	      foreach ($paramData as $object)
   	      {
   	    	$act=$object->name;
     	    $$act = $object->dataValue;
     	  }
     	}
     	
     	if ($lowerThresholdFraction<10) $lowerThresholdFraction="0".$lowerThresholdFraction;
     	if ($upperThresholdFraction<10) $upperThresholdFraction="0".$upperThresholdFraction;
     	
     	$actTag = str_replace("%TEMP_1%",$upperThreshold.".".$upperThresholdFraction, $actTag);
	  	$actTag = str_replace("%INTERVAL_OPTIONS%",getSelect($minReportTime."-".$reportTimeBase, $valsDauer, $namesDauer), $actTag);

        $options = getTimeOptionsHour($obj->startHour);
        $actTag = str_replace("%TIME_START_TIME_OPTIONS%", $options, $actTag);

        $options = getTimeOptionsHour($obj->endHour);
        $actTag = str_replace("%TIME_END_TIME_OPTIONS%", $options, $actTag);
	 }
	 
	 $actTag = str_replace("%REDUCTION%", $obj->reduction, $actTag);
	 
	 $actTag = str_replace("%TEMP%",$mySensor, $actTag);
	 
	 if ($obj->relay==0)
	 {
	 	 $myAktor="eintragen";
	 	 removeTag("%OPT_DELETE_AKTOR%",$actTag);
	 }
	 else
	 {
	 	  chooseTag("%OPT_DELETE_AKTOR%",$actTag);
	 	  $relayData = getFeatureInstanceData($obj->relay);
	 	  $myAktor = $data->roomName." > ".$relayData->featureInstanceName;
	 }
	 $actTag = str_replace("%SWITCH%",$myAktor, $actTag);
	 
	 if ($obj->diagram==0 || $obj->graphTitle==null)
	 {
	 	 if ($obj->sensor==0) $mySensor="</a>Zuerst Sensor auswählen";
	 	 else $mySensor="eintragen";
	 	 removeTag("%OPT_DELETE_DIAGRAM%",$actTag);
	 }
	 else
	 {
	 	 chooseTag("%OPT_DELETE_DIAGRAM%",$actTag);
	 	 $mySensor=$obj->graphTitle;
	 }

	 if ($obj->pump==0)
	 {
	 	 $myAktor="eintragen";
	 	 removeTag("%OPT_DELETE_PUMP%",$actTag);
	 }
	 else
	 {
	 	  chooseTag("%OPT_DELETE_PUMP%",$actTag);
	 	  $pumpData = getFeatureInstanceData($obj->pump);
	 	  $myAktor = $pumpData->roomName." > ".$pumpData->featureInstanceName;
	 }
	 $actTag = str_replace("%PUMP%",$myAktor, $actTag);

	 
	 //$actInactive=0;
	 $status="<img src='/homeserver/img/offline2.gif' title='Regeln für Thermostat noch nicht generiert'>";
	 
	 if ($obj->sensor>0 && $obj->relay>0)
	 {
	 	  //$actInactive=1;
	 	  $erg2= QUERY("select groups.id as groupId,
	 	                       basicRules.id as ruleId,
							   basicRules.active as ruleActive,
	 	                       basicrulesignals.id as signalId
	 	                      
	 	                from groups 
	 	                join groupFeatures on (groupFeatures.groupId = groups.id)
	 	                join basicRules on (basicRules.groupId = groups.id)
	 	                join basicrulesignals on (basicrulesignals.ruleId=basicRules.id)
	 	                where groupFeatures.featureInstanceId='$obj->relay' and groups.single=1
	 	                      and extras='Heizungssteuerung'
	 	                      and basicrulesignals.featureInstanceId='$obj->sensor'
	 	                ");
	 	  if ($obj2=MYSQLi_FETCH_OBJECT($erg2))
		  {
			  if ($action=="invertHeatingStatus" && $id==$obj->id)
			  {
				  if ($obj2->ruleActive==1)
				  {
					  QUERY("UPDATE basicRules set active=0 where id='$obj2->ruleId' limit 1");
					  $obj2->ruleActive=0;
				  }
				  else 
				  {
					  QUERY("UPDATE basicRules set active=1 where id='$obj2->ruleId' limit 1");
					  $obj2->ruleActive=1;
				  }
			  }

			  if ($obj2->ruleActive==1) $status="<img src='/homeserver/img/online2.gif' title='Regeln sind generiert und aktiv. Klicken, um Steuerung zu deaktivieren'>";
			  else $status="<img src='/homeserver/img/offline2.gif' title='Regeln sind generiert, aber inaktiv. Klicken, um Steuerung zu aktivieren'>";
		  }
	 	  else //if ($action=="generateRules")
	 	  {
	 	  	  $erg2= QUERY("select groups.id as groupId 
	 	  	                       from groups 
	 	  	          	           join groupFeatures on (groupFeatures.groupId = groups.id)
           	 	                 where groupFeatures.featureInstanceId='$obj->relay' and groups.single=1
     	                 ");
     	    if ($obj2=MYSQLi_FETCH_OBJECT($erg2))
     	    {
     	    	 $fktValue="0";
     	    	 if ($relayData->featureClassName=="Rollladen") $fktValue="true";
     	    	 
     	    	 QUERY("INSERT into basicRules (groupId, fkt1,fkt2,startDay,startHour,startMinute,endDay,endHour,endMinute,extras,active)
     	    	                         values('$obj2->groupId','$fktValue','true','7','31','255','7','31','255','Heizungssteuerung','1')");
     	    	 $ruleId = QUERY_INSERT_ID();
				 
 				 QUERY("update heating set basicRuleId='$ruleId' where id = '$obj->id' limit 1");


     	    	 QUERY("INSERT into basicRuleSignals (ruleId, featureInstanceId) values ('$ruleId','$obj->sensor')");
     	    	 $signalId = QUERY_INSERT_ID();
     	    	 
     	    	 $status="<img src='/homeserver/img/online2.gif' title='Regeln sind generiert und aktiv. Klicken um sie zu deaktivieren'>";
	 	  	     //$actInactive=0;
	 	  	     $debug.="Regel für Thermostat $mySensor wurde generiert <br>";
	 	  	     $generated=1;
     	    }
     	    else $debug.="<font color=#bb0000><b>Fehler: Gruppe für Sensor $mySensor konnte nicht gefunden werden </font><br>";
	 	  }
	 }
	 
	 //if ($actInactive==1) $foundInavtive=1;
	 
	 $actTag = str_replace("%STATUS%",$status, $actTag);
	 $actTag = str_replace("%DIAGRAM%",$mySensor, $actTag);
	 $actTag = str_replace("%ROOM%",$obj->roomName, $actTag);
	 
	 $actTag = str_replace("%SENSOR_PARAMS%",paramsToUrl("action=addSensor&id=$obj->id"), $actTag);
	 $actTag = str_replace("%AKTOR_PARAMS%",paramsToUrl("action=addAktor&id=$obj->id"), $actTag);
	 $actTag = str_replace("%PUMP_PARAMS%",paramsToUrl("action=addPump&id=$obj->id"), $actTag);
	 
	 $elements.=$actTag;
}

$html = str_replace("%ELEMENTS%",$elements,$html);

/*if ($foundInavtive==1) chooseTag("%OPT_GENERATE_RULES%",$html);
else removeTag("%OPT_GENERATE_RULES%",$html);
*/

if ($generated==1) $debug.="<font color=#bb0000><b>Achtung:</b> Nach dem Generieren neuer Regeln, müssen diese einmalig zu den Controllern übermittelt werden. Dies bitte unter System -> Controller: Regeln übermitteln durchführen oder <a href='/homeserver/editRules.php?action=submitRules' target='_blank'>hier klicken.</a></font><br>";
if ($debut!="") $debug="<br>".$debug."<br>";

$html = str_replace("%DEBUG%",$debug,$html);
$showReductionMode = $_SESSION["actReductionMode"];
if ($showReductionMode=="") $showReductionMode="-";
$html = str_replace("%ACT_REDUCTION_MODE%",$showReductionMode,$html);

die($html);


function getTimeOptionsHour($myValue)
{
  //$myOptions = "<option value='31'>Aus";
  //$myOptions .= "<option value='25'>Tagsüber";
  //$myOptions .= "<option value='26'>Nachts";

  for($hour = 0; $hour < 24; $hour++)
  {
    $myHour = $hour;
    if (strlen($myHour) == 1) $myHour = "0" . $myHour;
    $myOptions .= "<option value='$hour'>$myHour Uhr";
  }
      
  if ($myValue == "31") $myOptions = str_replace("value='31'","value='31' selected",$myOptions);
  else if ($myValue == "25") $myOptions = str_replace("value='25'","value='25' selected",$myOptions);
  else if ($myValue == "26") $myOptions = str_replace("value='26'","value='26' selected",$myOptions);

  $myOptions = str_replace("value='$myValue'","value='$myValue' selected",$myOptions);
  	
	return $myOptions;
}

function renewControl($id)
{
  deleteGeneratedBaseRule($id);
  deleteGeneratedDiagram($id, false);
  checkAutoGenerateDiagram($id);
}

function deleteGeneratedBaseRule($id) 
{
  $erg = QUERY("select sensor, relay, basicRuleId from heating where id='$id' limit 1");
  $obj=MYSQLi_FETCH_OBJECT($erg);
  if (($obj->sensor==0 || $obj->relay==0) && $obj->basicRuleId>0)
  {
	  deleteBaseRule($obj->basicRuleId);
	  QUERY("UPDATE heating set basicRuleId='0' where id='$id' limit 1");
  }
}


function deleteGeneratedDiagram($heatingId, $withTrigger=true)
{
	$erg = QUERY("select diagram from heating where id='$heatingId' limit 1");
	if ($obj=MYSQLi_FETCH_OBJECT($erg))
	{
	  if ($obj->diagram>0)
	  {
    	  QUERY("delete from graphs where id='$obj->diagram' and timeMode='days' and timeParam1='7' and timeParam2='' and width='' and height='700' and distValue='' and distType='' and theme='' and heightMode='fixed'");
  	      QUERY("update heating set diagram='0' where id='$heatingId' limit 1");
	  
	      if ($withTrigger) triggerTreeUpdate();
  	  }
  }
}

function checkAutoGenerateDiagram($heatingId)
{
	$erg = QUERY("select diagram,sensor,relay from heating where id='$heatingId' limit 1");
	if ($obj=MYSQLi_FETCH_OBJECT($erg))
	{
		if ($obj->diagram==0 && $obj->sensor>0 && $obj->relay>0)
		{
			  header("Location: heatingControl.php?action=createDiagram&id=$heatingId&diagramType=1");
			  exit;
		}
  }
}

?>  