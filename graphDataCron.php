<?php
if ($_SERVER["DOCUMENT_ROOT"] == "") $_SERVER["DOCUMENT_ROOT"] = "../";
include("/var/www/homeserver/include/all.php");
include("/var/www/homeserver/generateGraphData.php");

require __DIR__ . '/apiv1/vendor/autoload.php';

use InfluxDB2\Client;
use InfluxDB2\Point;
use InfluxDB2\Model\WritePrecision;

global $influxClient;
global $influxLastId;

generateGraphData("", true);
generateInfluxData();

function generateInfluxData()
{
	global $influxClient;
	global $influxLastId;

	if (getInfluxConnection())
	{
		$erg = QUERY("select featureInstanceId from influxSignals where influxId='-1' limit 1");
		if ($row = mysqli_fetch_ROW($erg)) $lastId = $row[0];
		else
		{
			$lastId = 0;
			QUERY("INSERT into influxSignals (influxId, featureInstanceId) values('-1','0')");
		}

		$where = "1=2 ";
		$sql = "SELECT influxSignals.id,
						influxSignals.title as signalTitle,
				      featureInstances.objectId,
					   featureFunctions.functionId,
					   influx.id as graphId,
					   influx.title as graphTitle,
					   influxSignalEvents.fkt,
					   influxSignalEvents.id as eventId
					   FROM influxSignals
						join influxSignalEvents on (influxSignals.id=influxSignalEvents.influxSignalsId)
						join influx on (influx.id=influxSignals.influxId)
						join featureInstances on ( featureInstances.id=influxSignalEvents.featureInstanceId  )
						join featureFunctions on ( featureFunctions.id=influxSignalEvents.functionId  )
						";
		$erg = QUERY($sql);
		//die($sql);
		if ($debug) echo $sql . "<hr>";
		while ($obj = mysqli_fetch_object($erg))
		{
			$signals[$obj->graphId][$obj->id][$obj->eventId] = $obj;
			//echo $obj->graphId." - ".$obj->id." - ".$obj->eventId."<br>";
			$newWhere = " or (senderObj='$obj->objectId' and fktId='$obj->functionId')";
			if (strpos($where, $newWhere) === FALSE) $where .= $newWhere;
		}

		if (count($signals) == 0) return;

		$myLastId = $lastId;
		$sql = "select functionData,senderObj,time,fktId,id from udpCommandLog where ($where) and id>'$lastId' order by id";
		//die($sql);
		if ($debug) echo $sql . "<hr>";
		$erg = QUERY($sql);
		while ($obj = mysqli_fetch_OBJECT($erg))
		{
			$myLastId = $obj->id;

			foreach ($signals as $graphId => $arr)
			{
				foreach ($arr as $signalId => $arr2)
				{
					foreach ($arr2 as $eventId => $sigObj)
					{
						if ($sigObj->objectId == $obj->senderObj && $sigObj->functionId == $obj->fktId)
						{
							$fktData = unserialize($obj->functionData);
							$fktParams = $fktData->paramData;
							$actFkt = $sigObj->fkt;
							foreach ($fktParams as $actParam)
							{
								if (strpos($actFkt, $actParam->name) !== FALSE) $actFkt = str_replace($actParam->name, $actParam->dataValue, $actFkt);
							}
							try
							{
								$value = matheval($actFkt);

								//echo "Graph: ".$graphId.", Signal: $signalId".", time = ".$obj->time.", value = ".$val."<br>";

								$objectId = $sigObj->objectId;

								if (!isset($senderData[$objectId]))
								{
									$erg2 = QUERY("select featureInstances.name as featureName,
							                      featureClasses.name as className,
												  rooms.name as roomName
												  from featureInstances
												  join featureClasses on (featureClasses.id=featureInstances.featureClassesId)
												  left join roomFeatures on (roomFeatures.featureInstanceId=featureInstances.id)
												  left join rooms on (rooms.ID=roomFeatures.roomId)
												  where objectId='$objectId' limit 1");
									if ($obj2 = MYSQLi_FETCH_OBJECT($erg2))
									{
										//echo "SET ".$objectId."<br>";
										$senderData[$objectId] = $obj2;
									}
								}

								if (isset($senderData[$objectId]))
								{
									$obj2 = $senderData[$objectId];

									//data in Point structure
									$point = InfluxDB2\Point::measurement($sigObj->graphTitle)
										->addTag("name", $sigObj->signalTitle)
										->addTag("room", $obj2->roomName)
										->addTag("class", $obj2->className)
										->addTag("objectId", $objectId)
										->addField("value", $value)
										->time($obj->time);
									$influxClient->write($point);
								}
							} catch (Exception $ex)
							{
								// just ignore for now
								if ($debug) echo $ex->getMessage() . "<hr>";
							}
						}
					}
				}
			}
		}

		if ($myLastId > $lastId) QUERY("UPDATE influxSignals set featureInstanceId='$myLastId' where influxId='-1' limit 1");
	}
}

function getInfluxConnection()
{
	global $influxClient;
	global $influxLastId;

	$erg = QUERY("select * from basicconfig where paramKey like 'influx%' limit 5");
	while ($obj = MYSQLi_FETCH_OBJECT($erg))
	{
		$key = $obj->paramKey;
		$$key = $obj->paramValue;
	}

	if ($influxBucket != "" && $influxOrg != "" && strlen($influxToken) > 5 && $influxServer != "")
	{
		try
		{
			$client = new InfluxDB2\Client([
				"url" => $influxServer, // url and port of your instance
				"token" => $influxToken,
				"bucket" => $influxBucket,
				"org" => $influxOrg,
				"precision" => InfluxDB2\Model\WritePrecision::S,
				"timeout" => 2,
			]);
			$influxClient = $client->createWriteApi();
			return true;
		} catch (\Exception $ex)
		{
			header('Access-Control-Allow-Origin: *');
			header('Content-Type: application/json');
			http_response_code(404);
			echo json_encode(array(
				'message' => $ex->getMessage(),
				'code' => $ex->getCode(),
				'trace' => $ex->getFile() . ':' . $ex->getLine()
			));
		}
	}
	return false;
}
