<?php
require("include/all.php");

session_start();

$tables=array("controller","featureInstances","groups","rooms","roomFeatures");


$myLastUpdate=$_SESSION["lastTreeUpdate"];
$newMax=-1;

$erg = QUERY("SELECT forceUpdate from treeUpdateHelper where forceUpdate='1' limit 1");
if ($row=mysqli_fetch_ROW($erg))
{
	query("truncate table treeUpdateHelper");
	die("1");
}


foreach($tables as $actTable)
{
  $erg = QUERY("SELECT max(lastChange) from $actTable where lastChange>'$myLastUpdate' order by lastChange desc limit 1");
  if ($row=mysqli_fetch_ROW($erg)) if ($row[0]>$newMax) $newMax= $row[0];
}

if ($newMax!=-1)
{
	 $_SESSION["lastTreeUpdate"]=$newMax;
	 echo "1";
	 flush();
	 ob_flush();
	 exit;
}


$erg = QUERY("select objectId from controller where online=1");
while($row=mysqli_fetch_row($erg))
{
	$myDeviceId= $row[0]>>16;
	$myControllers[$myDeviceId]=$row[0];
}

$min = time()-300;
$erg = QUERY("select distinct(senderObj>>16) from lastReceived where time>$min");
while($row=mysqli_fetch_row($erg))
{
	$deviceId=$row[0];
	
	$_SESSION["pingPending".$deviceId]=0;
	unset($myControllers[$deviceId]);
}

foreach($myControllers as $deviceId => $objectId)
{
	if ($_SESSION["pingPending".$deviceId]==1)
	{
		$_SESSION["pingPending".$deviceId]=0;
		QUERY("update controller set online='0' where objectId='$objectId' limit 1");
	}
	else
	{
		callObjectMethodByName($objectId, "ping");
		$_SESSION["pingPending".$deviceId]=1;
	}
	break;
}

die("0");

?>

