<?php
require($_SERVER["DOCUMENT_ROOT"]."/homeserver/include/all.php");

if ($action=="status")
{
	if (changesSince($lastStatusId))
	{
    callInstanceMethodByName($featureInstanceId, "getStatus");
    $result = waitForInstanceResultByName($featureInstanceId, 2, "Status", $lastLogId);
    updateLastLogId();
	$centi = getResultDataValueByName("centiCelsius", $result);
	if (strlen($centi)==1) $centi="0".$centi;
    die($lastLogId."#".getResultDataValueByName("celsius", $result).".".$centi);
  }
  exit;
}

$erg = QUERY("select featureClassesId from featureInstances where id='$featureInstanceId' limit 1");
if ($obj=mysqli_fetch_OBJECT($erg))
{
  $featureClassesId = $obj->featureClassesId;
}
else die("FEHLER! Ungültige featureInstanceId $featureInstanceId");

$html = loadTemplate("tempControl_design.html");
$html = str_replace("%FEATURE_INSTANCE_ID%",$featureInstanceId,$html);

$html = str_replace("%INITIAL_STATUS_ID%",updateLastLogId(),$html);


callInstanceMethodByName($featureInstanceId, "getStatus");
$result = waitForInstanceResultByName($featureInstanceId, 1, "Status", $lastLogId, "funtionDataParams", 0);
if ($result[1]->dataValue<10) $result[1]->dataValue="0".$result[1]->dataValue;
$status=$result[0]->dataValue.".".$result[1]->dataValue;
$html = str_replace("%TEMP%",$status,$html);

echo $html;

?>