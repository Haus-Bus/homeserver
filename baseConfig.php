<?php
include ($_SERVER["DOCUMENT_ROOT"] . "/homeserver/include/all.php");

if ($submitted != "" && $configTemplate>0)
{
	echo "<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Konfiguration wird durchgeführt.... Bitte warten...";
	flushIt();

	$erg = QUERY("select objectId from controller where id='$id' limit 1");
	if ($obj=MYSQLi_FETCH_OBJECT($erg))
	{
	  // Beim Reset wird der Controller einmal neu benannt, weil sonst die automatische Konfguration in Loxone nicht klappt
      QUERY("update controller set renamed='0' where id='$id' limit 1");

	  $objectId=$obj->objectId;
      $controllerId = getControllerId($objectId);

 	  baseConfigureModules($objectId, $configTemplate);
 	  baseConfigureIoPorts($objectId, $configTemplate, true);
      baseConfigureWaitForReset($objectId);
      baseConfigureInputsAndOutputs($objectId, $configTemplate, true);

     QUERY("UPDATE featureinstances set checked='0'");
     updateControllerStatus();
     QUERY("delete from featureInstances where controllerId='$controllerId' and checked='0'");
     
     echo "FERTIG!<br><br>";
	}
	else die("FEHLER: controller nicht gefunden");
}

if ($action=="special1")
{
	$erg = QUERY("select objectId from controller where id='$id' limit 1");
	$obj=MYSQLi_FETCH_OBJECT($erg);
	$controllerObjectId=$obj->objectId;
	
	generateLedTasterRule($controllerObjectId, $id, 65, 17);
	generateLedTasterRule($controllerObjectId, $id, 66, 18);
	generateLedTasterRule($controllerObjectId, $id, 67, 19);
	generateLedTasterRule($controllerObjectId, $id, 68, 20);
	generateLedTasterRule($controllerObjectId, $id, 69, 21);
	generateLedTasterRule($controllerObjectId, $id, 70, 22);

	echo "OK <br><br><br>";
}

if ($action=="special2")
{
	echo "<br><br>&nbsp;&nbsp;&nbsp;&nbsp;Konfiguration Loxone Rollomodul wird durchgeführt.... Bitte warten...";

	$erg = QUERY("select objectId from controller where controller.id='$id' limit 1");
	$row = MYSQLI_FETCH_ROW($erg);
	$controllerObjectId=$row[0];
	
	$erg = QUERY("select featureInstances.id,featureInstances.objectId from featureInstances join controller on (controller.id = featureInstances.controllerId) where featureClassesId='8' and controller.id='$id' order by featureInstances.objectId");
	while($row = MYSQLI_FETCH_ROW($erg))
	{
		if (getInstanceId($row[0])==210) continue;
		$myInstances[]=$row[0];
	}
	
	for ($i=0;$i<16;$i++)
	{
	  $firstInstance = $myInstances[$i];
	  if ($i%2==0) $secondInstance = $myInstances[$i]+1;
	  else  $secondInstance = $myInstances[$i]-1;
	  
	  $sql = "select groups.id from groups join groupFeatures on (groupFeatures.groupId = groups.id) where single=1 and featureInstanceId=$firstInstance limit 1";
	  echo $sql."<br>";
	  $erg2 = QUERY($sql);
      $row2=MYSQLI_FETCH_ROW($erg2);
	  $groupId=$row2[0];

	  QUERY("delete from rules where groupId='$groupId'");

      $sql = "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId) 
                         values('$groupId','7','31','255','7','31','255','0','0')";
	  //echo $sql."<br>";
	  QUERY($sql);
      $ruleId = query_insert_id();
	  
	  $sql = "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
 	                                values('$ruleId','$secondInstance','62')";
      //echo $sql."<br>";
      QUERY($sql);
	  $newRuleSignalId = query_insert_id();

      QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
	                                values('$newRuleSignalId','470','0')");

      /*QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
	                                values('$newRuleSignalId','265','0')");

      QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
	                                values('$newRuleSignalId','564','0')");
									*/

      $sql = "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
	                           values('$ruleId','$firstInstance','61')";
							   
      //echo $sql."<br>";
      QUERY($sql);
	  
      $newRuleActionId = query_insert_id();

      $sql = "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
                                   values('$newRuleActionId','563','0')";
      //echo $sql."<br>";
								   
      QUERY($sql);
	}
	
	die("<script>location = 'editRules.php?action=submitRules&singleControllerId=$controllerObjectId'</script>");
	echo "<br><br>ok";
}

setupTreeAndContent("baseConfig_design.html", $message);

$html = str_replace("%ID%", $id, $html);

show();


function generateLedTasterRule($controllerObjectId, $id, $ledInstance, $tasterInstance)
{
	$ledObject = new stdClass();
	$tasterObject = new stdClass();
	
	$deviceId = getDeviceId($controllerObjectId);
	$ledObject->objectId = getObjectId($deviceId, 21, $ledInstance);
	$tasterObject->objectId = getObjectId($deviceId, 16, $tasterInstance);

	$erg = QUERY("select id,objectId from featureInstances where objectId='".$ledObject->objectId."' or objectId='".$tasterObject->objectId."' limit 2");
	while ($obj=MYSQLi_FETCH_OBJECT($erg))
	{
		if ($obj->objectId == $ledObject->objectId) $ledObject->id = $obj->id;
		if ($obj->objectId == $tasterObject->objectId) $tasterObject->id = $obj->id;
	}
	
	//Ansschalten
	$erg = QUERY("select groups.id from groups join groupFeatures on (groupFeatures.groupId = groups.id) where featureInstanceId='".$ledObject->id."' and single=1 limit 1");
	$obj=MYSQLi_FETCH_OBJECT($erg);
	$ledObject->groupId=$obj->id;
			
	QUERY ("INSERT into rules (groupId,activationStateId,resultingStateId,startDay,startHour,startMinute,endDay,endHour,endMinute,signalType,baseRule,`generated`)
                        values('".$ledObject->groupId."','0','0','7','31','255','7','31','255','covered','0','0')" );
    $newRuleId = query_insert_id ();
          
    QUERY( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId) 
	                          values('$newRuleId','".$tasterObject->id."','43')" );
    $newSignalId = query_insert_id ();
          
    QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
	                               values('$newSignalId','567','255')");
        
    QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) 
	                           values('$newRuleId','".$ledObject->id."','101')");
    $newActionId = query_insert_id ();
            
    QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
	                                values('$newActionId','165','100')" );

    QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
	                                values('$newActionId','166','0')" );

    QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
	                                values('$newActionId','561','0')" );
									
    //Ausschalten									
	QUERY ("INSERT into rules (groupId,activationStateId,resultingStateId,startDay,startHour,startMinute,endDay,endHour,endMinute,signalType,baseRule,`generated`)
                        values('".$ledObject->groupId."','0','0','7','31','255','7','31','255','covered','0','0')" );
    $newRuleId = query_insert_id ();
          
    QUERY( "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId) 
	                          values('$newRuleId','".$tasterObject->id."','44')" );
    $newSignalId = query_insert_id ();
          
    QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
	                               values('$newSignalId','567','255')");
        
    QUERY ( "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) 
	                           values('$newRuleId','".$ledObject->id."','135')");
    $newActionId = query_insert_id ();
            
    QUERY ( "INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
	                                values('$newActionId','562','0')" );
}

?>