<?php
include ($_SERVER["DOCUMENT_ROOT"] . "/homeserver/include/all.php");

if ($action == "readRuleState")
{
	$erg = QUERY("select groupController, groupIndex from groups where id='$groupId' limit 1");
	$obj = MYSQLi_FETCH_OBJECT($erg);
	if ($obj->groupController==0) echo "Bisher auf keinen Controller übertragen<br>";
	else
	{
	  $data["index"] = $obj->groupIndex;
	  
	  $erg = QUERY("select objectId from controller where id='$obj->groupController' limit 1");
	  if ($obj = MYSQLi_FETCH_OBJECT($erg))
	  {
		  $result = callObjectMethodByNameAndRecover($obj->objectId, "getRuleState", $data, "RuleState", 3, 2);
		  $state = $result[1]->dataValue;

	      $erg = QUERY("select id,name,value from groupStates where groupId='$groupId' and value='$state' limit 1");
	      if ($obj = MYSQLi_FETCH_OBJECT($erg))
		  {
			  $state=$obj->name." (".$obj->value.")";
			  $activeStateId=$obj->id;
		  }
		  else $state="Unbekannt -> $state";
		  
		  echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;State: ".$state."<br>";
	  }
	  else echo "Controller nicht gefunden!<br>";
	}
}
else if($action=="setRuleState")
{
	$erg = QUERY("select groupController, groupIndex from groups where id='$groupId' limit 1");
    $obj = MYSQLi_FETCH_OBJECT($erg);
	if ($obj->groupController==0) echo "Bisher auf keinen Controller übertragen<br>";
	else
	{
	  $data["index"] = $obj->groupIndex;

      $erg2 = QUERY("select value from groupStates where id='$setState' limit 1");
      if ($obj2 = MYSQLi_FETCH_OBJECT($erg2)) $data["state"] = $obj2->value;
	  
  	  $erg = QUERY("select objectId from controller where id='$obj->groupController' limit 1");
	  if ($obj = MYSQLi_FETCH_OBJECT($erg))
	  {
	     callObjectMethodByName($obj->objectId, "setRuleState", $data);
	     header("Location: editRules.php?action=readRuleState&groupId=$groupId");
	     exit;
	  }
	}
}
else if ($action == "readRules")
{
  $ruleVersion=1;
  if (isFirmwareVersionGreaterOrEqual(1, 30, $objectId)) $ruleVersion=2;

  //callObjectMethodByName($objectId, "getConfiguration");
  //$result = waitForObjectResultByName($objectId, 5, "Configuration", $lastLogId);
  
  $result = callObjectMethodByNameAndRecover($objectId, "getConfiguration", "", "Configuration", 3, 2);

  $blockSize = getResultDataValueByName("dataBlockSize", $result);
  
  $rulesBytes = array();
  $rulesBytesPos = 0;
  while ( $rulesBytesPos < 8*1024 ) // 8k is max. for AR8
  {
    $data["offset"] = $rulesBytesPos;
    $data["length"] = $blockSize;
    //callObjectMethodByName($objectId, "readRules", $data);
    //$result = waitForObjectResultByName($objectId, 5, "RulesData", $lastLogId);
	
	$result = callObjectMethodByNameAndRecover($objectId, "readRules", $data, "RulesData", 3, 2);
    
    $parts = explode(",", getResultDataValueByName("data", $result));
    foreach ( (array)$parts as $value )
    {
      $rulesBytes[$rulesBytesPos++] = $value;
      //echo "0x".decHex($value).", ";
    }

    $regeln = "";
	
    if (parseGroup($rulesBytes, $rulesBytesPos, $regeln, $ruleVersion)) break;
  }
  die("<head><link rel='StyleSheet' href='css/main.css' type='text/css'/></head><body><table width=95% align=center><tr><td><div style='margin-right:16px' class='contentWrap'><br>" . $regeln."</div></td></tr></table><br><br></body></html>");
}
else if ($action == "checkRules")
{
  echo "<br><div style='padding-left:20px'>";
  echo "<u>Generiere und prüfe Regeln</u><br>";
  echo "<div style='width:98%;height:500px;font-face:verdana;font-size:11px' id='updateArea'></div>";
  flushIt();
  
  generateAndCheckRules();
  exit();
}
else if ($action == "addTimeSignal" || $action == "addTimeSignalSunrise" || $action == "addTimeSignalSunset")
{
  if ($submitted!=1)
  {
    setupTreeAndContent("chooseTimeSignal_design.html", $message);
    $html = str_replace("%GROUP_ID%", $groupId, $html);
	if ($logicalGroupMode==1)
	{
	  $html = str_replace("editBaseConfig.php?dummy=back", "editLogicalSignals.php?", $html);
	  $html = str_replace("editBaseConfig.php", "editRules.php", $html);
	  $html = str_replace("%LOGICAL_GROUP_MODE%", "1", $html);
	  $html = str_replace("%LOGICAL_STATE_ID%", $logicalStateId, $html);
	}
    else $html = str_replace("editBaseConfig.php", "editRules.php", $html);
    show();
    exit();
  }
  else
  {
	if ($logicalGroupMode==1)
	{
		$activationStateId=$logicalStateId;
		$controllerInstanceId=0;
	}
	else
	{
      $activationStateId="0";
      $erg = QUERY("select featureInstanceId from groupFeatures where groupId='$groupId' order by id limit 1");
      if ($row = mysqli_fetch_ROW($erg)) $firstAktor = $row[0];
      else die("Kein Aktor gefunden in Gruppe $groupId");
  
      $erg = QUERY("select controllerId from featureInstances where id='$firstAktor' limit 1");
      if ($row = mysqli_fetch_ROW($erg)) $controllerId = $row[0];
      else die("Controller zu featureId $firstAktor nicht gefunden");
  
      $erg = QUERY("select id from featureInstances where controllerId='$controllerId' and featureClassesId='$CONTROLLER_CLASSES_ID' limit 1");
      if ($row = mysqli_fetch_ROW($erg)) $controllerInstanceId = $row[0];
      else die("controllerInstanceId zu controllerId $controllerId nicht gefunden");
	}
  
    QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId) 
                        values('$groupId','7','31','255','7','31','255','$activationStateId','0')");
    $ruleId = query_insert_id();
  
    if ($action == "addTimeSignalSunrise") $myEvent = getClassesIdFunctionsIdByName($CONTROLLER_CLASSES_ID, "evDay");
    else if ($action == "addTimeSignalSunset") $myEvent = getClassesIdFunctionsIdByName($CONTROLLER_CLASSES_ID, "evNight");
    else $myEvent = getClassesIdFunctionsIdByName($CONTROLLER_CLASSES_ID, "evTime");
  
    QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                              values('$ruleId','$controllerInstanceId','$myEvent')");
    $ruleSignalId = query_insert_id();
  
    if ($action == "addTimeSignalSunrise" || $action == "addTimeSignalSunset")
    {
		if ($logicalGroupMode==1) header("Location: editLogicalSignals.php?groupId=$groupId");
		else header("Location: editRules.php?groupId=$groupId");
    	exit;
    }
    else
    {
      $erg = QUERY("select id from featureFunctionParams where featureFunctionId='$evTimeFunctionId' and name='weekTime' limit 1");
      $row = mysqli_fetch_ROW($erg);
      $paramsId = $row[0];
      QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                 values('$ruleSignalId','$paramsId','0')");
 
      header("Location: editRules.php?action=editSignalParams&groupId=$groupId&ruleId=$ruleId&ruleSignalId=$ruleSignalId&logicalGroupMode=$logicalGroupMode");
      exit;
    }
  }
}
else if ($action == "submitRules")
{
  global $raspberryFeatureInstanceId;
  global $systemVariableControllerId;
  global $systemVariableControllerObjectId;
  
  $raspberryFeatureInstanceId = getFirstSoftwareController()->featureInstanceId;  
  $systemVariableControllerId = getFirstNormalOnlineController()->controllerId;
  $systemVariableControllerObjectId = getFirstNormalOnlineController()->objectId;
  
  
  echo "<br><div style='padding-left:20px'><table width=98%><tr><td colspan=3 valign=top><font face=verdana size=2>";
  echo "<u>Generiere und püfe Regeln</u> <br>";
  echo "<div style='width:98%;height:170px;overflow-x:hidden;overflow-y:scroll;font-face:verdana;font-size:11px' id='updateArea'></div></td></tr>";
  echo "<tr><td valign=top width=50%><font face=verdana size=2>";
  
  flushIt();
  generateAndCheckRules();
  echo "<u>Konfiguriere Regeln</u> <div id='controllerProgress' style='font-face:verdana;font-size:11px'></div> <br><div style='font-face:verdana;font-size:11px'>";
  flushIt();
  
  // Gruppenstates durchnummerierte Values geben
  $erg = QUERY("select distinct(groupId) from groupstates");
  while ( $row = mysqli_fetch_ROW($erg) )
  {
    $i = 0;
    $erg2 = QUERY("select id from groupstates where groupId='$row[0]' order by id");
    while ( $row2 = mysqli_fetch_ROW($erg2) )
    {
      QUERY("UPDATE groupstates set value='$i' where id='$row2[0]' limit 1");
      $i++;
    }
  }
  
  $pcInstances = array();
  $erg = QUERY("select featureInstances.id from featureInstances join controller on (featureInstances.controllerId=controller.id) where size='999'");
  while ( $row = mysqli_fetch_ROW($erg) )
  {
    $pcInstances[$row[0]]=1;
  }

  // Zu allen Features von online Controllern Gruppenregeln suchen
  $filterDone = "";
  $fullGroups="";
  if ($singleControllerId!="")
  {
	  $pcInstances = array();
	  $andSingleController=" and objectId='$singleControllerId'";
  }
  
  $erg = QUERY("select id,objectId,name,firmwareId,size from controller where bootloader!='1' and online='1' $andSingleController");
  while ( $obj = mysqli_fetch_OBJECT($erg) )
  {
	echo "<script>document.getElementById('controllerProgress').innerHTML='$obj->name';</script>";
	flushIt();
  	//echo "Controller ".$obj->name."<br>";
    // PC-Server überspringen
    if ($obj->size == "999") continue;

    $start=microtime(true);
	
	$controllerId=$obj->id;
	
	$ruleVersion=1;
    if (isFirmwareVersionGreaterOrEqual(1, 30, $obj->objectId)) $ruleVersion=2;
      
      // Position 0 für Anzahl der Gruppen freihalten
    $dataPos = 1;
    $nrGroups = 0;
    $objectId = $obj->objectId;
    
    $groups = "";
    $controllerIsFull=false;
    
    $erg2 = QUERY("select id,featureClassesId,name from featureInstances where controllerId='$controllerId'");
    while ( $obj2 = mysqli_fetch_OBJECT($erg2) )
    {
	  //echo "Controller: $controllerId Instanz: ".$obj2->id." ".$obj2->name." - ".(microtime(true)-$start)."<br>";
      $erg3 = QUERY("select groupId, forcedController from groupFeatures join groups on (groups.id=groupFeatures.groupId) where featureInstanceId='$obj2->id'");
      while ( $obj3 = mysqli_fetch_OBJECT($erg3) )
      {
		$forcedController=$obj3->forcedController;
		
        // Prüfen obs das erste bzw einzige Feature in dieser Gruppe ist, oder ob unser aktueller Controller als Host vorgegeen wurde. Ansonsten ist wer anderes zuständig
		if ($forcedController>0)
		{
		  if ($controllerId!=$forcedController) continue; // Hier wurde ein Controller als Host vorgegeben, der nicht dem aktuellen entspricht
		}
		else
		{
		  $foundOtherResponsible=false;
          $erg4 = QUERY("select featureInstanceId from groupFeatures where groupId='$obj3->groupId' order by id limit 1");
          while ($row4 = mysqli_fetch_ROW($erg4))
		  {
			if ($pcInstances[$row4[0]]==1) continue;
			
			// Feature ist nicht erstes Feature der Gruppe, weshalb ein anderer Controller zuständig ist
            if ($row4[0] != $obj2->id) $foundOtherResponsible=true;
			break;
		  }
		  
		  if ($foundOtherResponsible) continue;
		}
		
		//echo "Gruppe: ".$obj3->groupId."<br>";
		
		// Gruppe ist für aktuellen controller

        // Auf Signal/Sondergruppen prüfen
        $erg5 = QUERY("select subOf,groupType,active from groups where id='$obj3->groupId' limit 1");
        $row5 = mysqli_fetch_ROW($erg5);
        if ($row5[1] != "") continue; // Hier wird die dazu generierte Gruppe konfiguriert
        if ($row5[2] ==0) continue; // Inaktive Gruppe überspringen

        if ($row5[0] > 0) $obj3->groupId = $row5[0]; // Zu Subgruppen wird die Vatergruppe generiert

        if ($filterDone[$obj3->groupId] == 1) continue; // Groupe bereits erledigt
        $filterDone[$obj3->groupId] = 1;
        
		$workGroupId=$obj3->groupId;
        
		$errorCounter=0;
		$workedOnWorkGroup=false;
		
	    if (!$controllerIsFull && $fullGroups!="" && $singleControllerId=="") echo "Verarbeite Gruppen vom vollen Vorgänger: $fullGroups <br>";

		while(!$workedOnWorkGroup && $errorCounter<30)
		{
		  $errorCounter++;
		  
		  if (!$controllerIsFull && $fullGroups!="")
		  {
			  $tmpPos = strpos($fullGroups,",");
			  if ($tmpPos===FALSE)
			  {
				  $workGroupId = $fullGroups;
				  $fullGroups="";
			  }
			  else
			  {
				  $workGroupId = substr($fullGroups,0,$tmpPos);
			      $fullGroups=substr($fullGroups,$tmpPos+1);
			  }
			  
			  //echo $workGroupId." - ".$fullGroups."<br>";
		  }
		  else
		  {
			  $workGroupId=$obj3->groupId;
			  $workedOnWorkGroup = true;
		  }
		  
          $erg4 = QUERY("select count(*) from rules where groupId='$workGroupId'");
          $row4 = mysqli_fetch_ROW($erg4);
          if ($row4[0] > 0)
          {
  		    if ($controllerIsFull)
		    {
  			  if ($fullGroups!="") $fullGroups.=",";
			  $fullGroups.=$workGroupId;
		    }
            else		  
		    {
              // Prüfen, ob die Regel mindestens einen Trigger und eine Aktion hat
		      $dataPosBefore = $dataPos;
              $groupHasValidRules = addGroupRuleData($workGroupId, $ruleVersion, $objectId);
              if ($groupHasValidRules)
              {
				//echo $workGroupId."<br>";
    			if ($obj->firmwareId == 3 && $dataPos>800)
			    {
  				  echo "Controller voll <br>";
				  $controllerIsFull=true;
				  if ($fullGroups!="") $fullGroups.=",";
				  $fullGroups.=$workGroupId;
				  $dataPos = $dataPosBefore;
			    }
				else
				{
				  QUERY("update groups set groupController='$controllerId', groupIndex='$nrGroups' where id='$workGroupId' limit 1");
                  $nrGroups++;
                  $groups .= $workGroupId. ",";
				}
			  }
			}
          }
        }
      }
    }
    
    // Der erste AR8 Controller muss noch die PC Regeln mit hosten
    if ($obj->firmwareId == 1 && $pcServerDone == 0 && $singleControllerId=="")
    {
      $pcServerDone = 1;
      
      $erg6 = QUERY("select groups.id from groups join groupFeatures on (groupFeatures.groupId=groups.id) join featureInstances on (featureInstances.id = groupFeatures.featureInstanceId) join controller on (controller.id=featureInstances.controllerId) where controller.size=999 and single='1' order by groups.id");
      while ($row6 = mysqli_fetch_ROW($erg6))
      {
        $pcServerGroupId=$row6[0];
        $groupHasValidRules = addGroupRuleData($pcServerGroupId, $ruleVersion, $obj->objectId);
		
        if ($groupHasValidRules)
        {
		  QUERY("update groups set groupController='$controllerId', groupIndex='$nrGroups' where id='$pcServerGroupId' limit 1");
          $nrGroups++;
          $groups .= $pcServerGroupId . ",";
        }
      }
	}

    // Der erste normale Controller muss alle setSystemVariable übernehmen (bzw. alle Gruppen, die keinen normalen Aktor als Feature haben, sondern nur SwController und nicht single sind)
	if ($controllerId==$systemVariableControllerId)
	{
	  $groupsWithOnlySoftwareControllerMembers = array();
      $erg3 =  QUERY("select groups.id, controller.size from groups left join groupfeatures on (groupfeatures.groupid = groups.id) left join featureinstances on (featureinstances.id = groupfeatures.featureInstanceId) left join controller on (controller.id=featureinstances.controllerId) where groups.single!=1 and subOf=0 and groupType=''");
      while ($obj3 = mysqli_fetch_object($erg3))
      {
	     if ($groupsWithOnlySoftwareControllerMembers[$obj3->id]==1) continue;
	     $groupsWithOnlySoftwareControllerMembers[$obj3->id]=0;
	     if ($obj3->size!=null && $obj3->size!=999) $groupsWithOnlySoftwareControllerMembers[$obj3->id]=1;
      }
	  
	  foreach((array)$groupsWithOnlySoftwareControllerMembers as $id=>$value)
	  {
		  if ($value==0)
		  {
             $groupHasValidRules = addGroupRuleData($id, $ruleVersion, $obj->objectId);
		
             if ($groupHasValidRules)
             {
		        QUERY("update groups set groupController='$controllerId', groupIndex='$nrGroups' where id='$id' limit 1");
                $nrGroups++;
                $groups .= $id . ",";
			 }
          }

	  }
    }
    
    $rulesData[$dataPos++] = 0; // 0 Terminierung
    

    //if ($nrGroups>0)
    {
      $rulesData[0] = $nrGroups;
      ksort($rulesData);
      
      $dataChanged = checkAndTraceDataDifferences($groups, $controllerId, $rulesData, $dataPos);
      if ($dataChanged || $nocache == 1 || $singleControllerId!="")
      {
        echo "<nobr> $nrGroups Regeln an Controller " . $obj->name . " ($dataPos Bytes)<br>";
        flushIt();
      }
      else
      {
        //liveOut("<i>Ungeändert $nrGroups Regeln an Controller ".$obj->name."</i>");
        continue;
      }
      
      $result = callObjectMethodByNameAndRecover($objectId, "getConfiguration","","Configuration",3,5,0);
      if ($result==-1)
      {
      	echo "<b>Controller antwortet nicht... Überspringe Regeln....!</b><br>";
      	continue;
      }
      
      $blockSize = getResultDataValueByName("dataBlockSize", $result);
      
      $memoryStatusOk = getFunctionParamEnumValueByName($objectId, "MemoryStatus", "status", "OK");
      unset($fileBuffer);
      $ready = 0;
      $firstWriteId = - 1;
      while ( 1 )
      {
        $rest = $dataPos - $ready;
        if ($rest >= $blockSize - 1) $actBlockSize = $blockSize;
        else $actBlockSize = $rest;
        
        unset($buffer);
        for($i = 0; $i < $actBlockSize; $i++)
        {
          $buffer .= chr($rulesData[$ready + $i]);
          $fileBuffer .= chr($rulesData[$ready + $i]);
        }
        
        $data["offset"] = $ready;
        $data["data"] = $buffer;
        if ($firstWriteId == - 1) $firstWriteId = updateLastLogId();
        
        $result = callObjectMethodByNameAndRecover($objectId, "writeRules", $data, "MemoryStatus", 3, 5,0);
        if ($result == - 1)
        {
          QUERY("delete from ruleCache where controllerId='$controllerId' limit 1");
          echo "<font color=#bb0000><b>FEHLER: Controller antwortet nicht </font><br>";
          break;
        }
        $memoryStatus = getResultDataValueByName("status", $result);
        
        if ($memoryStatus != $memoryStatusOk)
        {
          echo "<font color=#bb0000><b>Fehler: A) Controller hat Fehler gemeldet im MemoryStatus -> " . $memoryStatus . "</font><br>";
          break;
        }
        $ready += strlen($buffer);
        $rest = $dataPos - $ready;
        if ($rest < 1)
        {
          // Am ende noch ein 0 Package schicken, falls der letzte Block genau blockSize groß war
          if ($actBlockSize == $blockSize)
          {
            $data["offset"] = $ready;
            $data["data"] = ""; // leere daten
            $result = callObjectMethodByNameAndRecover($objectId, "writeRules", $data, "MemoryStatus", 3, 5,0);
            $memoryStatus = getResultDataValueByName("status", $result);
            if ($memoryStatus != $memoryStatusOk)
            {
              echo "<font color=#bb0000><b>Fehler: B) Controller hat Fehler gemeldet im MemoryStatus -> " . $memoryStatus . "</font><br>";
              break;
            }
          }
          break;
        }
      }
    }
    file_put_contents("0x".decHex($objectId).".bin", $fileBuffer);
  }
  
  echo "<script>document.getElementById('controllerProgress').innerHTML='';</script>";
  
  if ($singleControllerId=="")
  {
    echo "</div></td><td width=20>&nbsp;</td><td valign=top><font face=verdana size=2>";
    echo "<u>Konfiguriere Taster</u><br><div style='font-face:verdana;font-size:11px'>";
    flushIt();
    
    $tasterClassesId = getClassesIdByName("Taster");
    
    $erg = QUERY("select name, functionId,id from featureFunctions where type='EVENT' and featureClassesId='$tasterClassesId'");
    while ( $obj = mysqli_fetch_OBJECT($erg) )
    {
      $tasterEvents[$obj->functionId] = $obj->name;
      $tasterEventsId[$obj->id] = $obj->name;
    }
    
    $notifyOnCovered = getFunctionParamBitValueByNameForClassesId($tasterClassesId, "setConfiguration", "eventMask", "notifyOnCovered");
    $notifyOnClicked = getFunctionParamBitValueByNameForClassesId($tasterClassesId, "setConfiguration", "eventMask", "notifyOnClicked");
    $notifyOnDoubleClicked = getFunctionParamBitValueByNameForClassesId($tasterClassesId, "setConfiguration", "eventMask", "notifyOnDoubleClicked");
    $notifyOnStartHold = getFunctionParamBitValueByNameForClassesId($tasterClassesId, "setConfiguration", "eventMask", "notifyOnStartHold");
    $notifyOnEndHold = getFunctionParamBitValueByNameForClassesId($tasterClassesId, "setConfiguration", "eventMask", "notifyOnEndHold");
    $notifyOnFree = getFunctionParamBitValueByNameForClassesId($tasterClassesId, "setConfiguration", "eventMask", "notifyOnFree");
    //die($notifyOnCovered."-".$notifyOnClicked."-".$notifyOnDoubleClicked."-".$notifyOnStartHold."-".$notifyOnEndHold."-".$notifyOnFree);
    
    $bitmask = array();
    
	//echo "SELECT distinct featureInstanceId, functionId, controller.id from rulesignals join featureInstances on (featureInstances.id = rulesignals.featureInstanceId) join featureClasses on (featureClasses.id = featureInstances.featureClassesId) join featureFunctions on (featureFunctions.id=featureFunctionId) join controller on (controller.id = featureInstances.controllerId) where featureClasses.id='$tasterClassesId'  and online='1' and groupAlias='0' order by featureInstanceId <br>";
    $erg = QUERY("SELECT distinct featureInstanceId, functionId, controller.id from rulesignals join featureInstances on (featureInstances.id = rulesignals.featureInstanceId) join featureClasses on (featureClasses.id = featureInstances.featureClassesId) join featureFunctions on (featureFunctions.id=featureFunctionId) join controller on (controller.id = featureInstances.controllerId) where featureClasses.id='$tasterClassesId'  and online='1' and groupAlias='0' order by featureInstanceId");
    while ( $obj = mysqli_fetch_OBJECT($erg) )
    {
      if ($bitmask[$obj->featureInstanceId] == "") $bitmask[$obj->featureInstanceId] = 0;
      //echo $tasterEvents[$obj->functionId]."<br>";
      if ($tasterEvents[$obj->functionId] == "evCovered") $bitmask[$obj->featureInstanceId] |= pow(2, $notifyOnCovered);
      else if ($tasterEvents[$obj->functionId] == "evClicked") $bitmask[$obj->featureInstanceId] |= pow(2, $notifyOnClicked);
      else if ($tasterEvents[$obj->functionId] == "evDoubleClick") $bitmask[$obj->featureInstanceId] |= pow(2, $notifyOnDoubleClicked);
      else if ($tasterEvents[$obj->functionId] == "evHoldStart") $bitmask[$obj->featureInstanceId] |= pow(2, $notifyOnStartHold);
      else if ($tasterEvents[$obj->functionId] == "evHoldEnd") $bitmask[$obj->featureInstanceId] |= pow(2, $notifyOnEndHold);
      else if ($tasterEvents[$obj->functionId] == "evFree") $bitmask[$obj->featureInstanceId] |= pow(2, $notifyOnFree);
    }
	
    
    // Und noch Signale aus den Diagrammen berücksichtigen
    $erg = QUERY("SELECT DISTINCT featureInstanceId, graphsignalevents.functionId
  FROM graphsignalevents
  JOIN featureInstances ON ( featureInstances.id = graphsignalevents.featureInstanceId )
  JOIN featureClasses ON ( featureClasses.id = featureInstances.featureClassesId )
  JOIN featureFunctions ON ( featureFunctions.id = graphsignalevents.functionId )
  JOIN controller ON ( controller.id = featureInstances.controllerId )
  WHERE featureClasses.id = '1'
  AND online = '1'
  ORDER BY featureInstanceId");
    while ( $obj = mysqli_fetch_OBJECT($erg) )
    {
      //if ($bitmask[$obj->featureInstanceId] == "") $bitmask[$obj->featureInstanceId] = 0;
      
      if ($tasterEventsId[$obj->functionId] == "evCovered") $bitmask[$obj->featureInstanceId] |= pow(2, $notifyOnCovered);
      else if ($tasterEventsId[$obj->functionId] == "evClicked") $bitmask[$obj->featureInstanceId] |= pow(2, $notifyOnClicked);
      else if ($tasterEventsId[$obj->functionId] == "evDoubleClick") $bitmask[$obj->featureInstanceId] |= pow(2, $notifyOnDoubleClicked);
      else if ($tasterEventsId[$obj->functionId] == "evHoldStart") $bitmask[$obj->featureInstanceId] |= pow(2, $notifyOnStartHold);
      else if ($tasterEventsId[$obj->functionId] == "evHoldEnd") $bitmask[$obj->featureInstanceId] |= pow(2, $notifyOnEndHold);
      else if ($tasterEventsId[$obj->functionId] == "evFree") $bitmask[$obj->featureInstanceId] |= pow(2, $notifyOnFree);
    }
	
	// Und noch Bewegungsmelder und Fenster aus der Visualisierung
    $erg = QUERY("SELECT id from featureInstances where featureClassesId=1 and guiType!='' and guiType!='Taster'");
    while ( $obj = mysqli_fetch_OBJECT($erg) )
    {
      $bitmask[$obj->id] |= pow(2, $notifyOnCovered);
      $bitmask[$obj->id] |= pow(2, $notifyOnFree);
    }
    
    foreach ( $bitmask as $featureInstanceId => $mask )
    {
      $erg3 = QUERY("select controller.size as controllerSize,controller.name as controllerName,controller.objectId as controllerObjectId, featureInstances.name as featurInstanceName from featureInstances join controller on (controller.id=featureInstances.controllerId) where featureInstances.id='$featureInstanceId' limit 1");
      $obj3 = mysqli_fetch_OBJECT($erg3);
      if ($obj3->controllerSize == "999") continue; // PC rauslassen
      $tasterName = $obj3->controllerName . "-" . $obj3->featurInstanceName;
      
      callInstanceMethodByName($featureInstanceId, "getConfiguration");
      $result = waitForInstanceResultByName($featureInstanceId, 2, "Configuration", $lastLogId, "funtionDataParams", 0);
      if ($result == - 1)
      {
        echo "Wiederholung bei $tasterName <br>";
        flushIt();
        callInstanceMethodByName($featureInstanceId, "getConfiguration");
        $result = waitForInstanceResultByName($featureInstanceId, 2, "Configuration", $lastLogId, "funtionDataParams", 0);
        if ($result == - 1)
        {
          echo "Fehler bei $tasterName <br>";
          flushIt();
          continue;
        }
      }
      
      $holdTimeout = getResultDataValueByName("holdTimeout", $result);
      $eventMask = getResultDataValueByName("eventMask", $result);
      
	  //echo $featureInstanceId." - ".$mask."<br>";
      $mask+=($eventMask&0xC0); // Die oberen beiden Bits unverändert übernehmen
      $waitForDoubleClickTimeout = getResultDataValueByName("waitForDoubleClickTimeout", $result);
	  $optionMask = getResultDataValueByName("optionMask", $result);
	  
      $configArray = array (
          "holdTimeout" => $holdTimeout,
          "waitForDoubleClickTimeout" => $waitForDoubleClickTimeout,
          "eventMask" => $mask,
		  "optionMask" => $optionMask
      );
      $dataChanged = checkAndTraceConfigData($featureInstanceId, $configArray);
      
      if ($dataChanged || $nocache == 1)
      {
        echo "<nobr>$tasterName: hold = $holdTimeout, doubleClick = $waitForDoubleClickTimeout, mask = $mask <br>";
        flushIt();
        callInstanceMethodByName($featureInstanceId, "setConfiguration", $configArray);
      }
      //else liveOut("<i>Ungeändert: $tasterName: hold = $holdTimeout, doubleClick = $waitForDoubleClickTimeout, mask = $mask</i>");
    }
  }
  
  echo "</div></td></tr></table></div>";
  flushIt();
  triggerTreeUpdate();
  //echo "<script>setTimeout('location=\"controller.php\";',5000);</script>";
  exit();
  if ($groupId == "")
  {
    setupTreeAndContent("", $message);
    show();
  }
}

function addGroupRuleData($groupId, $ruleVersion, $hostControllerObjectId)
{
  $debug=0;
  if ($debug==1) echo "<br>Regeln für Gruppe $groupId <br>";
  
  global $rulesData;
  global $dataPos;
  global $CONTROLLER_CLASS_ID;
  global $FIRMWARE_INSTANCE_ID;
  global $raspberryFeatureInstanceId;
  global $systemVariableControllerId;
  global $systemVariableControllerObjectId;

  $startPos = $dataPos;

  $erg9 = QUERY("select * from rules where groupId='$groupId' order by id");
  while ( $obj9 = mysqli_fetch_OBJECT($erg9) )
  {
    $erg = QUERY("select count(*) from ruleSignals where ruleId='$obj9->id' and groupAlias='0' and featureInstanceId>0");
    $row = mysqli_fetch_ROW($erg);
    $nrSignals = $row[0];
    if ($nrSignals == 0) continue;

    $erg = QUERY("select count(*) from ruleActions where ruleId='$obj9->id' and featureInstanceId>0");
    $row = mysqli_fetch_ROW($erg);
    $nrActions = $row[0];
    
    $rulesData[$dataPos++] = $nrSignals;
    $rulesData[$dataPos++] = $nrActions;

    if ($obj9->activationStateId == 0) $rulesData[$dataPos++] = 255; // Aktivierungsstate egal
    else
    {

      $erg = QUERY("select value from groupStates where id='$obj9->activationStateId' limit 1");
      $row = mysqli_fetch_ROW($erg);
      $rulesData[$dataPos++] = $row[0]; // Aktivierungsstate
    }
	
    if ($obj9->resultingStateId == 0) $rulesData[$dataPos++] = 255; // ResultingState gleich
    else
    {
      $erg = QUERY("select value from groupStates where id='$obj9->resultingStateId' limit 1");
      $row = mysqli_fetch_ROW($erg);
      $rulesData[$dataPos++] = $row[0]; // ResultingState
    }

    if ($obj9->startHour == "25")
    {
      $obj9->startHour = 0;
      $obj9->startMinute = 60;
    }
    else if ($obj9->startHour == "26")
    {
      $obj9->startHour = 0;
      $obj9->startMinute = 61;
    }
    
    if ($obj9->endHour == "25")
    {
      $obj9->endHour = 0;
      $obj9->endMinute = 60;
    }
    else if ($obj9->endHour == "26")
    {
      $obj9->endHour = 0;
      $obj9->endMinute = 61;
    }
    
    if ($obj9->intraDay==1)
    {
    	 if ($obj9->startMinute==255) $obj9->startMinute=0;
    	 $obj9->startMinute|=0x40;
    }
    
    $rulesData[$dataPos++] = $obj9->startMinute;
    
    $rulesData[$dataPos++] = $obj9->startHour + ($obj9->startDay << 5);
    $rulesData[$dataPos++] = $obj9->endMinute;
    $rulesData[$dataPos++] = $obj9->endHour + ($obj9->endDay << 5);
    
	$signalNr=0;
    $erg = QUERY("select id, featureInstanceId, featureFunctionId from ruleSignals where ruleId='$obj9->id' and groupAlias='0' and featureInstanceId>0 order by id");
    while ( $obj = mysqli_fetch_OBJECT($erg) )
    {
	  $signalNr++;
	  if ($debug==1) echo "Signal $signalNr <br>";
	  
	  $usedParamLength = 0;

	  if ($obj->featureInstanceId == $raspberryFeatureInstanceId && $obj->featureFunctionId == 270) // Systemvariable
	  {
           $erg3 = QUERY("select paramValue,featureFunctionParamsId from ruleSignalParams where ruleSignalId='$obj->id' limit 3");
           while ($obj3 = mysqli_fetch_OBJECT($erg3))
		   {
			 if ($obj3->featureFunctionParamsId == 545) $type=$obj3->paramValue;
			 else if ($obj3->featureFunctionParamsId == 546) $index=$obj3->paramValue;
			 else if ($obj3->featureFunctionParamsId == 547) $value=$obj3->paramValue;
		   }
		 
		   $rulesData[$dataPos++] = $type; // 0=BIT 1=BYTE 2=WORD
		   $rulesData[$dataPos++] = 0xFE;  // Systemvariable
		   $rulesData[$dataPos++] = 0;     // AND Operator
		   $rulesData[$dataPos++] = 0;     // EQUALS
		   $rulesData[$dataPos++] = $index;// Index
		   $rulesData[$dataPos++] = $value;// Wert
		   $usedParamLength=1;
	  }
	  else
	  {
	     // Bei EvTime müssen wir noch den Absender auf den hostenden Controller abändern
	     if ($obj->featureFunctionId==129 || $obj->featureFunctionId==195 || $obj->featureFunctionId==196)
  	     {
		    $bytes = dWordToBytes($hostControllerObjectId); // Sender
		    if ($debug==1) echo "Controller ObjectId: $hostControllerObjectId <br>";
	     }
		 // Bei evSystemVariableChanged müssen wir den Controller eintragen, der für die Systemvariablen zuständig ist
		 else if ($obj->featureInstanceId == $raspberryFeatureInstanceId && $obj->featureFunctionId == 363) // evSystemVariableChanged
		 {
			 $bytes = dWordToBytes($systemVariableControllerObjectId); // Sender
		     if ($debug==1) echo "Sender ObjectId: $obj2->objectId <br>";
		 }
	     else
	     {
		   $bytes = dWordToBytes($obj->featureInstanceId);
           $erg2 = QUERY("select objectId from featureInstances where id='$obj->featureInstanceId' limit 1");
           if ($obj2 = mysqli_fetch_OBJECT($erg2))
	       {
			 $bytes = dWordToBytes($obj2->objectId); // Sender
		     if ($debug==1) echo "Sender ObjectId: $obj2->objectId <br>";
		   }
           else
           {
               showRuleError("Unbekannter Sender in Gruppe $groupId ruleId $obj9->id featureInstanceId = $obj->featureInstanceId", $groupId);
               $rulesData[$startPos] = 0;
               $dataPos = $startPos;
               return false; 
		   }
         }
	  
         foreach ( (array)$bytes as $value )
         {
           $rulesData[$dataPos++] = $value;
         }
        
	     $tmpSender = $obj2->objectId;
	  
         $erg2 = QUERY("select functionId from featureFunctions where id='$obj->featureFunctionId' limit 1");
         $obj2 = mysqli_fetch_OBJECT($erg2);
         $rulesData[$dataPos++] = $obj2->functionId; // FunctionId
		 
	
         $erg2 = QUERY("select id,type from featureFunctionParams where featureFunctionId='$obj->featureFunctionId' order by id");
         while ( $obj2 = mysqli_fetch_OBJECT($erg2) )
         {
           $erg3 = QUERY("select paramValue from ruleSignalParams where ruleSignalId='$obj->id' and featureFunctionParamsId='$obj2->id' limit 1");
           if ($obj3 = mysqli_fetch_OBJECT($erg3)) $value = $obj3->paramValue;
           else $value = getWildcardForType($obj2->type); // Parameter wurde nicht generiert und wird deshalb mit Wildcard gesetzt
		   
           $bytes = paramToBytes($value, $obj2->type, 1);
           foreach ( (array)$bytes as $value )
           {
             $rulesData[$dataPos++] = $value;
             $usedParamLength++;
           }
         }
	  }
	  
      for($i = $usedParamLength; $i < 5; $i++)
      {
        $rulesData[$dataPos++] = 255;
      }
    }
	 
	$actionNr=0;
    $erg = QUERY("select id, featureInstanceId, featureFunctionId from ruleActions where ruleId='$obj9->id' and featureInstanceId>0 order by id");
    while ( $obj = mysqli_fetch_OBJECT($erg) )
    {
	  $actionNr++;
	  if ($debug==1) echo "Action $signalNr <br>";

      $erg2 = QUERY("select objectId from featureInstances where id='$obj->featureInstanceId' limit 1");
      $obj2 = mysqli_fetch_OBJECT($erg2);
      $bytes = dWordToBytes($obj2->objectId); // Empfänger
      if ($debug==1) echo "Empfänger ObjectId: $obj2->objectId <br>";
	  
	  // bei setSystemVariable müssen wir einen Aufruf auf den ersten Online (nicht software) controller machen
	  if ($obj->featureFunctionId==268) $bytes = dWordToBytes($systemVariableControllerObjectId);

      foreach ( (array)$bytes as $value )
      {
        $rulesData[$dataPos++] = $value;
      }
      
      $actionLengthPos = $dataPos;
	  if ($ruleVersion==1) $dataPos++;
      
      $erg2 = QUERY("select functionId from featureFunctions where id='$obj->featureFunctionId' limit 1");
      $obj2 = mysqli_fetch_OBJECT($erg2);
      $rulesData[$dataPos++] = $obj2->functionId; // FunctionId
      

      $usedParamAndFunctionIdLength = 1;
      $erg2 = QUERY("select id,type from featureFunctionParams where featureFunctionId='$obj->featureFunctionId' order by id");
      while ( $obj2 = mysqli_fetch_OBJECT($erg2) )
      {
        $erg3 = QUERY("select paramValue,id from ruleActionParams where ruleActionId='$obj->id' and featureFunctionParamsId='$obj2->id' limit 1");
        if ($obj3 = mysqli_fetch_OBJECT($erg3))
        {
		  // Sonderlocke für exec vom Raspberry. Hier wird die ID des eigentlichen Befehls in den Regeln eingetragen
          if ($hostControllerObjectId>0 && $obj->featureFunctionId == "189")
          {
            $myNr = 0;
            $erg4 = QUERY("select ruleActionParams.id from ruleActionParams join ruleActions on (ruleActions.id=ruleActionParams.ruleActionId) join rules on (rules.id = ruleActions.ruleId) where groupId='$groupId' order by ruleActionParams.id");
            while ( $row4 = mysqli_fetch_ROW($erg4) )
            {
              if ($row4[0] == $obj3->id) break;
              $myNr++;
            }
            $value = $myNr;
            
          }
          else $value = $obj3->paramValue;
        }
        else $value = 0;

        $bytes = paramToBytes($value, $obj2->type);
		
		// Um keine halben Parameter (z.b. letzter Parameter ist ein WORD) einzutragen, überspringen wir den Parameter lieber komplett
		$lengthAfterParams = $usedParamAndFunctionIdLength + count($bytes); 
		if ($ruleVersion==1 && $lengthAfterParams>5) break;
		if ($ruleVersion==2 && $lengthAfterParams>6) break;
		
        foreach ( (array)$bytes as $value )
        {
          $rulesData[$dataPos++] = $value;
          $usedParamAndFunctionIdLength++;
        }
      }

	  if ($ruleVersion==2 && $usedParamAndFunctionIdLength>6) echo "Fehler in usedParamLength $ruleVersion  $usedParamAndFunctionIdLength <br>";
	  if ($ruleVersion==1 && $usedParamAndFunctionIdLength>5) echo "Fehler in usedParamLength $ruleVersion  $usedParamAndFunctionIdLength <br>";

      // Bei Version 1 < 1.30 hatten die Actions ein Byte für Länge gefolgt von FunctionID und 4 Parameter Bytes
      // Ab Version 2 >= 1.30 wurden die Länge entfernt und nach der FunctionId alle 5 Bytes für die Parameter verwendet
	  if ($ruleVersion==2)
	  {
		// Inklusive der FunctionID kommen wir bei Version 2 also auf 6 Bytes  
        for($i = $usedParamAndFunctionIdLength; $i < 6; $i++)
        {
          $rulesData[$dataPos++] = 0;
        }
	  }
	  else
	  {
		// Inklusive der FunctionID kommen wir bei Version 2 also auf 5 Bytes  
        $rulesData[$actionLengthPos] = $usedParamAndFunctionIdLength;
        for($i = $usedParamAndFunctionIdLength; $i < 5; $i++)
        {
          $rulesData[$dataPos++] = 0;
        }
	  }
    }
	 
  }
  
  if ($dataPos > $startPos)
  {
    $rulesData[$dataPos++] = 0; // 0 Terminierung
    return true;
  }
  else return false;
}

if ($action == "addSignal")
{
  if ($submitted == 1)
  {
    if ($signalGroupId > 0) // Aufruf aus Signalgruppe
    {
      QUERY("INSERT into basicRuleGroupSignals (ruleId, groupId, eventType, andCondition) values('-$ruleId','$signalGroupId','$signal','$andCondition')");
      $groupSignalId = query_insert_id();
      
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId,`generated`)
                               values('$ruleId','-$groupSignalId','0','0')");
							   
      if ($logicalGroupMode == 1) header("Location: editLogicalSignals.php?groupId=$groupId");
	  else header("Location: editRules.php?groupId=$groupId&action=dummy");
    }
    else if ($graphId > 0) // Aufruf aus Diagramm
    {
      $singleParamName = "";
      $erg = QUERY("select name from featureFunctionParams where featureFunctionId='$featureFunctionId' order by id limit 2");
      while ( $row = mysqli_fetch_ROW($erg) )
      {
        if ($singleParamName != "")
        {
          if ($singleParamName == "celsius" && $row[0] == "centiCelsius") $singleParamName = "celsius+centiCelsius/100";
          else if ($singleParamName == "relativeHumidity" && $row[0] == "centiHumidity") $singleParamName = "relativeHumidity+centiHumidity/100";
          else $singleParamName = "";
          break;
        }
        $singleParamName = $row[0];
      }
      
      QUERY("TRUNCATE graphData");
      
      if ($signalId > 0)
      {
      	 if ( $signalEventId > 0) QUERY("UPDATE graphSignalEvents set featureInstanceId='$featureInstanceId', functionId='$featureFunctionId',fkt='$singleParamName' where id='$signalEventId' and graphSignalsId='$signalId' limit 1");
         else  QUERY("INSERT into graphSignalEvents (graphSignalsId, featureInstanceId, functionId,fkt) values('$signalId','$featureInstanceId','$featureFunctionId','$singleParamName')");
      }
      else
      {
        QUERY("INSERT into graphSignals (graphId, color ) values('$graphId','red')");
        $signalId = query_insert_id();
        QUERY("INSERT into graphSignalEvents (graphSignalsId, featureInstanceId, functionId,fkt) values('$signalId','$featureInstanceId','$featureFunctionId','$singleParamName')");
      }
      header("Location: editGraphs.php?id=$graphId");
    }
	else if ($influxId > 0) // Aufruf aus Influx Diagramm
    {
      $singleParamName = "";
      $erg = QUERY("select name from featureFunctionParams where featureFunctionId='$featureFunctionId' order by id limit 2");
      while ( $row = mysqli_fetch_ROW($erg) )
      {
        if ($singleParamName != "")
        {
          if ($singleParamName == "celsius" && $row[0] == "centiCelsius") $singleParamName = "celsius+centiCelsius/100";
          else if ($singleParamName == "relativeHumidity" && $row[0] == "centiHumidity") $singleParamName = "relativeHumidity+centiHumidity/100";
          else $singleParamName = "";
          break;
        }
        $singleParamName = $row[0];
      }
      
      if ($signalId > 0)
      {
      	 if ( $signalEventId > 0) QUERY("UPDATE influxSignalEvents set featureInstanceId='$featureInstanceId', functionId='$featureFunctionId',fkt='$singleParamName' where id='$signalEventId' and influxSignalsId='$signalId' limit 1");
         else  QUERY("INSERT into influxSignalEvents (influxSignalsId, featureInstanceId, functionId,fkt) values('$signalId','$featureInstanceId','$featureFunctionId','$singleParamName')");
      }
      else
      {
        QUERY("INSERT into influxSignals (influxId) values('$influxId')");
        $signalId = query_insert_id();
        QUERY("INSERT into influxSignalEvents (influxSignalsId, featureInstanceId, functionId,fkt) values('$signalId','$featureInstanceId','$featureFunctionId','$singleParamName')");
      }
      header("Location: editInflux.php?id=$influxId");
    }
    else
    {
      $erg = QUERY("select id from ruleSignals where ruleId='$ruleId' and featureInstanceId='$featureInstanceId' and featureFunctionId='$featureFunctionId' limit 1");
      if ($row = mysqli_fetch_row($erg)) $ruleSignalId = $row[0];
      else
      {
        QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','$featureFunctionId')");
        $ruleSignalId = query_insert_id();
      }
      
      $erg = QUERY("select count(*) from featureFunctionParams where featureFunctionId='$featureFunctionId'");
      $row = mysqli_fetch_row($erg);
      if ($row[0] > 0)
      {
        $erg = QUERY("select featureClassesId from featureInstances where id='$featureInstanceId' limit 1");
        $row = mysqli_fetch_ROW($erg);
        $featureClassesId = $row[0];
        
        $irClassesId = getClassesIdByName("IR-Sensor");
        if ($featureClassesId == $irClassesId)
        {
          $i = 0;
          $erg = QUERY("select id from featureFunctionParams where featureFunctionId='$featureFunctionId' order by id");
          while ( $row = mysqli_fetch_ROW($erg) )
          {
            $i++;
            $value = "param" . $i . "Value";
            $value = $$value;
            QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                                 values('$ruleSignalId','$row[0]','$value')");
          }
          header("Location: editRules.php?groupId=$groupId&logicalGroupMode=$logicalGroupMode");
          exit();
        }
        header("Location: editRules.php?groupId=$groupId&action=editSignalParams&ruleSignalId=$ruleSignalId&featureFunctionId=$featureFunctionId&logicalGroupMode=$logicalGroupMode");
      }
      else
	  {
        if ($logicalGroupMode == 1) header("Location: editLogicalSignals.php?groupId=$groupId");
		else header("Location: editRules.php?groupId=$groupId&logicalGroupMode=$logicalGroupMode");
	  }
    }

    exit();
  }
  else
  {
    if ($liveMode == 1)
    {
      setupTreeAndContent("signalLiveMode_design.html");
      $html = str_replace("%LINK%", urlencode("editRules.php?action=$action&submitted=1&groupId=$groupId&ruleId=$ruleId&logicalGroupMode=$logicalGroupMode&graphId=$graphId&influxId=$influxId"), $html);
      show();
    }
    else
    {
      setupTreeAndContent("addRuleSignal_design.html");
      
      $withResults=1;
      if ($withResults==1) removeTag("%OPT_ADD_OTHERS%",$html);
      else
      {
      	chooseTag("%OPT_ADD_OTHERS%",$html);
        $html = str_replace("%MY_URI%",$REQUEST_URI,$html);
      }
    
      $html = str_replace("%LOGICAL_GROUP_MODE%", $logicalGroupMode, $html);
      $html = str_replace("%LOGICAL_STATE_ID%", $logicalStateId, $html);
      $html = str_replace("%GRAPH_ID%", $graphId, $html);
      $html = str_replace("%INFLUX_ID%", $influxId, $html);
      $html = str_replace("%SIGNAL_ID%", $signalId, $html);
      $html = str_replace("%SIGNAL_EVENT_ID%", $signalEventId, $html);
      
      $closeTreeFolder = "</ul></li> \n";
      
      $treeElements = "";
      if ($graphId > 0) $treeElements .= addToTree("<a href='editGraph.php?id=$graphId'>Neues Signal auswählen</a>", 1);
	  else if ($influxId > 0) $treeElements .= addToTree("<a href='editInflux.php?id=$influxId'>Neues InfluxDb Signal auswählen</a>", 1);
      else $treeElements .= addToTree("<a href='editRules.php?groupId=$groupId&logicalGroupMode=$logicalGroupMode'>Neues Trigger-Signal für diese Regel auswählen</a>", 1);
      $html = str_replace("%INITIAL_ELEMENT2%", "expandToItem('tree2','$treeElementCount');", $html);
      
      $erg = QUERY("select * from basicRuleGroupSignals where ruleId='-$ruleId'");
      while ( $obj = mysqli_fetch_OBJECT($erg) )
      {
      	if ($obj->groupId>0) $myGroupSignals[$obj->groupId] = 1;
      }
      
      $allMyRuleSignals = readMyRuleSignals($ruleId);
      $logicalButtonClass = getClassesIdByName("LogicalButton");
      
      if ($graphId > 0 || $influxId > 0 || $withResults==1) $or = "or featureFunctions.type='RESULT'";
      
      unset($ready);
      $lastRoom = "";
      $erg = QUERY("select rooms.id as roomId, rooms.name as roomName,
                                 roomFeatures.featureInstanceId,
                                 featureInstances.name as featureInstanceName, featureInstances.featureClassesId,
                                 featureClasses.name as featureClassName,
                                 featureFunctions.id  as featureFunctionId,featureFunctions.name  as featureFunctionName, featureFunctions.view as featureFunctionView
          
                                 
                                 from rooms
                                 join roomFeatures on (roomFeatures.roomId = rooms.id)
                                 join featureInstances on (featureInstances.id = roomFeatures.featureInstanceId)
                                 join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
                                 join featureFunctions on (featureInstances.featureClassesId=featureFunctions.featureClassesId) 
                                 
                                 where (featureFunctions.type='EVENT' $or) and featureInstances.featureClassesId!='$logicalButtonClass'
                                 order by roomName,featureClassName,featureInstanceName,featureFunctionName"); //and parentInstanceId='0' 
      while ( $obj = mysqli_fetch_object($erg) )
      {
        $ready[$obj->featureInstanceId] = 1;
        
        if ($allMyRuleSignals[$obj->featureInstanceId . "-" . $obj->featureFunctionId] != 1)
        {
          if ($ansicht == "Experte" && $obj->featureFunctionView == "Entwickler") continue;
          if ($ansicht == "Standard" && ($obj->featureFunctionView == "Experte" || $obj->featureFunctionView == "Entwickler")) continue;
          
          if ($obj->roomId != $lastRoom)
          {
            if ($lastRoom != "")
            {
              $treeElements .= $closeTreeFolder; // letzte instance
              $treeElements .= $closeTreeFolder; // letzte featureclass
              $treeElements .= $closeTreeFolder; // letzter raum
            }
            $lastRoom = $obj->roomId;
            $treeElements .= addToTree($obj->roomName, 1);
            $lastClass = "";
          }
          
          if ($obj->featureClassesId != $lastClass)
          {
            if ($lastClass != "")
            {
              $treeElements .= $closeTreeFolder; // letzte instance
              $treeElements .= $closeTreeFolder; // letzte featureclass
            }
            
            $lastClass = $obj->featureClassesId;
            $treeElements .= addToTree($obj->featureClassName, 1);
            $lastInstance = "";
          }
          
          if ($obj->featureInstanceId != $lastInstance)
          {
            if ($lastInstance != "")
              $treeElements .= $closeTreeFolder; // letzte instance
            $lastInstance = $obj->featureInstanceId;
            $treeElements .= addToTree($obj->featureInstanceName, 1);
          }
          
          $treeElements .= addToTree("<a href='editRules.php?action=$action&submitted=1&groupId=$groupId&ruleId=$ruleId&featureInstanceId=" . $obj->featureInstanceId . "&featureFunctionId=" . $obj->featureFunctionId . "&logicalGroupMode=$logicalGroupMode&graphId=$graphId&influxId=$influxId&signalId=$signalId&signalEventId=$signalEventId'>" . i18n($obj->featureFunctionName) . "</a>", 0);
        }
      }
      
      $treeElements .= $closeTreeFolder; // letzte instance
      $treeElements .= $closeTreeFolder; // letzte featureclass
      $treeElements .= $closeTreeFolder; // letzter raum
      

      $lastRoom = "";
      $lastController = "";
      $erg = QUERY("select featureInstances.id as featureInstanceId, featureInstances.name as featureInstanceName, featureInstances.featureClassesId,
                                 featureClasses.name as featureClassName,
                                 controller.id as controllerId, controller.name as controllerName,
                                 featureFunctions.id  as featureFunctionId,featureFunctions.name  as featureFunctionName, featureFunctions.view as featureFunctionView
                                 
                                 from featureInstances
                                 join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
                                 join controller on (featureInstances.controllerId = controller.id)
                                 join featureFunctions on (featureInstances.featureClassesId=featureFunctions.featureClassesId) 
                                 
                                 where (featureFunctions.type='EVENT' $or) and featureInstances.featureClassesId!='$logicalButtonClass'
                                 order by controllerName, featureClassName,featureInstanceName,featureFunctionName"); // and parentInstanceId='0' 
      while ( $obj = mysqli_fetch_object($erg) )
      {
        if ($ready[$obj->featureInstanceId] == 1) continue;
        
        if ($allMyRuleSignals[$obj->featureInstanceId . "-" . $obj->featureFunctionId] != 1)
        {
          if ($ansicht == "Experte" && $obj->featureFunctionView == "Entwickler") continue;
          if ($ansicht == "Standard" && ($obj->featureFunctionView == "Experte" || $obj->featureFunctionView == "Entwickler")) continue;
          
          if ($lastRoom == "")
          {
            $lastRoom = "dummy";
            $treeElements .= addToTree("Keinem Raum zugeordnet", 1);
          }
          
          if ($obj->controllerId != $lastController)
          {
            if ($lastController != "")
            {
              $treeElements .= $closeTreeFolder; // letztes instance
              $treeElements .= $closeTreeFolder; // letzte class
              $treeElements .= $closeTreeFolder; // letzter controller
            }
            $lastController = $obj->controllerId;
            $treeElements .= addToTree($obj->controllerName, 1);
            $lastClass = "";
          }
          
          if ($obj->featureClassesId != $lastClass)
          {
            if ($lastClass != "")
            {
              $treeElements .= $closeTreeFolder; // letzte instance
              $treeElements .= $closeTreeFolder; // letzte featureclass
            }
            
            $lastClass = $obj->featureClassesId;
            $treeElements .= addToTree($obj->featureClassName, 1);
            $lastInstance = "";
          }
          
          if ($obj->featureInstanceId != $lastInstance)
          {
            if ($lastInstance != "")
              $treeElements .= $closeTreeFolder; // letzte instance
            $lastInstance = $obj->featureInstanceId;
            $treeElements .= addToTree($obj->featureInstanceName, 1);
          }
          
          $treeElements .= addToTree("<a href='editRules.php?action=$action&submitted=1&groupId=$groupId&ruleId=$ruleId&featureInstanceId=" . $obj->featureInstanceId . "&featureFunctionId=" . $obj->featureFunctionId . "&logicalGroupMode=$logicalGroupMode&graphId=$graphId&influxId=$influxId&signalId=$signalId&signalEventId=$signalEventId'>" . i18n($obj->featureFunctionName) . "</a>", 0);
        }
      }
      
      $treeElements .= $closeTreeFolder; // letzte instance
      $treeElements .= $closeTreeFolder; // letzte featureclass
      $treeElements .= $closeTreeFolder; // letzter controller
      $treeElements .= $closeTreeFolder; // letzter raum
      

      $foundGroups = 0;
      $erg = QUERY("select id,name,groupType from groups where single!='1' and subOf='0' and groupType!='' and `generated`!=1 order by name");
      while ( $obj = mysqli_fetch_OBJECT($erg) )
      {
        if ($foundGroups == 0)
        {
          $foundGroups = 1;
          $treeElements .= addToTree("Signalgruppen Events", 1);
        }
        if ($myGroupSignals[$obj->id] != 1)
        {
          if ($obj->groupType == "SIGNALS-AND") $groupType = "UG";
          else if ($obj->groupType == "SIGNALS-OR") $groupType = "OG";
          else $groupType = "??";
          $treeElements .= addToTree($obj->name . " (" . $groupType . ")", 1);
          $treeElements .= addToTree("<a href='editRules.php?action=$action&submitted=1&groupId=$groupId&ruleId=$ruleId&groupSignal=1&signalGroupId=$obj->id&logicalGroupMode=$logicalGroupMode&signal=ACTIVE'>Beim Erreichen vom aktiven Zustand</a>", 0);
          $treeElements .= addToTree("<a href='editRules.php?action=$action&submitted=1&groupId=$groupId&ruleId=$ruleId&groupSignal=1&signalGroupId=$obj->id&logicalGroupMode=$logicalGroupMode&signal=DEACTIVE'>Beim Verlassen vom aktiven Zustand</a>", 0);
          $treeElements .= $closeTreeFolder;
        }
      }
	  
	  
	  $systemVariableInstanceId=getFirstSoftwareController()->id;
	  $systemVariableFunctionId=270;
      $treeElements .= $closeTreeFolder;
      $treeElements .= addToTree("UND Verknüpfungen", 1);
	  $treeElements .= addToTree("<a href='editRules.php?action=$action&submitted=1&groupId=$groupId&ruleId=$ruleId&featureInstanceId=$systemVariableInstanceId&featureFunctionId=$systemVariableFunctionId&logicalGroupMode=$logicalGroupMode&graphId=$graphId&influxId=$influxId&signalId=$signalId&signalEventId=$signalEventId'>Systemvariable</a>", 0);
      $treeElements .= $closeTreeFolder;
	  
	  /*$systemVariables = getSystemVariables();
	  $foundBit=false;
	  $foundByte=false;
	  $foundWord=false;
	  foreach ($systemVariables as $type=>$name)
	  {
		 if (strpos($type,"BIT")!==FALSE) $foundBit=true;
		 if (strpos($type,"BYTE")!==FALSE) $foundByte=true;
		 if (strpos($type,"WORD")!==FALSE) $foundWord=true;
	  }
	  
      $treeElements .= $closeTreeFolder;
      $treeElements .= addToTree("UND Verknüpfungen", 1);
	  
	  if ($foundBit)
	  {
        $treeElements .= addToTree("Systemvariablen Typ BIT", 1);
	    foreach ($systemVariables as $type=>$name)
	    {
		   if (strpos($type,"BIT")!==FALSE) $treeElements .= addToTree("<a href='editRules.php?action=$action&submitted=1&groupId=$groupId&ruleId=$ruleId&signalGroupId=$obj->id&logicalGroupMode=$logicalGroupMode&systemVariable=$type'>$name</a>", 0);
	    }
        $treeElements .= $closeTreeFolder;
	  }

      if ($foundByte)
	  {
        $treeElements .= addToTree("Systemvariablen Typ BYTE", 1);
	    foreach ($systemVariables as $type=>$name)
	    {
		   if (strpos($type,"BYTE")!==FALSE) $treeElements .= addToTree("<a href='editRules.php?action=$action&submitted=1&groupId=$groupId&ruleId=$ruleId&signalGroupId=$obj->id&logicalGroupMode=$logicalGroupMode&systemVariable=$type'>$name</a>", 0);
	    }
        $treeElements .= $closeTreeFolder;
	  }

      if ($foundWord)
	  {
        $treeElements .= addToTree("Systemvariablen Typ WORD", 1);
	    foreach ($systemVariables as $type=>$name)
	    {
		   if (strpos($type,"WORD")!==FALSE) $treeElements .= addToTree("<a href='editRules.php?action=$action&submitted=1&groupId=$groupId&ruleId=$ruleId&signalGroupId=$obj->id&logicalGroupMode=$logicalGroupMode&systemVariable=$type'>$name</a>", 0);
  	    }
        $treeElements .= $closeTreeFolder;
	  }*/
	  
      $treeElements .= $closeTreeFolder;
      
      $html = str_replace("%TREE_ELEMENTS%", $treeElements, $html);
      $html = str_replace("%GROUP_ID%", $groupId, $html);
      $html = str_replace("%RULE_ID%", $ruleId, $html);
      $html = str_replace("%ACTION%", $action, $html);
      show();
    }
  }
}
else if ($action == "addAction")
{
  if ($submitted == 1)
  {
    $erg = QUERY("select id from ruleActions where ruleId='$ruleId' and featureInstanceId='$featureInstanceId' and featureFunctionId='$featureFunctionId' limit 1");
    if ($row = mysqli_fetch_ROW($erg)) $ruleActionId = $row[0];
    else
    {
      QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','$featureFunctionId')");
      $ruleActionId = query_insert_id();
    }
    
    $erg = QUERY("select count(*) from featureFunctionParams where featureFunctionId='$featureFunctionId'");
    $row = mysqli_fetch_row($erg);
    if ($row[0] > 0) header("Location: editRules.php?groupId=$groupId&action=editActionParams&ruleActionId=$ruleActionId&featureFunctionId=$featureFunctionId");
    else header("Location: editRules.php?groupId=$groupId");
    exit();
  }
  else
  {
    $myGroupFeatures = readGroupFeatures($groupId);
    foreach ( $myGroupFeatures as $featureInstanceId => $dummy )
    {
      if (getClassesIdByFeatureInstanceId($featureInstanceId) == 24)
      {
        $isPc = 1;
        $erg = QUERY("select controllerId from featureInstances where id='$featureInstanceId' limit 1");
        $row = mysqli_fetch_ROW($erg);
        $erg = QUERY("select id from featureInstances where controllerId='$row[0]' and featureClassesId='12' limit 1");
        $row = mysqli_fetch_ROW($erg);
        $myGroupFeatures[$row[0]] = 1;
        break;
      }
    }
    
    setupTreeAndContent("addRuleAction_design.html");
    
    if ($withFunctions==1)
    {
    	 removeTag("%OPT_ADD_OTHERS%",$html);
    	 $orFunction=" or featureFunctions.type='FUNCTION'";
    }
    else
    {
    	chooseTag("%OPT_ADD_OTHERS%",$html);
      $html = str_replace("%MY_URI%",$REQUEST_URI,$html);
    }
    
    $ansicht = $_SESSION["ansicht"];
    
    $closeTreeFolder = "</ul></li> \n";
    
    $treeElements = "";
    $treeElements .= addToTree("<a href='editRules.php?groupId=$groupId'>Neue Aktion für diese Regel auswählen</a>", 1);
    //$html=str_replace("%INITIAL_ELEMENT2%","expandToItem('tree2','$treeElementCount');",$html);
    $html = str_replace("%INITIAL_ELEMENT2%", "expandTree('tree2');", $html);
    
    if ($isPc != 1) $allMyRuleActions = readMyRuleActions($ruleId);
    
    unset($ready);
    $lastRoom = "";
    $erg = QUERY("select rooms.id as roomId, rooms.name as roomName,
                                 roomFeatures.featureInstanceId,
                                 featureInstances.name as featureInstanceName, featureInstances.featureClassesId,
                                 featureClasses.name as featureClassName,
                                 featureFunctions.id  as featureFunctionId,featureFunctions.name  as featureFunctionName, featureFunctions.view as featureFunctionView
                                 
                                 from rooms
                                 join roomFeatures on (roomFeatures.roomId = rooms.id)
                                 join featureInstances on (featureInstances.id = roomFeatures.featureInstanceId)
                                 join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
                                 join featureFunctions on (featureInstances.featureClassesId=featureFunctions.featureClassesId) 
                                 
                                 where featureFunctions.type='ACTION' $orFunction
                                 order by roomName,featureClassName,featureInstanceName,featureFunctionName");
    while ( $obj = mysqli_fetch_object($erg) )
    {
      if ($myGroupFeatures[$obj->featureInstanceId] == "" && $withFunctions!=1)
        continue;
      if ($allMyRuleActions[$obj->featureInstanceId] != "")
        continue;
      
      if ($ansicht == "Experte" && $obj2->featureFunctionView == "Entwickler")
        continue;
      if ($ansicht == "Standard" && ($obj2->featureFunctionView == "Experte" || $obj2->featureFunctionView == "Entwickler"))
        continue;
      
      $ready[$obj->featureInstanceId] = 1;
      
      if ($obj->roomId != $lastRoom)
      {
        if ($lastRoom != "")
        {
          $treeElements .= $closeTreeFolder; // letzte instance
          $treeElements .= $closeTreeFolder; // letzte featureclass
          $treeElements .= $closeTreeFolder; // letzter raum
        }
        $lastRoom = $obj->roomId;
        $treeElements .= addToTree($obj->roomName, 1);
        $lastClass = "";
      }
      
      if ($obj->featureClassesId != $lastClass)
      {
        if ($lastClass != "")
        {
          $treeElements .= $closeTreeFolder; // letzte instance
          $treeElements .= $closeTreeFolder; // letzte featureclass
        }
        
        $lastClass = $obj->featureClassesId;
        $treeElements .= addToTree($obj->featureClassName, 1);
        $lastInstance = "";
      }
      
      if ($obj->featureInstanceId != $lastInstance)
      {
        if ($lastInstance != "")
          $treeElements .= $closeTreeFolder; // letzte instance
        $lastInstance = $obj->featureInstanceId;
        $treeElements .= addToTree($obj->featureInstanceName, 1);
      }
      
      $treeElements .= addToTree("<a href='editRules.php?action=addAction&submitted=1&groupId=$groupId&ruleId=$ruleId&featureInstanceId=" . $obj->featureInstanceId . "&featureFunctionId=" . $obj->featureFunctionId . "'>" . i18n($obj->featureFunctionName) . "</a>", 0);
    }
    
    $treeElements .= $closeTreeFolder; // letzte instance
    $treeElements .= $closeTreeFolder; // letzte featureclass
    $treeElements .= $closeTreeFolder; // letzter raum
    

    $lastRoom = "";
    $lastController = "";
    $erg = QUERY("select featureInstances.id as featureInstanceId, featureInstances.name as featureInstanceName, featureInstances.featureClassesId,
                                 featureClasses.name as featureClassName,
                                 controller.id as controllerId, controller.name as controllerName,
                                 featureFunctions.id  as featureFunctionId,featureFunctions.name  as featureFunctionName
                                 
                                 from featureInstances
                                 join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
                                 join controller on (featureInstances.controllerId = controller.id)
                                 join featureFunctions on (featureInstances.featureClassesId=featureFunctions.featureClassesId) 
                                 
                                 where featureFunctions.type='ACTION' $orFunction
                                 order by controllerName, featureClassName,featureInstanceName,featureFunctionName");
    while ( $obj = mysqli_fetch_object($erg) )
    {
      if ($myGroupFeatures[$obj->featureInstanceId] == "" && $withFunctions!=1)
        continue;
      if ($ready[$obj->featureInstanceId] == 1)
        continue;
      if ($allMyRuleActions[$obj->featureInstanceId] != "")
        continue;
      
      if ($lastRoom == "")
      {
        $lastRoom = "dummy";
        $treeElements .= addToTree("Keinem Raum zugeordnet", 1);
      }
      
      if ($obj->controllerId != $lastController)
      {
        if ($lastController != "")
        {
          $treeElements .= $closeTreeFolder; // letztes instance
          $treeElements .= $closeTreeFolder; // letzte class
          $treeElements .= $closeTreeFolder; // letzter controller
        }
        $lastController = $obj->controllerId;
        $treeElements .= addToTree($obj->controllerName, 1);
        $lastClass = "";
      }
      
      if ($obj->featureClassesId != $lastClass)
      {
        if ($lastClass != "")
        {
          $treeElements .= $closeTreeFolder; // letzte instance
          $treeElements .= $closeTreeFolder; // letzte featureclass
        }
        
        $lastClass = $obj->featureClassesId;
        $treeElements .= addToTree($obj->featureClassName, 1);
        $lastInstance = "";
      }
      
      if ($obj->featureInstanceId != $lastInstance)
      {
        if ($lastInstance != "")
          $treeElements .= $closeTreeFolder; // letzte instance
        $lastInstance = $obj->featureInstanceId;
        $treeElements .= addToTree($obj->featureInstanceName, 1);
      }
      
      $treeElements .= addToTree("<a href='editRules.php?action=addAction&submitted=1&groupId=$groupId&ruleId=$ruleId&featureInstanceId=" . $obj->featureInstanceId . "&featureFunctionId=" . $obj->featureFunctionId . "'>" . i18n($obj->featureFunctionName) . "</a>", 0);
    }
    
    $treeElements .= $closeTreeFolder; // letzte instance
    $treeElements .= $closeTreeFolder; // letzte featureclass
    $treeElements .= $closeTreeFolder; // letzter controller
    $treeElements .= $closeTreeFolder; // letzter raum
    

    $html = str_replace("%TREE_ELEMENTS%", $treeElements, $html);
    show();
  }
}
/*else if ($action == "chooseSignalAlias")
{
  $tasterClassesId = getClassesIdByName("Taster");
  $irClassesId = getClassesIdByName("IR-Sensor");
  
  setupTreeAndContent("addRuleSignal_design.html");
  removeTag("%OPT_ADD_OTHERS%",$html);
  
  $ruleId = $id;
  
  $html = str_replace("signalLiveMode.php", "signalLiveModeSensorAlias.php", $html);
  
  $closeTreeFolder = "</ul></li> \n";
  
  $treeElements = "";
  $treeElements .= addToTree("<a href='showSensorRules.php?id=$id'>Sensoralias wählen, dessen Regeln übernommen werden sollen</a>", 1);
  $html = str_replace("%INITIAL_ELEMENT2%", "expandToItem('tree2','$treeElementCount');", $html);
  
  $logicalButtonClass = getClassesIdByName("LogicalButton");
  
  unset($ready);
  $lastRoom = "";
  $erg = QUERY("select rooms.id as roomId, rooms.name as roomName,
                                 roomFeatures.featureInstanceId,
                                 featureInstances.name as featureInstanceName, featureInstances.featureClassesId,
                                 featureClasses.name as featureClassName
                                 from rooms
                                 join roomFeatures on (roomFeatures.roomId = rooms.id)
                                 join featureInstances on (featureInstances.id = roomFeatures.featureInstanceId)
                                 join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
                                 where (featureInstances.featureClassesId='$tasterClassesId' or featureInstances.featureClassesId='$irClassesId')
                                 order by roomName,featureClassName,featureInstanceName");
  while ( $obj = mysqli_fetch_object($erg) )
  {
    if ($ready[$obj->featureInstanceId] == 1)
      continue;
    
    $ready[$obj->featureInstanceId] = 1;
    
    if ($obj->roomId != $lastRoom)
    {
      if ($lastRoom != "")
        $treeElements .= $closeTreeFolder; // letzter raum
      

      $lastRoom = $obj->roomId;
      $treeElements .= addToTree($obj->roomName, 1);
    }
    
    $treeElements .= addToTree("<a href='showSensorRules.php?action=$action&id=$id&submitted=1&featureInstanceId=" . $obj->featureInstanceId . "'>" . $obj->featureInstanceName . "</a>", 0);
  }
  
  $treeElements .= $closeTreeFolder; // letzter raum
  

  $lastRoom = "";
  $lastController = "";
  $erg = QUERY("select featureInstances.id as featureInstanceId, featureInstances.name as featureInstanceName, featureInstances.featureClassesId,
                                 featureClasses.name as featureClassName,
                                 controller.id as controllerId, controller.name as controllerName
                                 from featureInstances
                                 join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
                                 join controller on (featureInstances.controllerId = controller.id)
                                 where (featureInstances.featureClassesId='$tasterClassesId'  or featureInstances.featureClassesId='$irClassesId')
                                 order by controllerName, featureClassName,featureInstanceName");
  while ( $obj = mysqli_fetch_object($erg) )
  {
    if ($ready[$obj->featureInstanceId] == 1)
      continue;
    
    if ($lastRoom == "")
    {
      $lastRoom = "dummy";
      $treeElements .= addToTree("Keinem Raum zugeordnet", 1);
    }
    
    if ($obj->controllerId != $lastController)
    {
      if ($lastController != "")
        $treeElements .= $closeTreeFolder; // letzter controller
      

      $lastController = $obj->controllerId;
      $treeElements .= addToTree($obj->controllerName, 1);
    }
    
    $treeElements .= addToTree("<a href='showSensorRules.php?action=$action&id=$id&submitted=1&featureInstanceId=" . $obj->featureInstanceId . "'>" . $obj->featureInstanceName . "</a>", 0);
  }
  
  $treeElements .= $closeTreeFolder; // letzter controller
  $treeElements .= $closeTreeFolder; // letzter raum
  

  $html = str_replace("%TREE_ELEMENTS%", $treeElements, $html);
  $html = str_replace("%GROUP_ID%", $groupId, $html);
  $html = str_replace("%RULE_ID%", $ruleId, $html);
  $html = str_replace("%ACTION%", $action, $html);
  show();
}*/
else if ($action == "editButton")
{
  $tasterClassesId = getClassesIdByName("Taster");
  $irClassesId = getClassesIdByName("IR-Sensor");
  
  setupTreeAndContent("addRuleSignal_design.html");
  removeTag("%OPT_ADD_OTHERS%",$html);
  
  $ruleId = $id;
  
  $html = str_replace("signalLiveMode.php?", "signalLiveModeButtonSignal.php?pageId=$pageId&buttonId=$buttonId&", $html);
  
  $closeTreeFolder = "</ul></li> \n";
  
  $treeElements = "";
  $treeElements .= addToTree("<a href='editButtonPage.php?pageId=$pageId'>Signal für den Button auswählen</a>", 1);
  $html = str_replace("%INITIAL_ELEMENT2%", "expandToItem('tree2','$treeElementCount');", $html);
  
  $logicalButtonClass = getClassesIdByName("LogicalButton");
  
  unset($ready);
  $lastRoom = "";
  $erg = QUERY("select rooms.id as roomId, rooms.name as roomName,
                                 roomFeatures.featureInstanceId,
                                 featureInstances.name as featureInstanceName, featureInstances.featureClassesId,
                                 featureClasses.name as featureClassName
                                 from rooms
                                 join roomFeatures on (roomFeatures.roomId = rooms.id)
                                 join featureInstances on (featureInstances.id = roomFeatures.featureInstanceId)
                                 join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
                                 where (featureInstances.featureClassesId='$tasterClassesId' or featureInstances.featureClassesId='$irClassesId')
                                 order by roomName,featureClassName,featureInstanceName");
  while ( $obj = mysqli_fetch_object($erg) )
  {
    if ($ready[$obj->featureInstanceId] == 1)
      continue;
    
    $ready[$obj->featureInstanceId] = 1;
    
    if ($obj->roomId != $lastRoom)
    {
      if ($lastRoom != "")
        $treeElements .= $closeTreeFolder; // letzter raum
      

      $lastRoom = $obj->roomId;
      $treeElements .= addToTree($obj->roomName, 1);
    }
    
    $treeElements .= addToTree("<a href='editButtonPage.php?action=$action&pageId=$pageId&buttonId=$buttonId&featureInstanceId=" . $obj->featureInstanceId . "'>" . $obj->featureInstanceName . "</a>", 0);
  }
  
  $treeElements .= $closeTreeFolder; // letzter raum
  

  $lastRoom = "";
  $lastController = "";
  $erg = QUERY("select featureInstances.id as featureInstanceId, featureInstances.name as featureInstanceName, featureInstances.featureClassesId,
                                 featureClasses.name as featureClassName,
                                 controller.id as controllerId, controller.name as controllerName
                                 from featureInstances
                                 join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
                                 join controller on (featureInstances.controllerId = controller.id)
                                 where (featureInstances.featureClassesId='$tasterClassesId'  or featureInstances.featureClassesId='$irClassesId')
                                 order by controllerName, featureClassName,featureInstanceName");
  while ( $obj = mysqli_fetch_object($erg) )
  {
    if ($ready[$obj->featureInstanceId] == 1)
      continue;
    
    if ($lastRoom == "")
    {
      $lastRoom = "dummy";
      $treeElements .= addToTree("Keinem Raum zugeordnet", 1);
    }
    
    if ($obj->controllerId != $lastController)
    {
      if ($lastController != "")
        $treeElements .= $closeTreeFolder; // letzter controller
      

      $lastController = $obj->controllerId;
      $treeElements .= addToTree($obj->controllerName, 1);
    }
    
    $treeElements .= addToTree("<a href='editButtonPage.php?action=$action&pageId=$pageId&buttonId=$buttonId&featureInstanceId=" . $obj->featureInstanceId . "'>" . $obj->featureInstanceName . "</a>", 0);
  }
  
  $treeElements .= $closeTreeFolder; // letzter controller
  $treeElements .= $closeTreeFolder; // letzter raum
  

  $html = str_replace("%TREE_ELEMENTS%", $treeElements, $html);
  $html = str_replace("%GROUP_ID%", $groupId, $html);
  $html = str_replace("%RULE_ID%", $ruleId, $html);
  $html = str_replace("%ACTION%", $action, $html);
  show();
}
else if ($action == "removeSignal") deleteRuleSignal($signalId);
else if ($action == "removeAction") deleteRuleAction($actionId);
else if ($action == "addRule") QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute) values('$groupId','7','31','255','7','31','255')");
else if ($action == "addTwoRules")
{
  $allGroupStates = readGroupStates();
  $myStatesNo = 0;
  
  foreach ( (array)$allGroupStates as $obj )
  {
    if ($obj->groupId == $groupId)
    {
      $myStates[$myStatesNo++] = $obj->id;
      if ($myStatesNo == 2)
        break;
    }
  }
  
  $first = $myStates[0];
  $second = $myStates[1];
  
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId) values('$groupId','7','31','255','7','31','255','$first','$second')");
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId) values('$groupId','7','31','255','7','31','255','$second','$first')");
}
else if ($action == "deleteRule") deleteRule($ruleId);
else if ($action == "changeActivationState") QUERY("UPDATE rules set activationStateId='$activationStateId' where id='$ruleId' limit 1");
else if ($action == "changeStartDay") QUERY("UPDATE rules set startDay='$startDay' where id='$ruleId' limit 1");
else if ($action == "changeEndDay") QUERY("UPDATE rules set endDay='$endDay' where id='$ruleId' limit 1");
else if ($action == "changeStartTime") QUERY("UPDATE rules set startHour='$startTime' where id='$ruleId' limit 1");
else if ($action == "changeStartTimeMinute") QUERY("UPDATE rules set startMinute='$startTimeMinute' where id='$ruleId' limit 1");
else if ($action == "changeEndTime") QUERY("UPDATE rules set endHour='$endTime' where id='$ruleId' limit 1");
else if ($action == "changeEndTimeMinute") QUERY("UPDATE rules set endMinute='$endTimeMinute' where id='$ruleId' limit 1");
else if ($action=="changeLock")
{
  if ($lockState=="true") $value=1;
  else $value=0;
  QUERY("UPDATE rules set groupLock='$value' where id='$ruleId' limit 1");
}
else if ($action=="changeIntraday")
{
  if ($intradayState=="true") $value=1;
  else $value=0;
  QUERY("UPDATE rules set intraDay='$value' where id='$ruleId' limit 1");
}
else if ($action == "changeResultingState") QUERY("UPDATE rules set resultingStateId='$resultingStateId' where id='$ruleId' limit 1");
else if ($action == "editActionParams")
{
  if ($submitted == 1)
  {
    QUERY("delete from ruleActionParams where ruleActionId='$ruleActionId'");
    trace("ruleActionParam mit ruleActionId $ruleActionId glöscht");
    
    $erg = QUERY("select id from featureFunctionParams where featureFunctionId='$featureFunctionId' order by id");
    while ( $row = mysqli_fetch_ROW($erg) )
    {
      $value = "param" . $row[0];
      $value = $$value;
      $value = addslashes($value);
      QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) values('$ruleActionId','$row[0]','$value')");
    }
    
    header("Location: editRules.php?groupId=$groupId");
    exit();
  }
  else
  {
    setupTreeAndContent("editRuleActionParams_design.html");
    
    $erg = QUERY("select featureFunctionParamsId,paramValue,id from ruleActionParams where ruleActionId='$ruleActionId'");
    while ( $obj = mysqli_fetch_object($erg) )
    {
      $myValues[$obj->featureFunctionParamsId] = $obj->paramValue;
      $ruleActionIds[$obj->featureFunctionParamsId] = $obj->id;
    }
    
    $html = str_replace("%GROUP_ID%", $groupId, $html);
    $html = str_replace("%RULE_ACTION_ID%", $ruleActionId, $html);
    $html = str_replace("%FEATURE_FUNCTION_ID%", $featureFunctionId, $html);
    
    $ansicht = $_SESSION["ansicht"];
    
    $erg = QUERY("select featureFunctionId from ruleActions where id='$ruleActionId' limit 1");
    if ($row = mysqli_fetch_ROW($erg))
    {
      $featureFunctionId = $row[0];
      
      $paramTag = getTag("%PARAM%", $html);
      $params = "";
      $erg2 = QUERY("select id,name,type,comment,view from featureFunctionParams where featureFunctionId='$featureFunctionId' order by id");
      while ( $obj2 = mysqli_fetch_OBJECT($erg2) )
      {
        if ($ansicht == "Experte" && $obj2->view == "Entwickler") continue;
        if ($ansicht == "Standard" && ($obj2->view == "Experte" || $obj2->view == "Entwickler")) continue;
        
        $actParamsTag = $paramTag;
        $actParamsTag = str_replace("%PARAM_NAME%", i18n($obj2->name), $actParamsTag);
        
        $myValue = $myValues[$obj2->id];
        
        if ($obj2->type == "ENUM")
        {
          $type = "<select name='param" . $obj2->id . "'>";
          $erg3 = QUERY("select id,name,value from featureFunctionEnums where featureFunctionId='$featureFunctionId' and paramId='$obj2->id' order by id");
          while ( $obj3 = mysqli_fetch_OBJECT($erg3) )
          {
            if ($myValue == $obj3->value) $selected = "selected";
            else $selected = "";
            
            $type .= "<option value='$obj3->value' $selected>" . i18n($obj3->name);
          }
          $type .= "</select>";
        }
        else if ($obj2->name == "command")
        {
        	 $type = "<textarea name='param" . $obj2->id . "' rows=6 cols=80>$myValue</textarea></tr>
        	 <tr><td align=right colspan=2><a href='executor.php?id=".$ruleActionIds[$obj2->id]."' target='_blank'>Diesen Befehl aufrufen</a></tr>
        	 <tr><td>";
        }
        else $type = "<input name='param" . $obj2->id . "' type='text' size=9 value='$myValue'>";
        
        $actParamsTag = str_replace("%PARAM_ENTRY%", $type, $actParamsTag);
        $actParamsTag = str_replace("%COMMENT%", $obj2->comment, $actParamsTag);
        $params .= $actParamsTag;
      }
      
      $html = str_replace("%PARAM%", $params, $html);
      
      if ($params == "")
      {
        getTag("%OPT_SUBMIT%", $html);
        $html = str_replace("%OPT_SUBMIT%", "Keine Parameter vorhanden", $html);
      }
      else chooseTag("%OPT_SUBMIT%", $html);
      
      show();
    }
    else die("Fehlerhafte ruleActionId $ruleActionId");
  }
}
else if ($action == "editSignalParams")
{
  if ($submitted == 1)
  {
    QUERY("delete from ruleSignalParams where ruleSignalId='$ruleSignalId'");
    trace("ruleSignalParam mit ruleSignalId $ruleSignalId glöscht");
    
    $erg = QUERY("select id,type from featureFunctionParams where featureFunctionId='$featureFunctionId' order by id");
    while ( $row = mysqli_fetch_ROW($erg) )
    {
      if ($row[1] == "WEEKTIME")
      {
        $value = "param" . $row[0] . "Day";
        $day = $$value;
        $value = "param" . $row[0] . "Hour";
        $hour = $$value;
        $value = "param" . $row[0] . "Minute";
        $minute = $$value;
        $value = toWeekTime($day, $hour, $minute);
      }
      else
      {
        $value = "param" . $row[0];
        $value = $$value;
        
        if ($value=="*" && $row[1]=="BYTE") $value=255;
        else if ($value=="*" && $row[1]=="SBYTE") $value=-1;
        else if ($value=="-1" && $row[1]=="SBYTE") $value=0;
        else if ($value=="*" && $row[1]=="WORD") $value=65535;
      }
      QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) values('$ruleSignalId','$row[0]','$value')");
    }
    
    if ($logicalGroupMode == 1) header("Location: editLogicalSignals.php?groupId=$groupId");
    else header("Location: editRules.php?groupId=$groupId");
    exit();
  }
  else
  {
    setupTreeAndContent("editRuleSignalParams_design.html");
    
    $erg = QUERY("select featureFunctionParamsId,paramValue from ruleSignalParams where ruleSignalId='$ruleSignalId'");
    while ( $obj = mysqli_fetch_object($erg) )
    {
      $myValues[$obj->featureFunctionParamsId] = $obj->paramValue;
    }
    $html = str_replace("%GROUP_ID%", $groupId, $html);
    $html = str_replace("%RULE_SIGNAL_ID%", $ruleSignalId, $html);
    $html = str_replace("%LOGICAL_GROUP_MODE%", $logicalGroupMode, $html);
	
	if ($logicalGroupMode==1) $html = str_replace("editRules.php?dummy=back", "editLogicalSignals.php?", $html);
    
    $erg = QUERY("select featureFunctionId, featureInstanceId from ruleSignals where id='$ruleSignalId' limit 1");
    if ($row = mysqli_fetch_ROW($erg))
    {
      $featureFunctionId = $row[0];
	  $featureInstanceId = $row[1];
      
      $ansicht = $_SESSION["ansicht"];
      $paramTag = getTag("%PARAM%", $html);
      $params = "";
      $erg2 = QUERY("select id,name,type,comment,view from featureFunctionParams where featureFunctionId='$featureFunctionId' order by id");
      while ( $obj2 = mysqli_fetch_OBJECT($erg2) )
      {
        if ($ansicht == "Experte" && $obj2->view == "Entwickler") continue;
        if ($ansicht == "Standard" && ($obj2->view == "Experte" || $obj2->view == "Entwickler")) continue;
        
        $actParamsTag = $paramTag;
        $actParamsTag = str_replace("%PARAM_NAME%", i18n($obj2->name), $actParamsTag);
        
        $myValue = $myValues[$obj2->id];
        if ($obj2->type=="BYTE" && $myValue==255) $myValue="*";
        else if ($obj2->type=="SBYTE" && ($myValue==-1 || $myValue==255)) $myValue="*"; // 255 wegen Kompatibilität zu vorherigem unsigned
        else if ($obj2->type=="WORD" && $myValue==65535) $myValue="*";
        
        if ($obj2->type == "ENUM")
        {
          $type = "<select name='param" . $obj2->id . "'>";
          if ($myValue==255) $type.= "<option value='255' selected>ANY";

          $erg3 = QUERY("select id,name,value from featureFunctionEnums where featureFunctionId='$featureFunctionId' and paramId='$obj2->id' order by id");
          while ( $obj3 = mysqli_fetch_OBJECT($erg3) )
          {
            if ($myValue == $obj3->value) $selected = "selected";
            else $selected = "";
            
            $type .= "<option value='$obj3->value' $selected>" . i18n($obj3->name);
          }
          $type .= "</select>";
        }
        else if ($obj2->type == "WEEKTIME")
        {
          $type = getWeekTime("param" . $obj2->id, $myValue);
        }
        else
        {
          $type = "<input name='param" . $obj2->id . "' type='text' size=9 value='$myValue'>";
        }
        
        $actParamsTag = str_replace("%PARAM_ENTRY%", $type, $actParamsTag);
        $actParamsTag = str_replace("%COMMENT%", $obj2->comment, $actParamsTag);
        $params .= $actParamsTag;
      }
      
      $html = str_replace("%PARAM%", $params, $html);
      
      if ($params == "")
      {
        getTag("%OPT_SUBMIT%", $html);
        $html = str_replace("%OPT_SUBMIT%", "Keine Parameter vorhanden", $html);
      }
      else
        chooseTag("%OPT_SUBMIT%", $html);
      
      $html = str_replace("%FEATURE_FUNCTION_ID%", $featureFunctionId, $html);
      
      show();
    }
    else
      die("Fehlerhafte ruleSignalId $ruleSignalId");
  }
}
else if ($action == "addSignalCopy")
{
  $erg = QUERY("select id from rules where groupId='$groupId' and id<'$ruleId' and `generated`!=1 order by id desc limit 1");
  if ($obj = mysqli_fetch_OBJECT($erg))
  {
    $parentId = $obj->id;
    
    $erg2 = QUERY("select id,featureInstanceId,featureFunctionId from ruleSignals where ruleId='$parentId' order by id");
    while ( $obj2 = mysqli_fetch_OBJECT($erg2) )
    {
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
  		                               values('$ruleId','$obj2->featureInstanceId','$obj2->featureFunctionId')");
      $newRuleSignalId = query_insert_id();
      
      $erg3 = QUERY("select featureFunctionParamsId,paramValue from ruleSignalParams where ruleSignalId='$obj2->id'");
      while ( $obj3 = mysqli_fetch_OBJECT($erg3) )
      {
        QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
     		                                    values('$newRuleSignalId','$obj3->featureFunctionParamsId','$obj3->paramValue')");
      }
    }
  }
}
else if ($action == "addActionCopy")
{
  $erg = QUERY("select id from rules where groupId='$groupId' and id<'$ruleId' and `generated`!=1 order by id desc limit 1");
  if ($obj = mysqli_fetch_OBJECT($erg))
  {
    $parentId = $obj->id;
    
    $erg2 = QUERY("select id,featureInstanceId,featureFunctionId from ruleActions where ruleId='$parentId' order by id");
    while ( $obj2 = mysqli_fetch_OBJECT($erg2) )
    {
      QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
  		                               values('$ruleId','$obj2->featureInstanceId','$obj2->featureFunctionId')");
      $newRuleActionId = query_insert_id();
      
      $erg3 = QUERY("select featureFunctionParamsId,paramValue from ruleActionParams where ruleActionId='$obj2->id'");
      while ( $obj3 = mysqli_fetch_OBJECT($erg3) )
      {
        QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                    values('$newRuleActionId','$obj3->featureFunctionParamsId','$obj3->paramValue')");
      }
    }
  }
}
else if ($action == "copyToClipboard")
{
  $_SESSION["copyRules"] = $groupId;
  $message = "Regeln wurden zum Kopieren markiert";
}
else if ($action == "insertFromClipboard")
{
	$myDebug=0;
	
  if (! isset($_SESSION["copyRules"])) $message = "Keine Regeln zum Einfügen vorhanden";
  else
  {
    $copyGroupId = $_SESSION["copyRules"];
    
    // States nach Name anlegen, falls nicht vorhanden
    $erg = QUERY("select * from rules where groupId='$copyGroupId' and `generated`=0 order by id");
    while ( $obj = mysqli_fetch_OBJECT($erg) )
    {
      if ($obj->activationStateId == "0") $myActivationStateId = 0;
      else
      {
        $erg2 = QUERY("select name,basics from groupStates where id='$obj->activationStateId' limit 1");
        if ($row2 = mysqli_fetch_ROW($erg2))
        {
          $activationStateName = $row2[0];
          $activationBasics = $row2[1];
          
          if ($myDebug==1) echo $activationStateName." - ".$activationBasics."<br>";
          
          $myActivationStateId = - 1;
          $erg2 = QUERY("select id from groupStates where groupId='$groupId' and name='$activationStateName' limit 1");
          if ($row2 = mysqli_fetch_ROW($erg2))
          {
          	$myActivationStateId = $row2[0];
          	if ($myDebug==1) echo "gefunden mit id ".$myActivationStateId."<br>";
          }
          else
          {
          	if ($myDebug==1) echo "nicht gefunden <br>";
            $erg2 = QUERY("Select max(value) from groupStates where groupId='$groupId'");
            if ($row2 = mysqli_fetch_ROW($erg2)) $nextValue = $row2[0] + 1;
            else $nextValue = 0;
            
            $sql = "INSERT into groupStates (groupId, name, value,basics) values('$groupId','" . query_real_escape_string($activationStateName) . "','$nextValue','$activationBasics')";
            if ($myDebug==1) echo $sql."<br>";
            else QUERY($sql);
            $myActivationStateId = query_insert_id();
          }
        }
      }
      
      if ($obj->resultingStateId == "0") $myResultingStateId = 0;
      else
      {
        $erg2 = QUERY("select name,basics from groupStates where id='$obj->resultingStateId' limit 1");
        if ($row2 = mysqli_fetch_ROW($erg2))
        {
          $resultingStateName = $row2[0];
          $resultingBasics = $row2[1];
          
          if ($myDebug==1) echo $resultingStateName." - ".$resultingBasics."<br>";
          
          $myResultingStateId = - 1;
          $erg2 = QUERY("select id from groupStates where groupId='$groupId' and name='$resultingStateName' limit 1");
          if ($row2 = mysqli_fetch_ROW($erg2))
          {
          	$myResultingStateId = $row2[0];
          	if ($myDebug==1) echo "gefunden mit id ".$myResultingStateId."<br>";
          }
          else
          {
          	if ($myDebug==1) echo "nicht gefunden <br>";
          	
            $erg2 = QUERY("Select max(value) from groupStates where groupId='$groupId'");
            if ($row2 = mysqli_fetch_ROW($erg2)) $nextValue = $row2[0] + 1;
            else $nextValue = 0;
            
            $sql = "INSERT into groupStates (groupId, name, value,basics) values('$groupId','" . query_real_escape_string($resultingStateName) . "','$nextValue','$resultingBasics')"; 
            if ($myDebug==1) echo $sql."<br>";
            else QUERY($sql);
            $myResultingStateId = query_insert_id();
          }
        }
      }
      
      $sql = "INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,intraDay) values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','$myActivationStateId','$myResultingStateId','$obj->intraDay')";

      if ($myDebug==1) echo $sql."<br>";                                  
      else QUERY($sql);
      $ruleId = query_insert_id();
      
      // Signale Kopieren
      $erg2 = QUERY("select id,featureInstanceId,featureFunctionId from ruleSignals where ruleId='$obj->id' order by id desc");
      while ( $obj2 = mysqli_fetch_OBJECT($erg2) )
      {
      	$sql = "INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$obj2->featureInstanceId','$obj2->featureFunctionId')";
      	if ($myDebug==1) echo $sql."<br>"; 
        else QUERY($sql);
        $newRuleSignalId = query_insert_id();
        
        $erg3 = QUERY("select featureFunctionParamsId,paramValue from ruleSignalParams where ruleSignalId='$obj2->id'");
        while ( $obj3 = mysqli_fetch_OBJECT($erg3) )
        {
        	$sql="INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) values('$newRuleSignalId','$obj3->featureFunctionParamsId','$obj3->paramValue')";
        	if ($myDebug==1) echo $sql."<br>"; 
          else QUERY($sql);
        }
      }
      
      unset($done);
      // Actions kopieren und umbauen
      $erg2 = QUERY("select id,featureInstanceId,featureFunctionId from ruleActions where ruleId='$obj->id' order by id desc");
      while ( $obj2 = mysqli_fetch_OBJECT($erg2) )
      {
        // Wenn es die gleiche Funktion in der neuen Gruppen gibt, übernehmen wir wie und tauschen die InstanzID aus
        $erg3 = QUERY("select featureClassesId from featureInstances where id='$obj2->featureInstanceId' limit 1");
        $row3 = mysqli_fetch_ROW($erg3);
        $actClassesId = $row3[0];
        
        $foundInstanceId = - 1;
        $erg3 = QUERY("select featureInstanceId from groupFeatures where groupId='$groupId'");
        while ( $row3 = mysqli_fetch_ROW($erg3) )
        {
          $erg4 = QUERY("select featureClassesId from featureInstances where id='$row3[0]' limit 1");
          if ($row4 = mysqli_fetch_ROW($erg4))
          {
            if ($row4[0] == $actClassesId && $done[$row3[0]] != 1)
            {
              $foundInstanceId = $row3[0];
              $done[$row3[0]] = 1;
              break;
            }
          }
        }
        
        if ($foundInstanceId != - 1)
        {
        	$sql = "INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$foundInstanceId','$obj2->featureFunctionId')";
        	if ($myDebug==1) echo $sql."<br>"; 
          else QUERY($sql);
          
          $newRuleActionId = query_insert_id();
          
          $erg3 = QUERY("select featureFunctionParamsId,paramValue from ruleActionParams where ruleActionId='$obj2->id'");
          while ( $obj3 = mysqli_fetch_OBJECT($erg3) )
          {
          	$sql="INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) values('$newRuleActionId','$obj3->featureFunctionParamsId','$obj3->paramValue')";
          	if ($myDebug==1) echo $sql."<br>"; 
            else QUERY($sql);
          }
        }
      }
    }
  }
}
else if ($action == "doubleActions")
{
  $erg = QUERY("select * from rules where groupId='$groupId' order by id");
  while ( $obj = mysqli_fetch_OBJECT($erg) )
  {
    QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,intraDay) 
                        values('$groupId','$obj->startDay','$obj->startHour','$obj->startMinute','$obj->endDay','$obj->endHour','$obj->endMinute','$obj->activationStateId','$obj->resultingStateId','$obj->intraDay')");
    $ruleId = query_insert_id();
    
    // Actions kopieren
    $erg2 = QUERY("select id,featureInstanceId,featureFunctionId from ruleActions where ruleId='$obj->id' order by id");
    while ( $obj2 = mysqli_fetch_OBJECT($erg2) )
    {
      QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
	                              values('$ruleId','$obj2->featureInstanceId','$obj2->featureFunctionId')");
      $newRuleActionId = query_insert_id();
      
      $erg3 = QUERY("select featureFunctionParamsId,paramValue from ruleActionParams where ruleActionId='$obj2->id'");
      while ( $obj3 = mysqli_fetch_OBJECT($erg3) )
      {
        QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
 		                                   values('$newRuleActionId','$obj3->featureFunctionParamsId','$obj3->paramValue')");
      }
    }
  }
}
else if ($action == "addNewRules")
{
  $rolloClassesId = getClassesIdByName("Rollladen");
  $dimmerClassesId = getClassesIdByName("Dimmer");
  $schalterClassesId = getClassesIdByName("Schalter");
  
  // Standardstates anlegen
  $erg = QUERY("select id from groupStates where groupId='$groupId' and name='1' limit 1");
  $row = mysqli_fetch_ROW($erg);
  $firstState = $row[0];
  $erg = QUERY("select id from groupStates where groupId='$groupId' and name='2' limit 1");
  $row = mysqli_fetch_ROW($erg);
  $secondState = $row[0];
  
  // evOn
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType) 
                     values('$groupId','7','31','255','7','31','255','$firstState','$secondState','evOn')");
  $myRuleIds[0] = query_insert_id();
  // evOff
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType) 
                     values('$groupId','7','31','255','7','31','255','$secondState','$firstState','evOff')");
  $myRuleIds[1] = query_insert_id();
  
  $erg = QUERY("select featureInstanceId,featureClassesId
	                    from groupFeatures 
	                    join featureInstances on (featureInstances.id=featureInstanceId) 
	                    where groupId='$groupId'");
  while ( $obj = mysqli_fetch_OBJECT($erg) )
  {
    $featureInstanceId = $obj->featureInstanceId;
    
    // Rollos
    if ($obj->featureClassesId == $rolloClassesId)
    {
      $startFunctionId = getClassesIdFunctionsIdByName($rolloClassesId, "start");
      $stopFunctionId = getClassesIdFunctionsIdByName($rolloClassesId, "stop");
      $paramToOpen = getFunctionParamEnumValueForClassesIdByName($rolloClassesId, "start", "direction", "TO_OPEN");
      $paramToClose = getFunctionParamEnumValueForClassesIdByName($rolloClassesId, "start", "direction", "TO_CLOSE");
      $paramToToggle = getFunctionParamEnumValueForClassesIdByName($rolloClassesId, "start", "direction", "TOGGLE");
      
      // DUMMY bei evOn
      $ruleId = $myRuleIds[0];
      //QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
      

      $evStopFunctionId = getClassesIdFunctionsIdByName($rolloClassesId, "evOpen");
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                                  values('$ruleId','$featureInstanceId','$evStopFunctionId')");
      /*$signalId=query_insert_id();
  
         $rolloParamPositionId = getClassesIdFunctionParamIdByName($rolloClassesId,"evStop","position");
         QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                       values('$signalId','$rolloParamPositionId','0')");
                                       */
      
      // DUMMY bei evOff
      $ruleId = $myRuleIds[1];
      //QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
      

      $evStopFunctionId = getClassesIdFunctionsIdByName($rolloClassesId, "evClosed");
      
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                                  values('$ruleId','$featureInstanceId','$evStopFunctionId')");
      /*$signalId=query_insert_id();

         $rolloParamPositionId = getClassesIdFunctionParamIdByName($rolloClassesId,"evStop","position");
 		     QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                       values('$signalId','$rolloParamPositionId','255')");
                                       */
    }
    // Dimmer
    else if ($obj->featureClassesId == $dimmerClassesId)
    {
      // Dummy für evOn                              
      $ruleId = $myRuleIds[0];
      //QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
      

      $evOnFunctionId = getClassesIdFunctionsIdByName($dimmerClassesId, "evOn");
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                                  values('$ruleId','$featureInstanceId','$evOnFunctionId')");
      $signalId = query_insert_id();
      
      $dimmerParamBrightnessId = getClassesIdFunctionParamIdByName($dimmerClassesId, "evOn", "brightness");
      QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                       values('$signalId','$dimmerParamBrightnessId','$signalParamWildcard')");
      
      // DUMMY für evOff
      $ruleId = $myRuleIds[1];
      //QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
      

      $evOffFunctionId = getClassesIdFunctionsIdByName($dimmerClassesId, "evOff");
      
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
 	                                values('$ruleId','$featureInstanceId','$evOffFunctionId')");
    }
    // Schalter
    else if ($obj->featureClassesId == $schalterClassesId)
    {
      // DUMMY bei evOn
      $ruleId = $myRuleIds[0];
      //QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
      

      $evOnFunctionId = getClassesIdFunctionsIdByName($schalterClassesId, "evOn");
      
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                                  values('$ruleId','$featureInstanceId','$evOnFunctionId')");
      
      // DUMMY bei evOff
      $ruleId = $myRuleIds[1];
      //QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
      

      $evOffFunctionId = getClassesIdFunctionsIdByName($schalterClassesId, "evOff");
      
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                                  values('$ruleId','$featureInstanceId','$evOffFunctionId')");
    }
  } //while
}
else if ($action == "defaultActions")
{
  // deprecated
  

  if ($confirm != 1)
  {
    $message = "Achtung!<br><br>Wenn Sie die Standardaktionen wiederherstellen, werden alle aktuellen Regeln zu diesem Aktor gelöscht und dann der Standard erstellt.<br><br><a href='editRules.php?groupId=$groupId&confirm=1&action=defaultActions&template=$template'>Ja, Standardregeln herstellen</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='editRules.php?groupId=$groupId'>Nein, zurück</a>";
    setupTreeAndContent("", $message);
    show();
  }
  
  // Alles löschen  		 	   
  QUERY("delete from groupStates where groupId='$groupId'");
  $erg = QUERY("select id from rules where groupId='$groupId'");
  while ( $row = mysqli_fetch_ROW($erg) )
  {
    deleteRule($row[0]);
  }
  
  // Standardstates anlegen
  QUERY("INSERT into groupStates (groupId, name) values('$groupId','1')");
  $firstState = query_insert_id();
  QUERY("INSERT into groupStates (groupId, name) values('$groupId','2')");
  $secondState = query_insert_id();
  
  $rolloClassesId = getClassesIdByName("Rollladen");
  $dimmerClassesId = getClassesIdByName("Dimmer");
  $schalterClassesId = getClassesIdByName("Schalter");
  
  // Standardregeln 
      // click
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType) 
                     values('$groupId','7','31','255','7','31','255','$firstState','$secondState','click')");
  $myRuleIds[0] = query_insert_id();
  // click
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType) 
                     values('$groupId','7','31','255','7','31','255','$secondState','$firstState','click')");
  $myRuleIds[1] = query_insert_id();
  
  // hold start
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType) 
                     values('$groupId','7','31','255','7','31','255','0','0','holdStart')");
  $myRuleIds[2] = query_insert_id();
  
  // hold end
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType) 
                     values('$groupId','7','31','255','7','31','255','0','0','holdEnd')");
  $myRuleIds[3] = query_insert_id();
  
  // doubleClick
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType) 
                     values('$groupId','7','31','255','7','31','255','0','0','doubleClick')");
  $myRuleIds[4] = query_insert_id();
  
  // covered
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType) 
                     values('$groupId','7','31','255','7','31','255','$firstState','$secondState','covered')");
  $myRuleIds[5] = query_insert_id();
  // covered
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType) 
                     values('$groupId','7','31','255','7','31','255','$secondState','$firstState','covered')");
  $myRuleIds[6] = query_insert_id();
  
  // evOn
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType) 
                     values('$groupId','7','31','255','7','31','255','$firstState','$secondState','evOn')");
  $myRuleIds[7] = query_insert_id();
  // evOff
  QUERY("INSERT into rules (groupId,startDay,startHour,startMinute,endDay,endHour,endMinute,activationStateId,resultingStateId,signalType) 
                     values('$groupId','7','31','255','7','31','255','$secondState','$firstState','evOff')");
  $myRuleIds[8] = query_insert_id();
  
  $isHomogenous = 1;
  //$mySingleClassesId=-1;
  $last = - 1;
  $erg = QUERY("select featureInstanceId,featureClassesId
	                    from groupFeatures 
	                    join featureInstances on (featureInstances.id=featureInstanceId) 
	                    where groupId='$groupId'");
  while ( $obj = mysqli_fetch_OBJECT($erg) )
  {
    //if($mySingleClassesId!=-1) $mySingleClassesId=-2;
      //else $mySingleClassesId=$obj->featureClassesId;
    

    if ($last == - 1)
      $last = $obj->featureClassesId;
    else if ($last != $obj->featureClassesId)
    {
      $isHomogenous = 0;
      break;
    }
  }
  
  $erg = QUERY("select featureInstanceId,featureClassesId
	                    from groupFeatures 
	                    join featureInstances on (featureInstances.id=featureInstanceId) 
	                    where groupId='$groupId'");
  while ( $obj = mysqli_fetch_OBJECT($erg) )
  {
    $featureInstanceId = $obj->featureInstanceId;
    
    // Rollos
    if ($obj->featureClassesId == $rolloClassesId)
    {
      $startFunctionId = getClassesIdFunctionsIdByName($rolloClassesId, "start");
      $stopFunctionId = getClassesIdFunctionsIdByName($rolloClassesId, "stop");
      $paramToOpen = getFunctionParamEnumValueForClassesIdByName($rolloClassesId, "start", "direction", "TO_OPEN");
      $paramToClose = getFunctionParamEnumValueForClassesIdByName($rolloClassesId, "start", "direction", "TO_CLOSE");
      $paramToToggle = getFunctionParamEnumValueForClassesIdByName($rolloClassesId, "start", "direction", "TOGGLE");
      
      if ($template == "1")
      {
        // Hochfahren bei Click (beide Regeln gleich belegen)
        $ruleId = $myRuleIds[0];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
     		                                  values('$ruleId','$featureInstanceId','$startFunctionId')");
        $newRuleActionId = query_insert_id();
        QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                       values('$newRuleActionId','142','$paramToOpen')");
        
        // Hochfahren bei Click (beide Regeln gleich belegen)
        $ruleId = $myRuleIds[1];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
     		                                  values('$ruleId','$featureInstanceId','$startFunctionId')");
        $newRuleActionId = query_insert_id();
        QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                       values('$newRuleActionId','142','$paramToOpen')");
        
        // Runterfahren bei holdStart
        $ruleId = $myRuleIds[2];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
      		                               values('$ruleId','$featureInstanceId','$startFunctionId')");
        $newRuleActionId = query_insert_id();
        QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                       values('$newRuleActionId','142','$paramToClose')");
        
        // Nichts zun bei HoldEnd
      

        // STOP bei doppelclick
        $ruleId = $myRuleIds[4];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
       		                                values('$ruleId','$featureInstanceId','$stopFunctionId')");
      }
      else if ($template == "2")
      {
        // Hochfahren (beide regeln gleich belegen)
        $ruleId = $myRuleIds[0];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
     		                                  values('$ruleId','$featureInstanceId','$startFunctionId')");
        $newRuleActionId = query_insert_id();
        QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                       values('$newRuleActionId','142','$paramToOpen')");
        
        // Hochfahren (beide regeln gleich belegen)
        $ruleId = $myRuleIds[1];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
     		                                  values('$ruleId','$featureInstanceId','$startFunctionId')");
        $newRuleActionId = query_insert_id();
        QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                       values('$newRuleActionId','142','$paramToOpen')");
        
        // TOGGLE bei HoldStart
        $ruleId = $myRuleIds[2];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
     		                                  values('$ruleId','$featureInstanceId','$startFunctionId')");
        $newRuleActionId = query_insert_id();
        QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                       values('$newRuleActionId','142','$paramToToggle')");
        
        // STOP bei HoldEnd
        $ruleId = $myRuleIds[3];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
       		                                values('$ruleId','$featureInstanceId','$stopFunctionId')");
        
        // Runterfahren bei Doppelclick
        $ruleId = $myRuleIds[4];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
      		                               values('$ruleId','$featureInstanceId','$startFunctionId')");
        $newRuleActionId = query_insert_id();
        QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                       values('$newRuleActionId','142','$paramToClose')");
      }
      else
        die("Unbekanntes Template $template");
        
        // DUMMY bei evOn
      $ruleId = $myRuleIds[7];
      //QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
      

      $evStopFunctionId = getClassesIdFunctionsIdByName($rolloClassesId, "evOpen");
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                                 values('$ruleId','$featureInstanceId','$evStopFunctionId')");
      /*$signalId=query_insert_id();
  
        $rolloParamPositionId = getClassesIdFunctionParamIdByName($rolloClassesId,"evStop","position");
        QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                      values('$signalId','$rolloParamPositionId','0')");
                                      */
      
      // DUMMY bei evOff
      $ruleId = $myRuleIds[8];
      //QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
      

      $evStopFunctionId = getClassesIdFunctionsIdByName($rolloClassesId, "evClosed");
      
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                                 values('$ruleId','$featureInstanceId','$evStopFunctionId')");
      /*$signalId=query_insert_id();

        $rolloParamPositionId = getClassesIdFunctionParamIdByName($rolloClassesId,"evStop","position");
 		    QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                      values('$signalId','$rolloParamPositionId','255')");
                                      */
    }
    // Dimmer
    else if ($obj->featureClassesId == $dimmerClassesId)
    {
      // 100% hell bei Click
      $ruleId = $myRuleIds[0];
      
      QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
      		                               values('$ruleId','$featureInstanceId','25')");
      $newRuleActionId = query_insert_id();
      
      QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                    values('$newRuleActionId','90','100')");
      QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                    values('$newRuleActionId','91','0')");
      
      // 0% hell bei Click
      $ruleId = $myRuleIds[1];
      QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
     		                               values('$ruleId','$featureInstanceId','25')");
      $newRuleActionId = query_insert_id();
      QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                    values('$newRuleActionId','90','0')");
      QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                    values('$newRuleActionId','91','0')");
      
      // DIMMEN Start bei HoldStart
      $ruleId = $myRuleIds[2];
      QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
     		                          values('$ruleId','$featureInstanceId','64')");
      $newRuleActionId = query_insert_id();
      QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                                    values('$newRuleActionId','115','0')");
      
      // DIMMEN Ende bei HoldEnd
      $ruleId = $myRuleIds[3];
      QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
      		                        values('$ruleId','$featureInstanceId','65')");
      
      // 50% Helligkeit bei Doppelclick
      $ruleId = $myRuleIds[4];
      QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId)
      	                          values('$ruleId','$featureInstanceId','25')");
      $newRuleActionId = query_insert_id();
      QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                               values('$newRuleActionId','90','50')");
      QUERY("INSERT into ruleActionParams (ruleActionId,featureFunctionParamsId,paramValue) 
     		                               values('$newRuleActionId','91','0')");
      
      // Dummy für evOn                              
      $ruleId = $myRuleIds[7];
      //QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
      

      $evOnFunctionId = getClassesIdFunctionsIdByName($dimmerClassesId, "evOn");
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                                  values('$ruleId','$featureInstanceId','$evOnFunctionId')");
      $signalId = query_insert_id();
      
      $dimmerParamBrightnessId = getClassesIdFunctionParamIdByName($dimmerClassesId, "evOn", "brightness");
      QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                       values('$signalId','$dimmerParamBrightnessId','$signalParamWildcard')");
      
      // DUMMY für evOff
      $ruleId = $myRuleIds[8];
      //QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
      

      $evOffFunctionId = getClassesIdFunctionsIdByName($dimmerClassesId, "evOff");
      
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
 	                                values('$ruleId','$featureInstanceId','$evOffFunctionId')");
    }
    // Schalter
    else if ($obj->featureClassesId == $schalterClassesId)
    {
      if ($isHomogenous == 1)
      {
        // Einschalten bei Covered
        $ruleId = $myRuleIds[5];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','60')");
        
        // Ausschalten bei Covered
        $ruleId = $myRuleIds[6];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','61')");
      }
      else
      {
        // Einschalten bei Clicked
        $ruleId = $myRuleIds[0];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','60')");
        
        // Ausschalten bei Clicked
        $ruleId = $myRuleIds[1];
        QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','61')");
      }
      
      // DUMMY bei evOn
      $ruleId = $myRuleIds[7];
      //QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
      

      $evOnFunctionId = getClassesIdFunctionsIdByName($schalterClassesId, "evOn");
      
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                                  values('$ruleId','$featureInstanceId','$evOnFunctionId')");
      
      // DUMMY bei evOff
      $ruleId = $myRuleIds[8];
      //QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
      

      $evOffFunctionId = getClassesIdFunctionsIdByName($schalterClassesId, "evOff");
      
      QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                                  values('$ruleId','$featureInstanceId','$evOffFunctionId')");
    }
  } //while
  

  /*
  if ($mySingleClassesId>0)
  {
  // DUMMY PING bei evOn
  $ruleId=$myRuleIds[7];
  QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");
    
	if ($mySingleClassesId==$dimmerClassesId)
  {
    $evOnFunctionId = getClassesIdFunctionsIdByName($dimmerClassesId,"evOn");
         
 	  QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                             values('$ruleId','$featureInstanceId','$evOnFunctionId')");
    $signalId=query_insert_id();

    $dimmerParamBrightnessId = getClassesIdFunctionParamIdByName($dimmerClassesId,"evOn","brightness");
    QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                  values('$signalId','$dimmerParamBrightnessId','100')");
  }
  else if ($mySingleClassesId==$schalterClassesId)
  {
    $evOnFunctionId = getClassesIdFunctionsIdByName($schalterClassesId,"evOn");
         
 		 QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                              values('$ruleId','$featureInstanceId','$evOnFunctionId')");
  }
  else if ($mySingleClassesId==$rolloClassesId)
  {
    $evStopFunctionId = getClassesIdFunctionsIdByName($rolloClassesId,"evStop");
       
 	 QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                            values('$ruleId','$featureInstanceId','$evStopFunctionId')");
   $signalId=query_insert_id();
  
   $rolloParamPositionId = getClassesIdFunctionParamIdByName($rolloClassesId,"evStop","position");
   QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                 values('$signalId','$rolloParamPositionId','0')");
  }
  
  // DUMMY PING bei evOff
  $ruleId=$myRuleIds[8];
  QUERY("INSERT into ruleActions (ruleId,featureInstanceId,featureFunctionId) values('$ruleId','$featureInstanceId','-1')");

 if ($mySingleClassesId==$dimmerClassesId)
	 {
 	 	  $evOffFunctionId = getClassesIdFunctionsIdByName($dimmerClassesId,"evOff");
         
	   	 QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
 	                                values('$ruleId','$featureInstanceId','$evOffFunctionId')");
   }
   else if ($mySingleClassesId==$schalterClassesId)
	 {
	 	  $evOffFunctionId = getClassesIdFunctionsIdByName($schalterClassesId,"evOff");
         
	   	 QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                                values('$ruleId','$featureInstanceId','$evOffFunctionId')");
   }
  else if ($mySingleClassesId==$rolloClassesId)
	 {
	 	  $evStopFunctionId = getClassesIdFunctionsIdByName($rolloClassesId,"evStop");
         
   	 QUERY("INSERT into ruleSignals (ruleId,featureInstanceId,featureFunctionId)
                               values('$ruleId','$featureInstanceId','$evStopFunctionId')");
     $signalId=query_insert_id();

     $rolloParamPositionId = getClassesIdFunctionParamIdByName($rolloClassesId,"evStop","position");
 		 QUERY("INSERT into ruleSignalParams (ruleSignalId,featureFunctionParamsId,paramValue) 
                                   values('$signalId','$rolloParamPositionId','255')");
   }
  }
  */
  
  // Regeln löschen, die keine Aktions bekommen haben
  for($i = 0; $i < count($myRuleIds); $i++)
  {
    $ruleId = $myRuleIds[$i];
    $erg = QUERY("select count(*) from ruleActions where ruleId='$ruleId'");
    while ( $row = mysqli_fetch_ROW($erg) )
    {
      if ($row[0] == 0)
        deleteRule($ruleId);
    }
  }
  
  // Optimierung für clicked regeln
  unset($firstActions);
  $i = 0;
  $erg = QUERY("select featureInstanceId, featureFunctionId,paramValue from ruleActions left join ruleActionParams on(ruleActionParams.ruleActionId=ruleActions.id) where ruleId='$myRuleIds[0]'");
  while ( $obj = mysqli_fetch_OBJECT($erg) )
  {
    $firstActions[$i++] = $obj;
  }
  
  //print_r($firstActions);
  

  $isEqual = 1;
  $i = 0;
  $erg = QUERY("select featureInstanceId, featureFunctionId,paramValue from ruleActions left join ruleActionParams on(ruleActionParams.ruleActionId=ruleActions.id) where ruleId='$myRuleIds[1]'");
  while ( $obj = mysqli_fetch_OBJECT($erg) )
  {
    if ($firstActions[$i]->featureInstanceId != $obj->featureInstanceId)
      $isEqual = 0;
    if ($firstActions[$i]->featureFunctionId != $obj->featureFunctionId)
      $isEqual = 0;
    if ($firstActions[$i]->paramValue != $obj->paramValue)
      $isEqual = 0;
    
    if ($isEqual == 0)
    {
      //print_r($obj);
      break;
    }
    $i++;
  }
  
  //echo "A".$isEqual."<br>";  
  

  if ($isEqual == 1)
  {
    // States bei Regel 0 auf all stellen
    QUERY("UPDATE rules set activationStateId='0', resultingStateId='0' where id='$myRuleIds[0]' limit 1");
    
    // Regel 1 löschen
    deleteRule($myRuleIds[1]);
  }
}
else if ($action=="forceController") QUERY("update groups set forcedController='$forceController' where id='$groupId' limit 1");

if ($logicalGroupMode == 1)
{
  header("Location: editLogicalSignals.php?groupId=$groupId");
  exit();
}

if ($ajax==1) exit;

setupTreeAndContent("editRules_design.html", $message);

$html = str_replace("%GROUP_ID%", $groupId, $html);

$last = "";
$hasLed = 0;
$hasRollo = 0;
$rolloClassesId = getClassesIdByName("Rollladen");
$ledClassesId = getClassesIdByName("Led");

$erg = QUERY("select forcedController, single from groups where id='$groupId' limit 1");
$obj = MYSQLi_FETCH_OBJECT($erg);
$forcedController = $obj->forcedController;
$groupIsSingle=$obj->single;

$erg = QUERY("select featureInstanceId,
	                    featureClassesId
 	                    from groupFeatures 
	                    join featureInstances on (featureInstances.id=featureInstanceId) 
						join groups on (groups.id=groupFeatures.groupId)
	                    where groupId='$groupId'");
while ( $obj = mysqli_fetch_OBJECT($erg) )
{
  if ($obj->featureClassesId == $rolloClassesId) $hasRollo = 1;
  if ($obj->featureClassesId == $ledClassesId) $hasLed = 1;
}



$allFeatureInstances = readFeatureInstances("featureClassesId,name");
$allFeatureClasses = readFeatureClasses();
$allFeatureFunctions = readFeatureFunctions();
$allRooms = readRooms();
$allRoomFeatures = readRoomFeatures();
$allGroupStates = readGroupStates();

$allMyStateIds="";
$allMyStateNames="";
foreach ( (array)$allGroupStates as $obj )
{
  if ($obj->groupId == $groupId)
  {
    if ($possibleValues != "")
    {
      $possibleValues .= ",";
      $possibleNames .= ",";
    }
    $possibleValues .= $obj->id;
    $possibleNames .= $obj->name;
	
	if ($allMyStateIds!="")
	{
		$allMyStateIds.=",";
		$allMyStateNames.=",";
	}
	$allMyStateIds.=$obj->id;
	$allMyStateNames.=$obj->name;
  }
}

$html = str_replace("%SET_STATE_OPTIONS%", getSelect(0, $allMyStateIds, $allMyStateNames), $html);

$allRuleActions = readRuleActions();
$allRuleSignals = readRuleSignals();

$rulesTag = getTag("%RULES%", $html);

$evTimeFunctionId = getClassesIdFunctionsIdByName($CONTROLLER_CLASSES_ID, "evTime");
$evDayFunctionId = getClassesIdFunctionsIdByName($CONTROLLER_CLASSES_ID, "evDay");
$evNightFunctionId = getClassesIdFunctionsIdByName($CONTROLLER_CLASSES_ID, "evNight");

$rules = "";
$first = 1;
$whereForControllerSelect="1=2 ";
$alternativeWhereForControllerSelect="1=2 ";
$erg99 = QUERY("select * from rules where groupId='$groupId' order by baseRule,id");
while ( $obj99 = mysqli_fetch_OBJECT($erg99) )
{
  $actTag = $rulesTag;
  
  if ($obj99->baseRule == 1)
  {
    getTag("%OPT_DELETE%", $actTag);
    $actTag = str_replace("%OPT_DELETE%", "<font size=1>BASIS-<br>REGEL", $actTag);
  }
  else
    chooseTag("%OPT_DELETE%", $actTag);
  
  if ($first == 1)
  {
    $first = 0;
    getTag("%OPT_COPY_SIGNAL%", $actTag);
    $actTag = str_replace("%OPT_COPY_SIGNAL%", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $actTag);
    getTag("%OPT_COPY_ACTION%", $actTag);
    $actTag = str_replace("%OPT_COPY_ACTION%", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", $actTag);
  }
  else
  {
    chooseTag("%OPT_COPY_SIGNAL%", $actTag);
    chooseTag("%OPT_COPY_ACTION%", $actTag);
  }
  
  $actTag = str_replace("%RULE_ID%", $obj99->id, $actTag);
  
  if ($activeStateId==$obj99->activationStateId) $selectMarked="style='background-color: #ffff00'";
  else $selectMarked="";
  $actTag = str_replace("%ACTIVATION_STATE_MARK%", $selectMarked, $actTag);
  
  $actTag = str_replace("%ACTIVATION_STATE_OPTIONS%", getSelect($obj99->activationStateId, "," . $possibleValues, "alle," . $possibleNames), $actTag);

  if ($activeStateId==$obj99->resultingStateId) $selectMarked="style='background-color: #ffff00'";
  else $selectMarked="";
  $actTag = str_replace("%RESULTING_STATE_MARK%", $selectMarked, $actTag);

  $actTag = str_replace("%RESULTING_STATE_OPTIONS%", getSelect($obj99->resultingStateId, "," . $possibleValues, "gleich," . $possibleNames), $actTag);
  
  if ($obj99->startDay >= 10)
  {
    $obj99->endDay = 7;
    $obj99->startHour = 31;
    $obj99->startMinute = 255;
    $obj99->endHour = 31;
    $obj99->endMinute = 255;
  }
  
  $actTag = str_replace("%TIME_START_DAY_OPTIONS%", getSelect($obj99->startDay, "7,0,1,2,3,4,5,6", "Immer,Mo.,Di.,Mi.,Do.,Fr.,Sa.,So."), $actTag);
  $actTag = str_replace("%TIME_END_DAY_OPTIONS%", getSelect($obj99->endDay, "7,0,1,2,3,4,5,6", "Immer,Mo.,Di.,Mi.,Do.,Fr.,Sa.,So."), $actTag);
  
  $options = getTimeOptionsHour($obj99->startHour);
  $actTag = str_replace("%TIME_START_TIME_OPTIONS%", $options, $actTag);

  $options = getTimeOptionsMinute($obj99->startMinute);
  $actTag = str_replace("%TIME_START_TIME_MINUTE_OPTIONS%", $options, $actTag);
  
  $options = getTimeOptionsHour($obj99->endHour);
  $actTag = str_replace("%TIME_END_TIME_OPTIONS%", $options, $actTag);

  $options = getTimeOptionsMinute($obj99->endMinute);
  $actTag = str_replace("%TIME_END_TIME_MINUTE_OPTIONS%", $options, $actTag);
  
  if ($obj99->groupLock==1) $locked="checked";
  else $locked="";
  $actTag = str_replace("%LOCK_CHECKED%", $locked, $actTag);

  if ($obj99->intraDay==1) $checked="checked";
  else $checked="";
  $actTag = str_replace("%INTRADAY_CHECKED%", $checked, $actTag);
  
  $signalsTag = getTag("%SIGNALS%", $actTag);
  
  $signals = "";
  $andConditions = "";
  foreach ( (array)$allRuleSignals as $obj )
  {
    if ($obj->ruleId == $obj99->id)
    {
      $actSignalTag = $signalsTag;
      $actSignalTag = str_replace("%SIGNAL_ID%", $obj->id, $actSignalTag);
	  
	  if ($obj->featureFunctionId==270)
	  {
		  $sytemVariableContent = getSystemVariableDataForSignal($obj->id);
		  $systemVariableName = $sytemVariableContent->name;
		  $systemVariableValue = $sytemVariableContent->value;

		  $actSignalTag = str_replace("%SIGNAL_ROOM%", "<b>UND</b>", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_CLASS%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_NAME%", $systemVariableName, $actSignalTag);
          $actSignalTag = str_replace("%FEATURE_FUNCTION_ID%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_FUNCTION%",  $systemVariableValue, $actSignalTag);
	  }
	  else
	  {
        $actClassesId = getClassesIdByFeatureInstanceId($obj->featureInstanceId);
      
        if ($obj->featureInstanceId < 0) //gruppenSignale
        {
          $signalGroupId = $obj->featureInstanceId * - 1;
        
          $erg34 = QUERY("select eventType,groups.name as groupName, groupType, andCondition from  basicRuleGroupSignals left join groups on (groups.id=basicRuleGroupSignals.groupId) where basicRuleGroupSignals.id='$signalGroupId' limit 1");
          $obj34 = mysqli_fetch_OBJECT($erg34);

          if ($obj34->groupType == "SIGNALS-AND") $groupType = "UG";
          else if ($obj34->groupType == "SIGNALS-OR") $groupType = "OG";
          else $groupType = "??";

          if ($obj34->eventType == "ACTIVE") $action = "Erreichen";
          else if ($obj34->eventType == "DEACTIVE") $action = "Verlassen";
          else $action = "Unbekannt";
        
          
          $actSignalTag = str_replace("%SIGNAL_ROOM%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_CLASS%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_NAME%", "</a>Logische Grupppe " . $obj34->groupName, $actSignalTag);
          $actSignalTag = str_replace("%FEATURE_FUNCTION_ID%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_FUNCTION%", $action . " des aktiven Zustandes", $actSignalTag);
        }
        else if ($actClassesId == $CONTROLLER_CLASSES_ID && $obj->featureFunctionId == $evTimeFunctionId) // evTime vom controller
        {
          $actSignalTag = str_replace("%SIGNAL_ROOM%", "", $actSignalTag);
        
          $erg = QUERY("select paramValue from ruleSignalParams where ruleSignalId='$obj->id' limit 1");
          if ($row = mysqli_fetch_ROW($erg)) $weekTime = parseWeekTimeToObject($row[0]);
        
          $actSignalTag = str_replace("%SIGNAL_CLASS%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_NAME%", "Zeitgesteuert", $actSignalTag);
          $actSignalTag = str_replace("%FEATURE_FUNCTION_ID%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_FUNCTION%", $weekTime->tag . " " . $weekTime->stunde . ":" . $weekTime->minute, $actSignalTag);
        }
        else if ($actClassesId == $CONTROLLER_CLASSES_ID && $obj->featureFunctionId == $evDayFunctionId)
        {
          $actSignalTag = str_replace("%SIGNAL_ROOM%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_CLASS%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_NAME%", "Zeitgesteuert", $actSignalTag);
          $actSignalTag = str_replace("%FEATURE_FUNCTION_ID%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_FUNCTION%", "Bei Sonnenaufgang", $actSignalTag);
        }
        else if ($actClassesId == $CONTROLLER_CLASSES_ID && $obj->featureFunctionId == $evNightFunctionId)
        {
          $actSignalTag = str_replace("%SIGNAL_ROOM%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_CLASS%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_NAME%", "Zeitgesteuert", $actSignalTag);
          $actSignalTag = str_replace("%FEATURE_FUNCTION_ID%", "", $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_FUNCTION%", "Bei Sonnenuntergang", $actSignalTag);
        }
        else
        {
          $myFeatureInstance = $allFeatureInstances[$obj->featureInstanceId];
          $myRoom = getRoomForFeatureInstance($obj->featureInstanceId);
          $add = "";
          if ($obj->groupAlias > 0) $add = "R ".$obj->groupAlias." ";
          $actSignalTag = str_replace("%SIGNAL_ROOM%", $add . $myRoom->name, $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_CLASS%", i18n($allFeatureClasses[$myFeatureInstance->featureClassesId]->name), $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_NAME%", $myFeatureInstance->name, $actSignalTag);
          $actSignalTag = str_replace("%FEATURE_FUNCTION_ID%", $obj->featureFunctionId, $actSignalTag);
          $actSignalTag = str_replace("%SIGNAL_FUNCTION%", i18n($allFeatureFunctions[$obj->featureFunctionId]->name), $actSignalTag);
        }
	  }
      
      $signals .= $actSignalTag;
	  
	  $alternativeWhereForControllerSelect.=" or featureInstances.id='".$obj->featureInstanceId."'";

    }
  }
  
  if ($andConditions!="") $signals.="<hr>".$andConditions;
  
  $actTag = str_replace("%SIGNALS%", $signals, $actTag);
  
  $actionsTag = getTag("%ACTIONS%", $actTag);
  $actions = "";
  foreach ( $allRuleActions as $obj )
  {
    if ($obj->ruleId == $obj99->id)
    {
        $actActionTag = $actionsTag;
        $actActionTag = str_replace("%ACTION_ID%", $obj->id, $actActionTag);
      
        $myFeatureInstance = $allFeatureInstances[$obj->featureInstanceId];
        $myRoom = getRoomForFeatureInstance($obj->featureInstanceId);
      
        $actActionTag = str_replace("%ACTION_ROOM%", $myRoom->name, $actActionTag);
        $actActionTag = str_replace("%ACTION_CLASS%", $allFeatureClasses[$myFeatureInstance->featureClassesId]->name, $actActionTag);
        $actActionTag = str_replace("%ACTION_NAME%", $myFeatureInstance->name, $actActionTag);
        $actActionTag = str_replace("%FEATURE_FUNCTION_ID%", $obj->featureFunctionId, $actActionTag);
        $actActionTag = str_replace("%ACTION_FUNCTION%", i18n($allFeatureFunctions[$obj->featureFunctionId]->name), $actActionTag);
		
		$whereForControllerSelect.=" or featureInstances.id='".$obj->featureInstanceId."'";
      
      $actions .= $actActionTag;
    }
  }
  $actTag = str_replace("%ACTIONS%", $actions, $actTag);
  
  $rules .= $actTag;
}

if ($groupIsSingle==1) removeTag("%OPT_CONTROLLER_VORGABE%",$html);
else
{
  chooseTag("%OPT_CONTROLLER_VORGABE%",$html);
  $controllerOptions="";
  $found=0;
  if (strlen($whereForControllerSelect)<5) $whereForControllerSelect=$alternativeWhereForControllerSelect;
  
  $erg = QUERY("select distinct(controller.id), controller.name from controller join featureInstances on (featureInstances.controllerId=controller.id) where ($whereForControllerSelect) and size!=999 order by controller.name");
  while($obj=MYSQLi_FETCH_OBJECT($erg))
  { 
	$selected="";
	if ($obj->id==$forcedController)
	{
		$selected="selected";
		$found=1;
	}
	$controllerOptions.="<option value='$obj->id' $selected>$obj->name";
  }
  if ($found==1) $selected="";
  else $selected="selected";
  $controllerOptions="<option value='0' $selected>AUTO".$controllerOptions;

  $html = str_replace("%CONTROLLER_OPTIONS%", $controllerOptions, $html);
}

$host="-";
$erg = QUERY("select controller.name from controller join groups on (groups.groupController=controller.id) where groups.id='$groupId' limit 1");
if ($row=MYSQLi_FETCH_ROW($erg)) $host=$row[0];
$html = str_replace("%HOST%", $host, $html);



$html = str_replace("%RULES%", $rules, $html);

show();

function parseGroup($rulesBytes, $rulesBytesPos, &$regeln, $ruleVersion)
{
  $myPos = 0;
  
  $numOfRules = $rulesBytes[$myPos++];
  $regeln .= "Anzahl Regeln: $numOfRules - Regelversion: $ruleVersion<hr>";
  
  for($i = 0; $i < $numOfRules; $i++)
  {
    $regeln .= "<b>Regel $i</b><br>";
    $ok = parseRule($rulesBytes, $myPos, $rulesBytesPos, $regeln, $ruleVersion);
    if (! $ok) return FALSE;
  }
  
  return TRUE;
}

function parseRule($rulesBytes, &$myPos, $rulesBytesPos, &$result, $ruleVersion)
{
  $errorCounter = 0;
  while ( 1 && $errorCounter < 100 )
  {
    $errorCounter++;
    $ok = parseRuleElement($rulesBytes, $myPos, $rulesBytesPos, $result, $ruleVersion);
    if (! $ok) return FALSE;
    if ($myPos >= $rulesBytesPos) return FALSE;
    
    if ($rulesBytes[$myPos] == 0)
    {
      $myPos++;
      return TRUE;
    }
  }
}

function parseRuleElement($rulesBytes, &$myPos, $rulesBytesPos, &$result, $ruleVersion)
{
  if ($myPos >= $rulesBytesPos) return FALSE;
  $numOfConditions = $rulesBytes[$myPos++];
  $result .= "numOfConditions = $numOfConditions <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
  $numOfActions = $rulesBytes[$myPos++];
  $result .= "numOfActions = $numOfActions <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
  $triggerState = $rulesBytes[$myPos++];
  $result .= "triggerState = $triggerState <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
  $nextState = $rulesBytes[$myPos++];
  $result .= "nextState = $nextState <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
   
  $startMinute = $rulesBytes[$myPos++];
  $result .= "startMinute = $startMinute <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
  $startHour = $rulesBytes[$myPos++];
  $startDay = ($startHour & 0xE0) >> 5;
  $startHour = $startHour & 0x1F;
  $result .= "startHour = $startHour <br>";
  $result .= "startDay = $startDay <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
  
  $endMinute = $rulesBytes[$myPos++];
  $result .= "endMinute = $endMinute <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
  $endHour = $rulesBytes[$myPos++];
  $endDay = ($endHour & 0xE0) >> 5;
  $endHour = $endHour & 0x1F;
  $result .= "endHour = $endHour <br>";
  $result .= "endDay = $endDay <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
  
  for($i = 0; $i < $numOfConditions; $i++)
  {
    $ok = parseCondition($rulesBytes, $myPos, $rulesBytesPos, $result);
    if (! $ok) return FALSE;
  }
  
  for($i = 0; $i < $numOfActions; $i++)
  {
    $ok = parseAction($rulesBytes, $myPos, $rulesBytesPos, $result,$ruleVersion);
    if (! $ok) return FALSE;
  }
  
  $result .= "<hr>";
  return true;
}

function parseCondition($rulesBytes, &$myPos, $rulesBytesPos, &$result)
{
  if ($myPos > $rulesBytesPos - 4) return FALSE;
  $sender = bytesToDword($rulesBytes, $myPos);
  $result .= "Sender = " .$sender." (0x".dechex($sender).") [".getNameForObjectId($sender)."]<br>";
  //$myPos+=4;
  if ($myPos >= $rulesBytesPos) return FALSE;
  $eventId = $rulesBytes[$myPos++];
  
  $result .= "eventId = $eventId [".getNameForFunctionOrEvent($sender, $eventId)."] <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
  $param = $rulesBytes[$myPos++];
  
  $result .= "param = $param <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
  $param = $rulesBytes[$myPos++];
  
  $result .= "param = $param <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
  $param = $rulesBytes[$myPos++];
  
  $result .= "param = $param <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
  $param = $rulesBytes[$myPos++];
  
  $result .= "param = $param <br>";
  if ($myPos >= $rulesBytesPos) return FALSE;
  $param = $rulesBytes[$myPos++];
  
  $result .= "param = $param <br>";
  return TRUE;
}

function parseAction($rulesBytes, &$myPos, $rulesBytesPos, &$result, $ruleVersion)
{
  if ($myPos > $rulesBytesPos - 4) return FALSE;
  $receiver = bytesToDword($rulesBytes, $myPos);
  $result .= "Empfänger = " .$receiver." (0x".dechex($receiver).") [".getNameForObjectId($receiver)."]<br>";

  //$myPos+=4;
  if ($myPos >= $rulesBytesPos) return FALSE;
    
  if ($ruleVersion>=2)
  {
	  $functionId = $rulesBytes[$myPos++];
	  $result .= "functionId = $functionId [".getNameForFunctionOrEvent($receiver, $functionId)."] <br>";
	  if ($myPos >= $rulesBytesPos) return FALSE;
	  
	  $param = $rulesBytes[$myPos++];
	  $result .= "param = $param <br>";
	  if ($myPos >= $rulesBytesPos) return FALSE;

	  $param = $rulesBytes[$myPos++];
	  $result .= "param = $param <br>";
	  if ($myPos >= $rulesBytesPos) return FALSE;

	  $param = $rulesBytes[$myPos++];
	  $result .= "param = $param <br>";
	  if ($myPos >= $rulesBytesPos) return FALSE;

	  $param = $rulesBytes[$myPos++];
	  $result .= "param = $param <br>";
	  if ($myPos >= $rulesBytesPos) return FALSE;

	  $param = $rulesBytes[$myPos++];
	  $result .= "param = $param <br>";
  }
  else
  {
	  $length = $rulesBytes[$myPos++];
	  $result .= "Parameterlänge = $length <br>";
	  if ($myPos >= $rulesBytesPos) return FALSE;

	  $functionId = $rulesBytes[$myPos++];
	  $result .= "functionId = $functionId <br>";
	  if ($myPos >= $rulesBytesPos) return FALSE;

	  $param = $rulesBytes[$myPos++];
	  $result .= "param = $param <br>";
	  if ($myPos >= $rulesBytesPos) return FALSE;

	  $param = $rulesBytes[$myPos++];
	  $result .= "param = $param <br>";
	  if ($myPos >= $rulesBytesPos) return FALSE;

	  $param = $rulesBytes[$myPos++];
	  $result .= "param = $param <br>";
	  if ($myPos >= $rulesBytesPos) return FALSE;

	  $param = $rulesBytes[$myPos++];
	  $result .= "param = $param <br>";
  }
  return TRUE;
}


function parseWeekTimeToObject($value)
{
  $result = "";
  $weekTime = getWeekTime("dummy", $value);
  //selected value='1'>Di.
  

  $pos = strpos($weekTime, "selected");
  if ($pos === FALSE) return $result;
  $pos2 = strpos($weekTime, ">", $pos);
  if ($pos2 === FALSE) return $result;
  $pos3 = strpos($weekTime, "<", $pos2 + 1);
  if ($pos3 === FALSE) return $result;
  $result->tag = substr($weekTime, $pos2 + 1, $pos3 - $pos2 - 1);
  
  $pos = strpos($weekTime, "selected", $pos3);
  if ($pos === FALSE) return $result;
  $pos2 = strpos($weekTime, ">", $pos);
  if ($pos2 === FALSE) return $result;
  $pos3 = strpos($weekTime, "<", $pos2 + 1);
  if ($pos3 === FALSE) return $result;
  $result->stunde = substr($weekTime, $pos2 + 1, $pos3 - $pos2 - 1);
  
  $pos = strpos($weekTime, "selected", $pos3);
  if ($pos === FALSE) return $result;
  $pos2 = strpos($weekTime, ">", $pos);
  if ($pos2 === FALSE) return $result;
  $pos3 = strpos($weekTime, "<", $pos2 + 1);
  if ($pos3 === FALSE) return $result;
  $result->minute = substr($weekTime, $pos2 + 1, $pos3 - $pos2 - 1);
  
  return $result;
}

function checkAndTraceDataDifferences($groups, $controllerId, $rulesData, $dataPos)
{
  $wurst = "";
  for($i = 0; $i < $dataPos; $i++)
  {
    $wurst .= $rulesData[$i] . ",";
  }
  
  $result = TRUE;
  $erg = QUERY("select data from ruleCache where controllerId='$controllerId' limit 1");
  if ($row = mysqli_fetch_ROW($erg))
  {
    if ($row[0] == $wurst) $result = FALSE;
    /*else
    {
    	 for ($i=0;$i<strlen($wurst);$i++)
    	 {
    	 	   if (substr($wurst,$i,1)!=substr($row[0],$i,1)) die("Unterschied an Position $i ".substr($wurst,$i,1)." != ".substr($row[0],$i,1));
    	 }
    }*/
    /*
	 	  $string1 = $row[0];
      $string2 = $wurst;
      $pos = strspn($string1 ^ $string2, "\0");
      printf('First difference at position %d: "%s" vs "%s"',$pos, $string1[$pos], $string2[$pos]);
  	  echo "Vorher:<br>$row[0]<br>Nachher:<br>$wurst <br>";
  	  echo substr($string1,0,$pos)."<br>";
  	  echo substr($string2,0,$pos)."<br>";
  	  exit;*/
  }
  
  QUERY("DELETE from ruleCache where controllerId='$controllerId' limit 1");
  QUERY("INSERT into ruleCache (groups, controllerId, data) values('$groups','$controllerId','$wurst')");
  
  return $result;
}

function checkAndTraceConfigData($featureInstanceId, $configArray)
{
  $wurst = "";
  foreach ( $configArray as $key => $value )
  {
    $wurst .= $key . "=" . $value . ",";
  }
  
  $erg = QUERY("select configData from configCache where featureInstanceId='$featureInstanceId' limit 1");
  if ($row = mysqli_fetch_ROW($erg))
  {
    if ($row[0] == $wurst)
      return FALSE;
  }
  
  QUERY("DELETE from configCache where featureInstanceId='$featureInstanceId' limit 1");
  QUERY("INSERT into configCache (featureInstanceId, configData) values('$featureInstanceId','$wurst')");
  return TRUE;
}


function getTimeOptionsHour($myValue)
{
	$myOptions = "<option value='31'>Immer";
	$myOptions .= "<option value='25'>Tagsüber";
 	$myOptions .= "<option value='26'>Nachts";

  for($hour = 0; $hour < 24; $hour++)
  {
    $myHour = $hour;
    if (strlen($myHour) == 1) $myHour = "0" . $myHour;
    $myOptions .= "<option value='$hour'>$myHour Uhr";
  }
      
  if ($myValue == "31") $myOptions = str_replace("value='31'","value='31' selected",$myOptions);
  else if ($myValue == "25") $myOptions = str_replace("value='25'","value='25' selected",$myOptions);
  else if ($myValue == "26") $myOptions = str_replace("value='26'","value='26' selected",$myOptions);

  $myOptions = str_replace("value='$myValue'","value='$myValue' selected",$myOptions);
  	
	return $myOptions;
}

function getTimeOptionsMinute($myValue)
{
	$myOptions = "<option value='255'>Immer";

  for($minute = 0; $minute < 60; $minute++)
  {
    $myMinute = $minute;
    if (strlen($myMinute) == 1) $myMinute = "0" . $myMinute;
    $myOptions .= "<option value='$minute'>$myMinute Min";
  }
      
  if ($myValue == "255") $myOptions = str_replace("value='255'","value='255' selected",$myOptions);

  $myOptions = str_replace("value='$myValue'","value='$myValue' selected",$myOptions);
  	
	return $myOptions;
}
?>