<?php
include ($_SERVER["DOCUMENT_ROOT"] . "/homeserver/include/all.php");

if ($submitted == 1)
{
  $data=array();

  $data["startupDelay"] = 0;
  $data["IP_0"] = 192;
  $data["IP_1"] = 168;
  $data["IP_2"] = 2;
  $data["IP_3"] = 200;
  
  $erg = QUERY("select objectId,id from controller where id='$id' limit 1");
  if ($row = mysqli_fetch_ROW($erg))
  {
    $oldObjectId = $row[0];
    $oldDeviceId = getDeviceId($oldObjectId);
	$oldController = $row[1];
	
    $data["deviceId"] = $oldDeviceId;
	
	echo "Alte DeviceID = $oldDeviceId <br>";

    
	// 1. Configuration des Controllers (DeviceId und Config wiederherstellen)
    $erg = QUERY("select functionData from lastReceived where function='Configuration' and senderObj='$oldObjectId' order by id desc limit 1");
    if ($row = mysqli_fetch_ROW($erg))
    {
      $config = unserialize($row[0])->paramData;
      foreach ( $config as $obj )
      {
        $data[$obj->name] = $obj->dataValue;
      }

    $data["deviceId"] = $oldDeviceId;
	  
	  $erg = QUERY("select objectId from controller where id='$newController' limit 1");
	  if ($row = mysqli_fetch_ROW($erg))
	  {
		$newObjectId = $row[0];
		$newDeviceId = getDeviceId($newObjectId);
	
        echo "Schreibe Configuration vom alten Controller in $newDeviceId <br>";	
	    print_r($data);
		echo "<br>";

		callObjectMethodByName($newObjectId, "setConfiguration", $data);
		sleep(1);

        echo "Resete neuen Controller <br>";	
		callObjectMethodByName($newObjectId, "reset");

        echo "Lösche neuen Controller <br>";	
		deleteController($newController);
		flushIt();
		flushIt();
		sleep(2);
	    waitForObjectEventByName($oldObjectId, 15, "evStarted", $lastLogId, "funtionDataParams", 0);
        echo "Alter Controller ist neu gestartet <br>";	
		flushIt();
		flushIt();
		
		//Dann Digitalports 
		$senderObj = "(1=2 ";
        $erg = QUERY("select objectId from featureInstances where controllerId='$oldController'");
        while($row = mysqli_fetch_ROW($erg))
		{
		  $actObjectId = $row[0];
		  if (getClassId($actObjectId)==15) // 15 = Digitalports
		  {
			 $senderObj.=" or senderObj='$actObjectId'";
		  }
		}
		$senderObj.=")";
		
        $erg = QUERY("select functionData,senderObj from lastReceived where function='Configuration' and $senderObj order by id desc");
        while ($row = mysqli_fetch_ROW($erg))
        {
		  $actObjectId = $row[1];
		  unset($data);
		  $config = unserialize($row[0])->paramData;
          foreach ( $config as $obj )
          {
            $data[$obj->name] = $obj->dataValue;
		  }

          echo "Konfiguriere Digitalport $actObjectId: <br>";	
		  print_r($data);
		  echo "<br>";
	            
  		  callObjectMethodByName($actObjectId, "setConfiguration", $data);
		}
		
		//Dann LogicalButtons 
		$senderObj = "(1=2 ";
        $erg = QUERY("select objectId from featureInstances where controllerId='$oldController'");
        while($row = mysqli_fetch_ROW($erg))
		{
		  $actObjectId = $row[0];
		  if (getClassId($actObjectId)==20) // 20 = Digitalports
		  {
			 $senderObj.=" or senderObj='$actObjectId'";
		  }
		}
		$senderObj.=")";
		
        $erg = QUERY("select functionData,senderObj from lastReceived where function='Configuration' and $senderObj order by id desc");
        while ($row = mysqli_fetch_ROW($erg))
        {
		  $actObjectId = $row[1];
		  unset($data);
		  $config = unserialize($row[0])->paramData;
          foreach ( $config as $obj )
          {
            $data[$obj->name] = $obj->dataValue;
          }
		  
          echo "Konfiguriere LogicalButton $actObjectId: <br>";	
		  print_r($data);
		  echo "<br>";
  		  callObjectMethodByName($actObjectId, "setConfiguration", $data);
		}
		
        echo "<br>Fertig!<br>";	

		updateControllerStatus();
		echo "<script>location='editController.php?id=$id';</script>";
		exit();
	  }
	  else
		die("Controller $newController nicht gefunden");
    }
  }
  else
    die("Alter controller $id nicht gefunden");
}

setupTreeAndContent("restoreController_design.html", $message);

$html = str_replace("%ID%", $id, $html);

$allController = readControllers();

$options = "";
foreach ( $allController as $obj )
{
  if ($obj->id == $id)
  {
    $objectId = $obj->objectId;
    $html = str_replace("%CONTROLLER_NAME%", $obj->name, $html);
    continue;
  }
  
  if ($obj->online != 1)
    continue;
  
  $options .= "<option value='$obj->id'>$obj->name";
}

$html = str_replace("%CONTROLLER_OPTIONS%", $options, $html);
if ($objectId == "")
  die("FEHLER! Ungültige ID $id");

show();

?>