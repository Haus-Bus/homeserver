<?php
include ($_SERVER["DOCUMENT_ROOT"] . "/homeserver/include/all.php");

$TFT_BLACK       = 0x0000;      /*   0,   0,   0 */
$TFT_NAVY        = 0x000F;      /*   0,   0, 128 */
$TFT_DARKGREEN   = 0x03E0;      /*   0, 128,   0 */
$TFT_DARKCYAN    = 0x03EF;      /*   0, 128, 128 */
$TFT_MAROON      = 0x7800;      /* 128,   0,   0 */
$TFT_PURPLE      = 0x780F;      /* 128,   0, 128 */
$TFT_OLIVE       = 0x7BE0;      /* 128, 128,   0 */
$TFT_LIGHTGREY   = 0xD69A;      /* 211, 211, 211 */
$TFT_DARKGREY    = 0x7BEF;      /* 128, 128, 128 */
$TFT_BLUE        = 0x001F;      /*   0,   0, 255 */
$TFT_GREEN       = 0x07E0;      /*   0, 255,   0 */
$TFT_CYAN        = 0x07FF;      /*   0, 255, 255 */
$TFT_RED         = 0xF800;      /* 255,   0,   0 */
$TFT_MAGENTA     = 0xF81F;      /* 255,   0, 255 */
$TFT_YELLOW      = 0xFFE0;      /* 255, 255,   0 */
$TFT_WHITE       = 0xFFFF;      /* 255, 255, 255 */
$TFT_ORANGE      = 0xFDA0;      /* 255, 180,   0 */
$TFT_GREENYELLOW = 0xB7E0;      /* 180, 255,   0 */
$TFT_PINK        = 0xFE19;      /* 255, 192, 203 */ //Lighter pink, was 0xFC9F      
$TFT_BROWN       = 0x9A60;      /* 150,  75,   0 */
$TFT_GOLD        = 0xFEA0;      /* 255, 215,   0 */
$TFT_SILVER      = 0xC618;      /* 192, 192, 192 */
$TFT_SKYBLUE     = 0x867D;      /* 135, 206, 235 */
$TFT_VIOLET      = 0x915C;      /* 180,  46, 226 */

$andId="";

$firstRound=false;
if ($id<=0)
{
	$id=(int)file_get_contents("displayLast.txt");
	$firstRound=true;
}

if ($id>0) $andId="where heating.id='$id'";


if ($action!=0)
{
  $erg = QUERY("select min(id), max(id) from heating");
  if ($row=MYSQLi_FETCH_ROW($erg))
  {
  	$min = $row[0];
	$max = $row[1];
	

    if ($action==-1) // rückwärts
	{
	  // zurück zur größten
	  if ($min==$id) $andId="where heating.id='$max'";
	  else $andId="where heating.id<'$id' order by heating.id desc";
	}
    else if ($action==1) // vorwärts
	{
	  // zurück zur kleinsten
	  if ($max==$id) $andId="where heating.id='$min'";
	  else $andId="where heating.id>'$id' order by heating.id";
	}
  }
}

echo "Action: $action, min = $min, max = $max : $andId \n";

$fileDebug = date("d.m.Y H:i.s").": ID = $id - action = $action - min = $min - max = $max - DB = $andId -> ";

$erg = QUERY("select heating.id, heating.sensor, heating.relay, rooms.name as roomName, rooms.id as roomId,
                     featureInstances.objectId as sensorObjectId
                     from heating 
					 join featureInstances on (featureInstances.id = heating.sensor)
                     left join roomFeatures on(roomFeatures.featureInstanceId=heating.sensor)
                     left join rooms on(rooms.id=roomFeatures.roomId)
					 $andId
                     limit 1");
while($obj=MYSQLi_FETCH_OBJECT($erg))
{
	$sensorObjectId = $obj->sensorObjectId;
	
	//echo $obj->id."<br>";
	//echo $obj->roomName."<br>";

    $sollTemperatur="";
    $erg2 = QUERY("select functionData from lastReceived where senderObj='".$sensorObjectId."' and function='Configuration' order by id desc limit 1");
    if ($row2=MYSQLi_FETCH_ROW($erg2))
    {
   	  $functionData = unserialize($row2[0]);
      $paramData = $functionData->paramData;
   	  foreach ($paramData as $object)
   	  {
   	  	$act=$object->name;
        $$act = $object->dataValue;
      }
   	  
	  if ($upperThresholdFraction<10) $upperThresholdFraction="0".$upperThresholdFraction;
	  $sollTemperatur = $upperThreshold.".".$upperThresholdFraction."°";
	   //echo $sollTemperatur."<br>";
 	}

    $heizen="";
    if ($obj->relay>0)
	{
       $erg2 = QUERY("select functionData,function from lastReceived 
	                  join featureInstances on (featureInstances.objectId=lastReceived.senderObj)
	                  where featureInstances.id='".$obj->relay."' and (function='Status' or function='evOff'  or function='evOn') order by lastReceived.id desc limit 1");
       if ($row2=MYSQLi_FETCH_ROW($erg2))
       {
   	      $functionData = unserialize($row2[0]);
          $paramData = $functionData->paramData;
		  if ($row2[1]=="evOn") $heizen="heizen";
		  else if ($row2[1]=="Status" && $paramData[0]->dataValue!=0) $heizen="heizen";
	   }
	}


    $actTemp="?";
    $erg2 = QUERY("select functionData from lastReceived where senderObj='".$obj->sensorObjectId."' and (function='Status' or function='evStatus') order by id desc limit 1");
    if ($row2=MYSQLi_FETCH_ROW($erg2))
    {
   	  $functionData = unserialize($row2[0]);
      $paramData = $functionData->paramData;
  	  $actTemp = $paramData[1]->dataValue;
      if (strlen($actTemp)==1) $actTemp="0".$actTemp;
	  $actTemp = $paramData[0]->dataValue.".".$actTemp."°";
 	}

    //echo "Act: $actTemp <br>";


    $luft="";
    $feuchteObjectId = getObjectId(getDeviceId($sensorObjectId), 34, getInstanceId($sensorObjectId));
    $erg2 = QUERY("select functionData from lastReceived where senderObj='".$feuchteObjectId."' and (function='Status' or function='evStatus')  order by id desc limit 1");
    if ($row2=MYSQLi_FETCH_ROW($erg2))
    {
   	    $functionData = unserialize($row2[0]);
        $paramData = $functionData->paramData;
	    $luft = $paramData[0]->dataValue.utf8_encode("%");
 	}

/*
    $erg2 = QUERY("select objectid from featureInstances 
	                 join roomFeatures on(roomFeatures.featureInstanceId=featureInstances.id)
                     join rooms on(rooms.id=roomFeatures.roomId)
					 where featureClassesId=23 and rooms.id='$obj->roomId' limit 1");
    if ($row2=MYSQLi_FETCH_ROW($erg2))
    {
		$sensorObjectId = $row2[0];
	}*/
	
    //echo "Luft: $luft <br>";

    $obj->roomName = str_replace("ü","ue",$obj->roomName);
	$obj->roomName = str_replace("Ü","Ue",$obj->roomName);
    $obj->roomName = str_replace("ä","ae",$obj->roomName);
	$obj->roomName = str_replace("Ä","Ae",$obj->roomName);
    $obj->roomName = str_replace("ö","oe",$obj->roomName);
	$obj->roomName = str_replace("Ö","Oe",$obj->roomName);
	
    $arr["id"]=$obj->id;
    $arr["interval"]="30000";
    $arr["background"]="".$TFT_BLACK;
    $arr["sensorObjectId"]="".$sensorObjectId;
    $arr["upperLeftString"]=$obj->roomName;
    $arr["upperLeftTextColor"]="".$TFT_WHITE;
    $arr["upperLeftTextSize"]="2";
    $arr["upperRightString"]=$heizen;
    $arr["upperRightTextColor"]="".$TFT_RED;
    $arr["upperRightTextSize"]="2";
    $arr["centralString"]=$actTemp;
    $arr["centralTextColor"]="".$TFT_GREEN;
    $arr["centralTextSize"]="9";
    $arr["lowerLeftString"]=$sollTemperatur;
    $arr["lowerLeftTextColor"]="".$TFT_WHITE;
    $arr["lowerLeftTextSize"]="2";
    $arr["lowerRightString"]=$luft;
    $arr["lowerRightTextColor"]="".$TFT_BLUE;
    $arr["lowerRightTextSize"]="2";

    $json = json_encode($arr);
	
	$lastDisplay = @file_get_contents("lastDisplay.txt");
	if ($lastDisplay!=$json || $firstRound)
	{
	  file_put_contents("lastDisplay.txt", $json);
      echo "JSON\r";
	  echo $json."\r";
	}
	else echo "unverändert \n";

    break;
}

$fileDebug.=$arr["id"]." / ".$obj->roomName."\n";

//file_put_contents("display.txt",$fileDebug,FILE_APPEND);

file_put_contents("displayLast.txt",$arr["id"]);

//file_put_contents("displayDebug.txt",print_r($arr, TRUE));

exit;


?>
