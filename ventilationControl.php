<?php
include ($_SERVER["DOCUMENT_ROOT"] . "/homeserver/include/all.php");

// sensor = Feuchtesensor
// fenster = relay
// ventilator = pump

// URL Variablen von addGenericFeature wieder in Variablen umwandeln
if ($params!="") urlToParams($params);

if ($action=="createNew") QUERY("INSERT into ventilation (sensor) values('0')");
else if ($action=="delete")
{
	 if ($confirm==1)
	 {
	 	 deleteGeneratedDiagram($id);
		 deleteGeneratedBaseRule($id);
	 	 QUERY("delete from ventilation where id='$id' limit 1");
	 }
	 else showMessage("Soll diese Lüftungssteuerung wirklich gelöscht werden?","Steuerung löschen?", $link = "ventilationControl.php?action=delete&confirm=1&id=$id", $linkName = "Ja, löschen", $link2 = "ventilationControl.php", $link2Name = "Nein, zurück");
}
else if ($action=="addSensor")
{
	QUERY("UPDATE ventilation set sensor='$featureInstanceId' where id='$id' limit 1");
	renewControl($id);
}
else if ($action=="deleteSensor")
{
	QUERY("UPDATE ventilation set sensor='0' where id='$id' limit 1");
	renewControl($id);
}
else if ($action=="addAktor")
{
  QUERY("INSERT into ventilationFenster (ventilationId,fensterInstanceId) values('$id', '$featureInstanceId')");
  renewControl($id);
}
else if ($action=="deleteAktor")
{
	QUERY("delete from ventilationFenster where ventilationId='$id' and fensterInstanceId='$fensterInstanceId' limit 1");
	renewControl($id);
}
else if ($action=="addPump")
{
	QUERY("UPDATE ventilation set ventilator='$featureInstanceId' where id='$id' limit 1");
	renewControl($id);
}
else if ($action=="deletePump")
{
	QUERY("UPDATE ventilation set ventilator='0' where id='$id' limit 1");
	renewControl($id);
}
else if ($action=="selectDiagram")
{
   $html = file_get_contents("templates/selectDiagram_design.html");
   $html = str_replace("heatingControl.php","ventilationControl.php", $html);
   
   $elementsTag = getTag("%ELEMENTS%",$html);
   $elements="";
   $erg = QUERY("select id,title from graphs order by id");
   while($obj=MYSQLi_FETCH_OBJECT($erg))
   {  
   	  $actTag = $elementsTag;
	  $actTag = str_replace("%DIAGRAM_ID%",$obj->id, $actTag);
	  $actTag = str_replace("%DIAGRAM_TITLE%",$obj->title, $actTag);
	  $elements.=$actTag;
   }
   
   $html = str_replace("%ELEMENTS%",$elements,$html);
   $html = str_replace("%ID%",$id, $html);
   
   $erg = QUERY("select diagram from ventilation where id='$id' limit 1");
   $row=MYSQLi_FETCH_ROW($erg);
   $html = str_replace("%GRAPH_ID%",$row[0], $html);
   
   die($html);
}
else if ($action=="deleteDiagram") QUERY("UPDATE ventilation set diagram='0' where id='$id' limit 1");
else if ($action=="setDiagram")
{
	deleteGeneratedDiagram($id);
	QUERY("UPDATE ventilation set diagram='$diagram' where id='$id' limit 1");
}
else if ($action=="changeType") QUERY("UPDATE ventilation set type='$newType' where id='$id' limit 1");
else if($action=="createDiagram")
{
	$erg = QUERY("select sensor, GROUP_CONCAT(ventilationFenster.fensterInstanceId) as fenster, ventilator,diagram from ventilation left join ventilationFenster on (ventilationFenster.ventilationId=ventilation.id) where id='$id' group by ventilation.id limit 1");
	if ($obj=MYSQLi_FETCH_OBJECT($erg))
	{
		$data = getFeatureInstanceData($obj->sensor);
		$sensorName = $data->featureInstanceName;

        // generierte Diagramme löschen
  	    deleteGeneratedDiagram($id);
		
  	    QUERY("INSERT into graphs (title,timeMode,timeParam1,timeParam2,width,height,distValue,distType,theme,heightMode) 
	                        values('Lüftungssteuerung $sensorName','days','7','','','700','','','','fixed')");
	    $graphId = query_insert_id();
    
        QUERY("TRUNCATE graphData");
    
        QUERY("INSERT into graphSignals (graphId, color, title) values('$graphId','007BFF','$sensorName')");
        $signalId = query_insert_id();
        QUERY("INSERT into graphSignalEvents (graphSignalsId, featureInstanceId, functionId,fkt) 
                                       values('$signalId','$obj->sensor','187','relativeHumidity')"); // 187 ist FunctionId vom Feuchtesensor Statusevent
		
        if ($diagramType==1 && $obj->ventilator>0)
        {
     	  $ventilatorData = getFeatureInstanceData($obj->ventilator);

      	  $onId=62;  //62 ist FunctionId vom Schalter evOn
      	  $offId=63;  //63 ist FunctionId vom Schalter evOff
  
          $valHigh = 30;
		  $valLow = 27;
		  $color =  "9900CC";

          QUERY("INSERT into graphSignals (graphId, color, title, type) values('$graphId','$color','Ventilator','steps')");
          $signalId = query_insert_id();
          QUERY("INSERT into graphSignalEvents (graphSignalsId, featureInstanceId, functionId,fkt) 
                                         values('$signalId','$obj->ventilator','$onId','$valHigh')"); 
          QUERY("INSERT into graphSignalEvents (graphSignalsId, featureInstanceId, functionId,fkt) 
                                         values('$signalId','$obj->ventilator','$offId','$valLow')"); 
        }

        if ($obj->fenster==null) $obj->fenster="";
	    $parts = explode(",",$obj->fenster);
	    $hasValidParts = count($parts)>0 && $parts[0]>0;
	 
        if ($diagramType==1 && $hasValidParts)
        {
		  $count=0;	
		  $colors = array("FF0000","00FF00","00FFFF","800000","FFFF00");
		  foreach((array)$parts as $part)
          {
    		$fensterData = getFeatureInstanceData($part);
		    $fensterName = $fensterData->featureInstanceName;

            $valHigh = 25-$count*5;
			$valLow = 22-$count*5;
			$color =  $colors[$count%5];
			
            QUERY("INSERT into graphSignals (graphId, color, title, type) values('$graphId','$color','$fensterName','steps')");
            $signalId = query_insert_id();
            QUERY("INSERT into graphSignalEvents (graphSignalsId, featureInstanceId, functionId,fkt) 
                                           values('$signalId','$part','44','$valHigh')"); // 44 ist FunctionId vom Taster evFree Event
            QUERY("INSERT into graphSignalEvents (graphSignalsId, featureInstanceId, functionId,fkt) 
                                           values('$signalId','$part','43','$valLow')"); // 43 ist FunctionId vom Taster evFree Event
										   
 			$count++;							   
		  }
		}
    
        QUERY("UPDATE ventilation set diagram='$graphId' where id='$id' limit 1");
    
	  triggerTreeUpdate();
	}
}
else if($action=="setConfig")
{
	$temp1="temp_1_$id";
	$temp1=trim($$temp1);
	$temp1 = str_replace(",",".",$temp1);
	$pos = strpos($temp1,".");
	if ($pos!==FALSE)
	{
		$temp1Fraction = substr($temp1,$pos+1);
		if (strlen($temp1Fraction)==1) $temp1Fraction*=10;
		$temp1 = substr($temp1,0,$pos);
	}
	else $temp1Fraction="0";

    /*
	$temp2="temp_2_$id";
	$temp2=trim($$temp2);
	$temp2 = str_replace(",",".",$temp2);
	$pos = strpos($temp2,".");
	if ($pos!==FALSE)
	{
		$temp2Fraction = substr($temp2,$pos+1);
		if (strlen($temp2Fraction)==1) $temp2Fraction*=10;
		$temp2 = substr($temp2,0,$pos);
	}
	else $temp2Fraction="0";
	
	$interval = "interval_$id";
	$interval = $$interval;
	$parts = explode("-",$interval);
	$minReportTime = $parts[0];
	$reportTimeBase = $parts[1];
	
	$maxReportTime = 2*$minReportTime;
	if ($maxReportTime>255) $maxReportTime=255;

    $upperTemp = $temp2*100 + $temp2Fraction;
    $lowerTemp = $temp1*100 + $temp1Fraction;
    $hysterese = (int)(($upperTemp - $lowerTemp) / 100);
    if ($hysterese>255) $hysterese=255;
	*/
	
	//echo "ID = $id Temp1 = $temp1 temp1Fraction = $temp1Fraction Temp2 = $temp2 temp2Fraction = $temp2Fraction Interval = $interval minReportTime = $minReportTime reportTimeBase = $reportTimeBase maxReportTime = $maxReportTime hysterese = $hysterese<br>";
	
	$erg = QUERY("select sensor, ventilator startHour, endHour,type from ventilation where id='$id' limit 1");
	if ($row = MYSQLi_FETCH_ROW($erg))
	{
		$data = getFeatureInstanceData($row[0]);
		$type = $row[4];
		$objectId = $data->objectId;
		$callArray = array();

        $erg2 = QUERY("select functionData from lastReceived where senderObj='".$objectId."' and function='Configuration' order by id desc limit 1");
        if ($row2=MYSQLi_FETCH_ROW($erg2))
        {
   	       $functionData = unserialize($row2[0]);
      	   $paramData = $functionData->paramData;
   	       foreach ($paramData as $object)
   	       {
			  $callArray[$object->name]=$object->dataValue;
     	   }
     	}
		
		if ($type=="Befeuchten")
		{
		  $lastLowerThreshold = (float)($callArray["lowerThreshold"].".".$callArray["lowerThresholdFraction"]);
		  $newLowerThreshold = (float)($temp1.".".$temp1Fraction);
		  $diff = $newLowerThreshold-$lastLowerThreshold;
		 
		  $lastUpperThreshold = (float)($callArray["upperThreshold"].".".$callArray["upperThresholdFraction"]);
		  $newUpperThreshold = $lastUpperThreshold+$diff;
		  if ($newUpperThreshold<=$newLowerThreshold) $newUpperThreshold=$newLowerThreshold+10;
	 
		  $callArray["lowerThreshold"]=$temp1;
		  $callArray["lowerThresholdFraction"]=$temp1Fraction;
		
		  $front = (int)$newUpperThreshold;
		  $rest = ($newUpperThreshold-$front)*100;
		  $callArray["upperThreshold"]=$front;
		  $callArray["upperThresholdFraction"]=$rest;
		}
		else // Bei keinem Typ oder Entfeuchten
		{
		  $lastUpperThreshold = (float)($callArray["upperThreshold"].".".$callArray["upperThresholdFraction"]);
		  $newUpperThreshold = (float)($temp1.".".$temp1Fraction);
		  $diff = $newUpperThreshold-$lastUpperThreshold;
		 
		  $lastLowerThreshold = (float)($callArray["lowerThreshold"].".".$callArray["lowerThresholdFraction"]);
		  $newLowerThreshold = $lastLowerThreshold+$diff;
		  if ($newLowerThreshold>=$newUpperThreshold) $newLowerThreshold=$newUpperThreshold-10;
	 
		  $callArray["upperThreshold"]=$temp1;
		  $callArray["upperThresholdFraction"]=$temp1Fraction;
		
		  $front = (int)$newLowerThreshold;
		  $rest = ($newLowerThreshold-$front)*100;
		  $callArray["lowerThreshold"]=$front;
		  $callArray["lowerThresholdFraction"]=$rest;
		}

		 
 	     callObjectMethodByName($objectId, "setConfiguration", $callArray);
	     sleepMs(200);
	     callObjectMethodByName($objectId, "getConfiguration");
	     sleepMs(2000);
		 		 
	     $debug="<b>Einstellung wurde gespeichert</b><br>";

		 // Zeitraum in Regel eintragen
		 QUERY("UPDATE ventilation set startHour='$nightStart', endHour='$nightEnd' where id='$id' limit 1");
		 
		 $erg2= QUERY("select basicRules.id as ruleId, basicRules.startHour, basicRules.endHour, basicRules.startMinute, basicRules.endMinute, basicRules.intraDay
	 	                from groups 
	 	                join groupFeatures on (groupFeatures.groupId = groups.id)
	 	                join basicRules on (basicRules.groupId = groups.id)
	 	                join basicrulesignals on (basicrulesignals.ruleId=basicRules.id)
	 	                where groupFeatures.featureInstanceId='$row[1]' and groups.single=1
	 	                      and extras='Lüftungssteuerung'
	 	                      and basicrulesignals.featureInstanceId='$row[0]'
	 	                ");
	 	 if ($obj2=MYSQLi_FETCH_OBJECT($erg2))
		 {
			$ruleId = $obj2->ruleId;
		    $actStartTime = $obj2->startHour;
		    $actEndTime = $obj2->endHour;
		    $actStartMinute = $obj2->startMinute;
		    $actEndMinute = $obj2->endMinute;
			$intraDay = 1;
			
			if ($nightStart==0 && $nightEnd==0)
			{
				$nightStart=31;
				$nightEnd=31;
				$nightStartMinute=255;
				$nightEndMinute=255;
			    $intraDay=0;
			}
			else 
			{
				$nightStartMinute=0;
				$nightEndMinute=0;
			}
			
			if ($actStartTime!=$nightEnd || $actEndTime!=$nightStart || $actStartMinute!=$nightStartMinute || $actEndMinute!=$nightEndMinute)
			{
			  QUERY("UPDATE basicRules set startDay='0', endDay='6', startHour='$nightEnd',startMinute='$nightStartMinute',endHour ='$nightStart',endMinute ='$nightEndMinute', intraDay='$intraDay' where id='$ruleId' limit 1");
			  
 			  $debug.="<b>Nach Änderung der Nachtabschaltung müssen die Regeln abschließend einmal übertragen werden!</b><br>";
			}
		 }
	}
	else $debug="Fehler: ungültige ID";
}


$html = file_get_contents("templates/ventilationControl_design.html");

// Sekunden =  Sekunden minReportTime * 1s baseTime
$namesDauer = "deaktiviert";
$valsDauer = "0-0";
for($i = 10; $i < 60; $i+=10)
{
  $namesDauer .= "," . $i . " Sek";
  $valsDauer .= "," . $i."-1";
}

// Minuten =  Minuten*2 minReportTime * 30s baseTime
for($i = 1; $i <= 10; $i++)
{
  $namesDauer .= "," . $i . " Min";
  $valsDauer .= "," . ($i * 2)."-30";
}

// 10er Minuten = Minuten minReportTime * 60s baseTime
for($i = 20; $i <= 50; $i+=10)
{
  $namesDauer .= "," . $i . " Min";
  $valsDauer .= "," .$i."-60";
}

// Stunden = Stunden * 60 minReportTime * 60s baseTime
for($i = 1; $i <= 4; $i++)
{
  $namesDauer .= "," . $i . " Std";
  $valsDauer .= "," . ($i * 60)."-60";
}

$elementsTag = getTag("%ELEMENTS%",$html);
$elements="";
//$foundInavtive=0;
$generated=0;
$erg = QUERY("select ventilation.id, ventilation.sensor,ventilation.type, GROUP_CONCAT(ventilationFenster.fensterInstanceId) as fenster, ventilation.diagram, ventilation.ventilator, ventilation.startHour, ventilation.endHour,
                     graphs.title as graphTitle, group_concat(rooms.name) as roomName
                     from ventilation 
					 left join ventilationFenster on (ventilationFenster.ventilationId=ventilation.id)
                     left join graphs on(graphs.id=ventilation.diagram)
                     left join roomFeatures on(roomFeatures.featureInstanceId=ventilation.sensor)
                     left join rooms on(rooms.id=roomFeatures.roomId)
                     where diagram!=-1
                     group by roomFeatures.featureInstanceId,ventilation.id
                     order by ventilation.id");
while($obj=MYSQLi_FETCH_OBJECT($erg))
{
	 $actTag = $elementsTag;
	 $actTag = str_replace("%ID%",$obj->id, $actTag);
	 
	 if ($obj->sensor==0)
	 {
	 	$mySensor="eintragen";
	 	removeTag("%OPT_SENSOR%",$actTag);
	 	removeTag("%OPT_DELETE_SENSOR%",$actTag);
	 }
	 else
	 {
	 	chooseTag("%OPT_SENSOR%",$actTag);
	 	chooseTag("%OPT_DELETE_SENSOR%",$actTag);

	 	$data = getFeatureInstanceData($obj->sensor);
	 	$mySensor = $data->roomName." > ".$data->featureInstanceName;

    	$minReportTime=2;
     	$reportTimeBase=60;
	 	  
	 	$erg2 = QUERY("select functionData from lastReceived where senderObj='".$data->objectId."' and function='Configuration' order by id desc limit 1");
      if ($row2=MYSQLi_FETCH_ROW($erg2))
      {
   	    $functionData = unserialize($row2[0]);
      	$paramData = $functionData->paramData;
   	    foreach ($paramData as $object)
   	    {
   	    	$act=$object->name;
     	 	  $$act = $object->dataValue;
     	 	}
     	}
     	
     	if ($lowerThresholdFraction<10) $lowerThresholdFraction="0".$lowerThresholdFraction;
     	if ($upperThresholdFraction<10) $upperThresholdFraction="0".$upperThresholdFraction;

		if ($obj->type=="Befeuchten") $actTag = str_replace("%TEMP_1%",$lowerThreshold.".".$lowerThresholdFraction, $actTag);
		else $actTag = str_replace("%TEMP_1%",$upperThreshold.".".$upperThresholdFraction, $actTag);
		
     	/*$actTag = str_replace("%TEMP_2%",$upperThreshold.".".$upperThresholdFraction, $actTag);
	  	$actTag = str_replace("%INTERVAL_OPTIONS%",getSelect($minReportTime."-".$reportTimeBase, $valsDauer, $namesDauer), $actTag);
		*/

	  	$actTag = str_replace("%TYPE_OPTIONS%",getSelect($obj->type, "Entfeuchten,Befeuchten", "Entfeuchten,Befeuchten"), $actTag);

        $options = getTimeOptionsHour($obj->startHour);
        $actTag = str_replace("%TIME_START_TIME_OPTIONS%", $options, $actTag);

        $options = getTimeOptionsHour($obj->endHour);
        $actTag = str_replace("%TIME_END_TIME_OPTIONS%", $options, $actTag);
	 }
	 $actTag = str_replace("%TEMP%",$mySensor, $actTag);
	 
	 $actorTag = getTag("%AKTOR%",$actTag);
	 $actors="";
	 if ($obj->fenster==null) $obj->fenster="";
	 else $obj->fenster.=","; // damit noch ein eintragen kommt
	 $parts = explode(",",$obj->fenster);
	 
	 foreach((array)$parts as $part)
	 {
	   $actActorTag = $actorTag;
	   
	   if ($part=="")
	   {
	 	 $myAktor="[ergänzen]";
	 	 removeTag("%OPT_DELETE_AKTOR%",$actActorTag);
	   }
	   else
	   {
	 	  chooseTag("%OPT_DELETE_AKTOR%",$actActorTag);
	 	  $relayData = getFeatureInstanceData($part);
	 	  $myAktor = $data->roomName." > ".$relayData->featureInstanceName;
		  $actActorTag = str_replace("%FENSTER_INSTANCE%",$part, $actActorTag);
	   }
       $actActorTag = str_replace("%SWITCH%",$myAktor, $actActorTag);
	   $actors.=$actActorTag;
	 }
	 $actTag = str_replace("%AKTOR%",$actors, $actTag);
	 
	 if ($obj->diagram==0 || $obj->graphTitle==null)
	 {
	 	 if ($obj->sensor==0) $mySensor="</a>Zuerst Sensor auswählen";
	 	 else $mySensor="eintragen";
	 	 removeTag("%OPT_DELETE_DIAGRAM%",$actTag);
	 }
	 else
	 {
	 	 chooseTag("%OPT_DELETE_DIAGRAM%",$actTag);
	 	 $mySensor=$obj->graphTitle;
	 }

	 if ($obj->ventilator==0)
	 {
	 	 $myAktor="eintragen";
	 	 removeTag("%OPT_DELETE_PUMP%",$actTag);
	 }
	 else
	 {
	 	  chooseTag("%OPT_DELETE_PUMP%",$actTag);
	 	  $pumpData = getFeatureInstanceData($obj->ventilator);
	 	  $myAktor = $pumpData->roomName." > ".$pumpData->featureInstanceName;
	 }
	 $actTag = str_replace("%PUMP%",$myAktor, $actTag);

	 
	 //$actInactive=0;
	 $status="<img src='/homeserver/img/offline2.gif' title='Regeln für Lüftungssteuerung noch nicht generiert'>";
	 
	 if ($obj->sensor>0 && $obj->ventilator>0)
	 {
	 	  //$actInactive=1;
	 	  $erg2= QUERY("select groups.id as groupId,
	 	                       basicRules.id as ruleId,
							   basicRules.active as ruleActive,
	 	                       basicrulesignals.id as signalId
	 	                      
	 	                from groups 
	 	                join groupFeatures on (groupFeatures.groupId = groups.id)
	 	                join basicRules on (basicRules.groupId = groups.id)
	 	                join basicrulesignals on (basicrulesignals.ruleId=basicRules.id)
	 	                where groupFeatures.featureInstanceId='$obj->ventilator' and groups.single=1
	 	                      and extras='Lüftungssteuerung'
	 	                      and basicrulesignals.featureInstanceId='$obj->sensor'
	 	                ");
	 	  if ($obj2=MYSQLi_FETCH_OBJECT($erg2))
		  {
			  if ($action=="invertHeatingStatus" && $id==$obj->id)
			  {
				  if ($obj2->ruleActive==1)
				  {
					  QUERY("UPDATE basicRules set active=0 where id='$obj2->ruleId' limit 1");
					  $obj2->ruleActive=0;
				  }
				  else 
				  {
					  QUERY("UPDATE basicRules set active=1 where id='$obj2->ruleId' limit 1");
					  $obj2->ruleActive=1;
				  }
			  }

			  if ($obj2->ruleActive==1) $status="<img src='/homeserver/img/online2.gif' title='Regeln sind generiert und aktiv. Klicken, um Steuerung zu deaktivieren'>";
			  else $status="<img src='/homeserver/img/offline2.gif' title='Regeln sind generiert, aber inaktiv. Klicken, um Steuerung zu aktivieren'>";
		  }
	 	  else //if ($action=="generateRules")
	 	  {
	 	  	  $erg2= QUERY("select groups.id as groupId 
	 	  	                       from groups 
	 	  	          	           join groupFeatures on (groupFeatures.groupId = groups.id)
           	 	                 where groupFeatures.featureInstanceId='$obj->ventilator' and groups.single=1
     	                 ");
     	    if ($obj2=MYSQLi_FETCH_OBJECT($erg2))
     	    {
     	    	 $fktValue="0";
     	    	 
     	    	 QUERY("INSERT into basicRules (groupId, fkt1,fkt2,startDay,startHour,startMinute,endDay,endHour,endMinute,extras,active)
     	    	                         values('$obj2->groupId','$fktValue','true','7','31','255','7','31','255','Lüftungssteuerung','1')");
     	    	 $ruleId = QUERY_INSERT_ID();
				 
				 QUERY("update ventilation set basicRuleId='$ruleId' where id = '$obj->id' limit 1");

     	    	 QUERY("INSERT into basicRuleSignals (ruleId, featureInstanceId) values ('$ruleId','$obj->sensor')");
     	    	 $signalId = QUERY_INSERT_ID();
     	    	 
     	    	 $status="<img src='/homeserver/img/online2.gif' title='Regeln sind generiert und activ. Klicken um sie zu deaktivieren'>";
	 	  	     //$actInactive=0;
	 	  	     $debug.="Regel für $mySensor wurde generiert <br>";
	 	  	     $generated=1;
     	    }
     	    else $debug.="<font color=#bb0000><b>Fehler: Gruppe für Sensor $mySensor konnte nicht gefunden werden </font><br>";
	 	  }
	 }
	 
	 //if ($actInactive==1) $foundInavtive=1;
	 
	 $actTag = str_replace("%STATUS%",$status, $actTag);
	 $actTag = str_replace("%DIAGRAM%",$mySensor, $actTag);
	 $actTag = str_replace("%ROOM%",$obj->roomName, $actTag);
	 
	 $actTag = str_replace("%SENSOR_PARAMS%",paramsToUrl("action=addSensor&id=$obj->id"), $actTag);
	 $actTag = str_replace("%AKTOR_PARAMS%",paramsToUrl("action=addAktor&id=$obj->id"), $actTag);
	 $actTag = str_replace("%PUMP_PARAMS%",paramsToUrl("action=addPump&id=$obj->id"), $actTag);
	 
	 $elements.=$actTag;
}

$html = str_replace("%ELEMENTS%",$elements,$html);

/*if ($foundInavtive==1) chooseTag("%OPT_GENERATE_RULES%",$html);
else removeTag("%OPT_GENERATE_RULES%",$html);
*/

if ($generated==1) $debug.="<font color=#bb0000><b>Achtung:</b> Nach dem Generieren neuer Regeln, müssen diese einmalig zu den Controllern übermittelt werden. Dies bitte unter System -> Controller: Regeln übermitteln durchführen oder <a href='/homeserver/editRules.php?action=submitRules' target='_blank'>hier klicken.</a></font><br>";
if ($debut!="") $debug="<br>".$debug."<br>";

$html = str_replace("%DEBUG%",$debug,$html);

die($html);


function getTimeOptionsHour($myValue)
{
  //$myOptions = "<option value='31'>Aus";
  //$myOptions .= "<option value='25'>Tagsüber";
  //$myOptions .= "<option value='26'>Nachts";

  for($hour = 0; $hour < 24; $hour++)
  {
    $myHour = $hour;
    if (strlen($myHour) == 1) $myHour = "0" . $myHour;
    $myOptions .= "<option value='$hour'>$myHour Uhr";
  }
      
  if ($myValue == "31") $myOptions = str_replace("value='31'","value='31' selected",$myOptions);
  else if ($myValue == "25") $myOptions = str_replace("value='25'","value='25' selected",$myOptions);
  else if ($myValue == "26") $myOptions = str_replace("value='26'","value='26' selected",$myOptions);

  $myOptions = str_replace("value='$myValue'","value='$myValue' selected",$myOptions);
  	
	return $myOptions;
}

function renewControl($id)
{
  deleteGeneratedBaseRule($id);
  deleteGeneratedDiagram($id, false);
  checkAutoGenerateDiagram($id);
}

function deleteGeneratedBaseRule($id) 
{
  $erg = QUERY("select sensor, ventilator, basicRuleId from ventilation where id='$id' limit 1");
  $obj=MYSQLi_FETCH_OBJECT($erg);
  if (($obj->sensor==0 || $obj->ventilator==0) && $obj->basicRuleId>0)
  {
	  deleteBaseRule($obj->basicRuleId);
	  QUERY("UPDATE ventilation set basicRuleId='0' where id='$id' limit 1");
  }
}


function deleteGeneratedDiagram($heatingId, $withTrigger=true)
{
	$erg = QUERY("select diagram from ventilation where id='$heatingId' limit 1");
	if ($obj=MYSQLi_FETCH_OBJECT($erg))
	{
	  if ($obj->diagram>0)
	  {
    	  QUERY("delete from graphs where id='$obj->diagram' and timeMode='days' and timeParam1='7' and timeParam2='' and width='' and height='700' and distValue='' and distType='' and theme='' and heightMode='fixed'");
  	      QUERY("update ventilation set diagram='0' where id='$heatingId' limit 1");
	  
	      if ($withTrigger) triggerTreeUpdate();
  	  }
  }
}

function checkAutoGenerateDiagram($heatingId)
{
	$erg = QUERY("select diagram,sensor from ventilation where id='$heatingId' limit 1");
	if ($obj=MYSQLi_FETCH_OBJECT($erg))
	{
		if ($obj->diagram==0 && $obj->sensor>0)
		{
			header("Location: ventilationControl.php?action=createDiagram&id=$heatingId&diagramType=1");
			exit;
		}
    }
}
?>  