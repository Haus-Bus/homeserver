<?php
include ($_SERVER["DOCUMENT_ROOT"] . "/homeserver/include/all.php");

$checked = "";
if ($selectAll==1) $checked="checked";

resetLastOpen();

$selectedClasses="";
$classesParts = explode(",",$classes);
foreach ($classesParts as $className)
{
	if ($selectedClasses!="") $selectedClasses.=" or ";
	$actId = getClassesIdByName($className);
	$selectedClasses.="featureInstances.featureClassesId='$actId'";
}

  if ($submitted == 1)
  {
  	$ids="";
    $erg = QUERY("select id from featureInstances");
    while ( $obj = mysqli_fetch_OBJECT($erg) )
    {
      $act = "id" . $obj->id;
      $act = $$act;
      if ($act == 1)
      {
      	 if ($ids!="") $ids.=",";
      	 $ids.=$obj->id;
      }
    }
    
    $url = $returnUrl."?ids=$ids&params=$params";
    header("Location: $url");
    exit();
  }
  else
  {
    setupTreeAndContent("addGenericGroupMember_design.html");
    
    $html = str_replace("%PARAMS%",$params,$html);
    $html = str_replace("%RETURN_URL%",$returnUrl,$html);
    
    $closeTreeFolder = "</ul></li> \n";
    
    $treeElements = "";
    $treeElements .= addToTree("Features auswählen", 1);
    $html = str_replace("%INITIAL_ELEMENT2%", "expandToItem('tree2','$treeElementCount');", $html);
    
    unset($ready);
    $lastRoom = "";
    $sql = "select rooms.id as roomId, rooms.name as roomName,
                                 roomFeatures.featureInstanceId,
                                 featureInstances.name as featureInstanceName, featureInstances.featureClassesId,
                                 featureClasses.name as featureClassName,
                                 featureFunctions.id  as featureFunctionId,featureFunctions.name  as featureFunctionName
                                 
                                 from rooms
                                 join roomFeatures on (roomFeatures.roomId = rooms.id)
                                 join featureInstances on (featureInstances.id = roomFeatures.featureInstanceId)
                                 join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
                                 join featureFunctions on (featureInstances.featureClassesId=featureFunctions.featureClassesId) 
                                 where ($selectedClasses)
                                 order by roomName,featureClassName,featureInstanceName";
                     
    //die($sql);            
    $erg = QUERY($sql); // and featureInstances.featureClassesId !='$logicalButtonClass'
    while ( $obj = mysqli_fetch_object($erg) )
    {
      $ready[$obj->featureInstanceId] = 1;
      
        if ($obj->roomId != $lastRoom)
        {
          if ($lastRoom != "")
          {
            $treeElements .= $closeTreeFolder; // letzte featureclass
            $treeElements .= $closeTreeFolder; // letzter raum
          }
          $lastRoom = $obj->roomId;
          $treeElements .= addToTree($obj->roomName, 1);
          $lastClass = "";
        }
        
        if ($obj->featureClassesId != $lastClass)
        {
          if ($lastClass != "") $treeElements .= $closeTreeFolder; // letzte featureclass
          
          $lastClass = $obj->featureClassesId;
          $treeElements .= addToTree($obj->featureClassName, 1);
          $lastInstance = "";
        }
        
        if ($obj->featureInstanceId != $lastInstance)
        {
          $lastInstance = $obj->featureInstanceId;
          $treeElements .= addToTree("<input type='checkbox' name='id$obj->featureInstanceId' value='1' $checked>$obj->featureInstanceName", 0);
        }
      }
    
    
    $treeElements .= $closeTreeFolder; // letzte featureclass
    $treeElements .= $closeTreeFolder; // letzter raum
    

    $lastRoom = "";
    $lastController = "";
	$myIdsCheck="";
	$myIdsUncheck="";
    $erg = QUERY("select featureInstances.id as featureInstanceId, featureInstances.name as featureInstanceName, featureInstances.featureClassesId,
                                 featureClasses.name as featureClassName,
                                 controller.id as controllerId, controller.name as controllerName,
                                 featureFunctions.id  as featureFunctionId,featureFunctions.name  as featureFunctionName
                                 
                                 from featureInstances
                                 join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
                                 join controller on (featureInstances.controllerId = controller.id)
                                 join featureFunctions on (featureInstances.featureClassesId=featureFunctions.featureClassesId) 
                                 
                                 where ($selectedClasses)
                                 order by controllerName, featureClassName,featureInstanceName,featureFunctionName"); // and featureInstances.featureClassesId !='$logicalButtonClass'
    while ( $obj = mysqli_fetch_object($erg) )
    {
      if ($ready[$obj->featureInstanceId] == 1) continue;
      
        if ($lastRoom == "")
        {
          $lastRoom = "dummy";
          $treeElements .= addToTree("Keinem Raum zugeordnet", 1);
        }
        
        if ($obj->controllerId != $lastController)
        {
          if ($lastController != "")
          {
            $treeElements .= $closeTreeFolder; // letzte class
            $treeElements .= $closeTreeFolder; // letzter controller
			$treeElements = str_replace("%PLACE_HOLDER_CHECK%",$myIdsCheck, $treeElements);
			$treeElements = str_replace("%PLACE_HOLDER_UNCHECK%",$myIdsUncheck, $treeElements);
			$myIdsCheck="";
			$myIdsUncheck="";
          }
          $lastController = $obj->controllerId;
          $treeElements .= addToTree("<input type='checkbox' onchange='if (this.checked) {%PLACE_HOLDER_CHECK%} else {%PLACE_HOLDER_UNCHECK%}' value='1' $checked>".$obj->controllerName, 1);
          $lastClass = "";
        }
        
        if ($obj->featureClassesId != $lastClass)
        {
          if ($lastClass != "")
          {
            $treeElements .= $closeTreeFolder; // letzte featureclass
          }
          
          $lastClass = $obj->featureClassesId;
          $treeElements .= addToTree($obj->featureClassName, 1);
          $lastInstance = "";
        }
        
        if ($obj->featureInstanceId != $lastInstance)
        {
          $lastInstance = $obj->featureInstanceId;
          $treeElements .= addToTree("<input type='checkbox' name='id$obj->featureInstanceId' value='1' $checked>$obj->featureInstanceName", 0);
		  $myIdsUncheck.="uncheckMe(\"id$obj->featureInstanceId\");";
		  $myIdsCheck.="checkMe(\"id$obj->featureInstanceId\");";
        }
    }
    
    $treeElements .= $closeTreeFolder; // letzte featureclass
    $treeElements .= $closeTreeFolder; // letzter controller
    $treeElements .= $closeTreeFolder; // letzter raum
    

    $html = str_replace("%TREE_ELEMENTS%", $treeElements, $html);
    $html = str_replace("%ID%", $id, $html);
    $html = str_replace("%ASSIGNED%", $assigned, $html);
    
    show();
  }


function resetLastOpen()
{
	global $lastOpen;
  $lastOpen=-11;
}

?>
