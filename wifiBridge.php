<?php
$WIFI_SCHALTER_CLASS_ID = 19;
$WIFI_ETHERNET_CLASS_ID = 162;

if ($argv[1]=="call")
{
  $command = $argv[2];
  $sender = $argv[3];
  $objectId = $argv[4];
  $wifiName = $argv[5];

  if (file_exists("/var/www/")) $_SERVER["DOCUMENT_ROOT"]="/var/www/";
  else $_SERVER["DOCUMENT_ROOT"]="c:/Haus-Bus-Homeserver/htdocs/";
  require($_SERVER["DOCUMENT_ROOT"]."/homeserver/include/all.php");

  if ($command=="getModuleId") sendWifiModuleId($sender, $objectId, $wifiName,1);
  else if ($command=="getConfiguration") sendWifiConfiguration($sender, $objectId, $wifiName,1);
  else if ($command=="getRemoteObjects") sendWifiRemoteObjects($sender, $objectId, $wifiName,1);
  else if ($command=="ping") sendWifiPong($sender, $objectId, $wifiName,1);
  else if ($command=="writeRules") sendWifiMemoryStatus($sender, $objectId, $wifiName,1);
  else if ($command=="reset") resetWifi($sender, $objectId, $wifiName,1);
  else if ($command=="getCurrentIp") sendWifiCurrentIp($sender, $objectId, $wifiName,1);
  else if ($command=="off") sendWifiSwitchOff($sender, $objectId, $wifiName,1);
  else if ($command=="on") sendWifiSwitchOn($sender, $objectId, $wifiName,1);
  else if ($command=="status") sendWifiSwitchStatus($sender, $objectId, $wifiName,1);

  exit;
}

if ($_GET["test"]==1) require($_SERVER["DOCUMENT_ROOT"]."/homeserver/include/all.php");


$getModuleIdFunctionId = getObjectFunctionIdByName($BROADCAST_OBJECT_ID, "getModuleId");
$getConfigurationFunctionId = getObjectFunctionIdByName($BROADCAST_OBJECT_ID, "getConfiguration");
$controllerConfigurationFktId = getObjectFunctionIdByName($BROADCAST_OBJECT_ID, "Configuration");
$getRemoteObjectsFunctionId = getObjectFunctionIdByName($BROADCAST_OBJECT_ID, "getRemoteObjects");
$controllerRemoteObjectsFktId = getObjectFunctionIdByName($BROADCAST_OBJECT_ID, "RemoteObjects");
$pingFunctionId = getObjectFunctionIdByName($BROADCAST_OBJECT_ID, "ping");
$writeRulesFunctionId = getObjectFunctionIdByName($BROADCAST_OBJECT_ID, "writeRules");
$resetFunctionId = getObjectFunctionIdByName($BROADCAST_OBJECT_ID, "reset");

$wifiRelevantFunctionIds[]=$getModuleIdFunctionId;
$wifiRelevantFunctionIds[]=$getConfigurationFunctionId;
$wifiRelevantFunctionIds[]=$getRemoteObjectsFunctionId;
$wifiRelevantFunctionIds[]=$pingFunctionId;
$wifiRelevantFunctionIds[]=$writeRulesFunctionId;
$wifiRelevantFunctionIds[]=$resetFunctionId;

function checkWifiFunction($receiver, $sender, $functionData, $commandId)
{
  global $BROADCAST_OBJECT_ID;
  global $WIFI_ETHERNET_CLASS_ID;
  global $WIFI_SCHALTER_CLASS_ID;
  global $getModuleIdFunctionId;
  global $getConfigurationFunctionId;
  global $getRemoteObjectsFunctionId;
  global $pingFunctionId;
  global $wifiRelevantFunctionIds;

  $receiverDeviceId = getDeviceId($receiver);
  $receiverClassId = getClassId($receiver);

  if ($receiverClassId==$WIFI_ETHERNET_CLASS_ID || $receiverClassId==$WIFI_SCHALTER_CLASS_ID ) {}
  else
  {
    $found=0;
    foreach ($wifiRelevantFunctionIds as $validFunctionId)
    {
  	  if ($functionData->functionId==$validFunctionId)
 	  {
		$found=1;
		break;
	  }
	}
	if ($found==0) return;
  }
  
  
  $erg = QUERY("select SQL_CACHE wifiName, objectId from controller where wifiName!=''");
  while($obj=MYSQLi_FETCH_OBJECT($erg))
  {
    if ($receiver == $obj->objectId || $receiver == $BROADCAST_OBJECT_ID)
    {		
      if ($functionData->functionId == $getModuleIdFunctionId) sendWifiModuleId($sender, $obj->objectId, $obj->wifiName);
      else if ($functionData->functionId == $getConfigurationFunctionId) sendWifiConfiguration($sender, $obj->objectId, $obj->wifiName);
      else if ($functionData->functionId == $getRemoteObjectsFunctionId) sendWifiRemoteObjects($sender, $obj->objectId, $obj->wifiName);
      else if ($functionData->functionId == $pingFunctionId) sendWifiPong($sender, $obj->objectId, $obj->wifiName);
      else if ($functionData->functionId == $writeRulesFunctionId) sendWifiMemoryStatus($sender, $obj->objectId, $obj->wifiName);
      else if ($functionData->functionId == $resetFunctionId) resetWifi($sender, $obj->objectId, $obj->wifiName);
    }
	else if ($receiverDeviceId == getDeviceId($obj->objectId))
	{
	   if (getClassId($receiver)== $WIFI_ETHERNET_CLASS_ID && $functionData->functionId==3) sendWifiCurrentIp($sender, $receiver, $obj->wifiName);
	   else if (getClassId($receiver)== $WIFI_SCHALTER_CLASS_ID && $functionData->functionId==2) sendWifiSwitchOff($sender, $receiver, $obj->wifiName);
	   else if (getClassId($receiver)== $WIFI_SCHALTER_CLASS_ID && $functionData->functionId==3) sendWifiSwitchOn($sender, $receiver, $obj->wifiName);
	   else if (getClassId($receiver)== $WIFI_SCHALTER_CLASS_ID && $functionData->functionId==5) sendWifiSwitchStatus($sender, $receiver, $obj->wifiName);
	}
  }
}

function sendWifiModuleId($sender, $objectId, $wifiName, $sync=0)
{
  if ($sync==0) return sendAsync("getModuleId $sender $objectId $wifiName");
  $info = getDeviceInfo($wifiName);
  
  QUERY("update controller set wifiIp='".$info->ip."' where objectId='$objectId' limit 1");
  
  global $BROADCAST_OBJECT_ID;
  $GET_MODULE_ID_RESULT_INDEX = getObjectFunctionsIdByName($BROADCAST_OBJECT_ID, "ModuleId");

  $paramData["name"]=$wifiName;
  $paramData["size"]="999";
  $paramData["majorRelease"]=$info->major;
  $paramData["minorRelease"]=$info->minor;
  $paramData["firmwareId"]="11";

  callInstanceMethodForObjectId($sender, $GET_MODULE_ID_RESULT_INDEX, $paramData, $objectId);
}

function sendWifiConfiguration($sender, $objectId, $wifiName, $sync=0)
{
  if ($sync==0) return sendAsync("getConfiguration $sender $objectId $wifiName");
  $info = getDeviceInfo($wifiName);

  global $BROADCAST_OBJECT_ID;

  $GET_CONFIGURATION_RESULT_INDEX = getObjectFunctionsIdByName($BROADCAST_OBJECT_ID, "Configuration");

  $paramData["dataBlockSize"]="775";

  callInstanceMethodForObjectId($sender, $GET_CONFIGURATION_RESULT_INDEX, $paramData, $objectId);
}

function sendWifiRemoteObjects($sender, $objectId, $wifiName, $sync=0)
{
  if ($sync==0) return sendAsync("getRemoteObjects $sender $objectId $wifiName");
  $info = getDeviceInfo($wifiName);

  global $BROADCAST_OBJECT_ID;
  global $WIFI_SCHALTER_CLASS_ID;
  global $WIFI_ETHERNET_CLASS_ID;

  $GET_REMOTE_OBJECTS_RESULT_INDEX =  getObjectFunctionsIdByName($BROADCAST_OBJECT_ID, "RemoteObjects");
 
  $result="1,$WIFI_SCHALTER_CLASS_ID;2,$WIFI_SCHALTER_CLASS_ID;1,$WIFI_ETHERNET_CLASS_ID";
 
  $paramData["objectList"]=$result;

  callInstanceMethodForObjectId($sender, $GET_REMOTE_OBJECTS_RESULT_INDEX, $paramData, $objectId);
}

function sendWifiCurrentIp($sender, $objectId, $wifiName, $sync=0)
{
  if ($sync==0) return sendAsync("getCurrentIp $sender $objectId $wifiName");

  $info = getDeviceInfo($wifiName);

  $GET_CURRENT_IP_RESULT_INDEX = getObjectFunctionsIdByName($objectId, "CurrentIp");
  
  $parts = explode(".",$info->ip);

  $paramData["IP0"]=$parts[0];
  $paramData["IP1"]=$parts[1];
  $paramData["IP2"]=$parts[2];
  $paramData["IP3"]=$parts[3];

  callInstanceMethodForObjectId($sender, $GET_CURRENT_IP_RESULT_INDEX, $paramData, $objectId);
}

function sendWifiSwitchOff($sender, $objectId, $wifiName, $sync=0)
{
  if ($sync==0) return sendAsync("off $sender $objectId $wifiName");

  $instance = getInstanceId($objectId);
  
  $command="http://$wifiName/cm?user=admin&password=hausbus&cmnd=Power$instance%200";
  $result = file_get_contents($command);
  
  if (strpos($result,'"ON"')!==FALSE)
  {
    $RESULT_INDEX = getObjectFunctionsIdByName($objectId, "evOn");
    callInstanceMethodForObjectId($sender, $RESULT_INDEX, $paramData, $objectId);
  }
  else if (strpos($result,'"OFF"')!==FALSE)
  {
    $RESULT_INDEX = getObjectFunctionsIdByName($objectId, "evOff");
    callInstanceMethodForObjectId($sender, $RESULT_INDEX, $paramData, $objectId);
  }
}

function sendWifiSwitchOn($sender, $objectId, $wifiName, $sync=0)
{
  if ($sync==0) return sendAsync("on $sender $objectId $wifiName");

  $instance = getInstanceId($objectId);

  $command="http://$wifiName/cm?user=admin&password=hausbus&cmnd=Power$instance%201";
  $result = file_get_contents($command);
  
  if (strpos($result,'"ON"')!==FALSE)
  {
    $RESULT_INDEX = getObjectFunctionsIdByName($objectId, "evOn");
    callInstanceMethodForObjectId($sender, $RESULT_INDEX, $paramData, $objectId);
  }
  else if (strpos($result,'"OFF"')!==FALSE)
  {
    $RESULT_INDEX = getObjectFunctionsIdByName($objectId, "evOff");
    callInstanceMethodForObjectId($sender, $RESULT_INDEX, $paramData, $objectId);
  }
}

function sendWifiSwitchStatus($sender, $objectId, $wifiName, $sync=0)
{
  if ($sync==0) return sendAsync("status $sender $objectId $wifiName");

  $instance = getInstanceId($objectId);

  $command="http://$wifiName/cm?user=admin&password=hausbus&cmnd=Power$instance";
  $result = file_get_contents($command);
  
  if (strpos($result,'"ON"')!==FALSE) $paramData["state"]="1";
  else if (strpos($result,'"OFF"')!==FALSE) $paramData["state"]="0";
  else exit;

  $RESULT_INDEX = getObjectFunctionsIdByName($objectId, "Status");
  callInstanceMethodForObjectId($sender, $RESULT_INDEX, $paramData, $objectId);
}

function sendWifiPong($sender, $objectId, $wifiName, $sync=0)
{
  if ($sync==0) return sendAsync("ping $sender $objectId $wifiName");
  $info = getDeviceInfo($wifiName);

  callObjectMethodByName($sender, "pong", $paramData, $objectId);
}

// "wifiBridge.php moduleId $sender $objectId $wifiName"
function sendAsync($command)
{
   global $pcMode;
   
   if ($pcMode==1)
   {
       $myDir = getcwd();
       $pos = strpos($myDir,"htdocs");
       $myDir = substr($myDir,0,$pos);
       
       $windowsCommand = "\"".$myDir."php\\php ".$myDir."htdocs\\homeserver\\wifiBridge.php call $command\"";
       $windowsCommand = 'start /B cmd /S /C '.$windowsCommand. ' &';

       pclose (popen($windowsCommand,"r") );
   }
   else exec("php /var/www/homeserver/wifiBridge.php call $command > /dev/null 2>/dev/null &");
}

function getDeviceInfo($wifiName)
{
   $info = file_get_contents("http://admin:hausbus@$wifiName/in");
   $pos = strpos($info,"Program Version");
   if ($pos===false) exit;
   $pos = strpos($info,"}",$pos);
   if ($pos===false) exit;

   $pos2 = strpos($info,".",$pos);
   if ($pos2===false) exit;
   $result->major=substr($info,$pos+1,$pos2-$pos-1);

   $pos = strpos($info,".",$pos2+1);
   if ($pos===false) exit;
   $result->minor=substr($info,$pos2+1,$pos-$pos2-1);

   $pos = strpos($info,"IP Address");
   if ($pos===false) exit;
   $pos = strpos($info,"}",$pos);
   if ($pos===false) exit;
   $pos2 = strpos($info,"}",$pos+1);
   if ($pos2===false) exit;
   $result->ip=substr($info, $pos+2, $pos2-$pos-2);
   
   return $result;
}

function debugOut($text)
{
  file_put_contents("/var/www/homeserver/test.txt",$text."\n", FILE_APPEND);	
}
?>