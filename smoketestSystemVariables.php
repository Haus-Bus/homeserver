<?php
include ($_SERVER ["DOCUMENT_ROOT"] . "/homeserver/include/all.php");

//callObjectMethodByName(508, "getModuleId", array("index"=>"0"));

if ($submitted == 1)
{
	  setupTreeAndContent ( "empty_design.html", $message );
    echo $html;
	  
	  liveOut("<b>Test:</b> $test");
	  unset($testControllerIds);
	  $controller="";
	  $erg = QUERY("select id, name, objectId from controller where id='$testController' limit 1");
    if($obj=mysqli_fetch_OBJECT($erg))
    {
  	  	$controller.=$obj->name;
   	  	$controllerObjectId=$obj->objectId;
    }
    else die("Fehler controller");
    
    liveOut("<b>Gewählte Controller:</b> ".$controller);
    liveOut("<b>Parameter:</b> Runden = ".$rounds.", Verzögerung = ".$delay);
    liveOut("");
   
    for ($round=1;$round<=$rounds;$round++)
    {
    	liveOut("Runde $round: ",0);
    	
    	// Systemvariablen Bits setzen

      for ($index=0;$index<32;$index++)
      {
      	$value=$index%2;
      	
        $result = executeCommand($controllerObjectId, "setSystemVariable", array("type" => "0","index" => "$index","value" => "$value"));
        sleepMs(200);
        $systemVariable = executeCommand($controllerObjectId, "getSystemVariable", array("type" => "0","index" => "$index"),"SystemVariable");
        if ($systemVariable==-1) die("Fehler: Antwort nicht erhalten");
        else if ($systemVariable["index"]!=$index || $systemVariable["value"]!=$value)
        {
        	print_r($systemVariable);
        	die("Erwartet Index = $index Value = $value");
        }
      }
die("OK");
    
      if ($delay>0) sleepMs($delay);
 	  }
 	  
    QUERY("TRUNCATE table smoketest");
    QUERY("TRUNCATE table smoketestHelper");

    liveOut("");
    liveOut("Smoketest beendet");
    exit;
}

setupTreeAndContent ("smoketestSystemVariables_design.html", $message );


$instanceTag = getTag("%INSTANCES%",$html);
$instances="";
$erg = QUERY("select id, name from controller where online='1' and size!='999' order by name");
while($obj=mysqli_fetch_OBJECT($erg))
{
  $actTag = $instanceTag;
  $actTag = str_replace("%CONTROLLER_NAME%",$obj->name, $actTag);
  $actTag = str_replace("%CONTROLLER_ID%",$obj->id, $actTag);
  $instances.=$actTag;
}
$html = str_replace("%INSTANCES%", $instances, $html);

show ();

?>