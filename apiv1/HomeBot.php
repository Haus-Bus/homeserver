<?php
namespace homeserver\apiv1;

use homeserver\apiv1\Telegram\HomeBot;

try 
{
    // initialize container
    $container = require __DIR__ . '/Configuration/container.php';

    /**
     * @var TelegramBot $telegram
     */
    $bot = $container->get(HomeBot::class);
    $bot->checkSensorsAlive();
    $bot->processMessages();

} catch (\Exception $ex) {
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');
    http_response_code(404);
    echo json_encode(array(
        'message' => $ex->getMessage(),
        'code' => $ex->getCode(),
        'trace' => $ex->getFile() . ':' . $ex->getLine()
    ));
}