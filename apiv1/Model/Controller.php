<?php

namespace homeserver\apiv1\Model;


class Controller extends BaseModel
{
    /**
     * @var int $objectId Firmware-ID of the instance
     */
    private $objectId;

     /**
     * @var bool controller is online
     */
    private $online;

    /**
     * @return int Firmware-ID of the instance
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @param int $objectId Firmware-ID of the instance
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
    }

    /**
     * @return bool controller is online
     */
    public function isOnline()
    {
        return $this->online;
    }

    /**
     * @param bool $online controller is online
     */
    public function setOnline($online)
    {
        $this->online = $online;
    }   

    public function jsonSerialize()
    {
        $baseArray = parent::jsonSerialize();
        $baseArray['objectId'] = $this->objectId;
        $baseArray['online'] = $this->online;
        return $baseArray;
    }
}
