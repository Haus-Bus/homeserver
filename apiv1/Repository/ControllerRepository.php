<?php

namespace homeserver\apiv1\Repository;


use Exception;
use homeserver\apiv1\Model\BaseModel;
use homeserver\apiv1\Model\Controller;

class ControllerRepository extends BaseRepository
{

    /**
     * @Inject("tablenames.controller")
     * @var string name of the db table
     */
    protected $tableName;

    /**
     * @param array $row the fetched row from the database
     * @return Controller the object instance
     * @throws \Exception
     */
    protected function createInstance(array $row)
    {
        $controller = $this->factory->make(Controller::class, [
            'id' => intval($row['id']),
            'name' => $row['name']
        ]);
        $controller->setOnline(boolval($row['online']));
        $controller->setObjectId(intval($row['objectId']));
        return $controller;
    }
}
