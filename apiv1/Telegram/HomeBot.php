<?php

namespace homeserver\apiv1\Telegram;

use homeserver\apiv1\Model\Feature;
use homeserver\apiv1\Repository\FeatureRepository;
use homeserver\apiv1\Repository\LastReceivedRepository;
use homeserver\apiv1\Telegram\Telegram;

// featureClassesId of needed features
const TEMP_ID = 2;
const HUMIDITY_ID = 23;
const BRIGHTNESS_ID = 34;
const POWER_METER_ID = 36;
const DEW_POINT_ID = 38;
const BOT_TOKEN = "2036965705:AAE76xPfEQ8v4WsB5ZQdEfQc8zipUhmbLIk";
const ADMIN_CHAT_ID = 272077072;

class HomeBot
{
    /**
     * @Inject
     * @var FeatureRepository the repository to get featureinstances
     */
    private $featureRepository;

    /**
     * @var Telegram api to communicate to users
     */
    private $telegram;

      /**
     * ApiController constructor.
     *
     * @param \Interop\Container\ContainerInterface $container
     */
    public function __construct()
    {
        $this->telegram = new Telegram(BOT_TOKEN);   
    }  
    

    public function checkSensorsAlive()
    {
        /**
        * @var Feature allSensors contains instances of type Feature
        */
        $allSensors = $this->featureRepository->getByFeatureClassesIds( array(HUMIDITY_ID, TEMP_ID, POWER_METER_ID, DEW_POINT_ID) ); 

        $fiveHoursAgo = new \DateTime();
        $fiveHoursAgo->sub( new \DateInterval('PT5H'));
        $message = "";

        foreach($allSensors as $sensor)
        {
            $lastReceived = $sensor->getLastReceived('evStatus');
            if ($lastReceived != null )
            {
                $lastReceivedDate = $lastReceived[0]->getCreated();
                if( $lastReceivedDate < $fiveHoursAgo )
                {
                    $controller = $sensor->getController();
                    if( $controller->isOnline() )
                    {
                        $message .= $controller->getName() . "->" . $sensor->getName() . " hat seit "
                                  . $lastReceivedDate->format('Y-m-d H:i:s') . " keinen Status mehr gemeldet!"
                                  . PHP_EOL;
                    }
                }
            }
            else
            {
                $controller = $sensor->getController();
                $message .= $controller->getName() . "->" . $sensor->getName() 
                          . " hat keinen Status mehr gemeldet!"
                          . PHP_EOL;
            }
        }
        if( !empty($message) )
        {
            $this->telegram->sendMessage(['chat_id' => ADMIN_CHAT_ID, 'text' => $message]);
        }
    }

    public function processMessages()
    {
        // Get all the new updates and set the new correct update_id
        $req = $this->telegram->getUpdates();

        for ($i = 0; $i < $this->telegram->UpdateCount(); $i++) {
            // You NEED to call serveUpdate before accessing the values of message in Telegram Class
            $this->telegram->serveUpdate($i);
            $text = $this->telegram->Text();
            $chat_id = $this->telegram->ChatID();

            echo $text.' from '.$this->telegram->FirstName().' in ChatId: '.$chat_id.'<br>';
            //var_dump($telegram->getData());

            if ($text == '/temp')
            {
                $reply = "Not supported !";
                $content = ['chat_id' => $chat_id, 'text' => $reply];
                $this->telegram->sendMessage($content);
            }
            if ($text == '/start') {
                $reply = 'Working';
                $content = ['chat_id' => $chat_id, 'text' => $reply];
                $this->telegram->sendMessage($content);
            }
            if ($text == '/status') {
                if ($this->telegram->messageFromGroup()) {
                    $reply = 'Chat Group';
                } else {
                    $reply = 'Private Chat';
                }
                // Create option for the custom keyboard. Array of array string
                $option = [['A', 'B'], ['C', 'D']];
                // Get the keyboard
                $keyb = $this->telegram->buildKeyBoard($option);
                $content = ['chat_id' => $chat_id, 'reply_markup' => $keyb, 'text' => $reply];
                $this->telegram->sendMessage($content);
            }
        }
    }
}
