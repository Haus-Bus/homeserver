<?php
include("../include/all.php");

/* über RS485 umkonfigurieren

\xFD\xFF\x00\x00\x01\x00\xBE\x2F\x00\x10\x00\x00\x05\x00\x01\x64\x32\x61\x03\xFE

FD   HEADER
FF    CHECKSUMME - FF = EGAL
00    CONTROLBYTE - 00 = EGAL
00    PAKETCOUNTER - 00 = EGAL
01    SENDER_INSTANCEID - Hier 1
00    SENDER_CLASSID - Hier 0 für Controller
BE    SENDER_DEVICEID_LOW - Hier BE für Raspberry
2F    SENDER_DEVICEID_HIGH - Hier 2F für Raspberry
00    RECEIVER_INSTANCEID  - 00 = Broadcast
10    RECEIVER_CLASSID - hier 16 für Taster
00    RECEIVER_DEVICEID_LOW – hier 00 = Broadcast
00    RECEIVER_DEVICEID_HIGH – hier 00 = Broadcast
05    Länge low
00    Länge high
01    FunktionID – Hier 1 für setConfiguration
64    HoldTimout – Hier 100
32    waitForDoubleClickTimeout – Hier 
61    EventMask – Hier 97
03    optionMask – Hier 3
FE   FOOTER
*/


$debug=0;
$port="15557";

// URL Variablen von addGenericGroup wieder in Variablen umwandeln
if ($params!="") urlToParams($params);

if ($action=="Weiter")
{
	$params = paramsToUrl("name=$name&ip=$ip&naming=$naming&interface=$interface&action=generate");
	header("Location: /homeserver/addGenericGroup.php?params=$params&selectAll=1&classes=Led,Taster,Schalter,Temperatursensor,Feuchtesensor,Dimmer,Rollladen,LogicalButton,Feuchtesensor,Helligkeitssensor,RGB-Dimmer,RFID-Reader&returnUrl=/homeserver/loxone/index.php");
	exit;
}
else if ($action=="wifi")
{
  if ($wifiId=="") $error="Bitte ID vom Aufkleber des WLAN Moduls eingeben!";
  else
  {
    $erg = QUERY("select name from controller where wifiName like '%$wifiId' limit 1");
    if ($row=mysqli_fetch_ROW($erg)) $error = "Modul existiert bereits mit Namen ".$row[0];
    else
    {
      $controllerName = "Wifi $wifiId";
      
	  $erg = QUERY("select count(*) from controller where wifiName!=''");
      $row=mysqli_fetch_ROW($erg);
	  $nextInstance=$row[0]+1;
	  $objectId = getObjectId(998+$nextInstance,0,1);


       $secondVariant=0;
       $info = @file_get_contents("http://admin:hausbus@tasmota-$wifiId/in");
       $pos = strpos($info,"Program Version");
       if ($pos===false)
       {
         $secondVariant=1;
         $info = @file_get_contents("http://admin:hausbus@tasmota_$wifiId/in");
         $pos = strpos($info,"Program Version");
       }
       
       if ($pos===false) $error="Gerät nicht im Netz gefunden. Bitte ins WLAN aufnehmen";
       else
       {
          if ($secondVariant==1) $myName = "tasmota_$wifiId";
          else $myName = "tasmota-$wifiId";

          QUERY("INSERT into controller (objectId,name,firmwareId, wifiName) values ('$objectId ','$controllerName','11','$myName')");
	  updateControllerStatus();
	  $error="Modul $wifiId wurde angelegt";
       }   
    }
  }
}

echo "<html>";
echo '<head><link rel="StyleSheet" href="/homeserver/css/main.css" type="text/css" />';
echo '<body><div class="contentWrap"  id="content" style="margin-right:16px;">';
echo "<br><table width=95% align=center><tr><td>";

if ($error!="") echo "<br><font color=#bb0000><b>$error</b></font><br><br>";

echo "<b>Loxone Miniserver Plugin</b><hr>";

if ($action=="generate")
{
	$where="1=2 ";
	$parts = explode(",",$ids);
	foreach($parts as $id)
	{
		$where.="or featureInstances.id='$id' ";
	}

    $vorlage = convertFilename($name);
      
    $outputs='<?xml version="1.0" encoding="utf-8"?>'."\n";
    $outputs.='<VirtualOut Title="Haus-Bus:'.$vorlage.'" Comment="" Address="/dev/udp/'.$ip.'/'.$port.'" CmdInit="" CloseAfterSend="true" CmdSep="">'."\n";

    $inputs='<?xml version="1.0" encoding="utf-8"?>'."\n";
    $inputs.='<VirtualInUdp Title="Haus-Bus:'.$vorlage.'" Comment="" Address="" Port="'.$port.'">'."\n";

    $rs485 = '<?xml version="1.0" encoding="utf-8"?>'."\n";
	$rs485.= '<Comm Title="Haus-Bus:'.$vorlage.'" Comment="" Baudrate="57600" Databits="8" Parity="1" EndChar="510">'."\n";
    
    $foundAndRs485OrLanElement=false;
	
	$erg = QUERY("select featureClasses.id as featureClassesId, featureClasses.name as featureClassesName, 
	                      featureInstances.objectId, featureInstances.name as featureInstanceName, featureInstances.id as featureInstanceId,
	                      rooms.name as roomName,
	                      controller.name as controllerName, controller.wifiIp, controller.wifiName
	                from featureInstances 
	                join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
	                join controller on (controller.id = featureInstances.controllerId)
	                left join roomFeatures on (roomFeatures.featureInstanceId = featureInstances.id)
	                left join rooms on (rooms.id = roomFeatures.roomId)
               	    where $where order by featureClassesName");
	while($obj=MYSQLi_FETCH_OBJECT($erg))
	{
		if ($obj->wifiName!="") continue;
		
  	  	$deviceId = getDeviceId($obj->objectId);
		$deviceIdHexHigh = encodeHex($deviceId, true);
		$deviceIdHexLow = encodeHex($deviceId);
        $instanceId = getInstanceId($obj->objectId);
		$instanceIdHex = encodeHex($instanceId);
	    $title = $obj->controllerName." - ".$obj->featureInstanceName;

  	  	if ($debug==1) echo "<br><br>".$title."<hr>";
		
		if ($obj->featureClassesName=="Dimmer")
		{
		    $commandOn=$deviceId.".DIM.".$instanceId.".ON.&lt;v&gt;.0";
			$commandOff=$deviceId.".DIM.".$instanceId.".OFF.0.0";
		    
			if ($interface=="rs485")
			{
				$commandOn='\xFD'.$commandOn.'\xFE'; 
				$commandOff='\xFD'.$commandOff.'\xFE';
				
    			$add = '<CommCmd Title="'.$title.'" Comment="" CmdOn="'.$commandOn.'" CmdOff="'.$commandOff.'" Unit="" Analog="true" Text="false" Sensor="false" SourceValHigh="100" DestValHigh="100"/>'."\n";
                if ($debug==1) echo htmlentities($add)."<br>";
				$rs485.=$add;
			}
			else if ($interface=="lan")
			{
			    $add = '<VirtualOutCmd Title="'.$title.'" Comment="" CmdOnMethod="GET" CmdOffMethod="GET" CmdOn="'.$commandOn.'" CmdOnHTTP="" CmdOnPost="" CmdOff="'.$commandOff.'" CmdOffHTTP="" CmdOffPost="" Analog="true" Repeat="0" RepeatRate="0" SourceValHigh="100" DestValHigh="100"/>'."\n";
 		        if ($debug==1) echo htmlentities($add)."<br>";
			    $outputs.=$add;
			}
			
			$foundAndRs485OrLanElement=true;
		}
		else if ($obj->featureClassesName=="RGB-Dimmer")
		{
		    $commandOn=$deviceId.".RGB.".$instanceId.".ON.&lt;v&gt;.0";
			$commandOff=$deviceId.".RGB.".$instanceId.".OFF.0.0";
		    
			if ($interface=="rs485")
			{
				$commandOn='\xFD'.$commandOn.'\xFE'; 
				$commandOff='\xFD'.$commandOff.'\xFE';
				
    			$add = '<CommCmd Title="'.$title.'" Comment="" CmdOn="'.$commandOn.'" CmdOff="'.$commandOff.'" Unit="" Analog="true" Text="false" Sensor="false" SourceValHigh="100" DestValHigh="100"/>'."\n";
                if ($debug==1) echo htmlentities($add)."<br>";
				$rs485.=$add;
			}
			else if ($interface=="lan")
			{
			    $add = '<VirtualOutCmd Title="'.$title.'" Comment="" CmdOnMethod="GET" CmdOffMethod="GET" CmdOn="'.$commandOn.'" CmdOnHTTP="" CmdOnPost="" CmdOff="'.$commandOff.'" CmdOffHTTP="" CmdOffPost="" Analog="true" Repeat="0" RepeatRate="0" SourceValHigh="100" DestValHigh="100"/>'."\n";
 		        if ($debug==1) echo htmlentities($add)."<br>";
			    $outputs.=$add;
			}
			
			$foundAndRs485OrLanElement=true;
		}
		else if ($obj->featureClassesName=="Schalter")
		{
			$commandOn=$deviceId.".OUT.".$instanceId.".ON.0";
			$commandOff=$deviceId.".OUT.".$instanceId.".OFF";
			
			if ($interface=="rs485")
			{
				$commandOn='\xFD'.$commandOn.'\xFE'; 
				$commandOff='\xFD'.$commandOff.'\xFE';
				
    			$add = '<CommCmd Title="'.$title.'" Comment="" CmdOn="'.$commandOn.'" CmdOff="'.$commandOff.'" Unit="" Analog="false" Text="false" Sensor="false" SourceValHigh="100" DestValHigh="100"/>'."\n";
                if ($debug==1) echo htmlentities($add)."<br>";
				$rs485.=$add;
			}
			else if ($interface=="lan")
			{
		      $add = '<VirtualOutCmd Title="'.$title.'" Comment="" CmdOnMethod="GET" CmdOffMethod="GET" CmdOn="'.$commandOn.'" CmdOnHTTP="" CmdOnPost="" CmdOff="'.$commandOff.'" CmdOffHTTP="" CmdOffPost="" Analog="false" Repeat="0" RepeatRate="0" SourceValHigh="100" DestValHigh="100"/>'."\n";
		      if ($debug==1) echo htmlentities($add)."<br>";
			  $outputs.=$add;
			}
			
     		$foundAndRs485OrLanElement=true;
		}
		else if ($obj->featureClassesName=="Led")
		{
			$commandOn=$deviceId.".LED.".$instanceId.".ON.&lt;v&gt;.0";
			$commandOff=$deviceId.".LED.".$instanceId.".OFF";
			
			if ($interface=="rs485")
			{
				$commandOn='\xFD'.$commandOn.'\xFE'; 
				$commandOff='\xFD'.$commandOff.'\xFE';
				
    			$add = '<CommCmd Title="'.$title.'" Comment="" CmdOn="'.$commandOn.'" CmdOff="'.$commandOff.'" Unit="" Analog="true" Text="false" Sensor="false" SourceValHigh="100" DestValHigh="100"/>'."\n";
                if ($debug==1) echo htmlentities($add)."<br>";
				$rs485.=$add;
			}
			else if ($interface=="lan")
			{
			    $add = '<VirtualOutCmd Title="'.$title.'" Comment="" CmdOnMethod="GET" CmdOffMethod="GET" CmdOn="'.$commandOn.'" CmdOnHTTP="" CmdOnPost="" CmdOff="'.$commandOff.'" CmdOffHTTP="" CmdOffPost="" Analog="true" Repeat="0" RepeatRate="0" SourceValHigh="100" DestValHigh="100"/>'."\n";
			    if ($debug==1) echo htmlentities($add)."<br>";
			    $outputs.=$add;
			}
			
		    $foundAndRs485OrLanElement=true;
		}
		else if ($obj->featureClassesName=="Taster")
		{
			$check=$deviceId.".BTN.".$instanceId.".STATUS.".'\v';
			
			if ($interface=="rs485")
			{
				$check='\xFD'.$check.'\xFE'; 
				
				$add = '<CommCmd Title="'.$title.'" Comment="" CmdOn="'.$check.'" CmdOff="" Unit="&lt;v.x&gt;" Analog="true" Text="false" Sensor="true" SourceValHigh="100" DestValHigh="100"/>'."\n";
				if ($debug==1) echo htmlentities($add)."<br>";
				$rs485.=$add;
			}
			else if ($interface=="lan")
			{
			    $add = '<VirtualInUdpCmd Title="'.$title.'" Comment="" Address="" Check="'.$check.'" Unit="&lt;v.x&gt;" Signed="false" Analog="true" SourceValLow="0" DestValLow="0" DefVal="0" MinVal="0" MaxVal="100" SourceValHigh="100" DestValHigh="100"/>'."\n";
			    if ($debug==1) echo htmlentities($add)."<br>";
			    $inputs.=$add;
			}
			
			$foundAndRs485OrLanElement=true;
		}
		else if ($obj->featureClassesName=="Temperatursensor")
		{
  		    $check=$deviceId.".TMP.".$instanceId.".STATUS.".'\v';
			
			if ($interface=="rs485")
			{
				$check='\xFD'.$check.'\xFE'; 
				
				$add = '<CommCmd Title="'.$title.'" Comment="" CmdOn="'.$check.'" CmdOff="" Unit="&lt;v.2&gt;" Analog="true" Text="false" Sensor="true" SourceValHigh="100" DestValHigh="100"/>'."\n";
				if ($debug==1) echo htmlentities($add)."<br>";
				$rs485.=$add;
			}
			else if ($interface=="lan")
			{
			  $add = '<VirtualInUdpCmd Title="'.$title.'" Comment="" Address="" Check="'.$check.'" Signed="true" Analog="true" SourceValLow="0" DestValLow="0" DefVal="0" MinVal="-100" MaxVal="100" SourceValHigh="100" DestValHigh="100"/>'."\n";
			  if ($debug==1) echo htmlentities($add)."<br>";
			  $inputs.=$add;
			}
			
			$foundAndRs485OrLanElement=true;
		}
		else if ($obj->featureClassesName=="Feuchtesensor")
		{
			$check=$deviceId.".RHD.".$instanceId.".STATUS.".'\v';
			
			if ($interface=="rs485")
			{
				$check='\xFD'.$check.'\xFE'; 
				
				$add = '<CommCmd Title="'.$title.'" Comment="" CmdOn="'.$check.'" CmdOff="" Unit="&lt;v.x&gt;" Analog="true" Text="false" Sensor="true" SourceValHigh="100" DestValHigh="100"/>'."\n";
				if ($debug==1) echo htmlentities($add)."<br>";
				$rs485.=$add;
			}
			else if ($interface=="lan")
			{
			   $add = '<VirtualInUdpCmd Title="'.$title.'" Comment="" Address="" Check="'.$check.'" Signed="false" Analog="true" SourceValLow="0" DestValLow="0" SourceValHigh="100" DestValHigh="100" DefVal="0" MinVal="0" MaxVal="100"/>'."\n";
			   if ($debug==1) echo htmlentities($add)."<br>";
			   $inputs.=$add;
			}
			
			$foundAndRs485OrLanElement=true;
		}
		else if ($obj->featureClassesName=="Helligkeitssensor")
		{
			$check=$deviceId.".BRS.".$instanceId.".STATUS.".'\v';
			
			if ($interface=="rs485")
			{
				$check='\xFD'.$check.'\xFE'; 
				
				$add = '<CommCmd Title="'.$title.'" Comment="" CmdOn="'.$check.'" CmdOff="" Unit="&lt;v.x&gt;" Analog="true" Text="false" Sensor="true" SourceValHigh="100" DestValHigh="100"/>'."\n";
				if ($debug==1) echo htmlentities($add)."<br>";
				$rs485.=$add;
			}
			else if ($interface=="lan")
			{
			   $add = '<VirtualInUdpCmd Title="'.$title.'" Comment="" Address="" Check="'.$check.'" Signed="false" Analog="true" SourceValLow="0" DestValLow="0" SourceValHigh="100" DestValHigh="100" DefVal="0" MinVal="0" MaxVal="100"/>'."\n";
			   if ($debug==1) echo htmlentities($add)."<br>";
			   $inputs.=$add;
			}
			
			$foundAndRs485OrLanElement=true;
		}
		else if ($obj->featureClassesName=="LogicalButton")
		{
		    $commandOn=$deviceId.".LBN.".$instanceId.".SET_MBR.&lt;v&gt;.0";
			$commandOff=$deviceId.".LBN.".$instanceId.".SET_MBR.0";
			
			if ($interface=="rs485")
			{
			   $commandOn='\xFD'.$commandOn.'\xFE'; 
			   $commandOff='\xFD'.$commandOff.'\xFE';
			   
    		   $add = '<CommCmd Title="'.$title.'" Comment="" CmdOn="'.$commandOn.'" CmdOff="'.$commandOff.'" Unit="" Analog="true" Text="false" Sensor="false" SourceValHigh="100" DestValHigh="100"/>'."\n";
			   if ($debug==1) echo htmlentities($add)."<br>";
			   $rs485.=$add;
			}
			else if ($interface=="lan")
			{
			   $add = '<VirtualOutCmd Title="'.$title.'" Comment="" CmdOnMethod="GET" CmdOffMethod="GET" CmdOn="'.$commandOn.'" CmdOnHTTP="" CmdOnPost="" CmdOff="'.$commandOff.'" CmdOffHTTP="" CmdOffPost="" Analog="true" Repeat="0" RepeatRate="0" SourceValHigh="100" DestValHigh="100"/>'."\n";
			   if ($debug==1) echo htmlentities($add)."<br>";
			   $outputs.=$add;
			}
			
			$foundAndRs485OrLanElement=true;
		}
		else if ($obj->featureClassesName=="RFID-Reader")
		{
			$check=$deviceId.".RFD.".$instanceId.".TAG_ID.".'\v';
			
			if ($interface=="rs485")
			{
				$check='\xFD'.$check.'\xFE'; 
				
				$add = '<CommCmd Title="'.$title.'" Comment="" CmdOn="'.$check.'" CmdOff="" Unit="&lt;v.x&gt;" Analog="false" Text="true" Sensor="true"/>'."\n";
				if ($debug==1) echo htmlentities($add)."<br>";
				$rs485.=$add;
			}
			else if ($interface=="lan")
			{
			   $add = '<VirtualInUdpCmd Title="'.$title.'" Comment="" Address="" Check="'.$check.'" Analog="false" Text="true" />'."\n";
			   if ($debug==1) echo htmlentities($add)."<br>";
			   $inputs.=$add;
			}
			
			$foundAndRs485OrLanElement=true;
		}
  	}
	
	$outputs.="</VirtualOut>";
    $inputs.="</VirtualInUdp>";
	$rs485.="</Comm>";
    
	if ($foundAndRs485OrLanElement)
	{
		if ($interface=="rs485")
		{
		  $outFile = $vorlage."_RS485_HausBusDe.xml";
		  file_put_contents($outFile, $rs485);
			  
		  echo "<br><b>RS485 Vorlagen runterladen</b><br><br>";
		  echo "<li> <a href='$outFile'><u>Vorlage für RS485 Interface runterladen</u></a>  &nbsp;(Rechts anklicken und 'Speichern unter' wählen)<br><br>";
		  echo "<br><br><br><br>";
		
		  echo "<b>Importieren im Miniserver</b><br><br><table>";
		  echo "<tr><td valign=top>- </td><td valign=top>Falls noch nicht geschehen, fügen Sie Ihre Loxone RS485 Extension im Peripheriebaum ein per Klick auf Ihren Schaltschrank und wählen Sie oben im Menü 'Extension einfügen' -> RS485 Extension<br>Damit die Extension aktiviert wird, müssen Sie diese anklicken und in den Eigenschaften die Seriennummer eintragen oder alternativ 'auto', falls nur eine RS485 Extension vorhanden ist.</td></tr>";
		  echo "<tr><td valign=top>- </td><td valign=top>Klicken Sie im Peripheriebaum die RS485 Extension an und anschließend oben im Menü auf 'Sensoren und Aktoren' -> 'Vordefinierte Geräte' -> 'Vorlage importieren' und wählen dann die zuvor gespeicherte Datei.</td></tr>";
		  echo "</table>";
		}
		else if ($interface=="lan")
		{
		  configureAllGatewaysForHausBusProtokoll(); 	
			
		  $outFile = $vorlage."_OUTPUTS_HausBusDe.xml";
		  $inFile = $vorlage."_INPUTS_HausBusDe.xml";

		  file_put_contents($outFile, $outputs);
		  file_put_contents($inFile, $inputs);
			  
		  echo "<br><b>LAN Vorlagen runterladen</b><br><br>";
		  echo "<li> <a href='$outFile'><u>Vorlage für Ausgänge runterladen</u></a>  &nbsp;(Rechts anklicken und 'Speichern unter' wählen)<br><br>";
		  echo "<li> <a href='$inFile'><u>Vorlage für Eingänge runterladen</u></a>  &nbsp;&nbsp;(Rechts anklicken und 'Speichern unter' wählen)<br><br>";
		  echo "<br><br>";
		
		  echo "<b>Importieren im Miniserver</b><br><br>";
		  echo "Im Miniserver beim Peripheriebaum auf 'Virtuelle Eingänge' klicken und dann oben im Menü auf 'Vordefinierte UDP-Geräte' -> Vorlage importieren -> zuvor gespeicherte Datei mit INPUTS im Namen wählen.<br><br>";
		  echo "Im Miniserver beim Peripheriebaum auf 'Virtuelle Ausgänge' klicken und dann oben im Menü auf 'Vordefinierte Geräte' -> Vorlage importieren -> zuvor gespeicherte Datei mit OUTPUTS im Namen wählen.<br><br><br><br>";
		}
	}
	
	$wifiElementFound=false;
	$lastControllerId=-1;
	$lastWifiName="";
	$lastControllerName="";
	$output="";
	
	$erg = QUERY("select featureClasses.id as featureClassesId, featureClasses.name as featureClassesName, 
	                      featureInstances.objectId, featureInstances.name as featureInstanceName, featureInstances.id as featureInstanceId,
	                      rooms.name as roomName,
	                      controller.name as controllerName, controller.wifiIp, controller.wifiName, controller.id as controllerId
	                from featureInstances 
	                join featureClasses on (featureClasses.id = featureInstances.featureClassesId)
	                join controller on (controller.id = featureInstances.controllerId)
	                left join roomFeatures on (roomFeatures.featureInstanceId = featureInstances.id)
	                left join rooms on (rooms.id = roomFeatures.roomId)
               	    where $where order by controller.id,featureClassesName");
	while($obj=MYSQLi_FETCH_OBJECT($erg))
	{
	   if ($obj->wifiName=="") continue;
	   
	   if (!$wifiElementFound) echo "<hr><br><br><b>WIFI Vorlagen runterladen</b><br><br>";
	   $wifiElementFound=true;

       if ($obj->controllerId!=$lastControllerId)
	   {
		  if ($output!="") saveWifiOutput($output, $vorlage, $lastWifiName, $lastControllerName);
		  $lastControllerId=$obj->controllerId;
		  $lastWifiName=$obj->wifiName;
		  $lastControllerName = $obj->controllerName;
		  $output='<?xml version="1.0" encoding="utf-8"?>'."\n";
          $output.='<VirtualOut Title="Haus-Bus:wifi_'.$vorlage.'_'.$obj->wifiName.'" Comment="" Address="http://'.$obj->wifiName.'" CmdInit="" CloseAfterSend="true" CmdSep="">'."\n";
	   }

	   $title = $obj->controllerName." - ".$obj->featureInstanceName;
       $instanceId = getInstanceId($obj->objectId);

       $commandOn="/cm?user=admin&amp;password=hausbus&amp;cmnd=Power".$instanceId."%201";
	   $commandOff="/cm?user=admin&amp;password=hausbus&amp;cmnd=Power".$instanceId."%200";
	   
	   $output.='<VirtualOutCmd Title="'.$title.'" Comment="" CmdOnMethod="GET" CmdOffMethod="GET" CmdOn="'.$commandOn.'" CmdOnHTTP="" CmdOnPost="" CmdOff="'.$commandOff.'" CmdOffHTTP="" CmdOffPost="" Analog="false" Repeat="0" RepeatRate="0"/>'."\n";
	}

    if ($wifiElementFound)
	{
	  saveWifiOutput($output, $vorlage, $lastWifiName, $lastControllerName);
	  
	  echo "<br><br>";
	  echo "<b>Importieren im Miniserver</b><br><br>";
	  //echo "Im Miniserver beim Peripheriebaum auf 'Virtuelle Eingänge' klicken und dann oben im Menü auf 'Vordefinierte UDP-Geräte' -> Vorlage importieren -> zuvor gespeicherte Datei mit WIFI_INPUTS im Namen wählen.<br><br>";
	  echo "Im Miniserver beim Peripheriebaum auf 'Virtuelle Ausgänge' klicken und dann oben im Menü auf 'Vordefinierte Geräte' -> Vorlage importieren -> zuvor gespeicherte Datei mit WIFI_OUTPUTS im Namen wählen.<br><br><br><br>";
	}
		
    exit;
}

function saveWifiOutput($output, $vorlage, $wifiName, $lastControllerName)
{
   $output.='</VirtualOut>';

   $outFile = $vorlage."_".$wifiName."_WIFI_OUTPUTS_HausBusDe.xml";
   
   file_put_contents($outFile, $output);
   echo "<li> <a href='$outFile'><u>Vorlage für Ausgänge ".$lastControllerName." runterladen</u></a>  &nbsp;(Rechts anklicken und 'Speichern unter' wählen)<br><br>";
}

echo "Mit diesem Plugin können automatisch Eingangs- und Ausgangsvorlagen für den Loxone Miniserver generiert und in diesen importiert werden.<br>Anschließend stehen im Miniserver sofort jeder Taster, Temperatur, Helligkeit und Luftfeuchtigkeit als Eingang sowie jede LED, Dimmer oder Relais als Ausgang zur Verfügung. Nach der Generierung der Vorlagen wird eine Anleitung für den Import im Miniserver angezeigt.";
echo "<br><br><br><br><br><br>";
echo "<b>Vorlagen generieren</b>";
echo "<br><br><form action='index.php' method='POST'>
<table>
<tr><td width=110>Vorlagenname</td><td><input type=text size=30 name='name' value='$name' style='width:120px'></td><td>&nbsp;&nbsp;</td><td>Frei wählbare Bezeichnung, die sich von vorherigen Imports im Miniserver unterscheiden muss</td></tr>";

if ($interface=="lan") $lanChecked=checked;
if ($interface=="rs485") $rs485Checked=checked;
echo "<tr><td><br></td></tr><tr><td valign=top>Schnittstelle</td><td valign=top><input onChange='document.forms[0].submit()' type=radio name=interface value=lan $lanChecked>LAN <input onChange='document.forms[0].submit()' type=radio name=interface value=rs485 $rs485Checked>RS485</td></td><td>&nbsp;&nbsp;</td><td>Schnittstelle wählen, über die die Haus-Bus Komponenten mit dem Miniserver kommunizieren sollen. Für eine Anbindung per RS485 wird die Loxone RS485 Extension benötigt. Für LAN muss ein Haus-Bus Modul ein LAN Gateway besitzen.</tr>";

if ($interface=="lan") echo "<tr><td><br></td><tr><td valign=top>IP Haus-Bus Modul</td><td valign=top><input type=text size=30 name='ip' style='width:120px'></td><td>&nbsp;&nbsp;</td><td>Feste IP vom Haus-Bus Modul - Nicht die vom Miniserver! Format xxx.xxx.xxx.xxx<br>Diese kann links im Baum beim Modul -> Ethernet -> Button ´getCurrentIP´ abgefragt werden.)<br>Bitte die IP im Router (z.b. Fritzbox) fest einstellen, damit sie sich nicht im Laufe der Zeit ändert.<br><br>Falls noch nicht geschehen, muss die feste IP des Miniservers ebenfalls einmalig beim Haus-Bus Modul konfiguriert werden. Dafür links im Baum beim Modul -> Ethernet 1 wählen und dort bei ´setConfiguration´ die Miniserver IP eintragen bei Loxone_IP0 bis Loxone_IP3 und anschließend auf ´Aufrufen´ klicken. Abschließend einmal im Baum das Modul selbst anklicken und dort auf ´Aufrufen´ bei ´Reset´ klicken.</td></tr>";
if ($interface!="") echo "<tr><td><br></td><tr><td></td><td><input type=submit name='action' value='Weiter'></td></tr>";
echo "
</table>
</form>

<br><br><br><br><br><br>
<b>Wichtige Infos</b><br>
<table>
<tr><td valign=top><li></td><td>Wenn mehrere Haus-Bus Module verwendet werden, sollten diese per RS485 verbunden und nur einmalig LAN angeschlossen werden. Falls mehrere Module per LAN betrieben werden, dürfen diese auf keinen Fall gleichzeitig per RS485 verbunden werden! Zusätzlich müssen die Features von mehrern per LAN angeschlossenen Haus-Bus Modulen immer nacheinander im Miniserver importiert werden, weil sich die IP Adresse der Module unterscheidet.</td><td></td></tr>
<tr><td valign=top><li></td><td>Wann immer im Miniserver eine zweite Vorlage importiert wird, legt dieser auch ein zweites UDP Device mit gleichem Port an, was später bei der Übermittlung in den Miniserver eine Fehlermeldung verursacht. Deshalb die neu importierten Eingänge mit der Maus markieren und mit rechter Maustaster kopieren, anschließend beim ersten UDP Gerät bei den Eingängen einfügen und anschließend die neu importierten Eingänge löschen. Bei den Ausgängen gibt es dieses Problem nicht.</td><td></td></tr>
</table>
<br><br><br><br><br><br>
<b>WIFI Komponenten hinzufügen</b><hr>
Hier können zusätzliche Haus-Bus.de kompatible WLAN Komponenten ergänzt und anschließend auf normalem Wege in den Loxone Miniserver integriert werden.<br>
<br><form action='index.php' method='POST'><input type=hidden name=action value=wifi>
<table>
<tr><td width=110>Device ID vom Aufkleber</td><td><input type=text name=wifiId size=16 style='width:130px'></td><td>(z.b. C472AA-4778)</td></tr>
<tr><td></td><td><input type=submit value='Gerät hinzufügen' style='width:130px'></td></tr>
</table
</form>
";

/*

DeviceId.ClassId.InstanceId.CommandId/EventId.Param1.Param2.ParamX
DEC     .TEXT   .DEC       .TEXT             .DEC   .DEC   .DEC

Bisher unterstützte ClassIDs:
LED - LED
BTN - BUTTON/Taster
OUT - Schalter/Relais
TMP - Temperatursensor
RHD - Feuchtigkeitssensor
DIM - Dimmer

CommandIDs
ON    - für LED, OUT, DIM
OFF   - für LED, OUT, DIM
START - für DIM
STOP  - für DIM

EventIDs
STATUS         - Alle Rückmeldungen / Statusmeldungen aller ClassIDs
FREE           - BTN wenn Eingang nicht betätigt
COVERED        - BTN wenn Eingang betätigt
CLICKED        - BTN wenn Eingang betätigt und wieder losgelassen wurde
DOUBLE_CLICKED - BTN wenn Eingang 2x betätigt und wieder losgelassen wurde
HOLD_START     - BTN wenn Eingang betätigt und länger als eingestellte Zeit gehalten wird
HOLD_END       - BTN wenn Eingang nach HOLD_START wieder losgelassen wird

Parameter1 bis X sind immer wie in der Homeserver Struktur definiert als Zahl einzugeben und durch '.' zu trennen.
Parameter können am Ende optional weggelassen werden. Wenn sie weggelassen werden, füllt die FW mit defaultwerten auf.

Beispiele:

8966.LED.51.STATUS.0
4102.BTN.22.FREE
4107.LED.65.ON.100.10
4107.LED.65.STATUS.100
4107.LED.65.STATUS.0
4608.BTN.83.COVERED
4107.LED.65.OFF
4107.LED.65.STATUS.0
4864.BTN.99.COVERED
4107.LED.65.ON
4107.LED.65.STATUS.100
100.OUT.5.ON
100.OUT.5.STATUS.1
100.OUT.5.OFF
100.OUT.5.STATUS.0
8448.BTN.99.COVERED
769.TMP.55.STATUS.19,30
769.RHD.55.STATUS.48
*/


function configureAllGatewaysForHausBusProtokoll()
{
  $erg = QUERY("select id, controllerId, objectId from featureInstances where featureClassesId='27' order by name");
  while($obj=mysqli_fetch_OBJECT($erg))
  {
    if (getInstanceId($obj->objectId)!=4) continue;

    callInstanceMethodForObjectId($obj->objectId, 217, array("options" => "1"));
	  
    // 1 = Taster: sonst werden Taster nicht richtig konfiguriert, wenn man mit Cache übermittelt
    if ($id==1)  QUERY("DELETE from configCache where featureInstanceId='$obj->id' limit 1"); 
    usleep(30000);
  }
}

function convertFilename($fileName)
{
  $newFilename=$fileName;
  $newFilename = str_replace("ü","ue",$newFilename);
  $newFilename = str_replace("ä","ae",$newFilename);
  $newFilename = str_replace("ö","oe",$newFilename);
  $newFilename = str_replace("ß","ss",$newFilename);
  $newFilename = str_replace("#","_",$newFilename);
  $newFilename = str_replace("&","_",$newFilename);
  $newFilename = str_replace(" ","_",$newFilename);
  $newFilename = str_replace("(","_",$newFilename);
  $newFilename = str_replace(")","_",$newFilename);
  $newFilename = str_replace("/","_",$newFilename);
  $newFilename = str_replace("\\","_",$newFilename);
  $newFilename = str_replace("'","_",$newFilename);
  $newFilename = str_replace("´","_",$newFilename);
  $newFilename = str_replace("`","_",$newFilename);
  $newFilename = str_replace('"',"_",$newFilename);
  $newFilename = str_replace(';',"_",$newFilename);

  $pos=strpos($newFilename,".");
  $pos=strpos($newFilename,".",$pos+1);
  if ($pos!==FALSE) $newFilename = str_replace(".","a",$newFilename);
  return $newFilename;
}

function encodeHex($dec, $highByte=false)
{
	if ($highByte) $dec/=256;
    $dec%=256;
		
	$result = dechex($dec);
	if ($dec<16) $result="0".$result;
	return strtoupper($result);
}
?>  

   