<?php
include("../include/all.php");

if ($datum!=date("Y-m-d")) die("Freischalten mit ?datum=".date("Y-m-d"));

QUERY("TRUNCATE `appmessages`");
QUERY("TRUNCATE `basicrulegroupsignals`");
QUERY("TRUNCATE `basicrules`");
QUERY("TRUNCATE `basicrulesignalparams`");
QUERY("TRUNCATE `basicrulesignals`");
QUERY("TRUNCATE `configcache`");
QUERY("TRUNCATE `controller`");
QUERY("TRUNCATE `databuffer`");
QUERY("TRUNCATE `featureinstances`");
QUERY("TRUNCATE `googlecalendar`");
QUERY("TRUNCATE `graphdata`");
QUERY("TRUNCATE `graphs`");
QUERY("TRUNCATE `graphsignalevents`");
QUERY("TRUNCATE `graphsignals`");
QUERY("TRUNCATE `groupfeatures`");
QUERY("TRUNCATE `groups`");
QUERY("TRUNCATE `groupstates`");
QUERY("TRUNCATE `groupsynchelper`");
QUERY("TRUNCATE `guicontrolssaved`");
QUERY("TRUNCATE `heating`");
QUERY("TRUNCATE `i2cnetworks`");
QUERY("TRUNCATE `i2ctimings`");
QUERY("TRUNCATE `languages`");
QUERY("TRUNCATE `lastreceived`");
QUERY("TRUNCATE `recovery`");
QUERY("TRUNCATE `roomfeatures`");
QUERY("TRUNCATE `rooms`");
QUERY("TRUNCATE `ruleactionparams`");
QUERY("TRUNCATE `ruleactions`");
QUERY("TRUNCATE `rulecache`");
QUERY("TRUNCATE `rules`");
QUERY("TRUNCATE `rulesignalparams`");
QUERY("TRUNCATE `rulesignals`");
QUERY("TRUNCATE `servervariables`");
QUERY("TRUNCATE `smoketest`");
QUERY("TRUNCATE `smoketesthelper`");
QUERY("TRUNCATE `trace`");
QUERY("TRUNCATE `treeupdatehelper`");
QUERY("TRUNCATE `udpcommandlog`");
QUERY("TRUNCATE `udpcommandlogimport`");
QUERY("TRUNCATE `udpdatalog`");
QUERY("TRUNCATE `udpdatalogimport`");
QUERY("TRUNCATE `udphelper`");
QUERY("TRUNCATE `userdata`");
QUERY("TRUNCATE `ventilation`");
QUERY("TRUNCATE `ventilationfenster`");
QUERY("TRUNCATE `webappgraphs`");
QUERY("TRUNCATE `webappgraphssignals`");
QUERY("TRUNCATE `webapppages`");
QUERY("TRUNCATE `webapppagesbuttons`");
QUERY("TRUNCATE `webapppageszeilen`");
?>  

   