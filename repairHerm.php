<?php
include("include/all.php");

QUERY("delete from featureClasses where id='34' and classId='33' limit 1");
QUERY("INSERT INTO `featureclasses` (`id`, `classId`, `name`, `guiControl`, `guiControlFunctions`, `smoketest`, `view`) VALUES
(19, 33, 'IR-Sensor', '', 0, '', 'Standard')");

QUERY("INSERT INTO `featurefunctions` (`id`, `featureClassesId`, `type`, `name`, `functionId`, `view`) VALUES
(109, 19, 'FUNCTION', 'off', 0, 'Standard'),
(110, 19, 'FUNCTION', 'on', 1, 'Standard'),
(112, 19, 'EVENT', 'evOff', 200, 'Standard'),
(113, 19, 'EVENT', 'evOn', 201, 'Standard'),
(111, 19, 'EVENT', 'evClicked', 202, 'Standard'),
(115, 19, 'EVENT', 'evHoldStart', 203, 'Standard'),
(116, 19, 'EVENT', 'evHoldEnd', 204, 'Standard')");


QUERY("INSERT INTO `featurefunctionparams` (`id`, `featureFunctionId`, `name`, `type`, `comment`, `view`) VALUES
(173, 111, 'address', 'WORD', 'IR Adresse', 'Standard'),
(174, 111, 'command', 'WORD', 'IR Kommando', 'Standard'),
(177, 115, 'address', 'WORD', 'IR Adresse', 'Standard'),
(178, 115, 'command', 'WORD', 'IR Kommando', 'Standard'),
(179, 116, 'address', 'WORD', 'IR Adresse', 'Standard'),
(180, 116, 'command', 'WORD', 'IR Kommando', 'Standard')");

?>