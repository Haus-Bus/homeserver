<?php
include("include/all.php");

if (!file_exists("../ha-bridge-5.2.2.jar"))
{
	 echo "Lade benötigte Dateien von haus-bus.de.... <br>";flushIt ();
	 file_put_contents("../ha-bridge-5.2.2.jar", file_get_contents ( "http://www.haus-bus.de/ha-bridge-5.2.2.jar", False, getStreamContext () ) );
}

if (!file_exists("/etc/init.d/alexaBridge"))
{
	echo "Erstelle init.d script ... <br>";
  $init = file_get_contents("/etc/init.d/udpWorker");
  $init = str_replace("udpWorker","udpClientServer",$init);
  file_put_contents("/etc/init.d/udpClientServer",$init);
  exec("chmod 777 /etc/init.d/udpClientServer");
}
else echo "udpClientServer init script already active \n"; 

$output = shell_exec('crontab -l');
if(strpos($output,"udpClientServer")===FALSE)
{
	echo "activating udpClientServer cronJob ... \n";
  file_put_contents('/tmp/crontab.txt', $output."* * * * * ps ax | grep 'udpClientServer' | grep -v 'grep' || /etc/init.d/udpClientServer start".PHP_EOL);
  exec('crontab /tmp/crontab.txt');
}
else echo "udpClientServer cronJob already active \n";

if(strpos($output,"udpClientTrigger")===FALSE)
{
	echo "activating udpClientServer timeTrigger ... \n";
  file_put_contents('/tmp/crontab.txt', $output."* * * * * /var/www/homeserver/udpClientTrigger.sh".PHP_EOL);
  exec('crontab /tmp/crontab.txt');
}
else echo "udpClientServer timeTrigger already active \n";

exec ("chmod 777 /var/www/homeserver/udpClientTrigger.sh");

?>